
$('#loginForm').on('submit',function(e){
  e.preventDefault();
  data = new Object();
  data.correo = $('#emailAddress').val();
  data.password = $('#loginPassword').val();
  data.token = true;

    $.ajax({
        type: "post",
        url: "https://humanly-sw.com/rest/has/public/api/patient/login",
        data: data,
        dataType: "json",
    }).done(res=>{
        console.log(res);
        window.location.href = 'http://localhost:4200/login/'+res.jwt;
    }).fail(err=>{
        console.log(err);
    })

})
