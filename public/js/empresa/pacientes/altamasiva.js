function mifuncion(){

}
function showModal(curp){
    $.ajax({
        type:'get',
        dataType:'json',
        url:"EmpleadoUpdate/"+curp,
        success: function(datas){
           var response =JSON.parse(JSON.stringify(datas));
            document.querySelector('#inputEncryptedIDProgramar').value=response.id_;
            document.querySelector('#clave').value=response.clave;
            document.querySelector('#nombre').value=response.nombre;
            document.querySelector('#app').value=response.apellido_paterno;
            document.querySelector('#apm').value=response.apellido_materno;
            document.querySelector('#curp').value=response.CURP;
            document.querySelector('#nacimiento').value=response.fecha_nacimiento;
            document.querySelector('#direccion').value=response.direccion;
            document.querySelector('#email').value=response.email;
            document.querySelector('#telefono').value=response.telefono;
            document.querySelector('#genero').value = response.genero;
            var select=document.getElementById("genero");
            for(var i=1;i<select.length;i++)
                {
                    if(select.options[i].value==response.genero)
                    {
                        select.selectedIndex=i;
                    }
                }
                $('#update').modal('show');
         }
     });
    }

function validaNumericos(event) {
        if(event.charCode >= 48 && event.charCode <= 57){
          return true;
         }
         return false;
    }

$('#alta').on('submit',function(e) {
    $("#btn_enviar").prop("disabled", true);
    e.preventDefault();
    $.ajax({
        type:'post',
        dataType:'json',
        url:'Empleado_Update',
        data:$('#alta').serialize()
    }).done(function(response){
        $('#update').animate({
            scrollTop: '0px'
        },100);
        tpl = ` <div class="alert alert-success" role="alert">
             Modificación realizada
            </div>`;
        $('#alerts').html(tpl);
        $("#btn_enviar").prop("disabled", false);
        refresh(response.identity_excel);
        setTimeout(() => {
            $('#alerts').html('');
        }, 4500);
    }).fail(function(error) {
        var message;
        if(error.status==422){
            message = "Algo ha ocurrido, verifica que todos los campos sean correctos, y que haya seleccionado un grupo";
        }else if (error.status==400){
            message = "La curp ingresada ya se encuentra registrado, intenta con otra";
        }else if(error.status==424){
            message = "El correo ingresado ya se encuentra registrado, intenta con otra";
        }else if(error.status==423){
            message = "La clave ingresada ya se encuentra registrada, intenta con otra";
        }else{
            message = "Algo ocurrio, intentalo mas tarde...";
        }
        $('#update').animate({
            scrollTop: '0px'
        },100);

        tpl = ` <div class="alert alert-danger" role="alert">
                ${message}
            </div>`;
        $('#alerts').html(tpl);
        $("#btn_enviar").prop("disabled", false);
    });

});

function refresh(id_excel){
    $.ajax({
        type:'get',
        dataType:'json',
        url:"Empleado_excel/"+id_excel,
        success: function(datas){
            var dataSet = [];
            $.each(datas,function(index, value){
                dataSet.push([
                   datas[index].clave,
                   datas[index].nombre,
                   datas[index].apellido_paterno,
                   datas[index].apellido_materno,
                   datas[index].CURP,
                   datas[index].genero,
                   datas[index].fecha_nacimiento,
                   datas[index].direccion,
                   datas[index].email,
                   datas[index].telefono,
                   '<button onclick="showModal(\''+datas[index].CURP.toString()+'\')" class="btn btn-sm btn-secondary">Modificar</button>']);
            });
            $('#table_estudios').dataTable().fnDestroy();
                $('#table_estudios').dataTable({
                   data: dataSet,
            });
            $('#update').modal('hide');
        }
     });
}
$('#grupo_name').hide();
$('#grupo').on('click', function(){
  value =$(this).val();
  if (value=='Otro') {
      $('#grupo_name').show();
  }else {
      $('#grupo_name').hide();
  }
});
