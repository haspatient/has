$(document).ready(function() {
  var id = $('#ecgId').val();
  $.ajax({
    type:'get',
    dataType:'json',
    url:'../ecg-series/'+id,
    beforeSend:()=>{
      $('.spinner').show();
    }
  }).done(res=>{
    console.log(res);
    res.forEach((item, i) => {
      electrocardiograma(item.digitos);
    });

    $('.spinner').hide();
  }).fail(err=>{
    console.log(err);
  })
});

var number = 1;
function electrocardiograma(res) {

  $('#content').append(
    `
    <div class="col-md-12">
    <div id="ecg${number}" style="height: 400px; width: 100%;"></div>
    </div>
    `
  )

  data = res.split(' ');
  dataEcg = [];
  data.forEach((item, i) => {
    dataEcg.push(parseInt(item));
  });


  var xAxisStripLinesArray = [];
  var yAxisStripLinesArray = [];
  var dps = [];
  var dataPointsArray =dataEcg;

  var chart = new CanvasJS.Chart(`ecg${number}`,
  {
  title:{
    text:"Electrocardiograma",
  },
  subtitles:[{
       text: "Paciente: "+$('#nombre').val()+' '+$('#surnames').val(),
      horizontalAlign: "left",
    },
    {
      // text: "Age: X-Years",
      text: "Sexo: "+$('#sexo').val(),
      horizontalAlign: "left",
    },
     {
      // text: "Doctor Sign",

      horizontalAlign: "right",
      verticalAlign: "bottom",
    },
  ],
  axisY:{
    stripLines:yAxisStripLinesArray,
    gridThickness: 2,
    gridColor:"#002b46",
    lineColor:"#002b46",
    tickColor:"#002b46",
    labelFontColor:"#002b46",
  },
  axisX:{
    stripLines:xAxisStripLinesArray,
    gridThickness: 2,
    gridColor:"#002b46",
    lineColor:"#002b46",
    tickColor:"#002b46",
    labelFontColor:"#002b46",
  },
  data: [
  {
    type: "spline",
    color:"black",
    dataPoints: dps
  }
  ]
  });
  number++;
  for(var i=0; i<dataEcg.length;i++){
    dps.push({y: dataPointsArray[i]});
  }
  //StripLines
  for(var i=0;i<3000;i=i+100){
  if(i%1000 != 0)
      yAxisStripLinesArray.push({value:i,thickness:0.7, color:"#002b46"});
  }
  for(var i=0;i<1400;i=i+20){
  if(i%200 != 0)
      xAxisStripLinesArray.push({value:i,thickness:0.7, color:"#002b46"});
  }
  chart.render();

}
