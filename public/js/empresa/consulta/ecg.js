$("#form_file_ecg").submit(function(e){
    e.preventDefault();

    var formData = new FormData(document.getElementById("form_file_ecg"));
    $.ajax({
    url: "../consultaEcg",
    type: "post",
    dataType: "json",
    data: formData,
    cache: false,
    contentType: false,
    processData: false,
    beforeSend:()=>{
        $('.spinner.ecg').show();
    }
    }).done(function(res){
        id = res.id;
        res = res.ecg;
        $('#form_file_ecg')[0].reset();
        $('.spinner.ecg').hide();
        toastr.success("El ECG se almaceno correctamente", "ECG Guardado", {
            positionClass: "toast-top-center",
            containerId: "toast-top-center"
        }).css({
            width: "700px",
            "max-width": "700px"
        })
        $(".content_ecg").append('<div id="ecg_'+res.id+'" class="d-flex col-md-12 mt-1 justify-content-start align-items-center mb-1">'+
        '<div class="mr-50">'+
            '<img src="https://img.icons8.com/color/48/000000/xml-file.png"/>'+
        '</div>'+
        '<div class="user-page-info">'+
            '<h6 class="mb-0">ECG</h6>'+
            '<span class="font-small-2">hace un momento</span>'+
        '</div>'+
        '<a type="button" href="../Ecg/'+id+'" class="btn btn-secondary btn-icon ml-auto waves-effect waves-light"><i class="feather icon-eye"></i></i></a>'+
        '<button type="button" data-key="'+res.id+'" class="delete_ecg btn btn-danger btn-icon ml-1 waves-effect waves-light"><i class="feather icon-trash"></i></i></button>'+
    '</div>')
    activarEliminar();
    }).fail(err=>{
    toastr.error("Error al subir el ECG, intentalo nuevamente.", "Error", {
        positionClass: "toast-top-center",
        containerId: "toast-top-center"
    }).css({
        width: "700px",
        "max-width": "700px"
    })
    });
});

function activarEliminar() {
    $(".delete_ecg").click(function(){
        swal({
            title: "Atención",
            text: "¿Seguro que deseas eliminar el archivo?",
            icon: "warning",
            buttons: {
                cancel: {
                    text: "No",
                    value: null,
                    visible: true,
                    className: "",
                    closeModal: false,
                },
                confirm: {
                    text: "Si",
                    value: true,
                    visible: true,
                    className: "",
                    closeModal: false
                }
            }
        })
        .then((isConfirm) => {
            if (isConfirm) {
                let id = $(this).data('key');
                let data =  {
                    "id" : id
                };
                $.ajax({
                    url: "../deleteEcg",
                    type: "post",
                    dataType: "json",
                    data: data
                }).done(function(res){
                    $('#ecg_'+id).remove();
                    swal("Eliminado", "El electrocardiograma se elimino correctamente", "success");
                }).fail(err=>{
                    swal("Error", "Error al eliminar el electrocardiograma, intentalo nuevamente.", "error");
                });
            } else {
            swal("Cancelado", "La operación se cancelo correctamente", "error");
            }
        });
    });
}

$(".delete_ecg").click(function(){
    swal({
        title: "Atención",
        text: "¿Seguro que deseas eliminar el archivo?",
        icon: "warning",
        buttons: {
            cancel: {
                text: "No",
                value: null,
                visible: true,
                className: "",
                closeModal: false,
            },
            confirm: {
                text: "Si",
                value: true,
                visible: true,
                className: "",
                closeModal: false
            }
        }
    })
    .then((isConfirm) => {
        if (isConfirm) {
            let id = $(this).data('key');
            let data =  {
                "id" : id
            };
            $.ajax({
                url: "../deleteEcg",
                type: "post",
                dataType: "json",
                data: data
            }).done(function(res){
                $('#ecg_'+id).remove();
                swal("Eliminado", "El electrocardiograma se elimino correctamente", "success");
            }).fail(err=>{
                swal("Error", "Error al eliminar el electrocardiograma, intentalo nuevamente.", "error");
            });
        } else {
        swal("Cancelado", "La operación se cancelo correctamente", "error");
        }
    });
});
