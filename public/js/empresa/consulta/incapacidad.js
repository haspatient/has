$(document).ready(function() {

});

new Quill('.quillIncapacidad', {
  theme: 'snow',
  placeholder:'Observaciones'
}).on('text-change', () => {
   consultaIncapacidad();
});

$('#in_motivo').on('change', function(){
  console.log($(this).val());
})


function consultaIncapacidad(){
  var estado;
  if( $('#incapacidadCheck').prop('checked') ) {
     estado = 1
  }else {
     estado = 2;
  }
  data = new Object();
  data.id = $('#historial_id').val();
  data.dateInit = $('#in_inicial').val();
  data.dateEnd = $('#in_final').val();
  data.motivo = $('#in_motivo').val();
  data.observation = $('.quillIncapacidad .ql-editor').html();
  data.estado = estado;
  data.tipo = 'incapacidad';
  $.ajax({
    type:'post',
    dataType:'json',
    data:data,
    url:'../diagnostico'
  }).done(res=>{

  }).fail(err=>{
    console.log(err);
  })
}

$('#incapacidadCheck').on('click', function(){
  if ($(this).prop('checked')) {
    toastr.success("Paciente Incapacitado, Recuerda completar la información","" ,{
        showMethod: "slideDown",
        hideMethod: "slideUp",
        timeOut: 2e3,
        positionClass: "toast-top-center",
        containerId: "toast-top-center"
    }).css({
        width: "700px",
        "max-width": "700px"
    })

  }
})
