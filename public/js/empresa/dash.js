$(document).ready(function() {
   $.ajax({
     type:'get',
     dataType:'json',
     url:'consultasStats/2021'
   }).done(res=>{
     data = arrayData(res)
     chart(data);
   })
});

function arrayData(res) {
  var stats = [0,0,0,0,0,0,0,0,0,0,0,0];
  res.forEach((item, i) => {
      month = item.month - 1;
      stats.splice(month,1,item.stats);
  });
  return stats;
}

function chart(data) {
  Highcharts.chart('container', {
      chart: {
          type: 'line'
      },
      title: {
          text: 'Consultas Por Mes'
      },
      subtitle: {
          text: ''
      },
      credits: {
        enabled: false
      },
      xAxis: {
          categories:["ene","feb","mar","abr","may","jun","jul","ago","sep","oct","nov","dic"]
      },
      yAxis: {
          title: {
              text: 'Cantidad'
          }
      },
      plotOptions: {
          line: {
              dataLabels: {
                  enabled: true
              },
              enableMouseTracking: false
          }
      },
      series: [{
          name: "2021",
          data: data
      }]
  });

}


$(document).ready(function(){
    $.ajax({
        type:'get',
        dataType:'json',
        url:'DiagnosticosIndicador'
      }).done(res=>{
          let nombre = [];
          let count = []
          res.forEach((item)=>{
            nombre.push(item.nombre);
            count.push(item.stats)
          })
        diagnosticos(nombre,count)
      })
})

function diagnosticos(nombres,count){
    $('#bar-chart').empty();
    var e = "#002b46"
    var t = [e, "#28C76F", "#EA5455", "#FF9F43", "#00cfe8"];
    var a = !1;
    var i = {
        chart: {
            height: 350,
            type: "bar"
        },
        colors: t,
        plotOptions: {
            bar: {
                horizontal: !0
            }
        },
        dataLabels: {
            enabled: !1
        },
        series: [{
            data: count
        }],
        xaxis: {
            categories: nombres,
            tickAmount: 5
        },
        yaxis: {
            opposite: a
        }
    };
    new ApexCharts(document.querySelector("#bar-chart"), i).render();
}

$('#diagnosticos').on('submit',function(e){
    e.preventDefault();
    $.ajax({
        type: "post",
        url: "DiagnosticosDate",
        data: $('#diagnosticos').serialize() ,
        dataType: "json",
    }).done(res=>{
        let nombre = [];
        let count = []
        res.forEach((item)=>{
          nombre.push(item.nombre);
          count.push(item.stats)
        })
       diagnosticos(nombre,count)
    }).fail(err=>{
    console.log(err);
    });

})
