$(document).ready(function() {
    $('.molestia_oido').select2({
        placeholder: "Molestia en algun oido"
    });

    $("#explosion_cercana").change(function(){
        if($(this).prop('checked'))
        {
            $("#div_explosion_oido").show();
            $("#explosion_oido").attr('required',true);
        }else{
            $("#div_explosion_oido").hide();
            $("#explosion_oido").attr('required',false);
        }
    });

    $("#audifonos_musica").change(function(){
        if($(this).prop('checked'))
        {
            $("#div_audifonos_musica").show();
            $("#audifonos_musica_volumen").attr('required',true);
            $("#audifonos_musica_no_dias").attr('required',true);
            $("#audifonos_musica_anios").attr('required',true);
        }else{
            $("#div_audifonos_musica").hide();
            $("#audifonos_musica_volumen").attr('required',false);
            $("#audifonos_musica_no_dias").attr('required',false);
            $("#audifonos_musica_anios").attr('required',false);
        }
    });

    $("#acude_eventos").change(function(){
        if($(this).prop('checked'))
        {
            $("#div_acude_eventos").show();
            $("#acude_eventos_frecuencia").attr('required',true);
        }else{
            $("#div_acude_eventos").hide();
            $("#acude_eventos_frecuencia").attr('required',false);
        }
    });

    $("#practica_actividades").change(function(){
        if($(this).prop('checked'))
        {
            $("#div_practica_actividades").show();
            $("#practica_actividades_frecuencia").attr('required',true);
        }else{
            $("#div_practica_actividades").hide();
            $("#practica_actividades_frecuencia").attr('required',false);
        }
    });

    $("#medicamentos_frecuentes").change(function(){
        if($(this).prop('checked'))
        {
            $("#div_medicamentos_frecuentes").show();
            $("#medicamentos_frecuentes_frecuencia").attr('required',true);
        }else{
            $("#div_medicamentos_frecuentes").hide();
            $("#medicamentos_frecuentes_frecuencia").attr('required',false);
        }
    });

    $("#form_carta_consentimiento").submit(function(e){
        e.preventDefault();
        let formulario = $(this).serialize();
        $.ajax({
            type: "POST",
            url: "../save_audiometria",
            data: formulario,
            dataType: "json",
        }).done(res=>{
            swal("Exito","La carta de consentimiento se actualizo correctamente, continua con la prueba.","success");
        }).fail(err=>{
            console.log(err);
            swal("Error","Ocurrio un error en el servidor, intentelo más tarde.","error");
        })
    });

    $(".input_decibeles").TouchSpin({
        min: -10,
        max: 120,
        step: 5,
        boostat: 5,
        maxboostedstep: 10,
    });

    $(".input_decibeles").change(function(){
        if($(this).val() == ""){
            $(this).val(0);
        }
    });

    var array_oido_izquierdo = [0,0,0,0,0,0,0,0,0];
    var array_oido_derecho = [0,0,0,0,0,0,0,0,0];
    var audiometria_id = $("#audiometria_id").val();

    $.ajax({
        type: "GET",
        url: "../getResultadosAudiometria/"+audiometria_id+"/izquierdo",
        dataType: "json",
    }).done(res=>{
        array_oido_izquierdo = [];
        res.forEach(element => {
            array_oido_izquierdo.push(parseInt(element.desibelios));
            $("#oido_izquierdo_"+element.frecuencia).val(element.desibelios)
        });
        initGrafica();
    }).fail(err=>{
        console.log(err);
    });

    $.ajax({
        type: "GET",
        url: "../getResultadosAudiometria/"+audiometria_id+"/derecho",
        dataType: "json",
    }).done(res=>{
        array_oido_derecho = [];
        res.forEach(element => {
            array_oido_derecho.push(parseInt(element.desibelios));
            $("#oido_derecho_"+element.frecuencia).val(element.desibelios)
        });
        initGrafica();
    }).fail(err=>{
        console.log(err);
    });

    function initGrafica() {
        let series = [
            {
                name: "Oído izquierdo",
                data:array_oido_izquierdo,
            },
            {
                name: "Oído derecho",
                data:array_oido_derecho
            }
        ];
        chart.updateSeries(series,true);
    }

    var options = {
        series: [
            {
                name: "Oído izquierdo",
                data:array_oido_izquierdo,
            },
            {
                name: "Oído derecho",
                data:array_oido_derecho
            }
        ],
        chart: {
            height: 350,
            type: 'line',
            zoom: {
                enabled: false
            },
            toolbar: {
                show: false
            }
        },
        colors:["#001eff", "#fc0000"],
        dataLabels: {
            enabled: false
        },
        stroke: {
            curve: 'straight',
            colors:["#001eff","#fc0000"],
            width: 3,
        },
        legend:{
            markers: {
                width: 12,
                height: 12,
                strokeWidth: 1,
                strokeColor:["#001eff","#fc0000"] ,
                fillColor: ["#001eff","#fc0000"],
                radius: 12,
                offsetX: 0,
                offsetY: 0
            },
        },
        title: {
            text: $("#tituloGeneral").text(),
            align: 'left'
        },
        grid: {
            row: {
                colors: ['#f3f3f3', 'transparent'], // takes an array which will be repeated on columns
                opacity: 1
            },
            column: {
                colors: ['transparent','#f3f3f3'], // takes an array which will be repeated on columns
                opacity: 0.5
            },
        },
        xaxis: {
            categories: ['125', '250', '500', '1K', '2K', '3K', '4K', '6K', '8K'],
            position: "top",
            offsetX: 0,
            offsetY: -0,
            labels:{
                show:true,
                hideOverlappingLabels:true,
                // offsetY:-15,
            }
        },
        yaxis:{
            reversed: true,
            tickAmount:10,
            forceNiceScale:true,
            min: -10,
            max: 120,
        }
    };

    var chart = new ApexCharts(document.querySelector("#line-chart2"), options);
    chart.render();

    $(".input_decibeles").change(function(){

        let oido = $(this).data('oido');
        let hz = $(this).data('hz');
        let valor = $(this).val();

        data = {
            "oido": oido,
            "hz":hz,
            "valor":valor,
            "audiometria_id": audiometria_id
        }

        $.ajax({
            type: "POST",
            url: "../guardarResultado",
            data: data,
            dataType: "json",
        }).done(res=>{
            console.log(res);
        }).fail(err=>{
            console.log(err);
        });

        if(oido == "i")
        {
            updateArray(array_oido_izquierdo,hz,valor);
        }else{
            updateArray(array_oido_derecho,hz,valor);
        }
        let series = [
            {
                name: "Oído izquierdo",
                data:array_oido_izquierdo,
            },
            {
                name: "Oído derecho",
                data:array_oido_derecho
            }
        ];
        chart.updateSeries(series,true);
    });

    function updateArray(array_elementos,elemento,valor) {
        switch (elemento) {
            case 125:
                array_elementos[0] = valor;
                break;
            case 250:
                array_elementos[1] = valor;
                break;
            case 500:
                array_elementos[2] = valor;
                break;
            case 1000:
                array_elementos[3] = valor;
                break;
            case 2000:
                array_elementos[4] = valor;
                break;
            case 3000:
                array_elementos[5] = valor;
                break;
            case 4000:
                array_elementos[6] = valor;
                break;
            case 6000:
                array_elementos[7] = valor;
                break;
            case 8000:
                array_elementos[8] = valor;
                break;
            default:
                break;
        }
    }

    $("#formFinal").submit(function(e){
        e.preventDefault();
        swal({
            title: "Atención",
            text: "Estas a punto de terminar, ¿Estás seguro de continuar?",
            icon: "warning",
            buttons: {
                cancel: {
                        text: "Cancelar",
                        value: null,
                        visible: true,
                        className: "",
                        closeModal: false,
                },
                confirm: {
                        text: "Confirmar",
                        value: true,
                        visible: true,
                        className: "",
                        closeModal: false
                }
            }
        })
        .then((isConfirm) => {
            if (isConfirm) {
                $.ajax({
                    type: "POST",
                    url: "../guardarFinal",
                    data: $(this).serialize(),
                    dataType: "json",
                }).done(res=>{
                    swal("Confirmado","La operación se realizo con éxito","success");
                    $(location).attr('href',$("#linkPaciente").attr("href"));
                }).fail(err=>{
                    console.log(err);
                    swal("Erro","La operación no se pudo completar, intentelo más tarde","error");
                });

            } else {
                swal("Cancelado", "la operación fue cancelada", "error");
            }
        });
    });


    $("#formDatos").submit(function(e){
        e.preventDefault();
        var formData = new FormData();
        var input_file = document.getElementById("img_firma");
        formData.append('firma',input_file.files[0]);
        formData.append('cedula',$("#cedula").val());
        formData.append('titulo_profesional',$("#titulo_profesional").val());
        formData.append('nombre_completo',$("#nombre_completo").val());
		$.ajax({
            url:'../guardarFirma',
            type:'post',
            data:formData,
            dataType:'json',
            contentType:false,
            processData:false,
            beforeSend:()=>{

            }
		}).done(res=>{
            swal("Exito","La información se ha actualizado correctamente.","success");
            $("#divBtnModal").html('<p>Puedes actualizar tus datos <button type="button" id="btnModal" class="btn btn-sm btn-outline-adn"><i class="feather icon-upload"></i> Actualizar</button></p>')
            $("#btnFormFinal").attr("disabled",false);
            $("#helpDatos").remove();
            $("#modal").modal('hide');
		}).fail(err=>{
            swal("Error","Error en el servidor, por favor intentelo más tarde.","error");
		});
    })

    $("#btnModal").click(function(){
        $("#modal").modal();
    });

    var toolbarOptions = [
		[{
			'list': 'ordered'
		}, {
			'list': 'bullet'
		}],
		[{
			'direction': 'rtl'
		}],
		// [{
		//   'size': ['small', false, 'large', 'huge']
		// }],
		[{
			'color': []
		}, {
			'background': []
		}],
		[{
			'font': []
		}],
		[{
			'align': []
		}],
		['clean']
	];

    var quill = new Quill('#editor', {
        modules: {
            toolbar: toolbarOptions
            },
        theme: 'snow',
        placeholder: 'Escribe tu nota...'
    });

    quill.on('text-change', function() {
        html = $('#editor').children('.ql-editor').html();
        $("#interpretacion_input").val(html);
    });
});

