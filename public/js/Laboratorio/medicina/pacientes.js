$(function () {
    $('[data-toggle="tooltip"]').tooltip()
})
function Modal(curp,id_estudio,ingreso){
    $('#estudios').modal('show');
    $('#estudio').val(id_estudio);
    $('#ingreso').val(ingreso);
}
    var i=1;
function addUrl(){
    if(i==6){
    alert('limite de 5 links por estudio');
    return;
    }
    $(".addUrl").append('<input type="url" class="form-control mt-2" placeholder="Enlaces a documentos del estudio en SASS" name="documentos'+i+'[]" required>');
    $(".checkbox").append(`
    <label class="pt-1 pb-2">
    <input data-toggle="tooltip" data-placement="top" title="Compartir Documentos" name="documentos${i}[]"  type="checkbox" checked style="display: none">
    </label>
    `);
    i++;
}

$('#formuploadajax').on('submit',e=>{
    e.preventDefault();
    $.ajax({
    dataType:'json',
    data:$('#formuploadajax').serialize(),
    type:'post',
    url:'../MedicinaUrl',
    beforeSend:()=>{
        $('#load').show();
        $('#formuploadajax').hide();
    }
    }).done((res)=>{
    $('#load').hide();
    $('#alert_success').show();
    $('#formuploadajax')[0].reset();
    setTimeout(()=>{
        location.reload();
    //  window.location.href ='https://expedienteclinico.humanly-sw.com/Clinica/public/Medicina';
    },2000)
    }).fail((err)=>{
    $('#load').hide();
    $('#formuploadajax').show();
    if(err.status==0){
        toastr.error('Verifica tu conexion a internet', 'Hubo un problema', {"showDuration": 500});
    }else{
        // toastr.error('Verifica que los campos sean correctos, o intentalos más tarde ', 'Algo a Ocurrido', {positionClass: 'toast-top-full-width', "showDuration": 500,containerId: 'toast-top-full-width'});
    }
    })

});


var userList = new List('admin_list', {
    valueNames: [ 'search_estudio','search_fecha','search_medico'],
    page: 6,
    pagination: {
        innerWindow: 1,
        left: 1,
        right: 1,
        paginationClass: "pagination",
        item:"<li class='page-item'><a class='page-link page' href='#'></a></li>",
    }
});
var length_lis = $('.list .list_custom').length;

if(length_lis == 0){
    $('.jPaginateNext').css('display','none');
    $('.jPaginateBack').css('display','none');
}
else{
    $("#anterior").addClass('pagination').html('<li class="page-item prev-item jPaginateBack"><button type="button" style="border-radius: 50%;" class="page-link navegadores"></button></li>');
    $("#siguiente").addClass('pagination').html('<li class="page-item next-item jPaginateNext"><button type="button" style="border-radius: 50%;" class="page-link navegadores"></button></li>');
    $('.jPaginateNext').css('display','block');
    $('.jPaginateBack').css('display','block');

    $('.jPaginateNext').on('click', function(e){
        var list = $('.pagination').find('li');
        $.each(list, function(position, element){
            if($(element).is('.active')){
                $(list[position+1]).find('a')[0].click();
            }
        })
    });

    $('.jPaginateBack').on('click', function(e){
        var list = $('.pagination').find('li');
        $.each(list, function(position, element){
            if($(element).is('.active')){
                $(list[position-1]).find('a')[0].click();
            }
        })
    });
}

$(".input_buscador").keyup(function(){
    if($('.list .list_custom').length == 0)
    {
        $("#msg-result").removeClass('d-none');
    }else{
        $("#msg-result").addClass('d-none');
    }
});

setInterval(() => {
    $(".page-link.page").click(function(e){
        e.preventDefault();
    });
}, 500);
