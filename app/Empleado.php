<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Grupo;
use App\EstudioProgramado;
use App\Empresa;
use App\Resultados;

class Empleado extends Model {
    protected $table = 'empleados';
    protected $guarded = [];

    public function grupo() {
        return $this->belongsTo(Grupo::class, 'grupo_id', 'id');
    }

    public function empresa() {
        return $this->belongsTo(Empresa::class, 'empresa_id', 'id');
    }

    public function estudiosProgramados() {
        return $this->hasMany(EstudioProgramado::class);
    }

    public function expediente()
    {
        return $this->hasOne(Expediente::class, 'curp', 'CURP');
    }
}
