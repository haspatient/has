<?php

namespace App\Imports;

use App\Empleado;
use Maatwebsite\Excel\Concerns\{Importable, ToModel, WithHeadingRow};
use Illuminate\Support\Facades\Session;

class EmpleadosImport implements  ToModel, WithHeadingRow
{
    use Importable;


    public function model(array $row)
    {

      $PHP_DATE = ($row['fecha_de_nacimiento_ano_mes_dia'] - 25569) * 86400;
      $fecha = gmdate("Y-m-d", $PHP_DATE);

      if (Session::get('empresa')->id == 8) {
        $post = [
            'nombre' => $row['nombre'],
            'a_paterno' => $row['apellido_paterno'],
            'a_materno' => $row['apellido_materno'],
            'correo' => $row['e_mail'],
            'fec_nacimiento' => $fecha,
            'telefono' => $row['telefono'],
            'sexo' => $row['genero_masculinofemenino'],
            'curp' => $row['curp'],
            'estatus' => 1,
            'claveTrabajador' => $row['clave'],
        ];

        $ch = curl_init('https://comerdis.ac-labs.com.mx/TalentoHumano/guardaEmpleado.php');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);

        // execute!
        curl_exec($ch);
        curl_close($ch);
      }

      if ($row['nombre'] != '' && $row['apellido_paterno'] !='' && $row['curp'] != '') {
        return new Empleado([
            'clave'=> $row['clave'],
            'nombre'=>$row['nombre'],
            'CURP'=>strtoupper($row['curp']),
            'apellido_paterno'=> $row['apellido_paterno'],
            'apellido_materno'=> $row['apellido_materno'],
            'genero'=> $row['genero_masculinofemenino'],
            'fecha_nacimiento'=> $fecha,
            'lugar_nacimiento'=> $row['lugar_de_nacimiento'],
            'nss'=> $row['numero_de_seguridad_social'],
            'empresa_id'=> Session::get('empresa')->id ,
            'status_id'=> 1,
            'grupo_id'=>Session::get('grupo'),
            'expediente_id'=> 0,
            'direccion'=> $row['direccion'],
            'colonia'=> $row['colonia'],
            'cp'=> $row['codigo_postal'],
            'municipio'=> $row['municipio'],
            'estado'=> $row['estado'],
            'email'=> $row['e_mail'],
            'telefono'=> $row['telefono'],
            'identity_excel'=>Session::get('excel')
        ]);
      }
    }
}
