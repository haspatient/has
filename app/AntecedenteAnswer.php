<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AntecedenteAnswer extends Model {
    protected $table = 'antecedentesAnswer';
}
