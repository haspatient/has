<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Expediente;

class Nota extends Model
{
    public function expediente() {
        return $this->belongsTo(expediente::class, 'expediente_id', 'id');            
    }
}
