<?php

namespace App\Http\Middleware;

use Illuminate\Support\Facades\Auth;
use Closure;

class RoleMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
      if (!Auth::user()) {
        return redirect()->route('login');
      }
        $role = Auth::user()->role_id;
        $empresa =  \Session::get('empresa');
        // if($empresa && $role == 2 || $role == 3){
         if($empresa){
            return $next($request);
        } else {
            return redirect()->route('admin');
        }
    }
}
