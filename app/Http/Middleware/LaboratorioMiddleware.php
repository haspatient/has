<?php

namespace App\Http\Middleware;

use Closure;
use App\EmpleadoHtds;

class LaboratorioMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $sessionToken = $request->session()->get('tokenEmpleado');
        $empleado = EmpleadoHtds::where('id', $request->session()->get('IdRegistro'))->first();

        if ($empleado) {
            if ($sessionToken == $empleado->token) {
                return $next($request);
            } else {
                return redirect()->back();
            }
        } else {
            return abort(404);
        }

        
    }
}
