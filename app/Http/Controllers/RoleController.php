<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

use App\Empresa;
use App\Empleado;
use App\User;
use App\HistorialClinico;
use App\Consulta;
use App\Accesos;

class RoleController extends Controller {
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct() {
		$this->middleware('auth');
	}

	/**
	 * Show the application dashboard.
	 *
	 * @return \Illuminate\Contracts\Support\Renderable
	 */
	public function index(Request $request) {
		$user = Auth::user();
        $AdminSession = session('Admin');
		if ($user->admin()) {
            return redirect('Empresas');
			return view('Administrador/DashboardAdmi');
			// return view('Administrador/DashboardAdmiBackUp');
		} else if ($user->empresa()) {
		$empresa = Empresa::select('id', 'nombre', 'logo')
			->where('user_id', $user->id)
			->first();
			Session::put('empresa', $empresa);
			$consultas = $this->consultas();
			$empleados = $this->personal();
			$femenino = $this->personalFemenino();
            $masculino = $this->personalMasculino();
			if ($AdminSession=="") {
                return view('Empresa/Empresa',[
                    'consultas' =>$consultas,
                    'personal' => $empleados,
                    'femenino' => $femenino,
                    'masculino' => $masculino
                ]);
			}else {
                return view('Empresa/Empresa',[
                    'userEmpresa' => 1,
                    //TODO: esto no se si va
                    'consultas' =>$consultas,
                    'personal' => $empleados,
                    'femenino' => $femenino,
                    'masculino' => $masculino
                ]);
			}

		}
		elseif ($user->role_id!=1 && $user->role_id != 2) {
			foreach ($user->accesos->groupBy('modulo_id') as $key => $value) {
				$permisos = [];
				foreach ($value as $acceso) {
					$permisos[$acceso->AccesoCrud->first()->operacion] = true;
				}
				Session::put($acceso->modulosAccesos->first()->nombre,$permisos);
			}

		$empresa = Empresa::select('id', 'nombre', 'logo')
			->where('id', $user->empresa_id)
			->first();

		Session::put('empresa', $empresa);
		$consultas = $this->consultas();
		$empleados = $this->personal();
		$femenino = $this->personalFemenino();
		$masculino = $this->personalMasculino();
			return view('Empresa/Empresa',[
			'consultas' =>$consultas,
			'personal' => $empleados,
			'femenino' => $femenino,
			'masculino' => $masculino
			]);
		}else {
			return abort(404);
		}
	}

	public function charts(){

	}

	public function consultas(){
		return Consulta::where('id_user',Auth::id())
		->where('estado','finalizado')
		->count();
	}

	private function personal(){
		return Empleado::where('empresa_id',Session::get('empresa')->id)
		->where('status_id',1)
		->count();
	}
	private function personalFemenino(){
		return Empleado::where('empresa_id',Session::get('empresa')->id)
		->where('status_id',1)
		->where('genero','like','%'.'Femenino'.'%')
		->count();
	}
	private function personalMasculino(){
		return Empleado::where('empresa_id',Session::get('empresa')->id)
		->where('status_id',1)
		->where('genero','like','%'.'Masculino'.'%')
		->count();
	}




	public function admin($nomEmpresa) {
	//dd($nomEmpresa);
		$empresa = Empresa::select('id', 'nombre', 'logo','user_id')
			->where('nombre', $nomEmpresa)
			->first();
		$usuario = User::find($empresa->user_id);
		Session::put('Admin', Auth::user());
		//$AdminSession = session('Admin');
		Auth::login($usuario);
		$user = Auth::user();

		if ($user->admin()) {
			return view('Administrador/DashboardAdmi');

		} else if ($user->empresa()) {
			$empresa = Empresa::select('id', 'nombre', 'logo')
				->where('user_id', $user->id)
				->first();

			$AdminSession = session('Admin');
			Session::put('empresa', $empresa);
			return view('Empresa/Empresa',[
				'userEmpresa' => 1
			]);

		} else {
			return abort(404);
		}
	}
	public function salirAdmin()
	{
		$AdminSession = session('Admin');
		Auth::login($AdminSession);
		Session::put('Admin', "");
		$AdminSession = session('Admin');
		//dd($user = Auth::user());
		return redirect()->action('RouteController@verEmpresas');
	}
}
