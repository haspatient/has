<?php

namespace App\Http\Controllers\Laboratorio;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Contracts\Encryption\DecryptException;

use App\RegistroEntrada;
use App\Empleado;
use App\Archivo;
use App\Expediente;
use App\EstudioProgramado;
use App\Resultados;
use App\Estudios;
use App\Formulario;
use App\HistorialClinico;
use App\RespuestaPaciente;
use App\SeccionFormulario;
use DB;

class MedicinaController extends Controller
{
    public function view() {
        // return redirect()->route('historial_empresas');
        //Se redirige a la empresas
        $pacientes = $this->getPacientes();
        return view('Laboratorio/Medicina/Pacientes', compact('pacientes'));
    }

    public function historial_pacientes()
    {
        $pacientes = Empleado::orderBy('apellido_paterno','ASC')->get();
        return view('Laboratorio/Medicina/historial/pacientes', compact('pacientes'));
    }

    public function historial_pacientes_estudios($curp_encrypted)
    {
        $this->permiso();
        try {
            $idempleado = decrypt($curp_encrypted);
        } catch (DecryptException $e) {
            return abort(404);
        }
        try {
            $empleado = Empleado::find($idempleado);
            $resultados = Resultados::where('expediente_id',$empleado->expediente->id)->where('categoria_id',3)->get();

            foreach ($resultados as $key => $value) {
                if($value->archivo != null)
                {
                    if($value->archivo->tipo == "html")
                    {
                        $value->archivo->archivo = base64_decode($value->archivo->archivo);
                    }
                }
            }
            return view('Laboratorio/Medicina/historial/paciente_estudios', compact('resultados'));
        } catch (\Throwable $th) {
            throw $th;
            return abort(404);
        }
    }

    public function permiso() {
        if (Session::get('permiso') != 'medico') {
            return abort(403);
        }
    }

    public function getPacientes() {
        $this->permiso();
        return RegistroEntrada::select('emp.id', 'emp.nombre', 'emp.CURP', 'emp.apellido_materno as apm', 'emp.apellido_paterno as app', 'emp.genero', 'emp.fecha_nacimiento','registrosentradas.categoria_id','registrosentradas.status')
            ->distinct('registrosentradas.empleado_id')
            ->whereIn('status', [0,1])
            ->whereIn('categoria_id', [3, 1])
            ->join('empleados as emp', 'emp.id', '=', 'registrosentradas.empleado_id')
            ->get();
    }

    public function pacienteEstudios($id_encrypt) {
        $this->permiso();
        try {
            $idempleado = decrypt($id_encrypt);
        } catch (DecryptException $e) {
            return abort(404);
        }

        $medicina = RegistroEntrada::select('estudios.id as estudios_id', 'estudios.nombre as estudio', 'registrosentradas.id as ingreso', 'empleados.id', 'empresas.nombre as empresa', 'empleados.nombre', 'empleados.apellido_paterno', 'empleados.apellido_materno', 'empleados.CURP', 'empleados.genero', 'empleados.fecha_nacimiento')
            ->where('empleado_id', $idempleado)
            ->where('status', 0 )
            ->where('registrosentradas.categoria_id', 3)
            ->join('empleados', 'empleados.id', '=', 'registrosentradas.empleado_id')
            ->join('empresas', 'empresas.id', '=', 'empleados.empresa_id')
            ->join('estudios', 'estudios.id', '=', 'registrosentradas.estudios_id')
            ->orderBy('registrosentradas.created_at', 'ASC')
            ->get();
        $laboratorio = RegistroEntrada::select('estudios.id as estudios_id', 'estudios.nombre as estudio', 'registrosentradas.id as ingreso', 'empleados.id', 'empresas.nombre as empresa', 'empleados.nombre', 'empleados.apellido_paterno', 'empleados.apellido_materno', 'empleados.CURP', 'empleados.genero', 'empleados.fecha_nacimiento')
            ->where('empleado_id', $idempleado)
            ->where('status', 1)
            ->where('registrosentradas.categoria_id', 1)
            ->join('empleados', 'empleados.id', '=', 'registrosentradas.empleado_id')
            ->join('empresas', 'empresas.id', '=', 'empleados.empresa_id')
            ->join('estudios', 'estudios.id', '=', 'registrosentradas.estudios_id')
            ->orderBy('registrosentradas.created_at', 'ASC')
            ->get();

        $empleado = Empleado::find($idempleado);

        $resultados = Resultados::where('expediente_id',$empleado->expediente->id)->where('categoria_id',3)->orderBy('created_at','DESC')->get();

        foreach ($resultados as $key => $value) {
            if($value->archivo != null)
            {
                if($value->archivo->tipo == "html")
                {
                    $value->archivo->archivo = base64_decode($value->archivo->archivo);
                }
            }
        }

        return view('Laboratorio/Medicina/EstudiosPaciente', [
            'empleado' => $empleado,
            'medicina' => $medicina,
            'resultados' => $resultados,
            'laboratorio' => $laboratorio
        ]);
    }


      public function getExpediente($empleado){
        $expediente = Expediente::where([
            ['curp', $empleado->CURP],
        ])->first();

        if ($expediente == null) {
            $expediente_new = new Expediente;
            //$expediente_new->fecha = \Carbon::now();
            $expediente_new->curp = $empleado->CURP;
            $expediente_new->save();
            $expediente_id = $expediente_new->id;
        } else {
            $expediente_id = $expediente->id;
        }
        return $expediente_id;
      }

      public function historial($id){
        $historial = HistorialClinico::where('origen',1)
        ->where('estado','proceso')
        ->where('expediente_id',$id)
        ->latest()
        ->first();
        if ($historial ==null) {
          $hist = new HistorialClinico;
          $hist->expediente_id = $id;
          $hist->origen = 1;
          $hist->estado = 'proceso';
          $hist->save();
          $historial_id = $hist->id;
        }else {
          $historial_id = $historial->id;
        }

        return $historial_id;
      }

    public function saveResult(Request $request) {
      //return $request->all();
        $this->permiso();
        $this->validate($request, [
            'empleado_id' => 'required',
            'documentos' => 'required',
            'estudio_id' => 'required',
            'registro_id' => 'required'
        ]);

        try {
            $empleado_id = decrypt($request->get('empleado_id'));
            $estudio_id = decrypt($request->get('estudio_id'));
            $entrada_id = decrypt($request->get('registro_id'));
        } catch (DecryptException $e) {
            return abort(404);
        }

        // Verificamos expediente si existe expediente.
        $empleado = Empleado::find($empleado_id);
        $expediente_id = $this->getExpediente($empleado);
        // Obtenemos el id de la cita de estudios programados.

        $programacion = EstudioProgramado::where([
        ['empleado_id', $empleado_id],
        ['status', 1]
        ])->first();

        $look = EstudioProgramado::where([
            ['empleado_id', $empleado_id],
            ['status', 3]
        ])->first();

        if ($look) {
            $jsonParseEstudio = NUll;
        } else {
            $jsonParseEstudio = count(json_decode($programacion->estudios));
        }

        // Confirmamos el estudios finalizado.
        $registro_entrada_update= RegistroEntrada::find($entrada_id);
        $registro_entrada_update->status = 2;
        $registro_entrada_update->save();

        $categoria_id = Estudios::where('id', $estudio_id)->first()->categoria_id;

        // NOTE: historial clinico
        $historial_id = $this->historial($expediente_id);
        // Generamos los resultados del estudio.
        $result = new Resultados;
        $result->comentarios = NULL;
        $result->hc = $historial_id;
        $result->estudio_id = $estudio_id;
        $result->categoria_id = $categoria_id;
        $result->estudiosProgramados_id = $programacion->id;
        $result->empresa_id = $programacion->empresa_id;
        $result->save();
        $result_id = $result->id;

        // Enviamos los enlaces del resultado.
        $array_request = $request->all();
        //destruimos todoas las variables que son diferente a la url
        unset($array_request['empleado_id'], $array_request['estudio_id'], $array_request['registro_id'], $array_request['_token']);
        foreach ($array_request as $value) {
            if (count($value) == 2) {
                $url = new Archivo;
                $url->archivo = $value[0];
                $url->tipo = 'url';
                $url->compartir = 'si';
                $url->resultado_id = $result_id;
                $url->save();

            } else {
                $url = new Archivo;
                $url->archivo = $value[0];
                $url->tipo = 'url';
                $url->compartir = 'no';
                $url->resultado_id = $result_id;
                $url->save();
            }
        }

        $result_prog = Resultados::where('estudiosProgramados_id', $programacion->id)->count();

        //return var_dump($result_prog,$jsonParseEstudio);

        if ($result_prog == $jsonParseEstudio) {
            // Actualizamos el status a 2 a tabla estudiosProrgramados para indicar que ha finalizado.
            $programacion->status = 2;
            $programacion->save();
            return json_encode($request->get('empleado_id'));
        }

        return $result;
    }

    public function seleccion_formularios($registro_entrada_id)
    {
        $registroEntrada = RegistroEntrada::find(decrypt($registro_entrada_id));
        if($registroEntrada == null){
            abort(404);
        }
        $empleado = Empleado::find($registroEntrada->empleado_id);
        if($empleado == null){
            abort(404);
        }

        $formularios = Formulario::all();

        $estudio_programado = EstudioProgramado::where('empleado_id',$empleado->id)
        ->whereIn('status',[1,3])
        ->first();

        $historial_clinico = HistorialClinico::where('estudio_programado_id',$estudio_programado->id)->first();

        // dd($estudio_programado);

        if($historial_clinico != null)
        {
            if($historial_clinico->formulario_id > 0){
                return redirect()->route('vista_formulario', ['registro_entrada_id' => $registro_entrada_id, 'formulario' => $historial_clinico->formulario_id]);
            }
        }

        return  view('Laboratorio/Medicina/formularios/seleccion')
        ->with('registroEntrada', $registroEntrada)
        ->with('empleado', $empleado)
        ->with('formularios', $formularios);
    }

    public function formulario_vista($registro_entrada_id,$formulario)
    {
        $registroEntrada = RegistroEntrada::find(decrypt($registro_entrada_id));
        if($registroEntrada == null){
            abort(404);
        }
        $empleado = Empleado::find($registroEntrada->empleado_id);
        if($empleado == null){
            abort(404);
        }
        $estudio_programado = EstudioProgramado::where('empleado_id',$empleado->id)
        ->whereIn('status',[1,3])
        ->first();

        $historial_clinico = HistorialClinico::where('estudio_programado_id',$estudio_programado->id)->first();
        if($historial_clinico == null)
        {
            $historial_clinico = new HistorialClinico();
            $historial_clinico->expediente_id = $empleado->expediente->id;
            $historial_clinico->estudio_programado_id = $estudio_programado->id;
            $historial_clinico->registro_entrada_id = $registroEntrada->id;
            $historial_clinico->estado = 'proceso';
            $historial_clinico->save();
        }

        if($historial_clinico->estado == "finalizado")
        {
            return redirect()->action('Laboratorio\MedicinaController@pacienteEstudios', ['id' => encrypt($empleado->id)]);
        }


        $historial_clinico->formulario_id = $formulario;
        $historial_clinico->save();

        $formulario = Formulario::find($formulario);
        return view('Laboratorio/Medicina/formularios/formulario')
        ->with('paciente', $empleado)
        ->with('formulario', $formulario)
        ->with('historial_clinico', $historial_clinico);

        dd('Alto',$formulario);
        if($formulario == 1){
            return redirect('Historial-Clinico/'.encrypt($empleado->id));
        }elseif($formulario == 2)
        {
            if($historial_clinico->formulario_id != 2)
            {
                $historial_clinico->generales_n = null;
                $historial_clinico->somatometria_n = null;
                $historial_clinico->signos_vitales_n = null;
                $historial_clinico->inspeccion_general_n = null;
                $historial_clinico->odontograma_n = null;
                $historial_clinico->conclusiones_n = null;
                $historial_clinico->formulario_id = 2;
                $historial_clinico->save();
            }

            $historial_clinico->generales_n = json_decode($historial_clinico->generales_n);
            $historial_clinico->antecedentes_n = json_decode($historial_clinico->antecedentes_n);
            $historial_clinico->antecedentes_personales_patologicos_n = json_decode($historial_clinico->antecedentes_personales_patologicos_n);
            $historial_clinico->exploracion_fisica_n = json_decode($historial_clinico->exploracion_fisica_n);
            $historial_clinico->conclusiones_n = json_decode($historial_clinico->conclusiones_n);

            return view('Laboratorio/Medicina/formularios/historia_clinica')
            ->with('paciente', $empleado)
            ->with('formulario', $formulario)
            ->with('historial_clinico', $historial_clinico);
        }elseif($formulario == 3){

            if($historial_clinico->formulario_id != 3)
            {
                $historial_clinico->generales_n = null;
                $historial_clinico->antecedentes_n = null;
                $historial_clinico->antecedentes_personales_patologicos_n = null;
                $historial_clinico->exploracion_fisica_n = null;
                $historial_clinico->conclusiones_n = null;
                $historial_clinico->formulario_id = 3;
                $historial_clinico->save();
            }

            if($historial_clinico->odontograma_n == "" || $historial_clinico->odontograma_n == null)
            {
                $historial_clinico->odontograma_n = '{"input_diente_1":null,"input_diente_2":null,"input_diente_3":null,"input_diente_4":null,"input_diente_5":null,"input_diente_6":null,"input_diente_7":null,"input_diente_8":null,"input_diente_9":null,"input_diente_10":null,"input_diente_11":null,"input_diente_12":null,"input_diente_13":null,"input_diente_14":null,"input_diente_15":null,"input_diente_16":null,"input_diente_17":null,"input_diente_18":null,"input_diente_19":null,"input_diente_20":null,"input_diente_21":null,"input_diente_22":null,"input_diente_23":null,"input_diente_24":null,"input_diente_25":null,"input_diente_26":null,"input_diente_27":null,"input_diente_28":null,"input_diente_29":null,"input_diente_30":null,"input_diente_31":null,"input_diente_32":null}';
                $historial_clinico->save();
            }

            $historial_clinico->generales_n = json_decode($historial_clinico->generales_n);
            $historial_clinico->somatometria_n = json_decode($historial_clinico->somatometria_n);
            $historial_clinico->signos_vitales_n = json_decode($historial_clinico->signos_vitales_n);
            $historial_clinico->inspeccion_general_n = json_decode($historial_clinico->inspeccion_general_n);
            $historial_clinico->odontograma_n = json_decode($historial_clinico->odontograma_n);
            $historial_clinico->conclusiones_n = json_decode($historial_clinico->conclusiones_n);


            $clases = array(
                "1" => "marcadoRojo marcado",
                "2" => "marcadoCafe marcado",
                "3" => "marcadoNaranja marcado",
                "4" => "marcadoTomate marcado",
                "5" => "marcadoMarron marcado",
                "6" => "marcadoMorado marcado",
                "7" => "marcadoVerde marcado",
                "8" => "marcadoAzul marcado",
            );

            return view('Laboratorio/Medicina/formularios/exploracion_fisica')
            ->with('paciente', $empleado)
            ->with('formulario', $formulario)
            ->with('clases', $clases)
            ->with('historial_clinico', $historial_clinico);
        }
        dd($registro_entrada_id,$formulario);
    }

    public function save_formulario(Request $request,$paso)
    {
        try {
            DB::beginTransaction();
                $historial_clinico = HistorialClinico::find($request->historial_clinico_id);

                if($paso == "finalizado")
                {
                    $historial_clinico->estado = "finalizado";
                    $registro_entrada =  RegistroEntrada::find($historial_clinico->registro_entrada_id);
                    $registro_entrada->status = 3;
                    $registro_entrada->save();

                    $new_resultado = new Resultados();
                    $new_resultado->comentarios = NULL;
                    $new_resultado->expediente_id = $historial_clinico->expediente_id;
                    $new_resultado->estudio_id = $registro_entrada->estudios_id;
                    $new_resultado->categoria_id = $registro_entrada->categoria_id;
                    $new_resultado->estudiosProgramados_id = $historial_clinico->estudio_programado_id;
                    $new_resultado->empresa_id = $registro_entrada->empleado->empresa_id;
                    $new_resultado->medico_id = Session::get('IdEmpleado');
                    $new_resultado->save();
                }else{
                    $seccion = SeccionFormulario::find($paso);

                    foreach ($seccion->preguntas as $pregunta) {
                        $nombre = 'campo_'.$pregunta->id;
                        $respuestas_paciente = RespuestaPaciente::where('historial_clinico_id',$historial_clinico->id)
                        ->where('pregunta_formulario_id',$pregunta->id)
                        ->first();

                        if($respuestas_paciente == null)
                        {
                            $respuestas_paciente = new RespuestaPaciente();

                            $respuestas_paciente->seccion_formulario_id = $pregunta->seccion_formulario_id;
                            $respuestas_paciente->pregunta_formulario_id = $pregunta->id;
                            $respuestas_paciente->formulario_id = $seccion->formulario_id;
                            $respuestas_paciente->historial_clinico_id = $historial_clinico->id;
                        }

                        if($pregunta->tipo == 'select')
                        {
                            $respuestas_paciente->respuesta_id = $request->$nombre;
                        }else{
                            $respuestas_paciente->respuesta = $request->$nombre;
                        }

                        $respuestas_paciente->tipo = $pregunta->tipo;

                        $respuestas_paciente->save();
                    }
                }

            $historial_clinico->save();
            DB::commit();

            return response()->json($historial_clinico, 200);
        } catch (\Throwable $th) {
            DB::rollback();
            dd($th);
            return response()->json('Error', 500);
        }
    }

    //Metodos para guardar el formulario 2, que es de historia clinica mecanico
    public function save_formulario_2(Request $request,$paso)
    {
        try {
            DB::beginTransaction();
                $historial_clinico = HistorialClinico::find($request->historial_clinico_id);
                if($paso === 0){
                    $historial_clinico->generales_n = $this->armar_datos_paso1($request->request);
                }
                elseif ($paso === 1) {
                    $historial_clinico->antecedentes_n = $this->armar_datos_paso2($request->request);
                }
                elseif ($paso === 2) {
                    $historial_clinico->antecedentes_personales_patologicos_n = $this->armar_datos_paso3($request->request);
                }
                elseif ($paso === 3) {
                    $historial_clinico->exploracion_fisica_n = $this->armar_datos_paso4($request->request);
                }
                elseif ($paso === 4) {
                    $historial_clinico->conclusiones_n = $this->armar_datos_paso5($request->request);
                }
                elseif ($paso === "finalizado") {
                    $historial_clinico->estado = "finalizado";
                }

                $historial_clinico->save();
            DB::commit();

            return response()->json($historial_clinico, 200);
        } catch (\Throwable $th) {
            DB::rollback();
            return response()->json('Error', 500);
        }
    }

    private function armar_datos_paso1($datos)
    {
        $array = array();
        $array["fecha"] = $datos->get("fecha");
        $array["tipo_examen"] = $datos->get("tipo_examen");
        $array["folio"] = $datos->get("folio");
        $array["nombre"] = $datos->get("nombre");
        $array["apellido_paterno"] = $datos->get("apellido_paterno");
        $array["apellido_materno"] = $datos->get("apellido_materno");
        $array["sexo"] = $datos->get("sexo");
        $array["edad"] = $datos->get("edad");
        $array["estado_civil"] = $datos->get("estado_civil");
        $array["fecha_nacimiento"] = $datos->get("fecha_nacimiento");
        $array["telefono"] = $datos->get("telefono");
        $array["escolaridad"] = $datos->get("escolaridad");
        $array["lugar_nacimiento"] = $datos->get("lugar_nacimiento");
        $array["grupo_rh"] = $datos->get("grupo_rh");
        $array["no_imss"] = $datos->get("no_imss");
        $array["domicilio"] = $datos->get("domicilio");
        $array["fecha_ingreso"] = $datos->get("fecha_ingreso");

        return json_encode($array);
    }

    private function armar_datos_paso2($datos)
    {
        $array = array();
        $array["descripcion_al"] = $datos->get("descripcion_al");
        $array["descripcion_ahf"] = $datos->get("descripcion_ahf");

        return json_encode($array);
    }

    private function armar_datos_paso3($datos)
    {
        $array = array();
        $array["tabaquismo"] = $datos->get("tabaquismo");
        $array["alcoholismo"] = $datos->get("alcoholismo");
        $array["drogas"] = $datos->get("drogas");
        $array["vacunas"] = $datos->get("vacunas");
        $array["ejercicio"] = $datos->get("ejercicio");
        $array["padecimiento_actual"] = $datos->get("padecimiento_actual");
        $array["fecha_ultima_consulta"] = $datos->get("fecha_ultima_consulta");

        return json_encode($array);
    }

    private function armar_datos_paso4($datos)
    {
        $array = array();
        $array["peso"] = $datos->get("peso");
        $array["talla"] = $datos->get("talla");
        $array["imc"] = $datos->get("imc");
        $array["per_abdom"] = $datos->get("per_abdom");
        $array["peso_max"] = $datos->get("peso_max");
        $array["kg_mas"] = $datos->get("kg_mas");
        $array["presion_arterial_1"] = $datos->get("presion_arterial_1");
        $array["presion_arterial_2"] = $datos->get("presion_arterial_2");
        $array["ta_media"] = $datos->get("ta_media");
        $array["frec_card"] = $datos->get("frec_card");
        $array["flexibilidad"] = $datos->get("flexibilidad");
        $array["apariencia_exterior"] = $datos->get("apariencia_exterior");
        $array["temp"] = $datos->get("temp");
        $array["f_resp"] = $datos->get("f_resp");
        $array["cabeza"] = $datos->get("cabeza");
        $array["ojos"] = $datos->get("ojos");
        $array["reflejos"] = $datos->get("reflejos");
        $array["pterigiones"] = $datos->get("pterigiones");
        $array["fondo_ojos"] = $datos->get("fondo_ojos");
        $array["av_izquierdo"] = $datos->get("av_izquierdo");
        $array["av_derecho"] = $datos->get("av_derecho");
        $array["vision_cromatica"] = $datos->get("vision_cromatica");
        $array["presbicia"] = $datos->get("presbicia");
        $array["oidos"] = $datos->get("oidos");
        $array["nariz"] = $datos->get("nariz");
        $array["faringe_lengua"] = $datos->get("faringe_lengua");
        $array["dental"] = $datos->get("dental");
        $array["cuello"] = $datos->get("cuello");
        $array["torax"] = $datos->get("torax");
        $array["corazon"] = $datos->get("corazon");
        $array["pulmones"] = $datos->get("pulmones");
        $array["torax_posterior_columna"] = $datos->get("torax_posterior_columna");
        $array["ingles_genitales"] = $datos->get("ingles_genitales");
        $array["extremidades_superiores"] = $datos->get("extremidades_superiores");
        $array["extremidades_inferiores"] = $datos->get("extremidades_inferiores");
        $array["piel_faneras"] = $datos->get("piel_faneras");
        $array["laboratorios"] = $datos->get("laboratorios");
        $array["glucosa"] = $datos->get("glucosa");

        return json_encode($array);
    }

    private function armar_datos_paso5($datos)
    {
        $array = array();
        $array["conclusion"] = $datos->get("conclusion");
        $array["recomendaciones"] = $datos->get("recomendaciones");

        return json_encode($array);
    }

    public function save_formulario_3(Request $request,$paso)
    {
        // for ($i=1; $i < 33; $i++) {
        //     $nombre = "input_diente_".$i;
        //     $valor = $request->$nombre;
        //     if($valor !== null){
        //         $prueba = json_decode($valor);
        //         foreach ($prueba as $key => $value) {
        //             dd($value,$key);
        //         }
        //     }
        // }
        try {
            DB::beginTransaction();
                $historial_clinico = HistorialClinico::find($request->historial_clinico_id);
                if($paso == 0){
                    $historial_clinico->generales_n = $this->crear_datos_paso1($request->request);
                }
                elseif ($paso == 1) {
                    $historial_clinico->somatometria_n = $this->crear_datos_paso2($request->request);
                }
                elseif ($paso == 2) {
                    $historial_clinico->signos_vitales_n = $this->crear_datos_paso3($request->request);
                }
                elseif ($paso == 3) {
                    $historial_clinico->inspeccion_general_n = $this->crear_datos_paso4($request->request);
                }
                elseif ($paso == 4) {
                    $historial_clinico->odontograma_n = $this->crear_datos_paso5($request->request);
                }
                elseif ($paso == 5) {
                    $historial_clinico->conclusiones_n = $this->crear_datos_paso6($request->request);
                }
                elseif ($paso == "finalizado") {
                    $historial_clinico->estado = "finalizado";
                }
                $historial_clinico->save();
            DB::commit();
            return response()->json($historial_clinico, 200);
        } catch (\Throwable $th) {
            DB::rollback();
            return response()->json('Error', 500);
        }
    }

    private function crear_datos_paso1($datos)
    {
        $array = array();
        $array["nombre"] = $datos->get("nombre");
        $array["apellido_paterno"] = $datos->get("apellido_paterno");
        $array["apellido_materno"] = $datos->get("apellido_materno");
        $array["puesto_solicitado"] = $datos->get("puesto_solicitado");
        $array["empresa"] = $datos->get("empresa");
        $array["edad"] = $datos->get("edad");

        return json_encode($array);
    }

    private function crear_datos_paso2($datos)
    {
        $array = array();
        $array["temperatura"] = $datos->get("temperatura");
        $array["talla"] = $datos->get("talla");
        $array["peso"] = $datos->get("peso");
        $array["imc"] = $datos->get("imc");
        $array["perimetro_abdominal"] = $datos->get("perimetro_abdominal");
        $array["perimetro_toracico"] = $datos->get("perimetro_toracico");

        return json_encode($array);
    }

    private function crear_datos_paso3($datos)
    {
        $array = array();
        $array["ta"] = $datos->get("ta");
        $array["fc"] = $datos->get("fc");
        $array["fr"] = $datos->get("fr");
        $array["sat_o2"] = $datos->get("sat_o2");

        return json_encode($array);
    }

    private function crear_datos_paso4($datos)
    {
        $array = array();

        $array["o_d"] = $datos->get("o_d");
        $array["sl_d"] = $datos->get("sl_d");
        $array["cl_d"] = $datos->get("cl_d");
        $array["o_i"] = $datos->get("o_i");
        $array["sl_i"] = $datos->get("sl_i");
        $array["cl_i"] = $datos->get("cl_i");
        $array["vision_colores"] = $datos->get("vision_colores");
        $array["usa_lentes"] = $datos->get("usa_lentes");
        $array["causa"] = $datos->get("causa");
        $array["reflejos_oculares"] = $datos->get("reflejos_oculares");
        $array["otros"] = $datos->get("otros");
        $array["nariz"] = $datos->get("nariz");
        $array["agudeza_auditiva_derecho"] = $datos->get("agudeza_auditiva_derecho");
        $array["agudeza_auditiva_izquierdo"] = $datos->get("agudeza_auditiva_izquierdo");
        $array["conductos_auditivos_derecho"] = $datos->get("conductos_auditivos_derecho");
        $array["conductos_auditivos_izquierdo"] = $datos->get("conductos_auditivos_izquierdo");
        $array["membrana_timpanica_derecho"] = $datos->get("membrana_timpanica_derecho");
        $array["membrana_timpanica_izquierdo"] = $datos->get("membrana_timpanica_izquierdo");
        $array["cavidad_oral_normal"] = $datos->get("cavidad_oral_normal");
        $array["cavidad_oral_anormal"] = $datos->get("cavidad_oral_anormal");
        $array["orofaringe_normal"] = $datos->get("orofaringe_normal");
        $array["orofaringe_anormal"] = $datos->get("orofaringe_anormal");
        $array["amigdalas_normal"] = $datos->get("amigdalas_normal");
        $array["amigdalas_anormal"] = $datos->get("amigdalas_anormal");
        $array["lengua_normal"] = $datos->get("lengua_normal");
        $array["lengua_anormal"] = $datos->get("lengua_anormal");
        $array["cuello_normal"] = $datos->get("cuello_normal");
        $array["cuello_anormal"] = $datos->get("cuello_anormal");
        $array["cuello_observaciones"] = $datos->get("cuello_observaciones");
        $array["torax_normal"] = $datos->get("torax_normal");
        $array["torax_anormal"] = $datos->get("torax_anormal");
        $array["torax_observaciones"] = $datos->get("torax_observaciones");
        $array["campos_pulmonares_normal"] = $datos->get("campos_pulmonares_normal");
        $array["campos_pulmonares_anormal"] = $datos->get("campos_pulmonares_anormal");
        $array["campos_pulmonares_observaciones"] = $datos->get("campos_pulmonares_observaciones");
        $array["movimientos_respiratorios_normal"] = $datos->get("movimientos_respiratorios_normal");
        $array["movimientos_respiratorios_anormal"] = $datos->get("movimientos_respiratorios_anormal");
        $array["movimientos_respiratorios_observaciones"] = $datos->get("movimientos_respiratorios_observaciones");
        $array["ruidos_cardiacos_normal"] = $datos->get("ruidos_cardiacos_normal");
        $array["ruidos_cardiacos_anormal"] = $datos->get("ruidos_cardiacos_anormal");
        $array["ruidos_cardiacos_observaciones"] = $datos->get("ruidos_cardiacos_observaciones");
        $array["abdomen_normal"] = $datos->get("abdomen_normal");
        $array["abdomen_anormal"] = $datos->get("abdomen_anormal");
        $array["abdomen_observaciones"] = $datos->get("abdomen_observaciones");
        $array["inspeccion_normal"] = $datos->get("inspeccion_normal");
        $array["inspeccion_anormal"] = $datos->get("inspeccion_anormal");
        $array["inspeccion_observaciones"] = $datos->get("inspeccion_observaciones");
        $array["ruidos_peristalticos_normal"] = $datos->get("ruidos_peristalticos_normal");
        $array["ruidos_peristalticos_anormal"] = $datos->get("ruidos_peristalticos_anormal");
        $array["ruidos_peristalticos_observaciones"] = $datos->get("ruidos_peristalticos_observaciones");
        $array["dolor_palpacion"] = $datos->get("dolor_palpacion");
        $array["dolor_palpacion_observaciones"] = $datos->get("dolor_palpacion_observaciones");
        $array["palpacion"] = $datos->get("palpacion");
        $array["colon"] = $datos->get("colon");
        $array["diastasis_rectos"] = $datos->get("diastasis_rectos");
        $array["higado"] = $datos->get("higado");
        $array["vibices"] = $datos->get("vibices");
        $array["hernias"] = $datos->get("hernias");
        $array["bazo"] = $datos->get("bazo");
        $array["tumoracion"] = $datos->get("tumoracion");
        $array["cicatriz"] = $datos->get("cicatriz");
        $array["ganglios"] = $datos->get("ganglios");
        $array["movimientos_normal"] = $datos->get("movimientos_normal");
        $array["movimientos_anormal"] = $datos->get("movimientos_anormal");
        $array["movimientos_observaciones"] = $datos->get("movimientos_observaciones");
        $array["marcha_normal"] = $datos->get("marcha_normal");
        $array["marcha_anormal"] = $datos->get("marcha_anormal");
        $array["marcha_observaciones"] = $datos->get("marcha_observaciones");
        $array["acortamiento"] = $datos->get("acortamiento");
        $array["acortamiento_observaciones"] = $datos->get("acortamiento_observaciones");
        $array["flogosis"] = $datos->get("flogosis");
        $array["flogosis_observaciones"] = $datos->get("flogosis_observaciones");
        $array["ulceras"] = $datos->get("ulceras");
        $array["ulceras_observaciones"] = $datos->get("ulceras_observaciones");
        $array["reflejo_patelar"] = $datos->get("reflejo_patelar");
        $array["reflejo_patelar_observaciones"] = $datos->get("reflejo_patelar_observaciones");
        $array["varices"] = $datos->get("varices");
        $array["varices_observaciones"] = $datos->get("varices_observaciones");
        $array["micosis"] = $datos->get("micosis");
        $array["micosis_observaciones"] = $datos->get("micosis_observaciones");
        $array["llenado_capilar"] = $datos->get("llenado_capilar");
        $array["llenado_capilar_observaciones"] = $datos->get("llenado_capilar_observaciones");
        $array["movimientos_toracicos_normal"] = $datos->get("movimientos_toracicos_normal");
        $array["movimientos_toracicos_anormal"] = $datos->get("movimientos_toracicos_anormal");
        $array["movimientos_toracicos_observaciones"] = $datos->get("movimientos_toracicos_observaciones");
        $array["llenado_capilar_toracico_normal"] = $datos->get("llenado_capilar_toracico_normal");
        $array["llenado_capilar_toracico_anormal"] = $datos->get("llenado_capilar_toracico_anormal");
        $array["llenado_capilar_toracico_observaciones"] = $datos->get("llenado_capilar_toracico_observaciones");
        $array["flogosis_toracico_normal"] = $datos->get("flogosis_toracico_normal");
        $array["flogosis_toracico_anormal"] = $datos->get("flogosis_toracico_anormal");
        $array["flogosis_toracico_observaciones"] = $datos->get("flogosis_toracico_observaciones");

        return json_encode($array);
    }

    private function crear_datos_paso5($datos)
    {
        $array = array();

        $array["input_diente_1"] = $datos->get("input_diente_1");
        $array["input_diente_2"] = $datos->get("input_diente_2");
        $array["input_diente_3"] = $datos->get("input_diente_3");
        $array["input_diente_4"] = $datos->get("input_diente_4");
        $array["input_diente_5"] = $datos->get("input_diente_5");
        $array["input_diente_6"] = $datos->get("input_diente_6");
        $array["input_diente_7"] = $datos->get("input_diente_7");
        $array["input_diente_8"] = $datos->get("input_diente_8");
        $array["input_diente_9"] = $datos->get("input_diente_9");
        $array["input_diente_10"] = $datos->get("input_diente_10");
        $array["input_diente_11"] = $datos->get("input_diente_11");
        $array["input_diente_12"] = $datos->get("input_diente_12");
        $array["input_diente_13"] = $datos->get("input_diente_13");
        $array["input_diente_14"] = $datos->get("input_diente_14");
        $array["input_diente_15"] = $datos->get("input_diente_15");
        $array["input_diente_16"] = $datos->get("input_diente_16");
        $array["input_diente_17"] = $datos->get("input_diente_17");
        $array["input_diente_18"] = $datos->get("input_diente_18");
        $array["input_diente_19"] = $datos->get("input_diente_19");
        $array["input_diente_20"] = $datos->get("input_diente_20");
        $array["input_diente_21"] = $datos->get("input_diente_21");
        $array["input_diente_22"] = $datos->get("input_diente_22");
        $array["input_diente_23"] = $datos->get("input_diente_23");
        $array["input_diente_24"] = $datos->get("input_diente_24");
        $array["input_diente_25"] = $datos->get("input_diente_25");
        $array["input_diente_26"] = $datos->get("input_diente_26");
        $array["input_diente_27"] = $datos->get("input_diente_27");
        $array["input_diente_28"] = $datos->get("input_diente_28");
        $array["input_diente_29"] = $datos->get("input_diente_29");
        $array["input_diente_30"] = $datos->get("input_diente_30");
        $array["input_diente_31"] = $datos->get("input_diente_31");
        $array["input_diente_32"] = $datos->get("input_diente_32");

        return json_encode($array);
    }

    private function crear_datos_paso6($datos)
    {
        $array = array();
        $array["transtorno_conducta"] = $datos->get("transtorno_conducta");
        $array["incoherencia"] = $datos->get("incoherencia");
        $array["transtorno_atencion"] = $datos->get("transtorno_atencion");
        $array["laboratorios"] = $datos->get("laboratorios");
        $array["diagnosticos"] = $datos->get("diagnosticos");
        $array["recomendaciones"] = $datos->get("recomendaciones");
        $array["coclusion"] = $datos->get("coclusion");

        return json_encode($array);
    }
}
