<?php

namespace App\Http\Controllers\Laboratorio;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

use App\Empleado;
use App\RegistroEntrada;
use App\Empresa;
use Illuminate\Support\Facades\Auth;
use mysqli;

class historialClinicoController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Controlador de historiales clínicos
    |--------------------------------------------------------------------------
    |
    | Permite la administración de vistas y APIs que involucren
    | datos de un historial clínico
    |
    */

    /**
     * Revisar si el permiso de usuario es de médico
     *
     * Revisa que el permiso guardado en la sesión sea la de
     * médico, sino manda un error 403 Prohibido
     *
     * @return error El error de Prohibido
     */
    private function permiso() {
        if (Session::get('permiso') != 'medico') {
            return abort(403);
        }
    }

    /**
     * Obtener la vista del historial clínico
     *
     * Devuelva la vista que contiene la información del paciente
     * incluyendo sus estudios.
     *
     * @return Laboratorio\Medicina\historial Vista con información del paciente
     */
    public function view($empresa_id) {
        $empresa = Empresa::find($empresa_id);
        if($empresa->dominio == null || $empresa->dominio == "")
        {
            abort(404);
        }
        $data_insert["fecha"] = now();
        $data_insert["nombre"] = 'Alvaro Arellano';
        $data_insert["puesto"] = 'Ventas';
        $data_insert["sucursal"] = 'Matriz';
        $data_insert["privilegio"] = 'medico';
        $data_insert["id_empleado"] = 'A005';
        $data_insert["token"] = now()->timestamp.rand(0,100);
        $data_db["host_db"] = $empresa->host_db;
        $data_db["user_db"] = $empresa->user_db;
        $data_db["password_db"] = $empresa->password_db;
        $data_db["name_db"] = $empresa->name_db;

        $this->insertarDatosHTDS($data_insert,$data_db);

        $token = $data_insert["token"];

        // return redirect($empresa->dominio.'auth/lab-login/'.$token);

        Session::put('empresa_select',$empresa_id);
        $this->permiso();
        $pacientes = $this->getPaciente($empresa_id);
        $tiene_pendientes = false;
        foreach ($pacientes as $paciente) {
            $paciente->estudios = RegistroEntrada::select('estudio.nombre', 'status', 'empleado_id')
                ->join('estudios as estudio', 'registrosentradas.estudios_id', 'estudio.id')
                ->whereIn('status', [0, 1, 2])
                ->where('empleado_id', $paciente->empleado_id)
                ->get();

            $estudio_pendiente = RegistroEntrada::where('empleado_id', $paciente->empleado_id)
                ->whereIn('status', [0, 1])
                ->first();

            if ($estudio_pendiente) {
                $tiene_pendientes = true;
            }
            else {
                $tiene_pendientes = false;
            }

            $paciente->tiene_pendientes = $tiene_pendientes;
        }

        return view('Laboratorio/Medicina/historialEmp')->with([
            'pacientes' => $pacientes
        ]);
    }

    // NOTE: Mostrar empresas que estan registradas

    public function view_empresas() {
        $this->permiso();
        $empresas = Empresa::all();

        return view('Laboratorio/Medicina/historial')->with([
            'empresas' => $empresas
        ]);
    }

    /**
     * Obtener información de un paciente
     *
     * Devuelve los datos del paciente.
     *
     * @return App\RegistroEntrada[] Arreglo que contiene solo un paciente con su información de estudios
     */
    public function getPaciente($id_empresa) {
      $empresa = Empresa::find($id_empresa);
      if ($empresa->id == 6) {
        return Empleado::where('empresa_id',$id_empresa)->get();
      }else {
        $this->permiso();
        return RegistroEntrada::select('ep.empleado_id', 'emp.nombre', 'emp.apellido_paterno as app', 'emp.apellido_materno as apm', 'emp.fecha_nacimiento', 'emp.CURP', 'emp.genero')
            ->distinct('empleado_id')
            ->join('empleados as emp', 'empleado_id', 'emp.id')
            ->join('estudiosProgramados as ep','ep.empleado_id', 'emp.id')
            ->where('emp.empresa_id',$id_empresa)
            ->where('ep.historialClinico', NULL)
            ->whereIn('registrosentradas.status', [0, 1, 2])
            ->get();
      }
    }

    /**
     * Obtener los estudios con registro de entrada
     *
     * Devuelve los estudios con registro de entrada.
     *
     * @return App\RegistroEntrada[] Arreglo de estudios con registro de entrada
     */
    public function getEstudios() {
        $this->permiso();
        return $estudios = RegistroEntrada::select('estudio.nombre', 'status', 'empleado_id')
            ->join('estudios as estudio', 'registrosentradas.estudios_id', 'estudio.id')
            ->where('status', 2)
            ->get();
    }

    /**
     * Obtener la cantidad de pacientes por género
     *
     * Devuelve la cantidad de pacientes agrupados
     * por género.
     *
     * @return json Datos sobre la cantidad de pacientes
     */
    public function numPaciente() {
        $this->permiso();
        if (Session::get('empresa_select')==6) {
          $sql =  \DB::select('select emp.genero, COUNT(*) as cantidad from empleados AS emp WHERE emp.empresa_id = 6 group by emp.genero');
        }else {
          $sql =  \DB::select('select emp.genero,COUNT(*) as cantidad from(SELECT ep.empleado_id FROM (select empleado_id from `registrosentradas` WHERE status in(0,1,2) GROUP BY empleado_id) as re JOIN estudiosProgramados as ep ON ep.empleado_id = re.empleado_id WHERE ep.historialClinico IS NULL) as regi join `empleados` as `emp` on regi.empleado_id = `emp`.`id` WHERE emp.empresa_id = '.Session::get('empresa_select').' group by emp.genero');
        }

        return json_encode($sql);
    }

    public function insertarDatosHTDS($data,$data_db)
    {
        $servername = $data_db["host_db"];
        $username = $data_db["user_db"];
        $password = $data_db["password_db"];
        $dbname = $data_db["name_db"];
        // Create connection
        $conn = new mysqli($servername, $username, $password, $dbname);
        // Check connection
        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        }
        $sql = 'INSERT INTO datosHtds (nombre, puesto, sucursal, privilegio, ID_empleado, token) VALUES ("'.$data["nombre"].'", "'.$data["puesto"].'", "'.$data["sucursal"].'", "'.$data["privilegio"].'", "'.$data["id_empleado"].'","'.$data["token"].'")';

        if ($conn->query($sql) === TRUE) {
            $conn->close();
            return true;
        } else {
            $conn->close();
            return false;
        }
    }
}
