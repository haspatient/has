<?php

namespace App\Http\Controllers\Laboratorio;

use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\Laboratorio\IngresosController;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Support\Facades\Session;

use App\datosUserHtds;
use App\EmpleadoHtds;
use App\Empresa;
use App\Empleado;
use App\EstudioProgramado;
use App\Estudios;
use App\RegistroEntrada;

/**
 * Controlador de las operaciones de inicio de sesión en laboratorio
 *
 * Permite la administración de vistas y APIs que involucren
 * datos de inicio de sesión en laboratorio y recepción.
 *
 * @author Humanly Software, Health Administration Solutions Team
 * @copyright Todos los derechos reservados 2020
 */
class LabLoginController extends Controller
{
    /**
     * TEMPORAL: Obtener la vista de opciones de acceso
     *
     * Devuelve la vista temporal utilizada para acceder como técnico radiólogo,
     * médico radiólogo, médico, recepción, laboratorio, etc. Simulando el inicio
     * de sesión a través de HTDS.
     *
     * @access public
     * @return Laboratorio/pruebaInicioSesion Vista con los botones de inicio de sesión
     */
    public function vistaPruebaLaboratorio() {
        return view('Laboratorio/pruebaIniciosesion');
    }

    /**
     * Iniciar sesión desde HTDS
     *
     * Da acceso a un usuario mediante un enlace con HTDS, iniciando sesión
     * confirmando que en la base de datos exista el token recibido.
     *
     * @access public
     * @param string $token Token que identifica y valida el inicio de sesión
     * @return View Vista correspondiente a los permisos del usuario que inicia sesión
     */
    public function labLogin($token) {
        if (Session::has('tokenEmpleado')) {
            Session::flush();
        }
        $empleadoHtds = EmpleadoHtds::where('token', $token)->first();
        if ($empleadoHtds) {
            $empleadosModificar = EmpleadoHtds::where('ID_empleado', $empleadoHtds->ID_empleado)
                ->update(['token' => $empleadoHtds->token]);
            session(['nombreEmpleado' => $empleadoHtds->nombre]);
            session(['puestoEmpleado' => $empleadoHtds->puesto]);
            session(['sucursalEmpleado' => $empleadoHtds->sucursal]);
            session(['privilegioEmpleado' => $empleadoHtds->privilegio]);
            session(['IdEmpleado' => $empleadoHtds->ID_empleado]);
            session(['tokenEmpleado' => $empleadoHtds->token]);
            session(['IdRegistro' => $empleadoHtds->id]);
        }

        $validacion_user_htds = datosUserHtds::where('ID_empleado',$empleadoHtds->ID_empleado)
                                ->where('privilegio',$empleadoHtds->privilegio)
                                ->first();

        if($validacion_user_htds == null)
        {
            $new_datos_user_htds = new datosUserHtds();
            $new_datos_user_htds->privilegio = $empleadoHtds->privilegio;
            $new_datos_user_htds->ID_empleado = $empleadoHtds->ID_empleado;
            $new_datos_user_htds->save();
        }

        return redirect()->route('lab-menu');
    }

    /**
     * Redireccionar a la página correspondiente a permisos de usuario
     *
     * Revisa el privilegio asignado al usuario y redirecciona a la
     * página correspondiente.
     *
     * @access public
     * @return View Vista correspondiente a los permisos del usuario que inicia sesión
     */
    public function labMenu() {
        $empleado = (object) [
            'nombre' => session('nombreEmpleado'),
            'puesto' => session('puestoEmpleado'),
            'sucursal' => session('sucursalEmpleado'),
            'privilegio' => session('privilegioEmpleado'),
            'id' => session('IdEmpleado')
          ];

        $privilegioEmpleado = Session::get('privilegioEmpleado');

        if ($privilegioEmpleado == "recepcion") {
            $empresas = Empresa::orderBy('nombre', 'ASC')->get();

            return view('Laboratorio/dashboardRecepcion', [
                'empleado' => $empleado,
                'empresas' => $empresas
            ]);

        } else if ($privilegioEmpleado == "tecnico radiologo") {
            Session::put('permiso', 2);
            return redirect()->route('Ingresos');

        } else if ($privilegioEmpleado == "medico radiologo") {
            Session::put('permiso', 'radiologo');
            return redirect()->route('pacientes');

        } else if ($privilegioEmpleado == "toma de muestra") {
            Session::put('permiso', 'laboratorio');
            return redirect()->route('pacienteLaboratorio');

        } else if ($privilegioEmpleado == "medico") {
            Session::put('permiso', 'medico');
            return redirect()->route('medicina');
        }
    }

    /**
     * Obtener logo de una empresa
     *
     * Devuelve el logotipo de la empresa almacenado en Storage.
     * En caso de no encontrarlo, devuelve una imagen noLogo.png por default.
     *
     * @access public
     * @param string $filename Nombre del archivo del logotipo
     * @return Response Respuesta de solicitud con imagen del logotipo
     */
    public function getLogoEmpresa($filename) {
        if ($filename == "null") {
            $file = Storage::disk('empresa')->get('noLogo.png');
        } else {
            $file = Storage::disk('empresa')->get($filename);
        }
        return new Response($file, 200);
    }

    /**
     * Obtener página con información de una empresa
     *
     * Devuelve la página que mostrará la lista de empleados con estudios
     * programados cuyo periodo abarque la fecha actual.
     *
     * @access public
     * @param string $nombreEmpresa Nombre de la empresa
     * @return Laboratorio/infoEmpresa Vista que muestra los empleados de la empresa con estudios programados
     */
    public function verEmpresa($nombreEmpresa) {

        $empresa = Empresa::where('nombre', $nombreEmpresa)->first();
        $encryptedID = encrypt($empresa->id);

        if ($nombreEmpresa == "null") {
            abort(404);
        }

        return view('Laboratorio/infoEmpresa', [
            'empresa' => $empresa,
            'encryptedID' => $encryptedID
        ]);
    }

    /**
     * Obtener los empleados con estudios programados
     *
     * Devuelve los empleados con estudios programados, en un periodo que abarque
     * la fecha actual, de la empresa cuyo ID corresponda con el ID encriptado
     * recibido como parámetro.
     *
     * @access public
     * @param string $encryptedID ID encriptado de la empresa
     * @return Datatables Tabla de datos con la información de los empleados
     */
    public function getEmpleadosProgramados($encryptedID) {
        try {
            $empresa_id = decrypt($encryptedID);
        } catch (DecryptException $e) {
            return abort(404);
        }

        $empleados = EstudioProgramado::select('empleados.nombre as nombre','empleados.apellido_paterno as apellido_paterno','empleados.apellido_materno as apellido_materno', 'empleados.CURP as CURP')
            ->join('empleados','empleados.id','=','estudiosProgramados.empleado_id')
            ->where([['estudiosProgramados.empresa_id', $empresa_id], ['estudiosProgramados.status', 0], ['estudiosProgramados.fecha_inicial', '<=', \Carbon::today()], ['estudiosProgramados.fecha_final', '>=', \Carbon::today()]])
            ->orWhere([['estudiosProgramados.empresa_id', $empresa_id], ['estudiosProgramados.status', 3], ['estudiosProgramados.fecha_inicial', '<=', \Carbon::today()], ['estudiosProgramados.fecha_final', '>=', \Carbon::today()]])
            ->get();

        return datatables()->of($empleados)->addColumn('btn', 'Laboratorio/empleadosActions')->rawColumns(['btn'])->toJson();
    }

    /**
     * Mostrar información del empleado con estudios programados
     *
     * Devuelve la vista que muestra información del paciente y
     * sus estudios programados para registrar su entrada al laboratorio.
     *
     * @access public
     * @param string $CURP CURP del empleado cuya información se mostrará
     * @return Laboratorio/infoEmpleado
     */
    public function showEmpleadoProgramado($CURP) {
        $empleado = Empleado::where('CURP', $CURP)->first();

        $estudios_registros = EstudioProgramado::where([['empleado_id', $empleado->id], ['status', 0]])
            ->orWhere([['empleado_id', $empleado->id], ['status', 3]])
            ->get();

        $estudios_programados = [];
        foreach ($estudios_registros as $estudio_registro) {
            $lista_estudios = json_decode($estudio_registro->estudios);

            foreach($lista_estudios as $estudio_id) {
                array_push($estudios_programados, Estudios::find($estudio_id));
            }
        }

        $empresa = Empresa::select('nombre')
            ->where('id', $empleado->empresa_id)
            ->first();

        return view('Laboratorio/infoEmpleado', [
            'empleado' => $empleado,
            'encryptedID' => encrypt($empleado->id),
            'estudios_programados' => $estudios_programados,
            'nombre_empresa' => $empresa->nombre
        ]);
    }

    /**
     * Registrar la entrada de un paciente al laboratorio
     *
     * Registra la entrada determinando qué estudios de los programados
     * se realizará en esa visita.
     *
     * @access public
     * @param Request $request Datos del formulario con lista de estudios por registrar
     * @return Laboratorio/infoEmpresa Vista con los empleados con estudios registros en la empresa
     */
    public function registrarEntrada(Request $request) {
        try {
            $empleado_id = decrypt($request->input('encryptedID'));
        } catch (DecryptException $e) {
            return abort(404);
        }

        $estudios_registrar = $request->input('estudios_registrar');
        $empleado = Empleado::find($empleado_id);

        if (empty($estudios_registrar)) {
            return redirect()->route('showEmpleadoProgramado', $empleado->CURP)
                ->withErrors(["No se puede registrar la entrada si no se realizará ningún estudio."]);
        }

        $empleado = Empleado::where('id', $empleado_id)
            ->with('empresa')
            ->first();

        if ($empleado == null) {
            return abort(404);
        }

        $registro_programado = EstudioProgramado::where([['empleado_id', $empleado->id], ['status', 0]])
            ->orWhere([['empleado_id', $empleado->id], ['status', 3]])
            ->first();
        $nuevos_estudios = json_decode($registro_programado->estudios);

        foreach($estudios_registrar as $estudio_id) {
            $estudio = Estudios::find($estudio_id);

            foreach ($nuevos_estudios as $index => $estudio_id) {
                if ($estudio_id == $estudio->id) {
                    unset($nuevos_estudios[$index]);
                }
            }

            if ($estudio->categoria_id == 3) {
                $status = 1;
            }
            else {
                $status = 0;
            }

            $registro_estudio = new RegistroEntrada;
            $registro_estudio->empleado_id = $empleado->id;
            $registro_estudio->estudios_id = $estudio->id;
            $registro_estudio->categoria_id = $estudio->categoria_id;
            $registro_estudio->status = 0;
            $registro_estudio->save();
        }

        if ($registro_programado->status == 0) {
            if (!empty($nuevos_estudios)) {
                $lista_actualizada = [];
                foreach ($nuevos_estudios as $estudio) {
                    array_push($lista_actualizada, $estudio);
                }

                $nuevo_registro_programado = new EstudioProgramado;
                $nuevo_registro_programado->estudios = json_encode($lista_actualizada);
                $nuevo_registro_programado->fecha_inicial = $registro_programado->fecha_inicial;
                $nuevo_registro_programado->fecha_final = $registro_programado->fecha_final;
                $nuevo_registro_programado->status = 3;
                $nuevo_registro_programado->empleado_id = $registro_programado->empleado_id;
                $nuevo_registro_programado->empresa_id = $registro_programado->empresa_id;
                $nuevo_registro_programado->token = $registro_programado->token;
                $nuevo_registro_programado->save();

                $estudios_programados = json_decode($registro_programado->estudios);
                foreach($lista_actualizada as $estudio_id) {
                    $key = array_search($estudio_id, $estudios_programados);
                    if ($key) {
                        unset($estudios_programados[$key]);
                    }
                }
                $estudios_programados = $this->limpiarArreglo($estudios_programados);
                $registro_programado->estudios = json_encode($estudios_programados);
            }

            $registro_programado->status = 1;
            $registro_programado->save();

        } else if ($registro_programado->status == 3) {
            $registro_proceso = EstudioProgramado::where([['empleado_id', $empleado->id], ['status', 1]])->first();
            $estudios_proceso = json_decode($registro_proceso->estudios);

            foreach ($estudios_registrar as $estudio_registrar) {
                array_push($estudios_proceso, (int)$estudio_registrar);
            }

            $registro_proceso->estudios = json_encode($estudios_proceso);
            $registro_proceso->save();

            if (!empty($nuevos_estudios)) {
                $lista_actualizada = [];
                foreach ($nuevos_estudios as $estudio) {
                    array_push($lista_actualizada, $estudio);
                }

                $nuevo_registro_programado = new EstudioProgramado;
                $nuevo_registro_programado->estudios = json_encode($lista_actualizada);
                $nuevo_registro_programado->fecha_inicial = $registro_programado->fecha_inicial;
                $nuevo_registro_programado->fecha_final = $registro_programado->fecha_final;
                $nuevo_registro_programado->status = 3;
                $nuevo_registro_programado->empleado_id = $registro_programado->empleado_id;
                $nuevo_registro_programado->empresa_id = $registro_programado->empresa_id;
                $nuevo_registro_programado->token = $registro_programado->token;
                $nuevo_registro_programado->save();
            }

            $registro_programado->delete();
        }

        return redirect()->route('lab-infoEmpresa', $empleado->empresa->nombre)
                ->with(['message'=>"Se ha registrado correctamente el ingreso de " . $empleado->nombre . " " . $empleado->apellido_paterno . " " . $empleado->apellido_materno . "."]);
    }

    /**
     * Elimina espacios entre los índices de un arreglo
     *
     * Hace continuos los índices de un arreglo. Por ejemplo:
     * {0: elemento1, 2: elemento2} --> [elemento1, elemento2]
     *
     * @access private
     * @param array $arreglo El arreglo por limpiar
     * @return array Arreglo con índices continuos
     */
    private function limpiarArreglo($arreglo) {
        $nuevoArreglo = [];
        $index = 0;
        foreach($arreglo as $key => $elemento) {
            $nuevoArreglo[$index] = $elemento;
            $index++;
        }
        return (array)$nuevoArreglo;
    }

        public function labLogout(Request $request) {
        // Cierra sesión en HAS.
        Session::flush();

        return redirect('http://asesores.ac-labs.com.mx/');
    }


}
