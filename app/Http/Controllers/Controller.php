<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

use App\Expediente;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected function verifyExpediente($curp){
      $expediente = Expediente::where('curp',$curp)
      ->first();
      
      if (!$expediente) {
        $expediente = new Expediente;
        $expediente->curp = $curp;
        $expediente->save();
      }
      return $expediente;
    }


}
