<?php

namespace App\Http\Controllers\Administrador;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Categoria;
use App\Estudios;

class EstudiosController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Controlador de todas las operaciones del modelo Estudios
    |--------------------------------------------------------------------------
    |
    | Permite la administración de vistas y APIs que involucren
    | datos los estudios disponibles en el sistema.
    |
    */

    /**
     * Añade el middleware de autenticación a la clase
     *
     * Las funciones de esta clase solo serán ejecutadas si el middleware
     * verifica que el usuario se ha autenticado.
     *
     * @return void
     */

    // public function __construct() {
    // 	$this->middleware('auth');
    // }

    /**
     * Mostrar la página de administración de estudios
     *
     * Devuelve la vista de administración de estudios mandando la lista
     * de categorías de estudios disponibles.
     *
     * @return Administrador\Estudios Vista para administrar los estudios disponibles en el sistema
     */
    public function estudioPage()
    {
        $categorias = $this->getCategorias();
        return view('Administrador/Estudios', [
            'categorias' => $categorias
        ]);
    }

    /**
     * Obtener la lista de categorías de estudios
     *
     * Devuelve la lista de categorías de estudios.
     *
     * @return \App\Categoria[] Arreglo con las categorías disponibles en el sistema
     */
    private function getCategorias()
    {
        return Categoria::orderBy('created_at', 'desc')->get();
    }

    /**
     * Obtener los estudios de una categoría
     *
     * Devuelve los estudios disponibles en el sistema correspondientes a la categoría
     * cuyo ID coincide con el recibido como parámetro.
     *
     * @param Integer $id ID de la categoría
     * @return json Lista de estudios correspondientes a la categoría seleccionada
     */
    public function getEstudios($id)
    {
        if ($id == 0) {
            $estudios = Estudios::select('estudios.id', 'estudios.nombre', 'estudios.codigo', 'estudios.duracion', 'estudios.costo', 'status.estado')
                ->join('status', 'estudios.statu_id', '=', 'status.id')
                ->get();
        } else {
            $estudios = Estudios::select('estudios.id', 'estudios.nombre', 'estudios.codigo', 'estudios.duracion', 'estudios.costo', 'status.estado')
                ->join('status', 'estudios.statu_id', '=', 'status.id')
                ->where('estudios.categoria_id', $id)
                ->get();
        }
        return json_encode($estudios);
    }

    /**
     * Obtener un estudio
     *
     * Devuelve el estudio correspondiente al ID que recibe como
     * parámetro de entrada.
     *
     * @param Integer $id ID del estudio por devolver
     * @return json Datos del estudio
     */
    public function getEstudio($id)
    {
        return json_encode(Estudios::find($id));
    }

    /**
     * Actualizar los datos de un estudio
     *
     * Permite actualizar los datos de un estudio
     *
     * @param \Illuminate\Http\Request $request Datos del formulario para editar un estudio
     * @return string Aviso de modificación realizada con éxito
     */
    public function updateEstudio(Request $request)
    {

        try {
            $this->validate($request, [
                'codigo' => 'required|string',
                'nombre' => 'required|string',
                'estado' => 'required|numeric',
                'costo' => 'required|numeric',
                'descripcion' => 'required|string',
                'requisitos' => 'required|string',
                'duracion' => 'required|numeric',
                'costo' => 'required|numeric'
            ]);
        } catch (\Throwable $th) {
            return abort(400);
        }

        $id = $request->id;
        $codigo = $request->input('codigo');
        $nombre = $request->input('nombre');
        $id_estado = $request->input('estado');
        $descripcion = $request->input('descripcion');
        $requisitos = $request->input('requisitos');
        $duracion = $request->input('duracion');
        $costo = $request->input('costo');

        $estudio = Estudios::find($id);
        $estudio->codigo = $codigo;
        $estudio->nombre = $nombre;
        $estudio->statu_id = $id_estado;
        $estudio->descripcion = $descripcion;
        $estudio->requisitos = $requisitos;
        $estudio->duracion = $duracion;
        $estudio->costo = $costo;
        $estudio->save();

        return "La modificación se realizó con éxito";
    }

    /**
     * Registrar nuevo estudio en el sistema
     *
     * Registra un nuevo estudio asignándole un nombre, categoría y
     * estado activo.
     *
     * @param \Illuminate\Http\Request $request Datos de formulario para registro de estudios
     * @return json Datos registrados en la base de datos
     */
    public function alta(Request $request)
    {
        $this->validate($request, [
            'estudio' => 'required|string',
            'categoria_id' => 'required|numeric',
            'codigo' => 'required|string',
            'descripcion' => 'required|string',
            'requisitos' => 'required|string',
            'duracion' => 'required|numeric',
            'costo' => 'required|numeric'
        ]);

        $nombre = $request->input('estudio');
        $categoria_id = $request->input('categoria_id');
        $codigo = $request->input('codigo');
        $descripcion = $request->input('descripcion');
        $requisitos = $request->input('requisitos');
        $duracion = $request->input('duracion');
        $costo = $request->input('costo');

        $estudio = new Estudios();
        $estudio->nombre = $nombre;
        $estudio->categoria_id = $categoria_id;
        $estudio->codigo = $codigo;
        $estudio->descripcion = $descripcion;
        $estudio->requisitos = $requisitos;
        $estudio->duracion = $duracion;
        $estudio->costo = $costo;
        $estudio->statu_id = 1;
        $estudio->save();

        return json_encode($estudio);
    }

    /**
     * Modificacion de los estudios
     *
     * @param  mixed $data
     * @return void
     */
    public function estudiosUpdateAll(Request $data)
    {
        $estudios = Estudios::where('nombre', $data->estudio['test'])->first();
        if ($estudios) {
            $estudios->costo = $data->estudio['price'];
            $estudios->codigo = $data->estudio['test_code'];
            $estudios->save();
        }
        return $estudios;
    }

    /**
     * Obtener los estudiospar para la api ap de agenda
     *
     * @return void
     */
    public function getAllEstudios()
    {
        $estudios = Estudios::all();
        return json_encode($estudios);
    }

    /**
     * Obetenero el estudio para la app de agenda
     *
     * @param  mixed $id
     * @return void
     */
    public function getEstudioId($id)
    {
        $estudio = Estudios::find($id);
        return json_encode($estudio);
    }
}
