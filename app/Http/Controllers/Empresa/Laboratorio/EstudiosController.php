<?php

namespace App\Http\Controllers\Empresa\Laboratorio;

use App\EstudioProgramado;
use App\Estudios;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;


class EstudiosController extends Controller {

    public function getEstudiosToma($id){
        $toma =  EstudioProgramado::find($id);
        $estudios = \json_decode($toma->estudios);
        $estudios = Estudios::whereIn('id',$estudios)->get();
        $toma = array(
            'toma' => $toma,
            'estudios' => $estudios
        );

        return response()->json($toma, 200);
    }

}
