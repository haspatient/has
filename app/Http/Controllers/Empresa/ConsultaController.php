<?php

namespace App\Http\Controllers\Empresa;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use DB;

use App\HistorialClinico;
use App\Empleado;
use App\Expediente;
use App\Diagnostico;
use App\Medicamentos;
use App\Procedimientos;
use App\Configuracion;
use App\Agenda;
use App\Consulta;
use App\AC;
use App\Cif;
use App\ConsultaPadecimiento;
use App\ConsultaExamenFisico;
use App\ConsultaProcedimiento;
use App\ConsultaDiagnostico;
use App\ConsultaDiscapacidad;
use App\ConsultaMedicamento;
use App\ConsultaTratamiento;
use App\ConsultaIncapacidad;
use App\Ecg;
use App\EcgSerie;
use App\EcgMedidas;
use App\EcgInterpretacion;

class ConsultaController extends Controller
{

    public function index(Request $request)
    {

        $id = $request->get('id');
        $motivo = $request->get('motivo');
        $empleado = $this->paciente($id);
        $historial = $this->historial($id, $motivo, $empleado);
        $this->signosVitales($historial);
        return redirect()->route('consulta_view', ['id' => encrypt($historial->id)]);
    }

    public function agenda(Request $request)
    {
        $id = $request->get('id');
        $motivo = $request->get('motivo');
        $empleado = $this->paciente($id);
        $historial = $this->historial($id, $motivo, $empleado);
        $this->signosVitales($historial);
        $idAgenda = $request->get('idAgenda');
        $agenda = Agenda::find($idAgenda);
        $agenda->statu = 2;
        $agenda->save();
        $data = array('encrypt' => encrypt($historial->id));
        return response()->json($data, 200);
    }

    public function signosVitales($expediente)
    {

        $consulta = Consulta::where('expediente_id', $expediente->expediente_id)
            ->where('estado', 'finalizado')
            ->latest()
            ->first();

        if ($consulta) {
            $ef = new ConsultaExamenFisico();
            $ef->consulta_id  = $expediente->id;
            $ef->oxigeno = $consulta->signos->oxigeno;
            $ef->temperatura = $consulta->signos->temperatura;
            $ef->fc = $consulta->signos->fc;
            $ef->fr = $consulta->signos->fr;
            $ef->altura = $consulta->signos->altura;
            $ef->peso = $consulta->signos->peso;
            $ef->imc = $consulta->signos->imc;
            $ef->save();
        }
    }

    public function config()
    {
        $con = Configuracion::where('id_user', \Auth::user()->id)->first();
        if ($con) {
            return 'si';
        } else {
            return 'no';
        }
    }

    public function consulta($id)
    {
        try {
            $id = decrypt($id);
        } catch (\Exception $e) {
            return abort(404);
        }

        //dejar variable historial
        $historial = Consulta::find($id);
        $archivos = $this->Archivos($historial->id);
        $imagenes = $this->Imgenes($historial->id);
        $empleado = Empleado::find($historial->empleado_id);
        $cedula =  $this->config();
        // dd($historial->ecg->ecg_series);
        return view('Empresa/consulta', compact('empleado', 'historial', 'cedula', 'archivos', 'imagenes'));
    }

    public function consulta_finalizada($id)
    {
        try {
            $id = decrypt($id);
        } catch (\Exception $e) {
            return abort(404);
        }
        //dejar variable historial
        $historial = Consulta::find($id);
        $archivos = $this->Archivos($historial->id);
        $imagenes = $this->Imgenes($historial->id);
        $empleado = Empleado::find($historial->empleado_id);
        $cedula =  $this->config();
        $datos = $this->getHistorial($id);
        $array_examenFisico = array('dolor', 'rm', 'fm', "apariencia", "psBrazoDerecho", "psBrazoIzquierdo", "cabezaOjos", "oidoNarizBoca", "dientesFaringe", "cuello", "tiroides", "nodoLinfatico", "toraxPulmes", "pecho", "corazon", "abdomen", "rectaDigital", "genitales", "columnaVertebral", "piel", "pulsoArterial", "extremidades", "musculoesqueleto", "reflejosNeurologicos", "estadoMental", "diagnosticoAudiologico", "ojoIzquierdo", "ojoDerecho", "otro");
        // return view('Mails/receta');
        // dd($archivos);
        // dd($id);
        return view('Empresa/consulta_finalizada', compact('empleado', 'historial', 'cedula', 'archivos', 'imagenes', 'datos', 'array_examenFisico'));
    }

    /**
     * retornar todos los archivos de una consulta
     * @param [type] $id [description]
     */
    public function Archivos($id)
    {
        return  AC::where('id_consulta', $id)
            ->whereIn('extencion', ['xlsx', 'csv', 'doc', 'docx', 'txt', 'pdf'])
            ->get();
    }

    public function Imgenes($id)
    {
        return  AC::where('id_consulta', $id)
            ->whereIn('extencion', ['png', 'jpg', 'jpeg'])
            ->get();
    }
    /**
     * Crear un hsitorial consulta
     * @param [type] $id [id del empleado]
     */
    public function historial($id, $motivo, $empleado)
    {

        $expediente = Expediente::where('curp', $empleado->CURP)->first();
        if (!$expediente) {
            $expediente = $this->new_expediente($empleado);
        }

        $consulta = new Consulta;

        $consulta->expediente_id = $expediente->id;
        $consulta->empleado_id = $id;
        $consulta->empresa_id = Session::get('empresa')->id;
        $consulta->motivo = $motivo;
        $consulta->estado = 'proceso';
        $consulta->id_user = \Auth::id();
        //$consulta->origen = 2;
        $consulta->save();
        return $consulta;
    }
    /**
     * Obtiene identifiacion anteriores
     * @param string $empleado [empleado]
     */
    // public function identificacion_auto($expediente){
    //     $identificacion = HistorialClinico::where('expediente_id',$expediente->id)
    //     ->whereNotNull('identificacion')
    //     ->orderBy('created_at','desc')
    //     ->first();
    //
    //     if ($identificacion) {
    //       return $identificacion->identificacion;
    //     }else {
    //       return $identificacion;
    //     }
    // }
    /**
     * crea un nuevo expediente del paciente
     * @param  [type] $empleado $empleado a crear el expediente
     * @return [type]           expediente creado
     */
    public function new_expediente($empleado)
    {
        $expediente = new Expediente;
        $expediente->curp = $empleado->CURP;
        $expediente->save();
        return $expediente;
    }
    /**
     * Crea la seccion de indentificacion del historial clinico
     * @param  [type] $empleado empleado
     * @return [type]          json identificacion
     */
    // public function new_identificacion($empleado){
    //   $identificacion =  json_encode([
    //       'fecha' => \Carbon::now(),
    //       'numEmpleado' => null,
    //       'departamento' => null,
    //       'estadoCivil' => null ,
    //       'escolaridad' => null,
    //       'domicilio' => null,
    //       'lugarNacimiento' => null ,
    //       'ciudad' =>null ,
    //       'municipio'=>null,
    //       'estado' =>null ,
    //       'nom_per_em' => null,
    //       'parentesco' =>null ,
    //       'telefono_1' => null,
    //       'domicilio_em' => null,
    //       'lug_trabajo' => null,
    //       'telefono_2' =>null ,
    //     ]);
    //     return $identificacion;
    // }
    /**
     * paciente en atencion
     * @param  [type] $id [id del paciente]
     * @return [type]     [paciente]
     */

    public function paciente($id)
    {
        return Empleado::find($id);
    }

    /**
     * obtiene Historial Clinico
     * @param  [type] $id [id historial clinico]
     * @return [type]     [historial]
     */
    public function getHistorial($id)
    {

        $padecimiento = ConsultaPadecimiento::where('consulta_id', $id)
            ->first();
        $examenFisico = ConsultaExamenFisico::where('consulta_id', $id)
            ->first();
        $tratamiento = ConsultaTratamiento::where('consulta_id', $id)
            ->first();
        $diagnostico = consultaDiagnostico::where('consulta_id', $id)
            ->get();
        $procedimiento = consultaProcedimiento::where('consulta_id', $id)
            ->get();
        $medicamento = ConsultaMedicamento::where('consulta_id', $id)
            ->get();
        $incapacidad = ConsultaIncapacidad::where('consulta_id', $id)
            ->first();
        $discapacidad = ConsultaDiscapacidad::where('consulta_id', $id)
            ->get();
        $data = array(
            'padecimiento' => $padecimiento,
            'examenFisico' => $examenFisico,
            'diagnostico' => $diagnostico,
            'procedimiento' => $procedimiento,
            'medicamento' => $medicamento,
            'tratamiento' => $tratamiento,
            'incapacidad' => $incapacidad,
            'discapacidad' => $discapacidad
        );

        return $data;
        Consulta::find($id);
    }


    public function diagnostico(Request $request)
    {
        $term = $request->get('term');
        return Diagnostico::where('nombre', 'like', '%' . $term . '%')
            ->orWhere('catalog_key', 'Like', '%' . $term . '%')
            ->get();
    }
    public function medicamentos(Request $request)
    {
        $term = $request->get('term');
        return Medicamentos::where('nombre', 'like', '%' . $term . '%')
            ->orWhere('clave', 'Like', '%' . $term . '%')
            ->get();
    }
    public function procediminetos(Request $request)
    {
        $term = $request->get('term');
        return Procedimientos::where('nombre', 'like', '%' . $term . '%')
            ->orWhere('clave', 'Like', '%' . $term . '%')
            ->get();
    }

    public function discapacidad(Request $request)
    {
        $term = $request->get('term');
        return Cif::where('nombre', 'like', '%' . $term . '%')
            ->orWhere('clave', 'Like', '%' . $term . '%')
            ->get();
    }


    public function padecimiento(Request $request)
    {
        //return $request->all();
        // NOTE: data request
        $consulta_id = $request->get('idHistorial');
        $exploracion = $request->get('exploracion');
        $sintomas = $request->get('sintomas');
        $razon = $request->get('razon');


        $padecimiento = ConsultaPadecimiento::where('consulta_id', $consulta_id)
            ->first();
        if ($padecimiento) {
            $padecimiento->razon = $razon;
            $padecimiento->exploracion = $exploracion;
            $padecimiento->sintomas = $sintomas;
            $padecimiento->save();
        } else {
            $padecimiento = new ConsultaPadecimiento;
            $padecimiento->consulta_id = $consulta_id;
            $padecimiento->razon = $razon;
            $padecimiento->exploracion = $exploracion;
            $padecimiento->sintomas = $sintomas;
            $padecimiento->save();
        }

        return $padecimiento;
    }



    public function examen_fisico(Request $data)
    {

        $consulta_id = $data->get('idHistorial');

        $ef = ConsultaExamenFisico::where('consulta_id', $consulta_id)
            ->first();

        if (!$ef) {
            $ef = new ConsultaExamenFisico;
            $ef->consulta_id = $consulta_id;
        }

        $ef->apariencia = $data->get('apariencia', '');
        $ef->temperatura = $data->get('temperatura', '');
        $ef->fc = $data->get('fc', '');
        $ef->fr = $data->get('fr', '');
        $ef->altura = $data->get('estatura', '');
        $ef->peso = $data->get('peso', '');
        $ef->imc = $data->get('imc', '');
        $ef->oxigeno = $data->so;
        $ef->presion = $data->pa;
        $ef->psBrazoDerecho = $data->get('psBrazoDerecho', '');
        $ef->psBrazoIzquierdo = $data->get('psBrazoIzquierdo', '');
        $ef->cabezaOjos = $data->get('cabezaCueroOjos', '');
        $ef->oidoNarizBoca = $data->get('oidoNarizBoca', '');
        $ef->dientesFaringe = $data->get('dientesFaringe', '');
        $ef->cuello = $data->get('cuello', '');
        $ef->tiroides = $data->get('tiroides', '');
        $ef->nodoLinfatico = $data->get('nodoLinfatico', '');
        $ef->toraxPulmes = $data->get('toraxPulmones', '');
        $ef->pecho = $data->get('pecho', '');
        $ef->corazon = $data->get('corazon', '');
        $ef->abdomen = $data->get('abdomen', '');
        $ef->rectaDigital = $data->get('rectalDigital', '');
        $ef->genitales = $data->get('genitales', '');
        $ef->columnaVertebral = $data->get('columnaVertebral', '');
        $ef->piel = $data->get('piel', '');
        $ef->pulsoArterial = $data->get('pulsoArterial', '');
        $ef->extremidades = $data->get('extremidades', '');
        $ef->musculoesqueleto = $data->get('musculoesqueleto', '');
        $ef->reflejosNeurologicos = $data->get('reflejosNeurologicos', '');
        $ef->estadoMental = $data->get('estadoMental', '');
        $ef->otro = $data->get('otro', '');
        $ef->ojoIzquierdo = $data->get('ojoIzquierdo', '');
        $ef->ojoDerecho = $data->get('ojoDerecho', '');
        $ef->diagnosticoAudiologico = $data->get('diagnosticoAudiologico', '');
        $ef->fm = $data->fm;
        $ef->rm = $data->rm;
        $ef->dolor = $data->dolor;
        $ef->save();

        return $ef;
    }

    public function diagnosticoH(Request $request)
    {
        $request->validate([
            'tipo' => 'required'
        ]);
        $tipo = $request->get('tipo');

        if ($tipo == 'diagnostico') {
            return $this->consultaDiagnostico($request);
        } else if ($tipo == 'procedimiento') {
            return $this->consultaProcedimiento($request);
        } elseif ($tipo == 'medicamento') {
            return $this->consultaMedicamento($request);
        } elseif ($tipo == 'tratamiento') {
            return $this->consultaTratamiento($request);
        } elseif ($tipo == 'incapacidad') {
            return $this->consultaIncapacidad($request);
        } elseif ($tipo == 'discapacidad') {
            return $this->consultaDiscapacidad($request);
        }

        // $diagnostico = array(
        //   'diagnostico' => $request->get('diagnostico'),
        //   'procedimiento' => $request->get('procedimiento') ,
        //   'medicamento' => $request->get('medicamento') ,
        //   'tratamiento' => $request->get('tratamiento'),
        // );
        // $historialId = $request->get('id');
        // $historial = Consulta::find($historialId);
        // $historial->diagnostico = json_encode($diagnostico);
        // $historial->save();
        // return $historial;
    }
    ////////////////////////////
    public function consultaDiagnostico($data)
    {

        $consulta_id = $data->get('id');
        $diagnosticos = $data->get('diagnostico');
        ConsultaDiagnostico::where('consulta_id', $consulta_id)
            ->delete();
        foreach ($diagnosticos as $value) {
            $diagnostico = ConsultaDiagnostico::where('consulta_id', $consulta_id)
                ->where('clave', $value['clave'])
                ->first();

            if (!$diagnostico) {
                $diagnostico = new ConsultaDiagnostico;
                $diagnostico->consulta_id = $consulta_id;
            }
            $diagnostico->clave = $value['clave'];
            $diagnostico->nombre = $value['nombre'];
            $diagnostico->comentario = $value['comentario'];
            $diagnostico->save();
        }
        return $diagnosticos;
    }

    public function consultaProcedimiento($data)
    {
        $consulta_id = $data->get('id');
        $procediminetos = $data->get('procedimiento');
        ConsultaProcedimiento::where('consulta_id', $consulta_id)
            ->delete();
        foreach ($procediminetos as $value) {
            $procedimiento = ConsultaProcedimiento::where('consulta_id', $consulta_id)
                ->where('clave', $value['clave'])
                ->first();

            if (!$procedimiento) {
                $procedimiento = new ConsultaProcedimiento;
                $procedimiento->consulta_id = $consulta_id;
            }
            $procedimiento->clave = $value['clave'];
            $procedimiento->nombre = $value['nombre'];
            $procedimiento->comentario = $value['comentario'];
            $procedimiento->save();
        }
        return $procediminetos;
    }

    public function consultaMedicamento($data)
    {
        $consulta_id = $data->get('id');
        $medicamentos = $data->get('medicamento');
        ConsultaMedicamento::where('consulta_id', $consulta_id)
            ->delete();
        foreach ($medicamentos as $value) {
            $medicamento = ConsultaMedicamento::where('consulta_id', $consulta_id)
                ->where('clave', $value['clave'])
                ->first();

            if (!$medicamento) {
                $medicamento = new ConsultaMedicamento;
                $medicamento->consulta_id = $consulta_id;
            }
            $medicamento->clave = $value['clave'];
            $medicamento->nombre = $value['nombre'];
            $medicamento->comentario = $value['comentario'];
            $medicamento->tomar = $value['tomar'];
            $medicamento->frecuencia = $value['frecuencia'];
            $medicamento->duracion = $value['duracion'];
            $medicamento->save();
        }
        return $medicamentos;
    }

    public function consultaTratamiento($data)
    {
        $consulta_id = $data->get('id');
        $tratamiento_text = $data->get('tratamiento');
        $tratamiento = ConsultaTratamiento::where('consulta_id', $consulta_id)
            ->first();

        if (!$tratamiento) {
            $tratamiento = new ConsultaTratamiento;
            $tratamiento->consulta_id = $consulta_id;
        }
        $tratamiento->notas = $data->notas;
        $tratamiento->tratamiento = $tratamiento_text;
        $tratamiento->save();

        return $tratamiento;
    }

    public function consultaDiscapacidad($data)
    {
        $consulta_id = $data->get('id');
        $discpacidades = $data->get('discapacidades');

        ConsultaDiscapacidad::where('consulta_id', $consulta_id)
            ->delete();

        foreach ($discpacidades as $value) {
            $discapaciad = ConsultaDiscapacidad::where('consulta_id', $consulta_id)
                ->where('clave', $value['clave'])
                ->first();

            if (!$discapaciad) {
                $discapaciad = new ConsultaDiscapacidad;
                $discapaciad->consulta_id = $consulta_id;
            }
            $discapaciad->clave = $value['clave'];
            $discapaciad->nombre = $value['nombre'];
            $discapaciad->comentario = $value['comentario'];
            $discapaciad->save();
        }
        return $discpacidades;
    }

    /**
     * Incapacidad del paciente
     * @param  [type] $data [data post]
     * @return [type]       [Peticion incapacidad]
     */
    public function consultaIncapacidad($data)
    {
        $consultaId = $data->get('id');
        $incapacidad = ConsultaIncapacidad::where('consulta_id', $consultaId)->first();

        if (!$incapacidad) $incapacidad = new ConsultaIncapacidad;

        $incapacidad->consulta_id = $data->get('id');
        $incapacidad->fechaInicial = $data->get('dateInit');
        $incapacidad->fechaFinal = $data->get('dateEnd');
        $incapacidad->motivo = $data->get('motivo');
        $incapacidad->observacion = $data->get('observation');
        $incapacidad->estado = $data->get('estado');
        $incapacidad->save();
        return $incapacidad;
    }
    /**
     * Finalizar la consulta del medico
     * @param  [type] $id [id de la consulta]
     * @return [type]     [retorna la informacion del medico]
     */
    public function finalizar($id)
    {
        $hc = Consulta::find($id);
        $hc->estado = 'finalizado';
        $hc->finalizado = \Carbon::now();
        $hc->save();

        $ex = Expediente::where('id', $hc->expediente_id)->first();

        $empleado = Empleado::where('CURP', $ex->curp)->first();
        return $empleado;
    }

    /**
     * Eliminar la consulta
     * @param string $id de la historial
     */
    public function deleteConsulta($id)
    {
        $historial = Consulta::find($id);
        $historial->delete();
        ConsultaPadecimiento::where('consulta_id', $id)->delete();
        ConsultaExamenFisico::where('consulta_id', $id)->delete();
        ConsultaProcedimiento::where('consulta_id', $id)->delete();
        ConsultaDiagnostico::where('consulta_id', $id)->delete();
        ConsultaMedicamento::where('consulta_id', $id)->delete();
        ConsultaTratamiento::where('consulta_id', $id)->delete();
        ConsultaIncapacidad::where('consulta_id', $id)->delete();
        ConsultaDiscapacidad::where('consulta_id', $id)->delete();

        $ac = AC::where('id_consulta', $id)->get();
        foreach ($ac as $value) {
            Storage::disk('consulta')->delete($value->storage);
            $value->delete();
        }
        $ecg = Ecg::where('consulta_id', $id)->get();
        foreach ($ecg as $value) {
            Storage::disk('ecgs')->delete($value->archivo);
            EcgSerie::where('ecg_id', $value->id)->delete();
            EcgMedidas::where('ecg_id', $value->id)->delete();
            EcgInterpretacion::where('ecg_id', $value->id)->delete();
            $value->delete();
        }
        $expediente = Expediente::find($historial->expediente_id);
        return $expediente;
    }



    /**
     * Archivos que se requiran en la consulta
     * @param  Request $request [data post]
     * @return [type]           [info del archivo]
     */
    public function archivo(Request $request)
    {

        $request->validate([
            'archivo' => 'required',
            'consulta' => 'required',
            'tipo' => 'required'
        ]);
        $file =  $request->file('archivo');
        $upload = $this->upload($file);

        if ($upload) {
            $archivo = new AC;
            $archivo->id_consulta = $request->get('consulta');
            $archivo->tipo = $request->get('tipo');
            $archivo->extencion = $upload['extencion'];
            $archivo->nombre = $file->getClientOriginalName();
            $archivo->storage = $upload['name'];
            $archivo->save();
            return $archivo;
        } else {
            return abort(500);
        }
    }

    public function upload($file)
    {
        $name = time() . '_' . $file->getClientOriginalName();
        $ext = pathinfo($name, PATHINFO_EXTENSION);
        Storage::disk('consulta')->put($name, File::get($file));
        $data = array(
            'extencion' => $ext,
            'name' => $name
        );
        return $data;
    }

    public function deleteArchivo(Request $request)
    {
        $file = AC::find($request->get('id'));
        $file->delete();
        Storage::disk('consulta')->delete($file->storage);
        return $file;
    }

    public function crear_ecg(Request $request)
    {
        try {
            DB::beginTransaction();
            $file =  $request->file('file_ecg');
            $name = time() . '_' . $file->getClientOriginalName();
            $ext = pathinfo($name, PATHINFO_EXTENSION);
            Storage::disk('ecgs')->put($name, File::get($file));

            $xmldata = simplexml_load_file($file) or die("Failed to load");

            $new_ecg = new Ecg();
            $new_ecg->consulta_id = $request->consulta;
            $new_ecg->archivo = $name;
            $new_ecg->nombre = $name;


            $assignment = $xmldata->componentOf->timepointEvent->componentOf->subjectAssignment->subject->trialSubject;
            $patient = $assignment->subjectDemographicPerson;


            $new_ecg->patientId = $assignment->id->attributes()->extension->__toString();
            $new_ecg->surnames = $patient->name->family->__toString();
            $new_ecg->sexo = $patient->administrativeGenderCode->attributes()->code->__toString();
            $new_ecg->birthTime = $patient->birthTime->attributes()->value->__toString();
            $new_ecg->patient = $patient->name->given->__toString();
            $new_ecg->save();

            foreach ($xmldata->component->series->subjectOf->annotationSet->component as $component) {

                if ($component->annotation->value) {
                    $ecg_medida = new EcgMedidas();
                    $ecg_medida->ecg_id     = $new_ecg->id;
                    $ecg_medida->code = $component->annotation->code->attributes()->code->__toString();
                    $ecg_medida->value = $component->annotation->value->attributes()->value->__toString();
                    $ecg_medida->unit = $component->annotation->value->attributes()->unit->__toString();
                    $ecg_medida->save();
                } elseif ($component->annotation->component) {
                    foreach ($component->annotation->component as $inter) {
                        $ecgInter = new EcgInterpretacion();
                        $ecgInter->ecg_id = $new_ecg->id;
                        $ecgInter->value = $inter->annotation->value->__toString();
                        $ecgInter->save();
                    }
                }
            }

            $contador = 0;
            foreach ($xmldata->component->series->component->sequenceSet->component as $componente) {
                if ($componente->sequence->code->attributes()->codeSystemName->__toString() == "MDC") {
                    $contador++;
                    $new_ecg_serie = new EcgSerie();
                    $new_ecg_serie->ecg_id = $new_ecg->id;
                    $new_ecg_serie->indice = $contador;
                    $digitos_string = $componente->sequence->value->digits->__toString();
                    // $digitos_array = explode(" ",$digitos_string);
                    $new_ecg_serie->digitos = $digitos_string;
                    $new_ecg_serie->save();
                }
            }
            DB::commit();
            $data = array(
                'ecg' => $new_ecg,
                'id' => encrypt($new_ecg->id)
            );
            return response()->json($data, 200);
        } catch (\Throwable $th) {
            DB::rollback();
            throw $th;
        }
    }

    public function eliminar_ecg(Request $request)
    {
        try {
            DB::beginTransaction();
            $ecg = Ecg::find($request->get('id'));
            $archivo = $ecg->archivo;
            $ecg->ecg_series()->delete();
            $ecg->ecg_medidas()->delete();
            $ecg->delete();
            Storage::disk('ecgs')->delete($archivo);
            return $request->get('id');
            DB::commit();
        } catch (\Throwable $th) {
            DB::rollback();
            throw $th;
        }
    }
    public function view_ecg($id)
    {
        try {
            $idE = decrypt($id);
        } catch (\Exception $e) {
            return abort(403);
        }
        $ecg = Ecg::find($idE);
        $medidas = $ecg->ecg_medidas;
        $interpretacion = $ecg->ecgInter;
        return view('Empresa/include_consulta/ecg-graph', compact('id', 'ecg', 'medidas', 'interpretacion'));
    }

    public function series_ecg($ecgId)
    {
        return EcgSerie::where('ecg_id', $ecgId)->get();
    }
}
