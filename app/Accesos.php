<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Modulos;
use App\Crud;

class Accesos extends Model {
    protected $table = 'accesos';

    public function modulosAccesos(){
      return $this->hasMany(Modulos::class,'id','modulo_id');
    }
    public function AccesoCrud(){
      return $this->hasMany(Crud::class,'id','crud_id');
    }
}
