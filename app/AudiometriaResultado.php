<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AudiometriaResultado extends Model {
    protected $table = 'audiometria_resultados';
}
