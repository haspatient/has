<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\AntecedenteAnswer;


class AntecedenteForm extends Model {
    protected $table = 'antecedentesForm';

    public function antecedentesAnswer()
    {
        return $this->hasMany(AntecedenteAnswer::class, 'antecedente_id', 'id')->orderBy('orderList','asc');
    }

    public function user(){
        return $this->hasOne(User::class, 'id', 'user_id');
    }
}
