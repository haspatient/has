<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Empleado;
use App\EstudioProgramado;

class Grupo extends Model {
    public function empleados() {
        return $this->hasMany(Empleado::class);
    }
}
