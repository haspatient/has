<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Audiometria extends Model {
    protected $table = 'audiometrias';

    public function resultados()
    {
        return $this->hasMany('App\AudiometriaResultado', 'audiometria_id', 'id');
    }

    public function registroEntrada()
    {
        return $this->hasOne('App\RegistroEntrada', 'id', 'registro_entrada_id');
    }
}
