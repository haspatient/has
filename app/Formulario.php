<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Formulario extends Model {
    protected $table = 'formularios';

    public function secciones()
    {
        return $this->hasMany(SeccionFormulario::class, 'formulario_id', 'id');
    }
}
