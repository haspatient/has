/* Alertas */
setInterval(function(){
    $('.alert-success').hide(700, "swing");
},3000);

setInterval(function(){
    $('.alert-danger').hide(700, "swing");
},3000);

/* Cambio de logotipo */
$('#logo').change(function(e) {
    addImage(e); 
});

function addImage(e){
    var file = e.target.files[0],
    imageType = /image.*/;

    if (!file.type.match(imageType))
    return;

    var reader = new FileReader();
    reader.onload = fileOnload;
    reader.readAsDataURL(file);
}

function fileOnload(e) {
    var result = e.target.result;
    $('#load_image').attr("src",result);
}

/* Selección de estudios */
$(document).ready(function(){
    $('.estudios_input').hide();
    $('.links').on('click',function(e){
        $('.estudios_input').hide();
        $('i').removeClass('actives');
        $(this).parent().find('i').addClass('actives');
        $('#selected').text($(this).text());
        var id = $(this).data('nombre');
        ajaxEstudio(id);
    });
});


function ajaxEstudio(id){
    var cont = '.'+id;
    $(cont).show();
}

// Función para seleccionar todos
$(document).ready(function() {
  selected = true;
  $('#BtnSeleccionar').click(function() {
    if (selected) {
      $('.estudios_input input[type=checkbox]').prop("checked", true);
      $('#BtnSeleccionar').val('Deseleccionar');

    } else {
      $('.estudios_input input[type=checkbox]').prop("checked", false);
      $('#BtnSeleccionar').val('Seleccionar todo');
    }
    selected = !selected;
  });
});

/* Mostrar modal de estudios */
function showEstudiosModal(encryptedID) {
    $.ajax({
        type:'get',
        dataType:'json',
        url:"../../getEstudiosEmpresa/" + encryptedID,
        success: function(data){
            var estudiosEmpresa = JSON.parse(JSON.stringify(data));

            for (estudio of estudiosEmpresa) {
                $('#estudio' + estudio.estudio_id).attr('checked', true);
            }
            $('#estudiosModal').modal('show');
        }
    });
}

/* Cargar estudios de la empresa */
function cargarEstudios(encryptedID) {
    $.ajax({
        type:'get',
        dataType:'json',
        url:"../../getEstudiosEmpresa/" + encryptedID,
        success: function(data){
            var estudiosEmpresa = JSON.parse(JSON.stringify(data));

            for (estudio of estudiosEmpresa) {
                $('#estudio' + estudio.estudio_id).attr('checked', true);
            }
        }
    });
}