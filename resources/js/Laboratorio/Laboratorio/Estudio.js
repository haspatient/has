$(function () {
    $('[data-toggle="tooltip"]').tooltip();
});

function Modal(curp, id_estudio, ingreso) {
    $("#estudios").modal("show");
    $("#estudio").val(id_estudio);
    $("#ingreso").val(ingreso);
}

$("#formuploadajax").on("submit", (e) => {
    e.preventDefault();
    $.ajax({
        dataType: "json",
        data: $("#formuploadajax").serialize(),
        type: "post",
        url: "../LaboratorioUrl",
        beforeSend: () => {
            $("#load").show();
            $("#formuploadajax").hide();
        },
    })
    .done((estudiosCompletados) => {
        $("#load").hide();
        $("#alert_success").show();
        $("#formuploadajax")[0].reset();      

        estudiosCompletados = JSON.parse(estudiosCompletados);

        if (estudiosCompletados) {
            setTimeout(() => {
                window.location.href =
                "http://asesoresconsultoreslabs.com/expedienteclinico/Clinica/public/Laboratorio";
            }, 2000);
        } else {
            window.location.reload();
        }
    })
    .fail((err) => {
        $("#load").hide();
        $("#formuploadajax").show();
        if (err.status == 0) {
            toastr.error("Verifica tu conexión a internet", "Hubo un problema", {
            showDuration: 500,
            });
        } else {
            toastr.error(
            "Verifica que los campos sean correctos, o inténtalo más tarde ",
            "Algo ha ocurrido",
            {
                positionClass: "toast-top-full-width",
                showDuration: 500,
                containerId: "toast-top-full-width",
            }
            );
        }
    });
});
