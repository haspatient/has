$('#loaders').hide();
$('#loadersDicom').hide();
$('#obs').on('submit', function (e) {
  e.preventDefault();
  var content = $('#summernote').html();
  $('#html_text').html(content);
  $.ajax({
    type: 'post',
    dataType: 'json',
    data: $('#obs').serialize(),
    url: '../Observaciones',
    beforeSend: function () {
      $('#obs').hide();
      $('#loaders').show();
      $('#guardar').hide();
    },
    complete: function () {
      $('#loaders').hide();
      $('#obs').show();
    }
  }).done(function (e) {
    $('#guardar').show();
    toastr.success('Se registrado la interpretación del estudio', '¡Exito!', {
      "showDuration": 500
    });
    setTimeout(function () {

      window.location.href = '../Estudios/' + e;
    }, 1000);
  }).fail(function (error) {
    console.log(error.status);
    if (error.status == 0) {
      toastr.error('Verifica tu conexion a internet', 'Hubo un problema', {
        "showDuration": 500
      });
    } else {
      toastr.error('No hay ninguna interpretación por gurdadar, verifica que todos los campos sean correctos, o intentalo mas tarde...', 'Hubo un problema', {
        "showDuration": 500
      });
    }
  });
});


cornerstoneWADOImageLoader.external.cornerstone = cornerstone;
cornerstoneWADOImageLoader.configure({
  beforeSend: function (xhr) {
    console.log(xhr, '------------------');
    // Add custom headers here (e.g. auth tokens)
    //xhr.setRequestHeader('APIKEY', 'my auth token');
  }
});
var loaded = false;

function loadAndViewImage(imageId) {
  var element = document.getElementById('dicomImage');

  try {
    var start = new Date().getTime();
    cornerstone.loadAndCacheImage(imageId).then(function (image) {
      console.log(image);
      var viewport = cornerstone.getDefaultViewportForImage(element, image);
      cornerstone.displayImage(element, image, viewport);


      document.getElementById('toggleModalityLUT').checked = (viewport.modalityLUT !== undefined);
      document.getElementById('toggleVOILUT').checked = (viewport.voiLUT !== undefined);
      cornerstone.displayImage(element, image, viewport);
      if (loaded === false) {
        cornerstoneTools.mouseInput.enable(element);
        cornerstoneTools.mouseWheelInput.enable(element);
        cornerstoneTools.wwwc.activate(element, 1); // ww/wc is the default tool for left mouse button
        cornerstoneTools.pan.activate(element, 2); // pan is the default tool for middle mouse button
        cornerstoneTools.zoom.activate(element, 4); // zoom is the default tool for right mouse button
        cornerstoneTools.zoomWheel.activate(element); // zoom is the default tool for middle mouse wheel
        loaded = true;
      }
      ///INFORMACION DEL PACIENTE
      function getnamepaciente() {
        return value = image.data.string('x00100010');
      }

      function getIdpaciente() {
        return value = image.data.string('x00100020');
      }

      function nacimiento() {
        return value = image.data.string('x00100030');
      }

      function sexo() {
        return value = image.data.string('x00100040');
      }
      document.getElementById('nombrepaciente').textContent = getnamepaciente();
      document.getElementById('idpaciente').textContent = getIdpaciente();
      document.getElementById('nacimiento').textContent = nacimiento();
      document.getElementById('sexo').textContent = sexo();

      ///Información de estudio
      function desestudio() {
        return value = image.data.string('x00081030');
      }

      function nomprotocol() {
        return value = image.data.string('x00181030');
      }

      function numacceso() {
        return value = image.data.string('x00080050');
      }

      function idestudio() {
        return value = image.data.string('x00200010');
      }

      function fechaestudio() {
        return value = image.data.string('x00080020');
      }

      function timestudio() {
        return value = image.data.string('x00080030');
      }
      document.getElementById('desestudio').textContent = desestudio();
      document.getElementById('nomprotocol').textContent = nomprotocol();
      document.getElementById('numacceso').textContent = numacceso();
      document.getElementById('idestudio').textContent = idestudio();
      document.getElementById('fechaestudio').textContent = fechaestudio();
      document.getElementById('timestudio').textContent = timestudio();

      ///Información de la serie
      function descserie() {
        return value = image.data.string('x0008103e');
      }

      function numserie() {
        return value = image.data.string('x00200011');
      }

      function modalidad() {
        return value = image.data.string('x00080060');
      }

      function partecuerpo() {
        return value = image.data.string('x00180015');
      }

      function fechaestudio() {
        return value = image.data.string('x00080020');
      }

      function timserie() {
        return value = image.data.string('x00080031');
      }
      document.getElementById('descserie').textContent = descserie();
      document.getElementById('numserie').textContent = numserie();
      document.getElementById('modalidad').textContent = modalidad();
      document.getElementById('partecuerpo').textContent = partecuerpo();
      document.getElementById('fechaestudio').textContent = fechaestudio();
      document.getElementById('timserie').textContent = timserie();

      ///Información de la serie
      function fabricante() {
        return value = image.data.string('x00080070');
      }

      function modelo() {
        return value = image.data.string('x00081090');
      }

      function estacion() {
        return value = image.data.string('x00081010');
      }

      function tituloae() {
        return value = image.data.string('x00020016');
      }

      function istitucion() {
        return value = image.data.string('x00080080');
      }

      function software() {
        return value = image.data.string('x00181020');
      }

      function implementacion() {
        return value = image.data.string('x00020013');
      }
      document.getElementById('fabricante').textContent = fabricante();
      document.getElementById('modelo').textContent = modelo();
      document.getElementById('estacion').textContent = estacion();
      document.getElementById('tituloae').textContent = tituloae();
      document.getElementById('istitucion').textContent = istitucion();
      document.getElementById('software').textContent = software();
      document.getElementById('implementacion').textContent = implementacion();


    }, function (err) {
      alert(err);
    });
  } catch (err) {
    alert(err);
  }
}

function downloadAndView(name) {
  let url = name;
  url = "wadouri:" + url;
  loadAndViewImage(url);
}
var element = document.getElementById('dicomImage');
cornerstone.enable(element);

cornerstone.events.addEventListener('cornerstoneimageloadprogress', function (event) {
  const eventData = event.detail;
  if (eventData.percentComplete == 100) {
    $('#dicomview').show();
    $('#loadersDicom').hide();
  } else {
    $('#loadersDicom').show();
    $('#dicomview').hide();
  }
});
function getUrlWithoutFrame() {
  var url = document.getElementById('wadoURL').value;
  var frameIndex = url.indexOf('frame=');
  if (frameIndex !== -1) {
    url = url.substr(0, frameIndex - 1);
  }
  return url;
}
document.getElementById('toggleModalityLUT').addEventListener('click', function() {
  var applyModalityLUT = document.getElementById('toggleModalityLUT').checked;
  console.log('applyModalityLUT=', applyModalityLUT);
  var image = cornerstone.getImage(element);
  var viewport = cornerstone.getViewport(element);
  if (applyModalityLUT) {
    viewport.modalityLUT = image.modalityLUT;
  } else {
    viewport.modalityLUT = undefined;
  }
  cornerstone.setViewport(element, viewport);
});

document.getElementById('toggleVOILUT').addEventListener('click', function() {
  var applyVOILUT = document.getElementById('toggleVOILUT').checked;
  console.log('applyVOILUT=', applyVOILUT);
  var image = cornerstone.getImage(element);
  var viewport = cornerstone.getViewport(element);
  if (applyVOILUT) {
    viewport.voiLUT = image.voiLUT;
  } else {
    viewport.voiLUT = undefined;
  }
  cornerstone.setViewport(element, viewport);
});
function double() {
  if ($('#dicom').attr('style')) {
    $('#dicom').removeAttr('style');
    $('#infoEstudios').removeAttr('style');
    $('canvas').removeAttr('style');
    $('.expand_').removeAttr('style');
    $('.note-editable').removeAttr('style');
    $('.note-resizebar').removeAttr('style');
    $('.double').removeAttr('style')
    $('.note-editable').removeAttr('style');
    $('.note-resizebar').removeAttr('style');
    $('#fexed').html('<i class="fas fa-scroll"></i> Fijar documento')
    $('#fexed').hide();
  } else {
    $('#infoestudioscard').attr('class', 'card-content collapse show')
    $('.double').attr('style', 'color:blue !important;')
    $('#dicom').attr('style', 'position:fixed;top:0;left:0;width:55%;z-index:1;border-right:solid 2px black');
    $('#infoEstudios').attr('style', 'position:fixed;top:0;right:0;width:45%;z-index:2;height:100vh;max-height:100vh;');
    $('canvas').attr('style', 'width: 100% !important; height: 78% !important;')
    $('.expand_').attr('style', 'display: none !important;');
    $('#fexed').show();
  }
}

function fixed() {
  if ($('.note-resizebar').attr('style')) {
    $('.note-editable').removeAttr('style');
    $('.note-resizebar').removeAttr('style');
    $('#fexed').html('<i class="fas fa-scroll"></i> Fijar documento')
  } else {
    $('#fexed').html('<i class="fas fa-scroll"></i> Expandir documento')
    $('.note-editable').attr('style', 'max-height: 18rem; height:18rem;');
    $('.note-resizebar').attr('style', 'display:none;');
  }

}

function version1() {
  if ($('#v1').attr('data')) {
    $('#v1').attr('class', 'btn btn-icon btn-outline-success float-right');
    $('#summernote').html('');
    $('#v1').removeAttr('data');
    $('#vi').removeAttr('style');
  } else {
    $('#v1').attr('class', 'btn btn-success text-white float-right');
    $('#v1').attr('data', 'click');
    $('#vi').attr('style', 'color:#fff !important');
    var html = `<span id="docs-internal-guid-674e41f1-7fff-b7f9-a3c4-f508d81cbf0f"><h1 style="line-height: 1.2; margin-right: 177pt; text-indent: -180.05pt; text-align: center; margin-top: 0.65pt; margin-bottom: 0pt; padding: 0pt 0pt 0pt 180.05pt;">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;        Diagnóstico Médico</h1><br><br><div dir="ltr" style="margin-left:6.8pt;" align="left"><table style="border: none;"><colgroup><col width="381"><col width="115"><col width="135"></colgroup><tbody><tr style="height:29.45pt"><td style="vertical-align:top;overflow:hidden;overflow-wrap:break-word;"><p dir="ltr" style="line-height:1.125;margin-left: 2.5pt;margin-top:0pt;margin-bottom:0pt;"><span style="font-size: 11pt; font-family: Calibri, sans-serif; background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;">Nombre del paciente:</span></p></td><td colspan="2" style="vertical-align:top;overflow:hidden;overflow-wrap:break-word;"><br></td></tr><tr style="height:38pt"><td style="vertical-align:top;overflow:hidden;overflow-wrap:break-word;"><br><p dir="ltr" style="line-height:1.2;margin-left: 2.5pt;margin-top:0pt;margin-bottom:0pt;"><span style="font-size: 11pt; font-family: Calibri, sans-serif; background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;">………………………………………..… </span><span style="font-size: 12pt; font-family: Calibri, sans-serif; background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;">, </span><span style="font-size: 11pt; font-family: Calibri, sans-serif; background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;">………………………………..…………</span></p></td><td style="vertical-align:top;overflow:hidden;overflow-wrap:break-word;"><br><p dir="ltr" style="line-height:1.2;margin-left: 12.4pt;margin-right: 8.65pt;text-align: center;margin-top:0pt;margin-bottom:0pt;"><span style="font-size: 16pt; font-family: Calibri, sans-serif; background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;">□ </span><span style="font-size: 11pt; font-family: Calibri, sans-serif; background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;">m </span><span style="font-size: 11pt; font-family: Arial; background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;">♂ </span><span style="font-size: 16pt; font-family: Calibri, sans-serif; background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;">□ </span><span style="font-size: 11pt; font-family: Calibri, sans-serif; background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;">f </span><span style="font-size: 11pt; font-family: Arial; background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;">♀</span></p></td><td style="vertical-align:top;overflow:hidden;overflow-wrap:break-word;"><br><p dir="ltr" style="line-height:1.2;margin-right: 2.45pt;text-align: right;margin-top:0pt;margin-bottom:0pt;"><span style="font-size: 11pt; font-family: Calibri, sans-serif; background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;">………………………………</span></p></td></tr><tr style="height:13.55pt"><td style="vertical-align:top;overflow:hidden;overflow-wrap:break-word;"><p dir="ltr" style="line-height:1.0999999999999999;margin-left: 41pt;margin-top:1.55pt;margin-bottom:0pt;"><span style="font-size: 10pt; font-family: Calibri, sans-serif; background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;">Apellido(s)</span><span style="font-size: 10pt; font-family: Calibri, sans-serif; background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;"><span class="Apple-tab-span" style="white-space:pre;">	</span></span><span style="font-size: 10pt; font-family: Calibri, sans-serif; background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;">Nombre(s)</span></p></td><td style="vertical-align:top;overflow:hidden;overflow-wrap:break-word;"><p dir="ltr" style="line-height:1.0999999999999999;margin-left: 8.65pt;margin-right: 8.65pt;text-align: center;margin-top:1.55pt;margin-bottom:0pt;"><span style="font-size: 10pt; font-family: Calibri, sans-serif; background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;">sexo</span></p></td><td style="vertical-align:top;overflow:hidden;overflow-wrap:break-word;"><p dir="ltr" style="line-height:1.0999999999999999;margin-right: 3.4pt;text-align: right;margin-top:1.55pt;margin-bottom:0pt;"><span style="font-size: 10pt; font-family: Calibri, sans-serif; background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;">nacido (dd/mm/aaaa)</span></p></td></tr></tbody></table></div><br><br><p dir="ltr" style="line-height:1.3900000000000001;margin-left: 8.9pt;margin-top:2.85pt;margin-bottom:0pt;"><span style="font-size: 11pt; font-family: Calibri, sans-serif; background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;">El que suscribe, Médico legalmente autorizado para ejercer su profesión, habiendo practicado reconocimiento médico al/a la paciente arriba indicado/a, certifica:</span></p><br><ul style="margin-bottom: 0px;"><li dir="ltr" style="list-style-type: disc; font-size: 16pt; font-family: Calibri, sans-serif; background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre; margin-left: -9.1pt; padding-left: 10.35pt;"><p dir="ltr" style="line-height:1.3800000000000001;margin-right: 5.55pt;margin-top:0pt;margin-bottom:0pt;" role="presentation"><span style="font-size: 11pt; background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;">No existen síntomas de enfermedad orgánica o infecciosa ni de ninguna otra enfermedad transmisible.</span></p></li></ul><br><ul style="margin-bottom: 0px;"><li dir="ltr" style="list-style-type: disc; font-size: 16pt; font-family: Calibri, sans-serif; background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre; margin-left: -9.15pt; padding-left: 10.4pt;"><p dir="ltr" style="line-height:1.2;margin-top:0.05pt;margin-bottom:0pt;" role="presentation"><span style="font-size: 11pt; background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;">El/la paciente no padece de ninguna enfermedad crónica que lo/la limite físicamente.</span></p></li><li dir="ltr" style="list-style-type: disc; font-size: 16pt; font-family: Calibri, sans-serif; background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre; margin-left: -9.15pt; padding-left: 10.4pt;"><p dir="ltr" style="line-height:1.2;margin-top:12.95pt;margin-bottom:0pt;" role="presentation"><span style="font-size: 11pt; background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;">Observaciones/enfermedades/comentarios:</span></p></li></ul><p dir="ltr" style="line-height:1.2;margin-left: 8.9pt;margin-top:12.75pt;margin-bottom:0pt;"><span style="font-size: 11pt; font-family: Calibri, sans-serif; background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;">…………………………………………………………………………………………………………………………………………………………………</span></p><br><p dir="ltr" style="line-height:1.2;margin-left: 8.9pt;margin-top:0pt;margin-bottom:0pt;"><span style="font-size: 11pt; font-family: Calibri, sans-serif; background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;">…………………………………………………………………………………………………………………………………………………………………</span></p><br><p dir="ltr" style="line-height:1.2;margin-left: 8.9pt;margin-top:0pt;margin-bottom:0pt;"><span style="font-size: 11pt; font-family: Calibri, sans-serif; background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;">…………………………………………………………………………………………………………………………………………………………………</span></p><br><p dir="ltr" style="line-height:1.2;margin-left: 8.9pt;margin-top:0pt;margin-bottom:0pt;"><br></p><br><p dir="ltr" style="line-height:1.2;margin-left: 8.9pt;margin-top:0pt;margin-bottom:0pt;"><span style="font-size: 11pt; font-family: Calibri, sans-serif; background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;">Grupo sanguíneo:</span><span style="font-size: 11pt; font-family: Calibri, sans-serif; background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;"><span class="Apple-tab-span" style="white-space:pre;">	</span></span><span style="font-size: 11pt; font-family: Calibri, sans-serif; background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;">……………</span></p><br><p dir="ltr" style="line-height:1.2;margin-left: 8.9pt;margin-top:0pt;margin-bottom:0pt;"><span style="font-size: 11pt; font-family: Calibri, sans-serif; background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;">Factor R.H.:</span><span style="font-size: 11pt; font-family: Calibri, sans-serif; background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;"><span class="Apple-tab-span" style="white-space:pre;">	</span></span><span style="font-size: 11pt; font-family: Calibri, sans-serif; background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;">……………</span></p><span style="font-size: 10pt; font-family: Calibri, sans-serif; background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;"><br></span><p dir="ltr" style="line-height:1.3800000000000001;margin-right: 1.15pt;text-indent: -5.9pt;margin-top:9.1pt;margin-bottom:0pt;padding:0pt 0pt 0pt 5.9pt;"><span style="font-size: 11pt; font-family: Calibri, sans-serif; background-color: transparent; font-style: italic; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;">Número de Cédula Profesional y/o Sello del Médico:</span></p><p dir="ltr" style="line-height:1.2;margin-top:0.4pt;margin-bottom:0pt;"><span style="font-size: 11pt; font-family: Calibri, sans-serif; background-color: transparent; font-style: italic; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;"><span style="width:100px;display:inline-block;position:relative;"></span></span></p><p dir="ltr" style="line-height:1.2;margin-left: 5.9pt;margin-top:0pt;margin-bottom:0pt;"><span style="font-size: 11pt; font-family: Calibri, sans-serif; background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;">Fecha:</span><span style="font-size: 11pt; font-family: Calibri, sans-serif; background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;"><span class="Apple-tab-span" style="white-space:pre;">	</span></span><span style="font-size: 11pt; font-family: Calibri, sans-serif; background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;">……………………………………………………</span></p><br><p dir="ltr" style="line-height:1.2;margin-left: 5.9pt;margin-top:0.05pt;margin-bottom:0pt;"><span style="font-size: 11pt; font-family: Calibri, sans-serif; background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;">Nombre Doctor:</span><span style="font-size: 11pt; font-family: Calibri, sans-serif; background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;"><span class="Apple-tab-span" style="white-space:pre;">	</span></span><span style="font-size: 11pt; font-family: Calibri, sans-serif; background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;">……………………………………………………</span></p><br><br><p dir="ltr" style="line-height:1.2;margin-left: 5.9pt;margin-top:0pt;margin-bottom:0pt;"><span style="font-size: 11pt; font-family: Calibri, sans-serif; background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;">Firma Doctor:</span><span style="font-size: 11pt; font-family: Calibri, sans-serif; background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;"><span class="Apple-tab-span" style="white-space:pre;">	</span></span><span style="font-size: 11pt; font-family: Calibri, sans-serif; background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;">…………………………………………………..</span></p><div><span style="font-size: 11pt; font-family: Calibri, sans-serif; background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;"><br></span></div></span>`;
    $('#summernote').html(html);
  }


}

function version2() {
  if ($('#v2').attr('data')) {
    $('#v2').attr('class', 'btn btn-icon btn-outline-success float-right mr-1');
    $('#summernote').html('');
    $('#v2').removeAttr('data');
    $('#va').removeAttr('style');
  } else {
    $('#v2').attr('class', 'btn btn-success text-white float-right mr-1');
    $('#v2').attr('data', 'click');
    $('#va').attr('style', 'color:#fff !important');
    var html = `<h1 style="text-align: center;"><span style="color: #3366ff;"><strong>Diagn&oacute;stico M&eacute;dico</strong></span></h1>
<p style="text-align: justify;">Este documento deber&aacute; ser llenado por el m&eacute;dico familiar o el m&eacute;dico de su elecci&oacute;n; ante cualquier informaci&oacute;n falsa que se proporcione, el AEL no tendr&aacute; ninguna responsabilidad legal. La informaci&oacute;n proporcionada es confidencial y s&oacute;lo el personal m&eacute;dico y de enfermer&iacute;a tienen acceso a &eacute;l. Cualquier padecimiento o enfermedad que presente el solicitante, no ser&aacute; motivo de rechazo en su ingreso al AEL. Para que este certificado sea v&aacute;lido, deber&aacute; contener todos los datos solicitados.</p>
<p style="text-align: justify;">&nbsp;</p>
<h4 style="padding-left: 120px;"><strong>Fecha:</strong></h4>
<h4 style="padding-left: 120px;"><strong>Nombre:</strong></h4>
<h4 style="padding-left: 120px;"><strong>Expediente AEL:</strong></h4>
<h4 style="padding-left: 120px;"><strong>Fecha de nacimiento:</strong></h4>
<h4 style="padding-left: 120px;"><strong>Sexo:</strong></h4>
<p style="padding-left: 120px;">&nbsp;</p>
<table style="height: 678px;" border="1px" width="632; ">
<tbody>
<tr>
<td style="width: 505px;">
<h4 style="text-align: center;"><strong>Historia Personal</strong></h4>
</td>
<td style="width: 52px;">
<p style="text-align: center;"><strong>S&iacute;</strong></p>
</td>
<td style="width: 53px;">
<p style="text-align: center;"><strong>No</strong></p>
</td>
</tr>
<tr>
<td style="width: 505px;">
<p>1. &iquest;Ha presentado crisis epil&eacute;pticas?</p>
</td>
<td style="width: 52px;">
<p>&nbsp;</p>
</td>
<td style="width: 53px;">
<p>&nbsp;</p>
</td>
</tr>
<tr>
<td style="width: 505px;">
<p>2. &iquest;Ha presentado crisis asm&aacute;ticas?</p>
</td>
<td style="width: 52px;">
<p>&nbsp;</p>
</td>
<td style="width: 53px;">
<p>&nbsp;</p>
</td>
</tr>
<tr>
<td style="width: 505px;">
<p>3. &iquest;Usa lentes?</p>
</td>
<td style="width: 52px;">
<p>&nbsp;</p>
</td>
<td style="width: 53px;">
<p>&nbsp;</p>
</td>
</tr>
<tr>
<td style="width: 505px;">
<p>4. &iquest;Fuma habitualmente?</p>
</td>
<td style="width: 52px;">
<p>&nbsp;</p>
</td>
<td style="width: 53px;">
<p>&nbsp;</p>
</td>
</tr>
<tr>
<td style="width: 505px;">
<p>5. &iquest;Ingiere bebidas alcoh&oacute;licas habitualmente?</p>
</td>
<td style="width: 52px;">
<p>&nbsp;</p>
</td>
<td style="width: 53px;">
<p>&nbsp;</p>
</td>
</tr>
<tr>
<td style="width: 505px;">
<p>6. &iquest;Ha estado hospitalizado durante el &uacute;ltimo a&ntilde;o?</p>
</td>
<td style="width: 52px;">
<p>&nbsp;</p>
</td>
<td style="width: 53px;">
<p>&nbsp;</p>
</td>
</tr>
<tr>
<td style="width: 505px;">
<p>7. &iquest;Ha tenido una cirug&iacute;a, enfermedades o lesi&oacute;n m&eacute;dica seria?</p>
</td>
<td style="width: 52px;">
<p>&nbsp;</p>
</td>
<td style="width: 53px;">
<p>&nbsp;</p>
</td>
</tr>
<tr>
<td style="width: 505px;">
<p>8. &iquest;Alguna vez se ha desmayado o ha perdido el conocimiento?</p>
</td>
<td style="width: 52px;">
<p>&nbsp;</p>
</td>
<td style="width: 53px;">
<p>&nbsp;</p>
</td>
</tr>
<tr>
<td style="width: 505px;">
<p>9. &iquest;Es al&eacute;rgico a alg&uacute;n medicamento o alimento?</p>
</td>
<td style="width: 52px;">
<p>&nbsp;</p>
</td>
<td style="width: 53px;">
<p>&nbsp;</p>
</td>
</tr>
<tr>
<td style="width: 505px;">
<p>10. &iquest;Est&aacute; bajo tratamiento m&eacute;dico?</p>
</td>
<td style="width: 52px;">
<p>&nbsp;</p>
</td>
<td style="width: 53px;">
<p>&nbsp;</p>
</td>
</tr>
<tr>
<td style="width: 505px;">
<p>11. &iquest;Tiene alguna limitaci&oacute;n para su participaci&oacute;n deportiva?</p>
</td>
<td style="width: 52px;">
<p>&nbsp;</p>
</td>
<td style="width: 53px;">
<p>&nbsp;</p>
</td>
</tr>
<tr>
<td style="width: 505px;">
<p>12. &iquest;Ha tenido limitaci&oacute;n m&eacute;dica para practicar alg&uacute;n deporte?</p>
</td>
<td style="width: 52px;">
<p>&nbsp;</p>
</td>
<td style="width: 53px;">
<p>&nbsp;</p>
</td>
</tr>
</tbody>
</table>
<p>&nbsp;</p>
<h4 style="padding-left: 90px;"><strong>Peso:</strong></h4>
<h4 style="padding-left: 90px;"><strong>Estatura:</strong></h4>
<h4 style="padding-left: 90px;"><strong>T.A:</strong></h4>
<h4 style="padding-left: 90px;"><strong>F.C:</strong></h4>
<h4 style="padding-left: 90px;"><strong>F.R:</strong></h4>
<h4 style="padding-left: 90px;"><strong>Extremidades:</strong></h4>
<h4 style="padding-left: 90px;"><strong>Coraz&oacute;n:</strong></h4>
<h4 style="padding-left: 90px;"><strong>Abdomen:</strong></h4>
<h4 style="padding-left: 90px;"><strong>Otros:</strong></h4>
<h4 style="padding-left: 90px;"><strong>Recomendaciones:</strong></h4>
<h4 style="padding-left: 90px;"><strong>Observaciones:</strong></h4>
<p>&nbsp;</p>`;
    $('#summernote').html(html);
  }

}

function version3() {

}

function gira_dicom(giro) {
  switch (giro) {
    case 90:
      document.getElementById('dicomImage').className = 'giro-90'
      break;
    case 180:
      document.getElementById('dicomImage').className = 'giro-180'
      break;
    case 270:
      document.getElementById('dicomImage').className = 'giro-270'
      break;
    case 360:
      document.getElementById('dicomImage').className = 'giro-360'
      break;
  }

}
