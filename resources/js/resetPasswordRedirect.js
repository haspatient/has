function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

jQuery(document).ready(function(){
    try {
        var x = document.getElementsByClassName("alert alert-success");
        var text = x[0].innerText;
        if (text == "¡Tu contraseña ha sido restablecida!") {
            sleep(5000);
            window.location.replace("https://expedienteclinico.humanly-sw.com/Clinica/public/");
        }
    }
    catch(error) {
        //Error
    }
});