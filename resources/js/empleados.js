var estudio_modal;

$(document).ready(function() {
    $('#load').hide();
    $('#load_delete').hide();
    $('#alert_delete').hide();
    $('ul.tabs a:first').addClass('active');
    $('#alert').hide();
    $('#alert_succes').hide();
    $('#alert_succes_delete').hide();
    $('.tabs_content section').hide();
    $('.tabs_content section:first').show();

    $('ul.tabs a').click(function() {
        $('ul.tabs a').removeClass('active');
        $(this).addClass('active');
        $('.tabs_content section').hide();

        var activeTab = $(this).attr('href');
        $(activeTab).show();
        return false;
    });

    loadEmpleados();
    loadGrupos();

    $('.estudios_input').hide();
    $('.links').on('click',function(e){
        $('.estudios_input').hide();
        $('i').removeClass('actives');
        $(this).parent().find('i').addClass('actives');
        $('#selected').text($(this).text());
        var id = $(this).data('nombre');
        ajaxEstudio(id);
    });

    setInterval(function(){
        $('.alert-success').hide(700, "swing");
    },2000);

    setInterval(function(){
        $('.alert-danger').hide(700, "swing");
    },2000);
});

function showEstudioModal(estudio_programado, estudio_nombre, id_array, index, empleado) {
    var estudio_id = id_array[index];

    document.getElementById("objetivo-header").innerHTML = "Estudio programado para " + empleado.nombre;
    document.getElementById("nombre-estudio").innerHTML = estudio_nombre;
    document.getElementById("fecha-inicio").innerHTML = getFechaFormateada(estudio_programado.fecha_inicial);
    document.getElementById("fecha-fin").innerHTML = getFechaFormateada(estudio_programado.fecha_final);
    document.getElementById("inputToken").value = estudio_programado.token;
    document.getElementById("inputNombreEstudio").value = estudio_nombre;
    document.getElementById("inputIDEstudio").value = estudio_id;
    document.getElementById("inputFechaInicialEliminar").value = estudio_programado.fecha_inicial;
    document.getElementById("inputFechaFinalEliminar").value = estudio_programado.fecha_final;

    $('#estudioModal').modal('show');
}

function loadEstudios(estudios_empresa) {
    document.getElementById("inputEstudio").innerHTML = "";
    var selectCategoria = document.getElementById("inputCategoria");

    for (var i = 0; i < estudios_empresa.length; i++) {
        var estudio = estudios_empresa[i];
        if (selectCategoria.options[selectCategoria.selectedIndex].value == estudio.estudio.categoria_id) {
            if (estudio_modal.estudio.nombre == estudio.estudio.nombre) {
                document.getElementById("inputEstudio").innerHTML += '<option value="' + estudio.estudio.nombre + '" selected="selected">' + estudio.estudio.nombre + '</option>';
            } else {
                document.getElementById("inputEstudio").innerHTML += '<option value="' + estudio.estudio.nombre + '">' + estudio.estudio.nombre + '</option>';
            }

        }
    }
}

function showProgramarModal(curp) {
    $.ajax({
        type:'get',
        dataType:'json',
        url:"getEstudiosProgramadosEmpleado/" + curp,
        success: function(datas){
            var registros_estudios = JSON.parse(JSON.stringify(datas));

            for (estudios_programados of registros_estudios) {
                if (estudios_programados) {
                    lista_estudios = JSON.parse(estudios_programados.estudios);

                    if (estudios_programados.grupo_id) {
                        document.getElementById("inputFechaInicioEmpleado").value = estudios_programados.fecha_inicial;
                        document.getElementById("inputFechaFinalEmpleado").value = estudios_programados.fecha_final;
                    }

                    for (estudio of lista_estudios) {
                        document.getElementById("id" + estudio + "_empleado").checked = true;
                    }
                }
            }
            $('#programarModal').modal('show');
        }
    });
}
var curpOriginal = $('#curp').val();
$('#empleado_form').on('submit', function(e) {
    e.preventDefault();
    $('#btn_enviar').attr('disabled', true);
    $.ajax({
        type:'POST',
        url:'updateEmpleado',
        dataType:'json',
        data: $('#empleado_form').serialize(),
        success: function(response){
            $('.modal-body').animate({
                scrollTop: '0px'
            },100);

            setTimeout(() => {
                $('#alerts_empleado').html('');
            }, 10000);

            tpl = '<div class="alert alert-success" role="alert"> La modificación se realizó correctamente</div>';
            $('#alerts_empleado').html(tpl);

            document.getElementById("campoClave").innerHTML = response.clave;
            document.getElementById("campoNombreCompleto").innerHTML = response.nombre + " " + response.apellido_paterno + " " + response.apellido_materno;
            document.getElementById("campoCurp").innerHTML = response.CURP;
            document.getElementById("campoGrupo").innerHTML = response.grupo.nombre;
            document.getElementById("campoFechaNacimiento").innerHTML = response.fecha_nacimiento;
            //document.getElementById("campoEdad").innerHTML = response[0].edad; //FALTA RECIBIR EDAD DESDE EMPLEADOS CONTROLLER
            document.getElementById("campoSexo").innerHTML = response.genero;
            document.getElementById("campoEmail").innerHTML = response.email;
            document.getElementById("campoDireccion").innerHTML = response.direccion;
            document.getElementById("campoTelefono").innerHTML = response.telefono;

            $('#btn_enviar').attr('disabled', false);
            dominio = document.domain;
            if (curpOriginal != $('#curp').val()) {
              window.location.replace("https://"+dominio+'/Clinica/public/Empleados');
            }
        }
    }).fail(function(error) {
        var message;
        if(error.status == 422) {
            message = "Todos los campos son obligatorios. Revise que haya ingresado correctamente la información.";
        } else if (error.status == 400 && error.responseJSON.message == "Error en CURP") {
            message = "La CURP ingresada ya se encuentra registrada, intente con otra";
        } else if (error.status == 400 && error.responseJSON.message == "Error en correo") {
            message = "El correo ingresado ya se encuentra registrado, intente con otro";
        } else if (error.status == 400 && error.responseJSON.message == "Error en clave") {
                message = "La clave del empleado ya se encuentra registrada, intente con otra";
        } else {
            message = "Algo ocurrió, inténtelo más tarde...";
        }

        console.error("Error " + error.status + ": " + error.responseJSON.message);

        $('.modal-body').animate({
            scrollTop: '0px',
        }, 100);

        var tpl = '<div class="alert alert-danger" role="alert">' + message + '</div>';
        $('#alerts_empleado').html(tpl);
        $("#btn_enviar").prop("disabled", false);
        $('#btn_enviar').attr('disabled', false);
    });
});

function loadEmpleados() {
    $('#tabla-empleados').DataTable({
        serverSide: true,
        ajax: "api/empleados",
        language: {
            "decimal": "",
            "emptyTable": "No hay información",
            "info": "",
            "infoEmpty": "",
            "infoFiltered": "(Filtrado de _MAX_ total entradas)",
            "infoPostFix": "",
            "thousands": ",",
            "lengthMenu": "Mostrar _MENU_ entradas",
            "loadingRecords": "Cargando...",
            "processing": "Procesando...",
            "search": "Buscar:",
            "zeroRecords": "Sin resultados encontrados",
            "paginate": {
                "first": "Primero",
                "last": "Ultimo",
                "next": "Siguiente",
                "previous": "Anterior"
            }
        },
        columns: [
            {data: 'clave'},
            {data: 'nombre'},
            {data: 'apellido_paterno'},
            {data: 'apellido_materno'},
            {data: 'CURP'},
            {data: 'grupo_id'},
            {data: 'btn'},
        ]
    });
}

function loadGrupos() {
    $('#tabla-grupos').DataTable({
        serverSide: true,
        ajax: "api/grupos",
        language: {
            "decimal": "",
            "emptyTable": "No hay información",
            "info": "",
            "infoEmpty": "",
            "infoFiltered": "(Filtrado de _MAX_ total entradas)",
            "infoPostFix": "",
            "thousands": ",",
            "lengthMenu": "Mostrar _MENU_ entradas",
            "loadingRecords": "Cargando...",
            "processing": "Procesando...",
            "search": "Buscar:",
            "zeroRecords": "Sin resultados encontrados",
            "paginate": {
                "first": "Primero",
                "last": "Ultimo",
                "next": "Siguiente",
                "previous": "Anterior"
            }
        },
        columns: [
            {data: 'nombre'},
            {data: 'btn'},
        ]
    });
}

function getFechaFormateada(fecha) {
    var miFecha = new Date(fecha);
    var diaNumero = miFecha.getUTCDate();
    var diaNombre = getNombreDia(miFecha.getUTCDay());
    var mesNombre = getNombreMes(miFecha.getUTCMonth());
    var ano = miFecha.getUTCFullYear();

    var fechaCompleta = diaNombre + " " + diaNumero + " de " + mesNombre + " de " + ano;

    return fechaCompleta;
}

function getNombreDia(numero) {
    var nombre;
    switch (numero) {
        case 0:
            nombre = "Domingo";
            break;
        case 1:
            nombre = "Lunes";
            break;
        case 2:
            nombre = "Martes";
            break;
        case 3:
            nombre = "Miércoles";
            break;
        case 4:
            nombre = "Jueves";
            break;
        case 5:
            nombre = "Viernes";
            break;
        case 6:
            nombre = "Sábado";
            break;
        default:
            nombre = "";
            break;
    }

    return nombre;
}

function getNombreMes(numero) {
    var nombre;

    switch (numero) {
        case 0:
            nombre = "enero";
            break;
        case 1:
            nombre = "febrero";
            break;
        case 2:
            nombre = "marzo";
            break;
        case 3:
            nombre = "abril";
            break;
        case 4:
            nombre = "mayo";
            break;
        case 5:
            nombre = "junio";
            break;
        case 6:
            nombre = "julio";
            break;
        case 7:
            nombre = "agosto";
            break;
        case 8:
            nombre = "septiembre";
            break;
        case 9:
            nombre = "octubre";
            break;
        case 10:
            nombre = "noviembre";
            break;
        case 11:
            nombre = "diciembre";
            break;
        default:
            nombre = "";
            break;
    }

    return nombre;
}

$('#formDate').on('submit',function(e) {
    e.preventDefault();
    $.ajax({
        type:'post',
        dataType:'json',
        data:$('#formDate').serialize(),
        url:'../refresh_expedientes',
        beforeSend: function() {
            $('#formDate').hide();
            $('#load').show();
        },
        complete: function() {
        }

    }).done(function(response) {
        var dataSet = [];
        var resultados = response[0];
        if (response[1]!="") {
            var examenFisico = JSON.parse(response[1]);
        }
        var expediente_id = JSON.parse(response[2]);

        document.getElementById("expedienteIDhidden").value = expediente_id;
        $("#btn-nota").attr("onclick","showModalNota(" + expediente_id + ")");

        setTimeout(function() {
            resultados.forEach((value, index) => {
                dataSet.push([
                    value.nombre,
                    '<a href="../Archivos/' + value.id + '" class="btn btn-outline-primary descargar btn-primary">Ver Resultados</a>'
                ]);
            });
            $('#table_estudios').dataTable().fnDestroy();
            $('#table_estudios').dataTable({
                data: dataSet,
                language: {
                    "decimal": "",
                    "emptyTable": "No hay información",
                    "info": "",
                    "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
                    "infoFiltered": "(Filtrado de _MAX_ total entradas)",
                    "infoPostFix": "",
                    "thousands": ",",
                    "lengthMenu": "Mostrar _MENU_ Entradas",
                    "loadingRecords": "Cargando...",
                    "processing": "Procesando...",
                    "search": "Buscar:",
                    "zeroRecords": "Sin resultados encontrados",
                    "paginate": {
                        "first": "Primero",
                        "last": "Ultimo",
                        "next": "Siguiente",
                        "previous": "Anterior"
                    }
                }
            });
            $('#load').hide();
            $('#version').modal('hide');
            $('#formDate').show();

            if (response[1]!="") {
            document.querySelector('#version_select').selectedIndex = '0';
            document.querySelector('#label-estatura').innerHTML = examenFisico.altura ? examenFisico.altura + ' m' : '-';
            document.querySelector('#label-peso').innerHTML = examenFisico.peso ? examenFisico.peso + ' kg' : '-';
            document.querySelector('#label-masa-corporal').innerHTML = examenFisico.imc ? examenFisico.imc + ' kg/m<sup>2</sup>' : '-';
            document.querySelector('#label-frec-respiratoria').innerHTML = examenFisico.fr ? examenFisico.fr : '-';
            document.querySelector('#label-frec-cardiaca').innerHTML = examenFisico.fc ? examenFisico.fc : '-';
            document.querySelector('#label-temperatura').innerHTML = examenFisico.temperatura ? examenFisico.temperatura + ' °C' : '-';
          }
        }, 800);

    }).fail(function(error) {
        $('#load').hide();
        $('#formDate').show();
        $('#alert').show();
    });
})

$('#formExpedienteActual').on('submit',function(e) {
    e.preventDefault();
    $.ajax({
        type:'post',
        dataType:'json',
        data:$('#formExpedienteActual').serialize(),
        url:'../expedienteActual'

    }).done(function(response) {
      if (response!="") {
        var dataSet = [];
        var resultados = response[0];
        if (response[1]!="") {
          var examenFisico = JSON.parse(response[1]);
        }
        var expediente_id = JSON.parse(response[2]);

        document.getElementById("expedienteIDhidden").value = expediente_id;
        $("#btn-nota").attr("onclick","showModalNota(" + expediente_id + ")");

        setTimeout(function() {
            resultados.forEach((value, index) => {
                dataSet.push([
                    value.nombre,
                    '<a href="http://asesoresconsultoreslabs.com/expedienteclinico/Clinica/public/Archivos/' + value.id + '" class="btn btn-outline-primary descargar btn-primary">Ver Resultados</a>'
                ]);
            });
            $('#table_estudios').dataTable().fnDestroy();
            $('#table_estudios').dataTable({
                data: dataSet,
                language: {
                    "decimal": "",
                    "emptyTable": "No hay información",
                    "info": "",
                    "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
                    "infoFiltered": "(Filtrado de _MAX_ total entradas)",
                    "infoPostFix": "",
                    "thousands": ",",
                    "lengthMenu": "Mostrar _MENU_ Entradas",
                    "loadingRecords": "Cargando...",
                    "processing": "Procesando...",
                    "search": "Buscar:",
                    "zeroRecords": "Sin resultados encontrados",
                    "paginate": {
                        "first": "Primero",
                        "last": "Ultimo",
                        "next": "Siguiente",
                        "previous": "Anterior"
                    }
                }
            });

            document.querySelector('#version_select').selectedIndex = '0';
            if (response[1]!="") {
            document.querySelector('#label-estatura').innerHTML = examenFisico.altura ? examenFisico.altura + ' m' : '-';
            document.querySelector('#label-peso').innerHTML = examenFisico.peso ? examenFisico.peso + ' kg' : '-';
            document.querySelector('#label-masa-corporal').innerHTML = examenFisico.imc ? examenFisico.imc + ' kg/m<sup>2</sup>' : '-';
            document.querySelector('#label-frec-respiratoria').innerHTML = examenFisico.fr ? examenFisico.fr : '-';
            document.querySelector('#label-frec-cardiaca').innerHTML = examenFisico.fc ? examenFisico.fc : '-';
            document.querySelector('#label-temperatura').innerHTML = examenFisico.temperatura ? examenFisico.temperatura + ' °C' : '-';
          }
        }, 100);

      }
      else {
        swal('Mensaje',"No hay expediente disponible",'info');
      }
    }).fail(function(error) {
        // Mostrar alerta. PENDIENTE
        //console.error(error)
    });
})

$('#nuevo_grupo').on('click',function(){
  $('#newgrupos').modal('show');
})

$('#formGrupo').on('submit',function(e){
    $('#alert').hide();
    e.preventDefault();
        $.ajax({
            type:'post',
            dataType:'json',
            data:$('#formGrupo').serialize(),
            url:'new_grupo',
            beforeSend: function(){
                $('#formGrupo').hide();
                $('#load').show();
              },
            complete: function() {
            }

        }).done(function(response){
            document.querySelector('#formGrupo').reset();
            var dataSet=[];
            response.forEach((value ,index)=>{
              dataSet.push([
                  value.nombre,
                    `<a href="../public/grupos/${value.nombre}" class="btn btn-sm btn-personalizado">Ver</a>
                    <a onclick="updateGrupo('${value.nombre}')" class="btn btn-sm btn-info" style="margin-left:1px">Modificar</a>
                     <a onclick="deleteGrupo('${value.nombre}')" class="btn btn-sm btn-danger" style="margin-left:1px">Eliminar</a>
                        `
                  ]);
                });
            $('#tabla-grupos').dataTable().fnDestroy();
            $('#tabla-grupos').dataTable({
              data: dataSet,
            });
            swal('Grupo Creado','El grupo se ha creado correctamente','info');
            $('#newgrupos').modal('hide');
            $('#load').hide();
            $('#formGrupo').show();



        }).fail(function(error){
           console.log(error.status);
            if (error.status==400) {
            $('#alert').text('El nombre del grupo ya existe');
            }
            $('#load').hide();
            $('#formGrupo').show();
            $('#alert').show();
            setTimeout(function(){
                $('#alert').hide(700, "swing");
                  $('#alert').text('Algo ha ocurrido, verifica que los campos sean correctos o intentalo mas tarde...');
            },2000)
        });
    })

function deleteGrupo(nombre){
    $('#nameGrupo').text(nombre);
    $('#btn_delete').attr('onclick',"excuteDelete('"+nombre+"')");
    $('#delete').modal('show');
}

function excuteDelete(nombre){
    $('#alert').hide();
    $.ajax({
        type:'get',
        dataType:'json',
        url:'delete_grupo/'+nombre,
        beforeSend: function(){
            $('#load_delete').show();
            $('#present_alert').hide();
            $('#btn_delete').hide();
          },
        complete: function() {
        }

    }).done(function(response){
        if(response!=null){
            var dataSet=[];

            response.forEach((value ,index)=>{
                dataSet.push([
                    value.nombre,
                    `<a href="../public/grupos/${value.nombre}" class="btn btn-sm btn-personalizado">Ver</a>
                    <a onclick="updateGrupo('${value.nombre}')" class="btn btn-sm btn-info" style="margin-left:1px">Modificar</a>
                    <a onclick="deleteGrupo('${value.nombre}')" class="btn btn-sm btn-danger" style="margin-left:1px">Eliminar</a>
                   `
                ]);
            });
            $('#tabla-grupos').dataTable().fnDestroy();
            $('#tabla-grupos').dataTable({
                data: dataSet,
            });
            $('#delete').modal('hide');
            swal('Elimincacion Exitosa','El grupo se ha eliminado de manera correcta','success');
            $('#load_delete').hide();
            $('#present_alert').show();
            $('#btn_delete').show();


        }else{
            $('#load_delete').hide();
            $('#alert_delete').html('El grupo no se puede eliminar debido a que <strong >tiene estudios programados</strong>');
            $('#alert_delete').show();
            setTimeout(function(){
                $('#delete').modal('hide');
                $('#alert_delete').hide(700, "swing");
            },2500)
            setTimeout(function(){
                $('#present_alert').show();
                $('#btn_delete').show();
            },3000)
        }

    }).fail(function(error){
        $('#alert_delete').text('Algo a ocurrido, intentalo mas tarde...');
        $('#load').hide();
        $('#load_delete').hide();
        $('#alert_delete').show();
        setTimeout(function(){
            $('#alert_delete').hide(700, "swing");
        },2000)
        setTimeout(function(){
            $('#present_alert').show();
            $('#btn_delete').show();
        },3000)
    })
}
//
// Modificaion de grupos

function updateGrupo(nombre){
  $('#load_update').hide();
  $('#alert_succes_update').hide();
  $('#title_update').text('Modificació del grupo '+ nombre);
  $('#alert_update').hide();
  $('#grupo_update').val(nombre);
  $('#nombreGrupo').val(nombre);
  $('#updategrupos').modal('show');
}
$('#formGrupoUpdate').on('submit',function(e){
  e.preventDefault();
  $.ajax({
    type:'POST',
    dataType:'json',
    data:$('#formGrupoUpdate').serialize(),
    url:'updateGrupos',
    beforeSend:function(){
      $('#formGrupoUpdate').hide();
      $('#load_update').show();
    },
    complete:function(){
  // $('#updategrupos').modal({backdrop: 'static', keyboard: false})
    }
  }).done(function(response){
        $('#load_update').hide();
        $('#alert_succes_update').show();
    setTimeout(()=>{
      var dataSet=[];
      response.forEach((value ,index)=>{
          dataSet.push([
              value.nombre,
              `<a href="../public/grupos/${value.nombre}" class="btn btn-sm btn-personalizado">Ver</a>
              <a onclick="updateGrupo('${value.nombre}')" class="btn btn-sm btn-info" style="margin-left:1px">Modificar</a>
              <a onclick="deleteGrupo('${value.nombre}')" class="btn btn-sm btn-danger" style="margin-left:1px">Eliminar</a>
             `
          ]);
      });
      $('#tabla-grupos').dataTable().fnDestroy();
      $('#tabla-grupos').dataTable({
          data: dataSet,
      });
      $('#alert_succes_update').hide();
      $('#updategrupos').modal('hide');
      setTimeout(()=>{
              $('#formGrupoUpdate').show();
      },500);
    },1500);
  }).fail(function(){
            $('#load_update').hide();
            $('#alert_update').show();
        setTimeout(()=>{
          $('#alert_update').hide();
          $('#formGrupoUpdate').show();
        },2000);
    });


});
/*seleccionar todos*/
$(document).ready(function() {
  selected = true;
  $('#BtnSeleccionar').click(function() {
    if (selected) {
      $('.seleall input[type=checkbox]').prop("checked", true);
      $('#BtnSeleccionar').val('Deseleccionar');

    } else {
      $('.seleall input[type=checkbox]').prop("checked", false);
      $('#BtnSeleccionar').val('Seleccionar todo');
    }
    selected = !selected;
  });
});

// Función que consulta la nota correspondiente al expediente que se está viendo y la muestra en un modal
function showModalNota(expediente_id) {
    $('#alertNota').hide();
    $('#successNota').hide();

    console.log(expediente_id);

    $.ajax({
        type:'get',
        dataType:'json',
        url:'../getNotaExpediente/' + expediente_id,
        beforeSend: function() {
            $('#alertNota').hide();
            $('#loadNota').hide();
        },
    })

    .done(function(nota) {
        console.log(nota);
        if (nota) {
            console.log("Se cambiará el contenido del textarea");
            console.log(nota.contenido);
            document.getElementById("textNota").innerHTML = nota.contenido;
            document.getElementById("textNota").value = nota.contenido;
            console.log(document.getElementById("textNota"));
        }
        else {
            document.getElementById("textNota").innerHTML = "";
        }
        $('#modalNota').modal('show');
    })

    .fail(function(error) {

    })
}

// Guardar los cambios en las notas del expediente
$('#formNota').on('submit', function(e) {
    e.preventDefault();

    $('#alertNota').hide();
    $('#successNota').hide();

    $.ajax({
        type:'post',
        dataType:'json',
        data: $('#formNota').serialize(),
        url: '../updateNotaExpediente'
    })

    .done(function(response) {
        $('#successNota').show();
    })

    .fail(function(error) {
        $('#alertNota').show();
    });
})


function interactiva(valorPorcentaje) {
    var valor = valorPorcentaje;
    var elArco = document.getElementById("arco");
    var CalcularValor = (180 / 100) * valor;
    elArco.innerHTML = valorPorcentaje + "%";
    if (valorPorcentaje < 50) {
        elArco.style.backgroundImage = "radial-gradient(circle at bottom, white 36%, transparent 38%, transparent 65%, white 67%), linear-gradient(" + CalcularValor + "deg, CornflowerBlue 50%, DarkGray 50%)";
    } else if (valorPorcentaje >= 50 && valorPorcentaje < 80) {
        elArco.style.backgroundImage = "radial-gradient(circle at bottom, white 36%, transparent 38%, transparent 65%, white 67%), linear-gradient(" + CalcularValor + "deg, DarkOrange 50%, DarkGray 50%)";
    } else if (valorPorcentaje >= 80) {
        elArco.style.backgroundImage = "radial-gradient(circle at bottom, white 36%, transparent 38%, transparent 65%, white 67%), linear-gradient(" + CalcularValor + "deg, FireBrick 50%, DarkGray 50%)";
    }
}

function calculateAge(birthday) {
    var birthday_arr = birthday.split("-");
    var birthday_date = new Date(birthday_arr[0], birthday_arr[1] - 1, birthday_arr[2]);
    var ageDifMs = Date.now() - birthday_date.getTime();
    var ageDate = new Date(ageDifMs);
    return Math.abs(ageDate.getUTCFullYear() - 1970);
}

function modalCalculadora(data) {
    document.getElementById("formNivelRiesgo").reset();
    tpl = `<div id="datos" class="float-center" style="margin-left:35%;margin-right:20%">


     </div>`;

    $('#datos').html(tpl);
    buton = `<div id="boton" style="margin-right:300px;margin-left:300px;margin-top:400px">
     <center>
         <button type="submit" class="btn btn-primary" id="btn_form_nombre">Consultar</button>
     </center>
     </div>`
    $('#boton').html(buton);
    interactiva(0);
    // document.getElementById('Factoresriesgo').innerHTML = "";
    // document.getElementById('Categoriariesgo').innerHTML = "";
    // document.getElementById('Nivel de riesgo').innerHTML = "";

    var ArregloDatos = data.split(",");
    var genero = ArregloDatos[0];
    var fecha = ArregloDatos[1];
    var age = calculateAge(fecha);
    document.getElementById('edad').value = age;
    document.getElementById('id').value = ArregloDatos[2];
    if (genero == "Masculino") {
        document.getElementById("generoH").checked = true;
    } else {
        document.getElementById("generoM").checked = true;
    }

    $.ajax({
        url: 'ObtenerCalculadoraC/' + ArregloDatos[2], //archivo que recibe la peticion
        type: 'get', //método de envio
        success: function(response) {

            var datosCalculadora = JSON.parse(response);
            var infoPaciente = JSON.parse(datosCalculadora.infoPersonal);
            if (infoPaciente.pesoActual == "bajo") {
                document.getElementById("pesoActualB").checked = true;
            } else if (infoPaciente.pesoActual == "normal") {
                document.getElementById("pesoActualN").checked = true;
            } else if (infoPaciente.pesoActual == "sobrepeso") {
                document.getElementById("pesoActualS").checked = true;
            } else if (infoPaciente.pesoActual == "obesidad") {
                document.getElementById("pesoActualO").checked = true;
            }
            if (infoPaciente.hipertension == "on") {
                document.getElementById("hipertension").checked = true;
            }
            if (infoPaciente.diabetes == "on") {
                document.getElementById("diabetes").checked = true;
            }
            if (infoPaciente.con_tabaco == "on") {
                document.getElementById("con_tabaco").checked = true;
            }
            if (infoPaciente.epoc == "on") {
                document.getElementById("epoc").checked = true;
            }
            if (infoPaciente.enf_cardio == "on") {
                document.getElementById("enf_cardio").checked = true;
            }
            if (infoPaciente.inmunosup == "on") {
                document.getElementById("inmunosup").checked = true;
            }
            // document.getElementById('Factoresriesgo').innerHTML = "Factores de riesgo: " + datosCalculadora.factoresRiesgo;
            // document.getElementById('Categoriariesgo').innerHTML = "Categoría de riesgo: " + datosCalculadora.categoríaRiesgo;
            // document.getElementById('Nivel de riesgo').innerHTML = "Nivel de riesgo: " + datosCalculadora.NivelRiesgo + "%";
            tpl = `<div id="datos" class="float-center" style="margin-left:35%;margin-right:20%">

                  <div>
                  Factores de riesgo:
                  <h5>` + datosCalculadora.factoresRiesgo + `</h5>
                  </div>
                   Categoría de riesgo:
                   <h6>` + datosCalculadora.categoríaRiesgo + `</h5>

                   Categoría de riesgo:
                   <h5>` + datosCalculadora.NivelRiesgo + `%</h5>
                  </div>`;

            $('#datos').html(tpl);
            buton = `<div id="boton" style="margin-right:300px;margin-left:300px;margin-top:50px">
              <center>
                  <button type="submit" class="btn btn-primary" id="btn_form_nombre">Consultar</button>
              </center>
              </div>`
            $('#boton').html(buton);
            if (response != null) {
                interactiva(datosCalculadora.NivelRiesgo);
            }
        }
    });

    $('#ModalCalculadoraCovid').modal('show');
}


$("#formNivelRiesgo").submit(function(event) {
    event.preventDefault();
    //$("#btn_guardar_pedido").attr("disabled", "disabled");
    //var formData = new FormData($(this)[0]);
    $.ajax({
        type: 'post',
        dataType: 'json',
        url: 'CalculadoraC',
        data: $('#formNivelRiesgo').serialize(),
        success: function(data) {
            tpl = `<div id="datos" class="float-center" style="margin-left:35%;margin-right:20%">

                <div>
                Factores de riesgo:
                <h5>` + data[0] + `</h5>
                </div>
                 Categoría de riesgo:
                 <h6>` + data[1] + `</h5>

                 Categoría de riesgo:
                 <h5>` + data[2] + `%</h5>
                </div>`;

            $('#datos').html(tpl);
            buton = `<div id="boton" style="margin-right:300px;margin-left:300px;margin-top:50px">
            <center>
                <button type="submit" class="btn btn-primary" id="btn_form_nombre">Consultar</button>
            </center>
            </div>`
            $('#boton').html(buton);
            interactiva(data[2]);
        }
    });



});
$('#EliminarEmpleado').on('submit', function(e) {
        e.preventDefault();
        $('#btn_enviar').attr('disabled', true);
        $.ajax({
            data: $('#EliminarEmpleado').serialize(), //datos que se envian a traves de ajax
            url: '../eliminarEmpleado', //archivo que recibe la peticion
            type: 'post', //método de envio
            success: function(response) { //una vez que el archivo recibe el request lo procesa y lo devuelve
                if (response == 'Si') {
                  window.location.href = "../Empleados";
                }
            }
        });
    });
function enviaDatosModal(nombre, apellido_materno, apellido_paterno, id) {
  $('#ModalEliminar').modal('show');
    document.getElementById('pregunta').innerHTML = '¿Desea eliminar al empleado ' + nombre + ' ' + apellido_paterno + ' ' + apellido_materno+'?';
  }

/**
 * [Peticion para programar un estudio y dar de alta en sass]
 * @type {Peticion}
 */
$('#programacion_estudios').on('submit',(e)=>{
  e.preventDefault();
  $.ajax({
    type:'post',
    dataType:'json',
    data:$('#programacion_estudios').serialize(),
    url:'../programarEstudiosEmpleado',
    beforeSend:()=>{
      $('#programarModal').modal('hide');
      $('#loading').modal('show');

    }
  }).done((res)=>{
    sass(res);
    console.log(res);
  }).fail(err=>{
      console.log(err);
        $('#loading').modal('hide');
  })

})

/**
 * Registrar a un nuevo paciente a sass
 * @param  {[json]} res [respuesta del paciente que recien fue registrado
 * en la base de datos]
 * @return {[json]}     [retorna el codigo id (nim) del paciente]
 */
function sass(responseJSON){
    estudios=[];
    responseJSON.detalle.forEach((item, i) => {
      code_sass =parseInt(item.codigo);
      estudios.push(code_sass);
    });

    var res = responseJSON.paciente;
    if (res.genero == 'Masculino') {
      res.genero = 'M'
    }else {
      res.genero = 'F';
    }
    var id_paciente;
    if (res.id_sass!=null) {
       id_paciente = parseInt(res.id_sass);
    }else {
      id_paciente = null;
    }
    var json_sass = new Object();
    json_sass.patient = {
        id:id_paciente ,
        first_name:res.nombre,
        last_name:res.apellido_paterno+' '+res.apellido_materno,
        birth_date:res.fecha_nacimiento,
        email:res.email,
        phone:res.telefono,
        gender:res.genero
    };
    json_sass.company="466",
    json_sass.doctor="299",
    json_sass.diagnose="Nutrición alterada",
    json_sass.observations="Paciente en ayuno",
    json_sass.client="ael",
    json_sass.user_id="1",
    json_sass.branch_id="1",
    json_sass.studies=estudios,
    json_sass.payment={
        amount:0,
        payment_type:0,
        reference:0
    };
    console.log(json_sass);
    data = JSON.stringify(json_sass);
    $.ajax({
        async:true,
        cache:false,
        type: 'POST',
        url  : 'http://sassdev.dyndns.org/ws/',
        data : data,
        dataType: 'json'
    }).done(function(recupera){
      var data = new Object();
      data.empresa_id = responseJSON.estudios.empresa_id;
      data.paciente_id = responseJSON.estudios.empleado_id;
      data.programacion = responseJSON.estudios.id;
      data.nim = recupera.data.codigo_toma;
      publish_orden(data);
    }).fail(()=>{
      $('#loading').modal('hide');
      console.log('Error al cargar.');
    });
}


/**
 * Guardar el nim que se obtiene de la orden emitida por has
 * @param  {[json]} data [form data]
 * @return {[type]}      []
 */
function publish_orden(data){
  console.log(data);
  $.ajax({
    type:'post',
    dataType:'json',
    data:data,
    url:'../sass_orden',
  }).done(res=>{
    console.log(res);
    $('#loading').modal('hide');
    swal({title:'Exito',text:'El estudios se programo de manera correcta',onClose:reload()});
  }).fail(err=>{

  })
}


function reload() {
  location.reload();
}
