@extends('layouts.Vuexy')
@section('title','Pacientes')
@section('begin_vendor_css')
 <link rel="stylesheet" href="{!! asset('public/vuexy/app-assets/vendors/css/tables/datatable/datatables.min.css') !!}">
@endsection
@section('page_css')

@endsection
@section('css_custom')

@endsection
@section('content')

<section id="basic-datatable">
  <div class="row">
      <div class="col-12">
          <div class="card">
              <div class="card-header">
                  <h4 class="card-title">Resultados de Laboratorio</h4>
              </div>
              <div class="card-content">
                  <div class="card-body card-dashboard">
                      <div class="">
                        <table class="table_estudios table" style="width:100%">
                             <thead>
                             <tr>
                               <th>Empleado</th>
                               <th>Estudios</th>
                               <th>Fecha</th>
                               <th>Acciones</th>
                             </tr>
                             </thead>
                             <tbody>
                                 @if ($resultados)
                                     @foreach($resultados as $row)
                                     <tr>
                                         <td>{{ $row->nombre.' '.$row->app.' '.$row->apm}}</td>
                                         <td>{{$row->estudio}}</td>
                                         <td>{{\Carbon::parse($row->created_at)->format('M / d / Y') }}</td>



                                     <td class="d-flex justify-content-center"><a href="{{route('archivo',['id' => encrypt($row->id) ])}}" class="btn btn-sm text-white btn-outline-primary descargar btn-primary">
                                       <i class="feather icon-eye"></i>
                                     </a>
                                     </td>


                                     </tr>
                                     @endforeach
                                 @endif
                           </tbody>
                        </table>

                      </div>
                  </div>
              </div>
          </div>
      </div>
  </div>

</section>

@endsection


@section('page_vendor_js')

<script src=" {!! asset('public/vuexy/app-assets/vendors/js/tables/datatable/datatables.min.js') !!}   "></script>
<script src=" {!! asset('public/vuexy/app-assets/vendors/js/tables/datatable/datatables.buttons.min.js') !!}   "></script>
<script src=" {!! asset('public/vuexy/app-assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js') !!}   "></script>
<script src=" {!! asset('public/vuexy/app-assets/vendors/js/tables/datatable/buttons.bootstrap.min.js') !!}   "></script>
<script src=" {!! asset('public/vuexy/app-assets/vendors/js/tables/datatable/dataTables.select.min.js') !!}   "></script>
<script src=" {!! asset('public/vuexy/app-assets/vendors/js/tables/datatable/datatables.checkboxes.min.js') !!}   "></script>
<script src="{!! asset('public/vuexy/app-assets/vendors/js/tables/datatable/buttons.html5.min.js') !!}" ></script>
<script src="{!! asset('public/vuexy/app-assets/vendors/js/tables/datatable/buttons.bootstrap.min.js') !!}"></script>
<script src="{!! asset('public/vuexy/app-assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js') !!}"></script>
@endsection
@section('page_js')

@endsection
@section('js_custom')
<script src="{!! asset('public/js/empresa/resultados.js') !!}" charset="utf-8"></script>
@endsection
