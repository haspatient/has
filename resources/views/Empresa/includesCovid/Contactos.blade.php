<fieldset>
  <div class="row">
    <div class="col-md-4">
      <div class="form-group">
        Nombre
        @if (isset($estudioCaso))
          <input type="text"  name="nombreContacto1" class="form-control" value="{{$estudioCaso->nombreContacto1}}">
        @else
          <input type="text" name="nombreContacto1" class="form-control">
        @endif
      </div>
    </div>
    @php
    $sexo = array('Masculino' => 'Masculino',
    'Femenino' => 'Femenino');
    @endphp
    <div class="col-md-2">
      <div class="form-group">
        Sexo
        <select name="sexoContacto1" class="form-control" id="tipoPac">
          <option value="">Sexo</option>
          @if (isset($estudioCaso))
            @foreach ($sexo as $key => $value)
              @if ($estudioCaso->sexoContacto1 == $value)
                <option selected value="{{$value}}">"{{$value}}"</option>
              @endif
            @endforeach
            @else
              @foreach ($sexo as $key => $value)
              <option value={{$value}}>{{$value}}</option>
              @endforeach
          @endif
        </select>
      </div>
    </div>
    <div class="col-md-2">
      <div class="form-group">
        Edad
        @if (isset($estudioCaso))
          <input type="text" name="edadContacto1" class="form-control" value="{{$estudioCaso->edadContacto1}}">
        @else
          <input type="text" name="edadContacto1" class="form-control">
        @endif
      </div>
    </div>
    @php
    $TipoContacto = array('ID' => 'ID',
    'ED' => 'ED');
    @endphp
    <div class="col-md-2">
      <div class="form-group">
        Tipo de contacto
        <select name="tipoContacto1" class="form-control" id="tipoPac">
          <option value="">Seleccione una opcion</option>
          @if (isset($estudioCaso))
            @foreach ($TipoContacto as $key => $value)
              @if ($estudioCaso->tipoContacto1 == $value)
                <option selected value={{$value}}>{{$value}}</option>
              @endif
            @endforeach
            @else
              @foreach ($TipoContacto as $key => $value)
              <option value={{$value}}>{{$value}}</option>
              @endforeach
          @endif
        </select>
      </div>
    </div>
    @php
    $sintomas = array('Fiebre' => 'Fiebre',
    'tos' => 'Tos',
    'dolor' => 'Dolor',
    'torácico' => 'Torácico',
    'dificultad_respiratoria' => 'Dificultad respiratoria',
    'ninguno' => 'Ninguno',
    'otros' => 'otros' );
    @endphp
    <div class="col-md-3">
      <div class="form-group">
        Presenta signos y sintomas
        <select name="sintomasContacto1" class="form-control" id="tipoPac">
          <option value="">Seleccione una opcion</option>
          @if (isset($estudioCaso))
            @foreach ($sintomas as $key => $value)
              @if ($estudioCaso->sintomasContacto1 == $value)
                <option selected value={{$value}}>{{$value}}</option>
              @endif
            @endforeach
            @else
              @foreach ($sintomas as $key => $value)
              <option value={{$value}}>{{$value}}</option>
              @endforeach
          @endif
        </select>
      </div>
    </div>
    <div class="col-md-9">
      <div class="form-group">
        Observaciones
        @if (isset($estudioCaso))
          <textarea name="observacionesContacto1" rows="3" cols="50">{{$estudioCaso->observacionesContacto1}}</textarea> </div>
        @else
          <textarea name="observacionesContacto1" rows="3" cols="50"></textarea> </div>
        @endif
    </div>

    <div class="col-md-4">
      <div class="form-group">
        Nombre
        @if (isset($estudioCaso))
          <input type="text"  name="nombreContacto2" class="form-control" value="{{$estudioCaso->nombreContacto2}}">
        @else
          <input type="text" name="nombreContacto2" class="form-control">
        @endif
      </div>
    </div>
    <div class="col-md-2">
      <div class="form-group">
        Sexo
        <select name="sexoContacto2" class="form-control" id="tipoPac">
          <option value="">Sexo</option>
          @if (isset($estudioCaso))
            @foreach ($sexo as $key => $value)
              @if ($estudioCaso->sexoContacto2 == $value)
                <option selected value={{$value}}>{{$value}}</option>
              @endif
            @endforeach
            @else
              @foreach ($sexo as $key => $value)
              <option value={{$value}}>{{$value}}</option>
              @endforeach
          @endif
        </select>
      </div>
    </div>
    <div class="col-md-2">
      <div class="form-group">
        Edad
        @if (isset($estudioCaso))
          <input type="text" name="edadContacto2" class="form-control" value="{{$estudioCaso->edadContacto2}}">
        @else
          <input type="text" name="edadContacto2" class="form-control">
        @endif      </div>
    </div>
    <div class="col-md-2">
      <div class="form-group">
        Tipo de contacto
        <select name="tipoContacto2" class="form-control" id="tipoPac">
          <option value="">Seleccione una opcion</option>
          @if (isset($estudioCaso))
            @foreach ($TipoContacto as $key => $value)
              @if ($estudioCaso->tipoContacto2 == $value)
                <option selected value={{$value}}>{{$value}}</option>
              @endif
            @endforeach
            @else
              @foreach ($TipoContacto as $key => $value)
              <option value={{$value}}>{{$value}}</option>
              @endforeach
          @endif
        </select>
      </div>
    </div>

    <div class="col-md-3">
      <div class="form-group">
        Presenta signos y sintomas
        <select name="sintomasContacto2" class="form-control" id="tipoPac">
          <option value="">Seleccione una opcion</option>
          @if (isset($estudioCaso))
            @foreach ($sintomas as $key => $value)
              @if ($estudioCaso->sintomasContacto2 == $value)
                <option selected value={{$value}}>{{$value}}</option>
              @endif
            @endforeach
            @else
              @foreach ($sintomas as $key => $value)
              <option value={{$value}}>{{$value}}</option>
              @endforeach
          @endif
        </select>
      </div>
    </div>
    <div class="col-md-9">
      <div class="form-group">
        Observaciones
        @if (isset($estudioCaso))
          <textarea name="observacionesContacto2" rows="3" cols="50">{{$estudioCaso->observacionesContacto2}}</textarea> </div>
        @else
          <textarea name="observacionesContacto2" rows="3" cols="50"></textarea> </div>
        @endif    </div>
    {{-- --}}
    <div class="col-md-4">
      <div class="form-group">
        Nombre
        @if (isset($estudioCaso))
          <input type="text"  name="nombreContacto3" class="form-control" value="{{$estudioCaso->nombreContacto3}}">
        @else
          <input type="text" name="nombreContacto3" class="form-control">
        @endif      </div>
    </div>
    <div class="col-md-2">
      <div class="form-group">
        Sexo
        <select name="sexoContacto3" class="form-control" id="tipoPac">
          <option value="">Sexo</option>
          @if (isset($estudioCaso))
            @foreach ($sexo as $key => $value)
              @if ($estudioCaso->sexoContacto3 == $value)
                <option selected value={{$value}}>{{$value}}</option>
              @endif
            @endforeach
            @else
              @foreach ($sexo as $key => $value)
              <option value={{$value}}>{{$value}}</option>
              @endforeach
          @endif
        </select>
      </div>
    </div>
    <div class="col-md-2">
      <div class="form-group">
        Edad
        @if (isset($estudioCaso))
          <input type="text" name="edadContacto3" class="form-control" value="{{$estudioCaso->edadContacto3}}">
        @else
          <input type="text" name="edadContacto3" class="form-control">
        @endif      </div>
    </div>
    <div class="col-md-2">
      <div class="form-group">
        Tipo de contacto
        <select name="tipoContacto3" class="form-control" id="tipoPac">
          <option value="">Seleccione una opcion</option>
          @if (isset($estudioCaso))
            @foreach ($TipoContacto as $key => $value)
              @if ($estudioCaso->tipoContacto3 == $value)
                <option selected value={{$value}}>{{$value}}</option>
              @endif
            @endforeach
            @else
              @foreach ($TipoContacto as $key => $value)
              <option value={{$value}}>{{$value}}</option>
              @endforeach
          @endif
        </select>
      </div>
    </div>

    <div class="col-md-3">
      <div class="form-group">
        Presenta signos y sintomas
        <select name="sintomasContacto3" class="form-control" id="tipoPac">
          <option value="">Seleccione una opcion</option>
          @if (isset($estudioCaso))
            @foreach ($sintomas as $key => $value)
              @if ($estudioCaso->sintomasContacto3 == $value)
                <option selected value={{$value}}>{{$value}}</option>
              @endif
            @endforeach
            @else
              @foreach ($sintomas as $key => $value)
              <option value={{$value}}>{{$value}}</option>
              @endforeach
          @endif
        </select>
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        Observaciones
        @if (isset($estudioCaso))
          <textarea name="observacionesContacto3" rows="3" cols="50">{{$estudioCaso->observacionesContacto3}}</textarea> </div>
        @else
          <textarea name="observacionesContacto3" rows="3" cols="50"></textarea> </div>
        @endif    </div>
    <div>
</fieldset>
