<fieldset>
  <div class="row">
    <div class="row col-md-12">
      <div class="col-md-6">
        <div class="form-group">
          <label for="">¿Tuvo contacto con otros casos de influenza o COVID-19 en las ultimas dos semanas?</label>
        </div>
      </div>
      @if (isset($estudioCaso) && $estudioCaso->contactoOtrosCovid == 'Si')
        <div class="col-md-6">
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" checked value="Si" name="contactoOtrosCovid" id="Caso_de_Contacto_SI">
            <label class="custom-control-label" for="Caso_de_Contacto_SI">Si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success"  value="No" name="contactoOtrosCovid" id="Caso_de_Contacto_No">
            <label class="custom-control-label" for="Caso_de_Contacto_No">No</label>
          </div>
        </div>
      </div>
      @else
        <div class="col-md-6">
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" value="Si" name="contactoOtrosCovid" id="Caso_de_Contacto_SI">
            <label class="custom-control-label" for="Caso_de_Contacto_SI">Si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" checked value="No" name="contactoOtrosCovid" id="Caso_de_Contacto_No">
            <label class="custom-control-label" for="Caso_de_Contacto_No">No</label>
          </div>
        </div>
      </div>
      @endif

    @php
    $animales = array('aves' => 'Aves',
    'cer' => 'Cerdos',
    'otroAnimal' => 'Otro animal');
    @endphp
    <div class="col-md-7">
      <div class="form-group">
        Durante las semanas previas al inicio de los sintomas tuvo contacto con: <br>
        <select name="contactoAnimales" class="form-control" id="tipoAnimales">
          <option value="">Seleccione una opcion</option>
          @if (isset($estudioCaso))
            @foreach ($animales as $key => $value)
              @if ($estudioCaso->aves == $key || $estudioCaso->cerdos == $key)
                <option selected value={{$key}}>{{$value}}</option>
              @endif
            @endforeach
            @else
              @foreach ($animales as $key => $value)
              <option value={{$key}}>{{$value}}</option>
              @endforeach
          @endif
        </select>
      </div>
    </div>
    <div class="col-md-5">
      <div class="form-group">
        Otro animal:
        @if (isset($estudioCaso))
          <input type="text"  name="otroAnimal" class="form-control" value="{{$estudioCaso->otroAnimal}}">
        @else
          <input type="text" name="otroAnimal" class="form-control">
        @endif
      </div>
    </div>

    <div class="row col-md-12">
      <div class="col-md-6">
        <div class="form-group">
          <label for="">¿Realizo algun viaje 7 dias antes del incio de signos y sintomas?</label>
        </div>
      </div>
      @if (isset($estudioCaso) && $estudioCaso->realizoViaje == 'Si')
        <div class="col-md-6">
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" checked value="Si" name="realizoViaje" id="Viaje_SI">
            <label class="custom-control-label" for="Viaje_SI">Si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" value="No" name="realizoViaje" id="Viaje_No">
            <label class="custom-control-label" for="Viaje_No">No</label>
          </div>
        </div>
      </div>
      @else
        <div class="col-md-6">
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" value="Si" name="realizoViaje" id="Viaje_SI">
            <label class="custom-control-label" for="Viaje_SI">Si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" checked value="No" name="realizoViaje" id="Viaje_No">
            <label class="custom-control-label" for="Viaje_No">No</label>
          </div>
        </div>
      </div>
      @endif

    <div class="col-md-3">
      <div class="form-group">
        Pais:
        @if (isset($estudioCaso))
          <input type="text"  name="pais" class="form-control" value="{{$estudioCaso->pais}}">
        @else
          <input type="text" name="pais" class="form-control">
        @endif
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        Ciudad:
        @if (isset($estudioCaso))
          <input type="text"  name="ciudad" class="form-control" value="{{$estudioCaso->ciudad}}">
        @else
          <input type="text" name="ciudad" class="form-control">
        @endif
      </div>
    </div>

    <div class="row col-md-7">
      <div class="col-md-6">
        <div class="form-group">
          <label for="">¿Recibio la vacuna contra influenza en ultimo año?</label>
        </div>
      </div>
      @if (isset($estudioCaso) && $estudioCaso->vacunaInfluenza == 'Si')
        <div class="col-md-6">
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" checked value="Si" name="vacunaInfluenza" id="recivio_Vacuna_Influenza_SI">
            <label class="custom-control-label" for="recivio_Vacuna_Influenza_SI">Si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" value="No" name="vacunaInfluenza" id="recivio_Vacuna_Influenza_No">
            <label class="custom-control-label" for="recivio_Vacuna_Influenza_No">No</label>
          </div>
        </div>
      </div>
      @else
        <div class="col-md-6">
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" value="Si" name="vacunaInfluenza" id="recivio_Vacuna_Influenza_SI">
            <label class="custom-control-label" for="recivio_Vacuna_Influenza_SI">Si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" checked value="No" name="vacunaInfluenza" id="recivio_Vacuna_Influenza_No">
            <label class="custom-control-label" for="recivio_Vacuna_Influenza_No">No</label>
          </div>
        </div>
      </div>
      @endif

    <div class="col-md-5">
      <div class="form-group">
        Fecha de vacunacion
        @if (isset($estudioCaso))
          <input type="date"  class="form-control" id="fechavacInfluenza" name="fechavacInfluenza" value="{{$estudioCaso->fechavacInfluenza}}">
        @else
          <input type="date" class="form-control" id="fechavacInfluenza" name="fechavacInfluenza">
        @endif
      </div>
    </div>

  </div>
</fieldset>
