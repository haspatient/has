<fieldset>
    <div class="row">

        <div class="col-md-4">
            <div class="form-group">
                Nombre y cargo de quien elaboro
                @if (isset($estudioCaso))
                <input type="text"  name="nombElaboro" class="form-control" value="{{$estudioCaso->nombElaboro}}">
                @else
                <input type="text" name="nombElaboro" class="form-control">
                @endif
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                Nombre y cargo de quien autorizo
                @if (isset($estudioCaso))
                <input type="text"  name="nombAutorizo" class="form-control" value="{{$estudioCaso->nombAutorizo}}">
                @else
                <input type="text" name="nombAutorizo" class="form-control">
                @endif
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                Fecha de elaboracion:
                @if (isset($estudioCaso))
                <input type="date" class="form-control" id="fechaElaboracion" name="fechaElaboracion" value="{{$estudioCaso->fechaElaboracion}}">
                @else
                <input type="date" class="form-control" id="fechaElaboracion" name="fechaElaboracion" value="{{\Carbon\Carbon::now()->format('Y-m-d')}}">
                @endif
            </div>
        </div>
    </div>
</fieldset>
