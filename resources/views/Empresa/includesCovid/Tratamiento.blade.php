<fieldset>
  <div class="row">
    <div class="row col-md-12">
      <div class="col-md-6">
        <div class="form-group">
          <label for="">¿Desde el inicio de los sintomas ha recibido tratamiento con antipireticos?</label>
        </div>
      </div>
      @if (isset($estudioCaso) && $estudioCaso->tratAntipireticos == 'Si')
        <div class="col-md-6">
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" checked value="Si" name="tratAntipireticos" id="tratamiento_antipireticos_SI">
            <label class="custom-control-label" for="tratamiento_antipireticos_SI">Si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" value="No" name="tratAntipireticos" id="tratamiento_antipireticos_No">
            <label class="custom-control-label" for="tratamiento_antipireticos_No">No</label>
          </div>
        </div>
      </div>
      @else
        <div class="col-md-6">
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" value="Si" name="tratAntipireticos" id="tratamiento_antipireticos_SI">
            <label class="custom-control-label" for="tratamiento_antipireticos_SI">Si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" checked value="No" name="tratAntipireticos" id="tratamiento_antipireticos_No">
            <label class="custom-control-label" for="tratamiento_antipireticos_No">No</label>
          </div>
        </div>
      </div>
      @endif

    <div class="row col-md-12">
      <div class="col-md-6">
        <div class="form-group">
          <label for="">¿Desde el inicio de los sintomas ha recibido tratamiento con antivirales</label>
        </div>
      </div>
      @if (isset($estudioCaso) && $estudioCaso->tratamientoAntivirales == 'Si')
        <div class="col-md-6">
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" checked value="Si" name="tratamientoAntivirales" id="tratamiento_antivirales_SI">
            <label class="custom-control-label" for="tratamiento_antivirales_SI">Si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" value="No" name="tratamientoAntivirales" id="tratamiento_antivirales_No">
            <label class="custom-control-label" for="tratamiento_antivirales_No">No</label>
          </div>
        </div>
      </div>
      @else
        <div class="col-md-6">
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" value="Si" name="tratamientoAntivirales" id="tratamiento_antivirales_SI">
            <label class="custom-control-label" for="tratamiento_antivirales_SI">Si</label>
          </div>
          <div class="d-inline-block custom-control custom-radio mr-1">
            <input type="radio" class="custom-control-input bg-success" checked value="No" name="tratamientoAntivirales" id="tratamiento_antivirales_No">
            <label class="custom-control-label" for="tratamiento_antivirales_No">No</label>
          </div>
        </div>
      </div>
      @endif
    @php
    $antivirales = array('Amantadina' => 'Amantadina',
    'Rimantadina' => 'Rimantadina',
    'Oseltamivir' => 'Oseltamivir',
    'Zanamivir' => 'Zanamivir',
    'Otro' => 'Otro');
    @endphp
    <div class="col-md-3">
      <div class="form-group">
        Seleccione el antiviral:
        <select name="antivirales" class="form-control">
          <option value="">Seleccione una opcion</option>
          @if (isset($estudioCaso) && $estudioCaso->antivirales != '')
            @foreach ($antivirales as $key => $value)
              @if ($estudioCaso->antivirales == $value)
                <option  selected value={{$value}}>{{$value}}</option>
              @endif
            @endforeach
            @else
              @foreach ($antivirales as $key => $value)
                <option value={{$value}}>{{$value}}</option>
              @endforeach
          @endif
        </select>
      </div>
    </div>
    <div class="col-md-4">
      <div class="form-group">
        Especifique otro:
        @if (isset($estudioCaso))
          <input type="text"  name="otroAntiviral" class="form-control" value="{{$estudioCaso->otroAntiviral}}">
        @else
          <input type="text" name="otroAntiviral" class="form-control">
        @endif
    </div>
  </div>
    <div class="col-md-3">
      <div class="form-group">
        ¿Cuando se inicio el tratamiento con ese antiviral?
        @if (isset($estudioCaso))
          <input type="date"  class="form-control" id="fechaTratAntiviral" name="fechaTratAntiviral" value="{{$estudioCaso->fechaTratAntiviral}}">
        @else
          <input type="date" class="form-control" id="fechaTratAntiviral" name="fechaTratAntiviral">
        @endif
      </div>
    </div>
      <div class="col-md-12">
        <div class="form-group">
          En la unidad medica:
        </div>
      </div>
        <div class="row col-md-12">
          <div class="col-md-6">
            <div class="form-group">
              <label for="">¿Se inicia tratamiento con antimicrobianos?</label>
            </div>
          </div>
          @if (isset($estudioCaso) && $estudioCaso->iniciaAntimicrobianos == 'Si')
            <div class="col-md-6">
              <div class="d-inline-block custom-control custom-radio mr-1">
                <input type="radio" class="custom-control-input bg-success" checked value="Si" name="iniciaAntimicrobianos" id="inicia_T_antimicrobianos_SI">
                <label class="custom-control-label" for="inicia_T_antimicrobianos_SI">Si</label>
              </div>
              <div class="d-inline-block custom-control custom-radio mr-1">
                <input type="radio" class="custom-control-input bg-success"  value="No" name="iniciaAntimicrobianos" id="inicia_T_antimicrobianos_No">
                <label class="custom-control-label" for="inicia_T_antimicrobianos_No">No</label>
              </div>
            </div>
          </div>
          @else
            <div class="col-md-6">
              <div class="d-inline-block custom-control custom-radio mr-1">
                <input type="radio" class="custom-control-input bg-success" value="Si" name="iniciaAntimicrobianos" id="inicia_T_antimicrobianos_SI">
                <label class="custom-control-label" for="inicia_T_antimicrobianos_SI">Si</label>
              </div>
              <div class="d-inline-block custom-control custom-radio mr-1">
                <input type="radio" class="custom-control-input bg-success" checked value="No" name="iniciaAntimicrobianos" id="inicia_T_antimicrobianos_No">
                <label class="custom-control-label" for="inicia_T_antimicrobianos_No">No</label>
              </div>
            </div>
          </div>
          @endif

        <div class="row col-md-12">
          <div class="col-md-6">
            <div class="form-group">
              <label for="">¿Se inicia tratamiento con antivirales?</label>
            </div>
          </div>
          @if (isset($estudioCaso) && $estudioCaso->iniciaAntivirales == 'Si')
            <div class="col-md-6">
              <div class="d-inline-block custom-control custom-radio mr-1">
                <input type="radio" class="custom-control-input bg-success" checked value="Si" name="iniciaAntivirales" id="inicia_T_antivirales_SI">
                <label class="custom-control-label" for="inicia_T_antivirales_SI">Si</label>
              </div>
              <div class="d-inline-block custom-control custom-radio mr-1">
                <input type="radio" class="custom-control-input bg-success" value="No" name="iniciaAntivirales" id="inicia_T_antivirales_No">
                <label class="custom-control-label" for="inicia_T_antivirales_No">No</label>
              </div>
            </div>
          </div>
          @else
            <div class="col-md-6">
              <div class="d-inline-block custom-control custom-radio mr-1">
                <input type="radio" class="custom-control-input bg-success" value="Si" name="iniciaAntivirales" id="inicia_T_antivirales_SI">
                <label class="custom-control-label" for="inicia_T_antivirales_SI">Si</label>
              </div>
              <div class="d-inline-block custom-control custom-radio mr-1">
                <input type="radio" class="custom-control-input bg-success" checked value="No" name="iniciaAntivirales" id="inicia_T_antivirales_No">
                <label class="custom-control-label" for="inicia_T_antivirales_No">No</label>
              </div>
            </div>
          </div>
          @endif

        <div class="col-md-3">
          <div class="form-group">
            Seleccione el antiviral:
            <select name="tratAntivirales" class="form-control">
              <option value="">Seleccione una opcion</option>
              @if (isset($estudioCaso))
                @foreach ($antivirales as $key => $value)
                  @if ($estudioCaso->tratAntivirales == $value)
                    <option  selected value={{$value}}>{{$value}}</option>
                  @endif
                @endforeach
                @else
                  @foreach ($antivirales as $key => $value)
                  <option value={{$value}}>{{$value}}</option>
                  @endforeach
              @endif
            </select>
          </div>
        </div>
        <div class="col-md-3">
          <div class="form-group">
            Especifique otro:
            @if (isset($estudioCaso))
              <input type="text"  name="tratAntiviralesOtro" class="form-control" value="{{$estudioCaso->tratAntiviralesOtro}}">
            @else
              <input type="text" name="tratAntiviralesOtro" class="form-control">
            @endif
          </div>
        </div>
      </div>
</fieldset>
