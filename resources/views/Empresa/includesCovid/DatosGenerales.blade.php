<fieldset>
  <div class="row">
    <div class="col-md-3">
      <div class="form-group">
        Nombre de la unidad
        @if (isset($estudioCaso))
        <input type="text" name="nomUnidad" class="form-control" value="{{$estudioCaso->nomUnidad}}">
        @else
        <input type="text" name="nomUnidad" class="form-control">
        @endif
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        Fecha de notificacion en la plataforma
        @if (isset($estudioCaso))
        <input type="date"  class="form-control" id="fechaNotPlat" name="fechaNotPlat" value="{{$estudioCaso->fechaNotPlat}}">
        @else
        <input type="date" class="form-control" id="fechaNotPlat" name="fechaNotPlat" value="{{\Carbon\Carbon::now()->format('Y-m-d')}}">
        @endif
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        Folio plataforma
        @if (isset($estudioCaso))
        <input type="text"  name="folioPlataforma" class="form-control" value="{{$estudioCaso->folioPlataforma}}">
        @else
        <input type="text" name="folioPlataforma" class="form-control">
        @endif
      </div>
    </div>
    <div class="col-md-4">
      <div class="form-group">
        Apellido Paterno
        @if (isset($estudioCaso))
        <input type="text"  name="aPaterno" class="form-control" value={{$estudioCaso->aPaterno}}>
      @elseif (!empty($empleado))
          <input type="text" name="aPaterno" class="form-control" value={{$empleado->apellido_paterno}}>
        @endif
      </div>
    </div>
    <div class="col-md-4">
      <div class="form-group">
        Apellido Materno
        @if (isset($estudioCaso))
        <input type="text"  name="aMaterno" class="form-control" value={{$estudioCaso->aMaterno}}>
        @elseif (!empty($empleado))
        <input type="text" name="aMaterno" class="form-control" value={{$empleado->apellido_materno}}>
        @endif
      </div>
    </div>
    <div class="col-md-4">
      <div class="form-group">
        Nombre(s)
        @if (isset($estudioCaso))
        <input type="text"  name="nombre" class="form-control" value={{$estudioCaso->nombre}}>
        @elseif (!empty($empleado))
        <input type="text" name="nombre" class="form-control" value={{$empleado->nombre}}>
        @endif
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        Fecha de nacimiento
        @if (isset($estudioCaso))
          <input type="date"  class="form-control" id="diaFechaNac" name="diaFechaNac" value={{$empleado->fecha_nacimiento}}>
          @elseif (!empty($empleado))
            <input type="date" class="form-control" id="diaFechaNac" name="diaFechaNac" value={{$empleado->fecha_nacimiento}}>
        @endif
      </div>
    </div>
    <div class="col-md-4">
      <div class="form-group">
        Curp
        @if (isset($estudioCaso))
        <input type="text"  name="curp" class="form-control" value={{$estudioCaso->curp}}>
        @elseif (!empty($empleado))
        <input type="text" name="curp" class="form-control" value={{$empleado->CURP}}>
        @endif
      </div>
    </div>
    @php
    $genero = array('Masculino' => 'Masculino',
    'Femenino' => 'Femenino');
    @endphp
    <div class="col-md-3">
      <div class="form-group">
        Sexo
        @if (isset($estudioCaso))
        <select name="sexo" class="form-control">
          <option value="">Seleccione una opcion</option>
          @foreach ($genero as $key => $value)
          @if ($estudioCaso->sexo==$value)
          <option  selected value={{$value}}>{{$value}}</option>
          @endif
          @endforeach
        </select>
        @else
        <select name="sexo" class="form-control">
          <option value="">Seleccione una opcion</option>
          @foreach ($genero as $key => $value)
          @if (!empty($empleado) && $empleado->genero==$value)
          <option selected value={{$value}}>{{$value}}</option>
          @else
          <option value={{$value}}>{{$value}}</option>
          @endif
          @endforeach
        </select>
        @endif

      </div>
    </div>
    @php
    $Afirmaciones = array('No' => 'No',
    'Si' => 'Si');
    @endphp
    @if (isset($estudioCaso) && $estudioCaso->sexo == 'Femenino' || $empleado['genero'] == 'Femenino')
      <div class="col-md-3">
        <div class="form-group">
          ¿Esta embarazada?
          <select name="embarazada" class="form-control">
            <option value="">Seleccione una opcion</option>
            @if (isset($estudioCaso) )
            @foreach ($Afirmaciones as $key => $value)
            @if ($estudioCaso->embarazada == $value)
            <option selected value={{$value}}>{{$value}}</option>
            @else
            <option value={{$value}}>{{$value}}</option>
            @endif
            @endforeach
            @else
            @foreach ($Afirmaciones as $key => $value)
            <option value={{$value}}>{{$value}}</option>
            @endforeach
            @endif
          </select>
        </div>
      </div>
      <div class="col-md-3">
        <div class="form-group">
          Meses de embarazo
          <select name="mesesEmbarazo" class="form-control">
            <option value="">Seleccione una opcion</option>
            @if (isset($estudioCaso))
            @for ($i=1; $i
            < 10; $i++) @if ($estudioCaso->mesesEmbarazo == $i)
            <option  selected value={{$i}}>{{$i}}</option>
            @endif
            @endfor
            @else
            @for ($i=1; $i < 10; $i++)
              <option value={{$i}}>{{$i}}</option>
              @endfor
              @endif

          </select>
        </div>
      </div>
      <div class="col-md-3">
        <div class="form-group">
          Se encuentra en periodo de puerperio
          <select name="periodoPuerperio" class="form-control">
            @if (isset($estudioCaso) )
            @foreach ($Afirmaciones as $key => $value)
            @if ($estudioCaso->periodoPuerperio == $value)
            <option selected value={{$value}}>{{$value}}</option>
            @endif
            @endforeach
            @else
            @foreach ($Afirmaciones as $key => $value)
            <option value={{$value}}>{{$value}}</option>
            @endforeach
            @endif
          </select>
        </div>
      </div>
      <div class="col-md-3">
        <div class="form-group">
          Dias de puerperio
          <select name="diasPuerperio" class="form-control">
            <option value="">Seleccione una opcion</option>
            @if (isset($estudioCaso))
            @for ($i=1; $i <= 60; $i++)
               @if ($estudioCaso->diasPuerperio == $i)
            <option  selected value={{$i}}>{{$i}}</option>
            @endif
            @endfor
            @else
            @for ($i=1; $i < 10; $i++)
              <option value={{$i}}>{{$i}}</option>
              @endfor
              @endif
          </select>
        </div>
      </div>
    @endif
    @php
      $nacionalidad = array('Mexicana' => 'Mexicana',
                'Mexicana' => 'Mexicana');
    @endphp
    <div class="col-md-3">
      <div class="form-group">
        Nacionalidad
        <select name="nacionalidad" class="form-control">
          @if (isset($estudioCaso))
          @foreach ($nacionalidad as $key => $value)
            @if ($estudioCaso->nacionalidad == $value)
              <option  selected value={{$value}}>{{$value}}</option>
              @else
                <option value={{$value}}>{{$value}}</option>
            @endif
          @endforeach
          @else
            @foreach ($nacionalidad as $key => $value)
                  <option value={{$value}}>{{$value}}</option>
            @endforeach
        @endif
        </select>
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        ¿Es migrante?
        <select name="migrante" class="form-control">
          @if (isset($estudioCaso))
          @foreach ($Afirmaciones as $key => $value)
          @if ($estudioCaso->migrante == $value)
          <option selected value={{$value}}>{{$value}}</option>
          @endif
          @endforeach
          @else
          @foreach ($Afirmaciones as $key => $value)
          <option value={{$value}}>{{$value}}</option>
          @endforeach
          @endif
        </select>
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        Pais de nacionalidad
        @if (isset($estudioCaso))
          <input type="text"  name="paisNacionalidad" class="form-control" value={{$estudioCaso->paisNacionalidad}}>
        @else
          <input type="text" name="paisNacionalidad" class="form-control">
        @endif
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        Pais de origen
        @if (isset($estudioCaso))
          <input type="text"  name="paisOrigen" class="form-control" value={{$estudioCaso->paisOrigen}}>
        @else
          <input type="text" name="paisOrigen" class="form-control">
        @endif
      </div>
    </div>
    <div class="col-md-12">
      <div class="form-group">
        Paises en transito en los ultimos tres meses
      </div>
    </div>
    <div class="col-md-4">
      <div class="form-group">
        Paises en transito en los ultimos tres meses
        @if (isset($estudioCaso))
          1. <input type="text"  name="paisTransito1" class="form-control" value="{{$estudioCaso->paisTransito1}}">
        @else
          1. <input type="text" name="paisTransito1" class="form-control">
        @endif
      </div>
    </div>
    <div class="col-md-4">
      <div class="form-group">
        @if (isset($estudioCaso))
          2. <input type="text"  name="paisTransito2" class="form-control" value="{{$estudioCaso->paisTransito2}}">
        @else
          2. <input type="text" name="paisTransito2" class="form-control">
        @endif
      </div>
    </div>
    <div class="col-md-4">
      <div class="form-group">
        @if (isset($estudioCaso))
          3. <input type="text"  name="paisTransito3" class="form-control" value="{{$estudioCaso->paisTransito3}}">
        @else
          3. <input type="text" name="paisTransito3" class="form-control">
        @endif
      </div>
    </div>
    <div class="col-md-12">
      <div class="form-group">
        @if (isset($estudioCaso))
          Otro. <input type="text"  name="paisTransitoOtro" class="form-control" value="{{$estudioCaso->paisTransitoOtro}}">
        @else
          Otro. <input type="text" name="paisTransitoOtro" class="form-control">
        @endif
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        Fecha de ingreso a Mexico
        @if (isset($estudioCaso))
          <input type="date"  class="form-control" id="fechaIngMexico" name="fechaIngMexico" value="{{$estudioCaso->fechaIngMexico}}">
        @else
          <input type="date" class="form-control" id="fechaIngMexico" name="fechaIngMexico">
        @endif
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        Pais de nacimiento
        @if (isset($estudioCaso))
          <input type="text"  name="paisdeNacimiento" class="form-control" value="{{$estudioCaso->paisNacimiento}}">
        @else
          <input type="text" name="paisNacimiento" class="form-control">
        @endif
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        Entidad federativa de nacimiento
        @if (isset($estudioCaso))
          <input type="text"  name="entidadFedNacimiento" class="form-control" value="{{$estudioCaso->entidadFedNacimiento}}">
        @else
          <input type="text" name="entidadFedNacimiento" class="form-control">
        @endif
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        Entidad de residencia
        @if (isset($estudioCaso))
          <input type="text"  name="entidadResidencia" class="form-control" value="{{$estudioCaso->entidadResidencia}}">
        @else
          <input type="text" name="entidadResidencia" class="form-control">
        @endif
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        Municipio de residencia
        @if (isset($estudioCaso))
          <input type="text"  name="municipioResidencia" class="form-control" value="{{$estudioCaso->municipioResidencia}}">
        @else
          <input type="text" name="municipioResidencia" class="form-control">
        @endif
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        Localidad
        @if (isset($estudioCaso))
          <input type="text"  name="localidad" class="form-control" value="{{$estudioCaso->localidad}}">
        @else
          <input type="text" name="localidad" class="form-control">
        @endif
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        Calle
        @if (isset($estudioCaso))
          <input type="text"  name="calle" class="form-control" value="{{$estudioCaso->calle}}">
        @else
          <input type="text" name="calle" class="form-control">
        @endif
      </div>
    </div>
    <div class="col-md-2">
      <div class="form-group">
        Numero
        @if (isset($estudioCaso))
          <input type="text"  name="numero" class="form-control" value="{{$estudioCaso->numero}}">
        @else
          <input type="text" name="numero" class="form-control">
        @endif
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        Entre que calles
        @if (isset($estudioCaso))
          <input type="text"  name="entreCalle1" class="form-control" value="{{$estudioCaso->entreCalle1}}"> y <input type="text"  name="entreCalle2" class="form-control" value="{{$estudioCaso->entreCalle2}}">
        @else
          <input type="text" name="entreCalle1" class="form-control"> y <input type="text" name="entreCalle2" class="form-control">
        @endif
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        Colonia
        @if (isset($estudioCaso))
          <input type="text"  name="colonia" class="form-control" value="{{$estudioCaso->colonia}}">
        @else
          <input type="text" name="colonia" class="form-control">
        @endif
      </div>
    </div>
    <div class="col-md-2">
      <div class="form-group">
        CP
        @if (isset($estudioCaso))
          <input type="number"  name="cp" class="form-control" value="{{$estudioCaso->cp}}">
        @else
          <input type="number" name="cp" class="form-control">
        @endif
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        Telefono
        @if (isset($estudioCaso))
          <input type="text"  name="telefono" class="form-control" value="{{$estudioCaso->telefono}}">
        @elseif (!empty($empleado))
          <input type="text" name="telefono" class="form-control" value="{{$empleado->telefono}}">
        @endif
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        ¿Se reconoce como indigena?
        <select name="indigena" class="form-control">
          @if (isset($estudioCaso))
          @foreach ($Afirmaciones as $key => $value)
          @if ($estudioCaso->indigena == $value)
          <option selected value="{{$value}}">{{$value}}</option>
          @endif
          @endforeach
          @else
          @foreach ($Afirmaciones as $key => $value)
          <option value="{{$value}}">{{$value}}</option>
          @endforeach
          @endif
        </select>
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        ¿Habla alguna lengua indigena?
        <select name="lenguaIndigena" class="form-control">
          @if (isset($estudioCaso))
          @foreach ($Afirmaciones as $key => $value)
          @if ($estudioCaso->lenguaIndigena == $value)
          <option selected value="{{$value}}">{{$value}}</option>
          @endif
          @endforeach
          @else
          @foreach ($Afirmaciones as $key => $value)
          <option value="{{$value}}">{{$value}}</option>
          @endforeach
          @endif
        </select>
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        Ocupacion
        @if (isset($estudioCaso))
          <input type="text"  name="ocupacion" class="form-control" value="{{$estudioCaso->ocupacion}}">
        @else
          <input type="text" name="ocupacion" class="form-control">
        @endif
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        ¿Pertenece a alguna institucion educativa?
        @if (isset($estudioCaso))
          <input type="text"  name="institucionEducativa" class="form-control" value="{{$estudioCaso->institucionEducativa}}">
        @else
          <input type="text" name="institucionEducativa" class="form-control">
        @endif
      </div>
    </div>
  </div>
</fieldset>
