@extends('layouts.Vuexy')
@section('title')
  Resultados
@endsection
@section('begin_vendor_css')
<link rel="stylesheet" type="text/css" href="{!! asset('public/vuexy/app-assets/vendors/css/tables/datatable/datatables.min.css') !!}">
@endsection
@section('page_css')

@endsection
@section('content')
  <input type="hidden" id="idSass" value="{{ $nim->nim_sass }}">
  <input type="hidden" id="idExamen" value="{{ $estudio->codigo }}">
  <!-- End vertical navbar -->

 <div class="title-content">
     <div class="seccion-navegacion">
         <p class="navegacion"> <a href="{{route('admin')}}">Inicio</a> / <a href="{{ route('empleados.show', $paciente->CURP)}}">Expediente</a> / <span>Documentos</span> </p>
     </div>
 </div>
<div class="panel panel-default">

@if ($paciente && $estudio )
 <div class="row">
   <div class="col-md-12">
    <div class="card">
      <div class="card-header bg-secondary">
        <h4 class="card-title text-white">Resultados </h4>
      </div>
      <div class="card-content collapse show">
        <div class="card-body row">
          <div class="col-4">
            <h4 class="title_text">Sección</h4>
            <p class="text seccion">
              {{-- {{ $estudios_detail->Seccion }} --}}
            </p>
            <h4 class="title_text">Nombre del Estudio</h4>
            <p class="text">
              {{ $estudio->nombre }}
            </p>
          </div>

          <div class="col-4">
            <h4 class="title_text">Nombre del Paciente</h4>
            <p class="text">
              {{ $paciente->nombre }} {{ $paciente->apellido_paterno }} {{ $paciente->apellido_materno }}
            </p>
            <h4 class="title_text">CURP</h4>
            <p class="text">
              {{ $paciente->CURP }}
            </p>

          </div>
          <div class="col-4">
            <h4 class="title_text">Codigo de la toma</h4>
            <p class="text">
              {{ $nim->nim_sass }}
            </p>
          </div>

        </div>
      </div>
    </div>
  </div>



 </div>
@endif

 <div class="row">
   <div class="col-md-12">
    <div class="card">
      <div class="card-header bg-secondary text-center">
          <h4 class="card-title text-center text-white">Resultados</h4>
      </div>
      <div class="card-content collapse show">
        <div class="card-body">
          <table id="example" class="table table-striped table-bordered" style="width:100%">
            <thead>
                <tr>
                    <th>Sección</th>
                    <th>Examen</th>
                    <th>Abreviatura</th>
                    <th>Resultado</th>
                    {{-- <th>Fecha de liberación</th> --}}
                    <th>Metodo</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
        </div>
      </div>
    </div>
  </div>

{{-- <div class="col-md-6">
<div class="card">
<div class="card-header bg-secondary text-center">
    <h4 class="card-title text-center text-white">Documentos Emitidos</h4>
</div>
<div class="card-content collapse show">
  <div class="card-body">
    @if ($archivos)
        <div class="main">
            <ul class="cards">
                @foreach ($archivos as $archivo)
                    <li class="cards_item">
                        <div class="card">
                            <div class="card_image">
                                @if ($archivo->tipo == "url")
                                    <iframe src="{{$archivo->archivo}}"></iframe>
                                @else
                                    <iframe src="{{route('resultado-pdf', ['id' => $archivo->id])}}"></iframe>
                                @endif
                                <div class="card_content">
                                    <p class="card_text">{{$archivo->estudio}}</p>
                                    <div class="d-flex">
                                        @if ($archivo->tipo == "url")
                                            <button onclick="showModal('{{$archivo->archivo}}')" class="btn btn-sm mr-1 btn-sm btn-secondary">
                                              Ver
                                            </button>
                                            <a href="{{$archivo->archivo}}" target="_blank" download="{{$archivo->nombre .'_'.$archivo->app.'_'.$archivo->apm.'_'.$archivo->estudio}}" class="btn btn-sm  btn-secondary">
                                              Descargar
                                            </a>
                                        @else
                                            <button onclick="showModal('{{route('resultado-pdf', ['id' => $archivo->id])}}')" class="btn btn-secondary mr-1 btn-sm">
                                              Ver
                                            </button>
                                            <a href="{{route('resultado-pdf', ['id' => $archivo->id])}}" target="_blank" download="{{$archivo->nombre .'_'.$archivo->app.'_'.$archivo->apm.'_'.$archivo->estudio}}" class="btn btn-secondary btn-sm text-white">
                                              Descargar
                                            </a>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                @endforeach
            </ul>
        </div>
    @endif

    @if(count($archivos) <= 0)
        <h2 class="font-weight-light mt-1 text-center">
          Aún no hay documentos sobre el estudio
        </h2>
    @endif
  </div>
</div>
</div>
</div> --}}

</div>
</div>


@if ($imagenologia)
  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header bg-secondary">
          <h4 class="card-title text-white">Placas Radiográficas</h4>
        </div>
        <div class="card-content collapse show">
          <div class="card-body">
            @foreach ($imagenologia as $archivo)
            <div class="loadersDicom" id="loadersDicom">
                 <div class="loader-container">
                   <div class="ball-clip-rotate-multiple loader-success">
                     <div></div>
                     <div></div>
                   </div>
                 </div>
             </div>
             <div class="col-md-3">
               <div class="card text-white">
                 <div class="card-content">
                   <img class="card-img img-fluid" src="{!! asset('public/img/radiologia.jpg') !!}" alt="Card image">
                   <div class="card-img-overlay overflow-hidden overlay-cyan">
                     <p class="card_text text-center">Fecha de carga</p>
                     <p class="card_text text-center">{{ \Carbon::parse($archivo->created_at)->format('Y/m/d') }}</p>
                     <div class="d-flex text-center">
                         <button onclick="downloadAndView('{{$archivo->ruta}}','loadersDicom{{ $archivo->id }}')" class="btn btn-sm mr-1 btn-primary">
                           Ver
                         </button>
                         <a href="{{ $archivo->ruta }}" target="_blank"  download="{{ $archivo->ruta }}" class="btn btn-sm btn-primary">
                           Descargar
                         </a>
                     </div>
                   </div>
                 </div>
               </div>
             </div>
            @endforeach
          </div>
        </div>
      </div>
    </div>
  </div>
@endif




 <div class="row " id="dicomview">
      <div class="col-md-12">
          <div style="width:100%;height:600px;position:relative;color: white;display:inline-block;border-width: 1px; border-color:gray;"
               oncontextmenu="return false"
               class='disable-selection noIbar'
               unselectable='on'
               onselectstart='return false;'
               onmousedown='return false;'>
              <div id="dicomImage"
                   style="width:100% ;height:550px;top:0px;left:0px; position:absolute">
              </div>
          </div>
      </div>
  </div>


 {{-- Modal --}}
 <div id="modal" class="modal fade bd-example-modal-xl" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
     <div class="modal-dialog modal-xl">
         <div class="modal-content">
             <div class="modal-header bg-primary">
                 <h4 class="modal-title float-left">Resultados de Estudios</h4>
                 <button type="button" class="close" data-dismiss="modal">&times;</button>
             </div>
             <div class="modal-body">
                 <embed id="embed" width="100%" style="height:60vh;" height="60vh" type="">
             </div>
             <div class="modal-footer">
                 <a class="btn btn-primary" id="link" target="blank">Desplegar en otra pagina</a>
                 <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Cerrar</button>
             </div>
         </div>
     </div>
 </div>
@endsection


@section('js_custom')


<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
<script src="../../resources/js/ajax/datatables/datas.js"> </script>


{{-- inicia --}}
<script src="public/Laboratorio/librerias_dicom/librerias-dicom/-assets/js/scripts/gallery/photo-swipe/photoswipe-script.js"
type="text/javascript"></script>
<script src="public/Laboratorio/librerias_dicom/librerias-dicom/-assets/vendors/js/forms/tags/form-field.js" type="text/javascript"></script>
<script>window.cornerstoneWADOImageLoader || document.write('<script src="https://unpkg.com/cornerstone-wado-image-loader">\x3C/script>')</script>
<script src="{{ asset('public/Laboratorio/librerias_dicom/librerias-dicom/cornerstone.min.js') }} "></script>
<SCRIPT src="{{ asset('public/Laboratorio/librerias_dicom/librerias-dicom/cornerstoneMath.min.js') }} "></SCRIPT>
<SCRIPT src="{{ asset('public/Laboratorio/librerias_dicom/librerias-dicom/cornerstoneTools.min.js') }} "></SCRIPT>
<!-- include the dicomParser library as the WADO image loader depends on it -->
<script src="{{ asset('public/Laboratorio/librerias_dicom/librerias-dicom/dicomParser.min.js') }} "></script>
<!-- include the cornerstoneWADOImageLoader library -->

<script src="https://rawgit.com/cornerstonejs/cornerstoneWADOImageLoader/master/examples/utils/initializeWebWorkers.js"></script>
<script src="https://rawgit.com/cornerstonejs/cornerstoneWADOImageLoader/master/examples/dicomfile/uids.js"></script>
<!-- BEGIN Optional Codecs -->
<!-- OpenJPEG based jpeg 2000 codec -->
{{-- <script src="public/Laboratorio/librerias_dicom/librerias-dicom/openJPEG-FixedMemory.js"></script> --}}
<!-- PDF.js based jpeg 2000 codec -->
<!-- NOTE: do not load the OpenJPEG codec if you use this one -->
<!--<script src="js/jpx.min.js"></script>-->
<!-- JPEG-LS codec -->
{{-- <script src="public/Laboratorio/librerias_dicom/librerias-dicom/charLS-FixedMemory-browser.js"></script> --}}
<!-- JPEG Lossless codec -->
<script src="public/Laboratorio/librerias_dicom/librerias-dicom/jpegLossless.js"></script>
<!-- JPEG Baseline codec -->
<script src="public/Laboratorio/librerias_dicom/librerias-dicom/jpeg.js"></script>
<!-- Deflate transfer syntax codec -->
<script src="public/Laboratorio/librerias_dicom/librerias-dicom/pako.min.js"></script>
{{--  termina --}}


<script src="{!! asset('public/js/empresa/resultados/resultados.js') !!}"></script>
@endsection
