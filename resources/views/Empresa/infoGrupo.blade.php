
@extends('layouts.Vuexy')
@section('title','Grupos')
@section('begin_vendor_css')
  <link rel="stylesheet" type="text/css" href=" {!! asset('public/vuexy/app-assets/vendors/css/tables/datatable/datatables.min.css') !!}">
<link rel="stylesheet" type="text/css" href=" {!! asset('public/vuexy/app-assets/vendors/css/file-uploaders/dropzone.min.css') !!}">

@endsection
@section('page_css')
  <link rel="stylesheet" type="text/css" href=" {!! asset('public/vuexy/app-assets/css/plugins/file-uploaders/dropzone.min.css') !!}">
<link rel="stylesheet" type="text/css" href=" {!! asset('public/vuexy/app-assets/css/pages/data-list-view.min.css') !!}">
@endsection
@section('css_custom')
<link rel="stylesheet" href="{!! asset('css/empresa/pacientes.css') !!}">
@endsection

@section('content')

<div class="row">
  <div class="col-md-12">
    <div class="card">
      <div class="card-header bg-secondary">
        <h4 class="card-title text-white">Grupo {{ $grupo->nombre }}
            <span class="font-small-2 block">{{ \Carbon::parse($grupo->created_at)->format('d-m-Y') }}</span>
        </h4>


      </div>
      <div class="card-content collapse show">
        <div class="card-body">
          <div class="row">
            <div class="col-md-12">
              <div class="contenedor">
                  @if(session('message'))
                      <div class="alert alert-success" >
                          {{session('message')}}
                      </div>
                  @elseif ($errors->any())
                      <div class="alert alert-danger" >
                          <p>Ha ocurrido un error al programar los estudios</p>
                          <ul>
                              @foreach ($errors->all() as $error)
                                  <li>{{$error}}</li>
                              @endforeach
                          </ul>
                      </div>
                  @endif
                  {{-- {{
                    var_dump(\Session::get('Programar Estudios'))
                   }} --}}

                  @isset(\Session::get('Programar Estudios')['insert'])
                    <div class="row">
                        <div class="col-md-12  mb-2">
                          <button class="btn btn-secondary btn-sm" onclick="showProgramarModal('{{ $grupo->nombre }}')">Programar estudios</button>

                          {{-- <button class="btn btn-secondary" onclick="showAgregarEmpleadosModal()">Agregar empleados</button> --}}
                        </div>
                    </div>
                  @endisset



                  <div class="row">
                        @forelse ($estudios_programados as $estudio_programado)
                            @foreach (json_decode($estudio_programado->estudios, true) as $estudio_nombre)

                              <div class="col-md-4 " onclick="showEstudioModal({{ $estudio_programado }}, '{{ $estudio_nombre }}', {{ $estudio_programado->estudios_id }}, {{ $loop->index }})">
                                  <div class="card border-secondary">
                                    <div class="card-header">
                                      <h4 class="card-title">{{ $estudio_nombre }}</h4>
                                    </div>
                                    <div class="card-content collapse show">
                                      <div class="card-body">
                                        <p>Inicio: {{ \Carbon::parse($estudio_programado->fecha_inicial)->toFormattedDateString()  }}</p> <!-- Poner fecha en español: Google: laraveles, laravel en español configurando Carbon multi idioma -->
                                        <p>Fin: {{ \Carbon::parse($estudio_programado->fecha_final)->toFormattedDateString() }}</p>

                                      </div>
                                    </div>
                                  </div>
                                </div>

                            @endforeach
                        @empty
                        @endforelse
                  </div>

              </div>

            </div>
            <div class="col-md-12">

                  <h3 class="text-center">Empleados del grupo</h3>
                  <div class="container">
                      <div class="tabla table-responsive">
                          <table id="tabla-empleados" class="table table-striped table-bordered" style="width:100%">
                              <thead>
                                  <tr>
                                      <th>Clave</th>
                                      <th>Nombre</th>
                                      <th>Primer apellido</th>
                                      <th>Segundo apellido</th>
                                      <th>CURP</th>
                                      <th>Grupo</th>
                                      <th>&nbsp;</th>
                                  </tr>
                              </thead>
                          </table>
                      </div>
                  </div>

            </div>
          </div>

        </div>
      </div>
    </div>
  </div>

</div>

  <form role="form" id="agregar_empleados" method="post" action="{{route('agregarEmpleadosGrupo')}}" enctype="multipart/form-data">
      @csrf
      <input type="hidden" name="encryptedID" value="{{ $encryptedID }}" id="inputEncryptedIDAgregar">
      <!-- Modal para agregar empleados al grupo -->
      <div id="agregarEmpleadosModal" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-lg">
          <div class="modal-content">
              <div class="modal-header">
                  <h4 class="modal-title float-left">Agregar empleados al grupo</h4>
                  <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
              </div>
              <div class="d-block modal-body modal-body-agregar">
                  <p>A continuación se muestran los empleados sin grupo asignado.</p>
                  <p>Seleccione aquellos que desea agregar:</p>
                  <div class="seccion-empleados">
                      <div class="barra-busqueda">
                          <input type="text" name="texto-busqueda" class="campo-busqueda" placeholder="Nombre, CURP o ID del trabajador">
                      </div>
                      <div class="container-fluid">
                          <div class="container">
                              <div id="lista-empleados-agregar" class="custom-checkbox row"></div>
                          </div>
                      </div>
                      <div class="aviso-vacio" style="display: none;">
                          <p>Ningún empleado coincide con su búsqueda.</p>
                      </div>
                  </div>
              </div>
              <div class="modal-footer">
                  <button type="submit" class="btn btn-personalizado" id="im2">Agregar al grupo</button>
                  <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Cerrar</button>
              </div>
              </div>
          </div>
      </div>
  </form>

  <!-- Modal para programar un nuevo estudio para un empleado -->
  <div id="programarEmpleadoModal" class="modal fade" role="dialog">
      <div class="modal-dialog">
          <div class="modal-content">
              <div class="modal-header bg-primary">
                  <h4 class="modal-title float-left" id="programar_empleado_titulo"></h4>
              </div>
              <div class="modal-body">
                  <div id="alerts">
                      <!--Aquí se muestran las alertas generadas-->
                  </div>
                  <div class="modal-main-content">
                      <form role="form" class="col-md-12" id="programacion_estudios_empleado" method="post" action="{{route('programarEstudiosEmpleadoGrupo')}}" enctype="multipart/form-data">
                          @csrf
                          <input type="hidden" name="encryptedID" value="{{ $encryptedID }}" id="inputEncryptedIDProgramarEmpleado">
                          <input type="hidden" name="CURP" value="" id="inputCURP">

                          <div class="form-row">
                              <div class="form-group col-md-12">
                                  <div class="selectores-fechas">
                                      <label for="inputFechaInicioEmpleado">Fecha inicial</label>
                                      <input type="date" name="fechaInicio"  class="form-control" value="{{ \Carbon::today()->toDateString() }}" id="inputFechaInicioEmpleado" min={{ \Carbon::today()->toDateString() }} required>

                                      <label for="inputFechaFinalEmpleado">Fecha final</label>
                                      <input type="date" name="fechaFinal"  class="form-control" value="{{ \Carbon::today()->addWeeks(1)->toDateString() }}" id="inputFechaFinalEmpleado" min={{ \Carbon::today()->toDateString() }} required>
                                  </div>

                                  <div class="estudios-por-programar custom-checkbox">
                                      @foreach ($estudios_grupo as $estudio_grupo)
                                          <div class="col-6 estudios_input2">
                                              <input type="checkbox" name="estudios_programar[{{$loop->index}}]" class="custom-control-input" value="{{$estudio_grupo->estudio->id}}" id="id{{ $estudio_grupo->estudio->id}}_empleado">
                                              <label class="custom-control-label" for="id{{$estudio_grupo->estudio->id}}_empleado">{{ $estudio_grupo->estudio->nombre}}</label>
                                          </div>
                                      @endforeach
                                  </div>

                              </div>
                          </div>
                          <button type="submit" class="btn  btn-personalizado "id="btn_enviar_empleado">Programar estudios</button>
                      </form>
                  </div>
                  <div class="modal-alt-content">

                  </div>
              </div>
              <div class="modal-footer">
                  <button type="button" class="btn btn-outline-danger " data-dismiss="modal">Cerrar</button>
              </div>
          </div>

      </div>
  </div>

  <!-- Modal para programar un nuevo estudio -->
  <div id="programarModal" class="modal fade" role="dialog">
      <div class="modal-dialog">
          <div class="modal-content">
              <div class="modal-header bg-secondary">
                  <h4 class="modal-title float-left">Programar estudio para el grupo</h4>
              </div>
              <div class="modal-body">
                  <div id="alerts">
                      <!--Aquí se muestran las alertas generadas-->
                  </div>
                  <form role="form" class="col-md-12" id="programacion_estudios" method="post" action="{{route('programarEstudiosGrupo')}}" enctype="multipart/form-data">
                      @csrf
                      <input type="hidden" name="encryptedID" value="{{ $encryptedID }}" id="inputEncryptedIDProgramar">

                      <div class="form-row">
                          <div class="form-group col-md-12">
                              <div class="selectores-fechas">
                                  <label for="inputFechaInicioGrupo">Fecha inicial</label>
                                  <input type="date" name="fechaInicio"  class="form-control" value="{{ \Carbon::today()->toDateString() }}" id="inputFechaInicioGrupo" min={{ \Carbon::today()->toDateString() }} required>

                                  <label for="inputFechaFinalGrupo">Fecha final</label>
                                  <input type="date" name="fechaFinal"  class="form-control" value="{{ \Carbon::today()->addWeeks(1)->toDateString() }}" id="inputFechaFinalGrupo" min={{ \Carbon::today()->toDateString() }} required>
                              </div>
                              <br>

                              @if (count($estudios_grupo)>0)
                              {{-- <input type="button" id="BtnSeleccionar1" class="btn btn-personalizado im22 col-12" value="Seleccionar todo " > --}}
                              @endif

                              <div class="estudios-por-programar custom-checkbox">

                                  @foreach ($estudios_grupo as $estudio_grupo)
                                      <div class="col-6 estudios_input2">
                                          <input type="checkbox" name="estudios_programar[{{$loop->index}}]" class="custom-control-input" value="{{$estudio_grupo->estudio->id}}" id="id{{ $estudio_grupo->estudio->id}}">
                                          <label class="custom-control-label" for="id{{$estudio_grupo->estudio->id}}">{{ $estudio_grupo->estudio->nombre}}</label>
                                      </div>
                                  @endforeach

                              </div>

                          </div>
                      </div>
                      <button type="submit" class="btn btn-primary btn-sm" id="btn_enviar">Programar estudios</button>
                    </form>
              </div>
              <div class="modal-footer">
                  <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Cerrar</button>
              </div>
          </div>

      </div>
  </div>

  <!-- Modal para ver estudio programado-->
  <div id="estudioModal" class="modal fade" role="dialog">
      <div class="modal-dialog">
          <div class="modal-content">
              <div class="modal-header bg-primary">
                  <h4 class="modal-title float-left">Estudio programado para el grupo</h4>

              </div>
              <div class="modal-body">
                  <div class="form-row">
                      <div class="form-group col-md-12">
                          <h4>Nombre</h4>
                          <p id="nombre-estudio"></p>
                          <h4>Fecha de inicio</h4>
                          <p id="fecha-inicio"></p>
                          <h4>Fecha de fin</h4>
                          <p id="fecha-fin"></p>
                          <form role="form"  class="col-md-12" id="eliminar_estudio" method="post" action="{{route('eliminarEstudioGrupo')}}" enctype="multipart/form-data">
                              @csrf
                              <input type="hidden" name="encryptedID" value="{{ $encryptedID }}" id="inputEncryptedIDEliminar">
                              <input type="hidden" name="token" value="" id="inputToken">
                              <input type="hidden" name="nombreEstudio" value="" id="inputNombreEstudio">
                              <input type="hidden" name="idEstudio" value="" id="inputIDEstudio">
                              <input type="hidden" name="fechaInicial" value="" id="inputFechaInicialEliminar">
                              <input type="hidden" name="fechaFinal" value="" id="inputFechaFinalEliminar">
                              <button type="submit" class="btn btn-danger">Eliminar</button>
                          </form>
                      </div>
                  </div>
              </div>
              <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
              </div>
          </div>

      </div>
  </div>


@endsection

@section('page_vendor_js')
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src=" {!! asset('public/vuexy/app-assets/vendors/js/extensions/dropzone.min.js') !!}   "></script>
<script src=" {!! asset('public/vuexy/app-assets/vendors/js/tables/datatable/datatables.min.js') !!}   "></script>
<script src=" {!! asset('public/vuexy/app-assets/vendors/js/tables/datatable/datatables.buttons.min.js') !!}   "></script>
<script src=" {!! asset('public/vuexy/app-assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js') !!}   "></script>
<script src=" {!! asset('public/vuexy/app-assets/vendors/js/tables/datatable/buttons.bootstrap.min.js') !!}   "></script>
<script src=" {!! asset('public/vuexy/app-assets/vendors/js/tables/datatable/dataTables.select.min.js') !!}   "></script>
@endsection
@section('page_js')

@endsection
@section('js_custom')

      <script src=" {!! asset('resources/js/infoGrupos.js') !!}  "></script>
      <script> loadEmpleados("{{ $encryptedID }}")</script>
      <script src=" {!! asset('resources/js/jquery.scrollUp.js') !!}  "></script>
@endsection
