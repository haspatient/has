
@extends('layouts.Vuexy')
@section('title','Pacientes')
@section('begin_vendor_css')
  <link rel="stylesheet" type="text/css" href=" {!! asset('public/vuexy/app-assets/vendors/css/tables/datatable/datatables.min.css') !!}">
<link rel="stylesheet" type="text/css" href=" {!! asset('public/vuexy/app-assets/vendors/css/file-uploaders/dropzone.min.css') !!}">
{{-- @dd(Session::all()) --}}
@endsection
@section('page_css')
  <link rel="stylesheet" type="text/css" href=" {!! asset('public/vuexy/app-assets/css/plugins/file-uploaders/dropzone.min.css') !!}">
<link rel="stylesheet" type="text/css" href=" {!! asset('public/vuexy/app-assets/css/pages/data-list-view.min.css') !!}">
@endsection
@section('css_custom')
<link rel="stylesheet" href="{!! asset('public/css/empresa/pacientes.css') !!}">
@endsection
@section('content')
<div id="validacion" data-validacion="@if(Auth::user()->usuarioPrincipal() || isset(Session::get('Pacientes')['insert'])) true @else false @endif"></div>
  <section id="data-list-view" class="data-list-view-header">
    <!-- DataTable starts -->
    <div class="table-responsive">
      <table class="table data-list-view">
        <thead>
          <tr>
            <th></th>
            <th>Nombre</th>
            <th>Apellido Paterno</th>
            <th>Apellido Materno</th>
            <th>Edad</th>
            <th>Curp</th>
            <th>Grupo</th>
            <th>Ver</th>
            <th>Eliminar</th>
          </tr>
        </thead>
        <tbody>
          @foreach ($empleados as $value)
            <tr class="mb-1">

              <td>{{ $value->clave }}</td>
              <td>{{ $value->nombre }}</td>
              <td>{{ $value->apellido_paterno }}</td>
              <td>{{ $value->apellido_materno }}</td>
              <td>{{ \Carbon::parse($value->fecha_nacimiento)->age }}</td>
              <td>{{ $value->CURP }}</td>
              <td>{{ $value->grupo }}</td>
              <td>
                <a href="{{ route('empleados.show', $value->CURP)}}" class="action-view actions_button">
                    <i class="feather icon-eye"></i>
                </a>
              </td>
              <td>

                {{-- <span class="action-edit actions_button"><i class="feather icon-edit"></i></span>--}}
                @if(Auth::user()->usuarioPrincipal() || isset(Session::get('Pacientes')['delete']))
                <span class="action-delete actions_button" data-id="{{ $value->id }}"><i class="feather icon-trash"></i></span>
                @endif
              </td>
            </tr>
          @endforeach

        </tbody>
      </table>
    </div>
    <!-- DataTable ends -->
    <!-- add new sidebar starts -->
    <div class="add-new-data-sidebar">
      <div class="overlay-bg"></div>
      <div class="add-new-data">
        <div class="div mt-2 px-2 d-flex new-data-title justify-content-between">
          <div>
            <h4 class="text-uppercase">Nuevo Paciente</h4>
          </div>
          <div class="hide-data-sidebar">
            <i class="feather icon-x"></i>
          </div>
        </div>
        <div class="data-items pb-3">
          <div class="data-fields px-2 mt-3">
            <div class="row">
              <form method="POST" id='alta'>
                @csrf
                <div class="form-group">
                  <label for="grupo">*Grupo</label>
                  <select id="grupo" name="grupo"  class="form-control @error('grupo') is-invalid @enderror" required>
                    <div id="opciones-grupos">
                      @foreach ($grupos as $grupo)
                      <option value="{{$grupo->id}}">{{$grupo->nombre}}</option>
                      @endforeach
                    </div>
                  </select>
                </div>

                <div class="form-group">
                  <label for="clave">*Clave</label>
                  <input type="text" required class="form-control @error('clave') is-invalid @enderror" name="clave" id="clave">
                </div>
                <div class="form-group">
                  <label for="nombre">*Nombre</label>
                  <input type="text" class="form-control @error('nombre') is-invalid @enderror" name="nombre" id="nombre" required>
                </div>
                <div class="form-row">

                  <div class="form-group col-md-6">
                    <label for="app">*Apellido Paterno</label>
                    <input type="text" class="form-control @error('app') is-invalid @enderror" name="app" id="app" required>
                  </div>
                  <div class="form-group col-md-6">
                    <label for="apm">*Apellido Materno</label>
                    <input type="text" class="form-control @error('apm') is-invalid @enderror" name="apm" id="apm" required>
                  </div>

                </div>

                <div class="form-group">
                  <label for="curp">*CURP <a href="https://www.gob.mx/curp/" target="blank"> (Consulta curp aquí)</a></label>
                  <input type="text" class="form-control @error('nombre') is-invalid @enderror" name="curp" value="{{old('curp')}}" id="curp" maxlength="18" required>
                </div>

                <div class="form-row">

                  <div class="form-group col-md-6">
                    <label for="nacimiento">*Fecha de Nacimiento</label>
                    <input type="date" class="form-control @error('nombre') is-invalid @enderror" name="nacimiento" id="nacimiento" required>
                  </div>

                  <div class="form-group col-md-6">
                    <label for="genero">*Género</label>
                    <select id="genero" name="genero" class="form-control @error('genero') is-invalid @enderror" required>
                      <option value="Masculino">Masculino</option>
                      <option value="Femenino">Femenino</option>
                    </select>
                  </div>

                </div>

                <div class="form-row">

                  <div class="form-group col-md-6">
                    <label for="lugarNacimiento">Lugar de nacimiento</label>
                    <input type="text" class="form-control @error('lugarNacimiento') is-invalid @enderror" id="lugarNacimiento" name="lugarNacimiento">
                  </div>
                  <div class="form-group col-md-6">
                    <label for="nss">Número de seguridad social</label>
                    <input type="text" class="form-control @error('nss') is-invalid @enderror" name="nss" id="nss">
                  </div>

                </div>

                <div class="form-group">
                  <label for="direccion">*Dirección</label>
                  <input type="text" class="form-control @error('direccion') is-invalid @enderror" required id="direccion" name="direccion">
                </div>

                <div class="form-row">
                  <div class="form-group col-md-6">
                    <label for="colonia">Colonia</label>
                    <input type="text" class="form-control @error('colonia') is-invalid @enderror" id="colonia" name="colonia">
                  </div>
                  <div class="form-group col-md-6">
                    <label for="cp">Codigo postal</label>
                    <input type="text" class="form-control @error('cp') is-invalid @enderror" name="cp" id="cp">
                  </div>
                </div>

                <div class="form-row">
                  <div class="form-group col-md-6">
                    <label for="municipio">Municipio</label>
                    <input type="text" class="form-control @error('municipio') is-invalid @enderror" id="municipio" name="municipio">
                  </div>
                  <div class="form-group col-md-6">
                    <label for="estado">Estado</label>
                    <input type="text" class="form-control @error('estado') is-invalid @enderror" id="estado" name="estado">
                  </div>
                </div>

                <div class="form-row">

                  <div class="form-group col-md-6">
                    <label for="email">Email*</label>
                    <input type="email" class="form-control @error('email') is-invalid @enderror" name="email" id="email" required>
                  </div>

                  <div class="form-group col-md-6">
                    <label for="telefono">Teléfono</label>
                    <input type="text"  class="form-control @error('telefono') is-invalid @enderror" id="telefono" name="telefono">
                  </div>

                </div>

                <button type="submit" id="btn_enviar" class="btn btn-sm btn-primary">Agregar</button>
              </form>

            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- add new sidebar ends -->
  </section>
@endsection

@section('page_vendor_js')
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src=" {!! asset('public/vuexy/app-assets/vendors/js/extensions/dropzone.min.js') !!}   "></script>
<script src=" {!! asset('public/vuexy/app-assets/vendors/js/tables/datatable/datatables.min.js') !!}   "></script>
<script src=" {!! asset('public/vuexy/app-assets/vendors/js/tables/datatable/datatables.buttons.min.js') !!}   "></script>
<script src=" {!! asset('public/vuexy/app-assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js') !!}   "></script>
<script src=" {!! asset('public/vuexy/app-assets/vendors/js/tables/datatable/buttons.bootstrap.min.js') !!}   "></script>
<script src=" {!! asset('public/vuexy/app-assets/vendors/js/tables/datatable/dataTables.select.min.js') !!}   "></script>
<script src=" {!! asset('public/vuexy/app-assets/vendors/js/tables/datatable/datatables.checkboxes.min.js') !!}   "></script>
@endsection
@section('page_js')

@endsection
@section('js_custom')
  <script src="{!! asset('public/js/empresa/paciente.js') !!}"></script>
@endsection
