
<section class="signos_vitales col-md-4">

		<div class="card">
			<div class="card-header bg-secondary text-center" style="padding: .6rem !important;">
				<h6 class="card-title font-weight-light text-white text-center">
					Ultimos Signos Vitales
				</h6>
				<button  data-toggle="popover" data-placement="top" data-content="Visualiza el historial de signos vitales de {{$empleado->nombre }} {{$empleado->apellido_paterno}}" data-trigger="hover" data-original-title="Ver Indicadores" onclick="showModalIndicadores('{{ encrypt($empleado->id) }}')" type="button" class="btn btn-icon btn-icon rounded-circle btn-primary btn-sm waves-effect waves-light">
					<i class="feather float-right icon-bar-chart-2"></i>
				</button>
			</div>
			<div class="card-content collapse show" style="">
				<div class="card-body">
					@if(!$ef)
						<div class="alert alert-primary" role="alert">
							<h4 class="alert-heading">¡Atención!</h4>
								<p class="mb-0">No hay consultas recientes</p>
						</div>
                    @endif
                    @if ($ef)
                        <div role="alert" class="alert" style="
                        background-color:
                        @if(floatval($ef->imc) < 18.5)
                        #feff99
                        @elseif(floatval($ef->imc) >= 18.5&& floatval($ef->imc) < 25)
                        #cdffcc
                        @elseif(floatval($ef->imc) == 25)
                        #fe9901
                        @elseif(floatval($ef->imc) > 25 && floatval($ef->imc) < 30)
                        #fe9901
                        @elseif(floatval($ef->imc) == 30)
                        #cd3301
                        @elseif(floatval($ef->imc) > 30 && floatval($ef->imc) < 35)
                        #cd3301
                        @elseif(floatval($ef->imc) >= 35 && floatval($ef->imc) < 40)
                        #cd3301
                        @else
                        #cd3301
                        @endif
                        ">
                        @if(floatval($ef->imc) < 18.5)
                        <h4 class="alert-heading">Clasificación IMC(Indice de masa corporal)</h4>
                        <p class="mb-0">Insuficiencia ponderal</p>
                        @elseif(floatval($ef->imc) >= 18.5&& floatval($ef->imc) < 25)
                        <h4 class="alert-heading">Clasificación IMC(Indice de masa corporal)</h4>
                        <p class="mb-0">Intervalo normal</p>
                        @elseif(floatval($ef->imc) == 25)
                        <h4 class="alert-heading text-white">Clasificación IMC(Indice de masa corporal)</h4>
                        <p class="mb-0 text-white">Sopreso</p>
                        @elseif(floatval($ef->imc) > 25 && floatval($ef->imc) < 30)
                        <h4 class="alert-heading text-white">Clasificación IMC(Indice de masa corporal)</h4>
                        <p class="mb-0 text-white">Preobesidad</p>
                        @elseif(floatval($ef->imc) == 30)
                        <h4 class="alert-heading text-white">Clasificación IMC(Indice de masa corporal)</h4>
                        <p class="mb-0 text-white">Obesidad</p>
                        @elseif(floatval($ef->imc) > 30 && floatval($ef->imc) < 35)
                        <h4 class="alert-heading text-white">Clasificación IMC(Indice de masa corporal)</h4>
                        <p class="mb-0 text-white">Obesidad clase 1</p>
                        @elseif(floatval($ef->imc) >= 35 && floatval($ef->imc) < 40)
                        <h4 class="alert-heading text-white">Clasificación IMC(Indice de masa corporal)</h4>
                        <p class="mb-0 text-white">Obesidad clase 2</p>
                        @else
                        <h4 class="alert-heading text-white">Clasificación IMC(Indice de masa corporal)</h4>
                        <p class="mb-0 text-white">Obesidad clase 3</p>
                        @endif
                        </div>
                    @endif
					 <div class="col-md-12">
						<div class="row mb-1">
							<div class="col-md-2 text-center">
								<i class="fa fa-long-arrow-up icon_vital"></i>
							</div>
							<div class="col-md-5">
								Estatura
							</div>
							<div class="col-md-2 secondary">
								@if ($ef)
									{{$ef->altura}}
								@endif
							</div>
							<div class="col-md-3">
								<span class="text-muted" style="color:#626262;">m</span>
							</div>
						</div>
						<div class="row mb-1">
							<div class="col-md-2 text-center">
								<i class="fa fa-tachometer icon_vital"></i>
							</div>
							<div class="col-md-5">
								Peso
							</div>
							<div class="col-md-2 secondary">
								@if ($ef)
									{{$ef->peso}}
								@endif
							</div>
							<div class="col-md-3">
								<span class="text-muted" style="color:#626262;">Kg</span>
							</div>
						</div>
						<div class="row mb-1">
							<div class="col-md-2 text-center">
								<i class="fa fa-male icon_vital"></i>
							</div>
							<div class="col-md-5">
								Masa Corporal
							</div>
							<div class="col-md-2 secondary">
								@if ($ef)
									{{$ef->imc}}
								@endif
							</div>
							<div class="col-md-3">
								<span class="text-muted" style="color:#626262;">kg/m2</span>
							</div>
						</div>
						<div class="row mb-1">
							<div class="col-md-2 text-center">
								<i class="fa fa-thermometer-three-quarters icon_vital"></i>
							</div>
							<div class="col-md-5">
								Temperatura
							</div>
							<div class="col-md-2 secondary">
								@if ($ef)
									{{$ef->temperatura}}
								@endif
							</div>
							<div class="col-md-3">
								<span class="text-muted" style="color:#626262;">C</span>
							</div>
						</div>
						<div class="row mb-1">
							<div class="col-md-2 text-center">
								<i class="feather icon-activity icon_vital"></i>
							</div>
							<div class="col-md-5">
								 Frecuencia Respiratoria
							</div>
							<div class="col-md-2 secondary">
								@if ($ef)
									{{$ef->fr}}
								@endif
							</div>
							<div class="col-md-3">
								<span class="text-muted" style="color:#626262;">r/m</span>
							</div>
						</div>
						<div class="row mb-1">
							<div class="col-md-2 text-center">
								<i class="fa fa-heartbeat icon_vital"></i>
							</div>
							<div class="col-md-5">
								 Frecuencia Cardiaca
							</div>
							<div class="col-md-2 secondary">
								@if ($ef)
									{{$ef->fc}}
								@endif
							</div>
							<div class="col-md-3">
								<span class="text-muted" style="color:#626262;">bpm</span>
							</div>
                        </div>
                        <div class="row mb-1">
							<div class="col-md-2 text-center">
                                <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"  width="30" height="30"  viewBox="0 0 172 172" style=" fill:#000000;"><g fill="none" fill-rule="nonzero" stroke="none" stroke-width="1" stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0" font-family="none" font-weight="none" font-size="none" text-anchor="none" style="mix-blend-mode: normal"><path d="M0,172v-172h172v172z" fill="none"></path><g fill="#f26b3e"><path d="M86,0c-3.16643,0 -5.73333,2.5669 -5.73333,5.73333c0,3.16643 2.5669,5.73333 5.73333,5.73333c3.16643,0 5.73333,-2.5669 5.73333,-5.73333c0,-3.16643 -2.5669,-5.73333 -5.73333,-5.73333zM86,22.93333c-3.16643,0 -5.73333,2.5669 -5.73333,5.73333c0,3.16643 2.5669,5.73333 5.73333,5.73333c3.16643,0 5.73333,-2.5669 5.73333,-5.73333c0,-3.16643 -2.5669,-5.73333 -5.73333,-5.73333zM57.33333,40.13333c-15.82973,0 -45.86667,53.94493 -45.86667,97.46667c0,5.71613 12.83693,11.46667 28.66667,11.46667c15.82973,0 28.66667,-7.1036 28.66667,-22.93333c0,-4.64012 -0.0297,-14.11777 -0.31354,-24.96016l4.23282,-2.1164c5.50671,-2.75336 10.01315,-6.90313 13.28073,-11.8586c3.26758,4.95547 7.77402,9.10524 13.28073,11.8586l4.23282,2.1164c-0.28384,10.84239 -0.31354,20.32003 -0.31354,24.96016c0,15.82973 12.83693,22.93333 28.66667,22.93333c15.82973,0 28.66667,-5.75053 28.66667,-11.46667c0,-43.52173 -30.03693,-97.46667 -45.86667,-97.46667c-6.86489,0 -9.57717,24.9174 -10.68281,48.40859c-7.5217,-3.96692 -12.25052,-11.72497 -12.25052,-20.25703v-16.68489c0.02122,-1.54972 -0.58581,-3.04203 -1.68279,-4.1369c-1.09698,-1.09487 -2.59045,-1.69903 -4.14013,-1.67482c-3.16203,0.04943 -5.68705,2.6496 -5.64375,5.81172v16.68489c0,8.53206 -4.72882,16.29011 -12.25052,20.25703c-1.10564,-23.49119 -3.81792,-48.40859 -10.68281,-48.40859z"></path></g></g></svg>							</div>
							<div class="col-md-5">
								 Saturacón de Oxígeno
							</div>
							<div class="col-md-3 secondary">
								@if ($ef)
									{{$ef->oxigeno}}
								@endif
							</div>
							<div class="col-md-2">
								<span class="text-muted" style="color:#626262;">%</span>
							</div>
						</div>
                        <div class="row mb-1">
                            <div class="col-md-2 text-center">
                                <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="28" height="28" viewBox="0 0 172 172" style=" fill:#000000;"><g fill="none" fill-rule="nonzero" stroke="none" stroke-width="1" stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0" font-family="none" font-weight="none" font-size="none" text-anchor="none" style="mix-blend-mode: normal"><path d="M0,172v-172h172v172z" fill="none"></path><g fill="#f26b3e"><path d="M24.08,6.88c-9.64576,0 -17.2,11.33136 -17.2,25.8c0,12.6936 5.81704,22.97393 13.76,25.30281v83.05719h0.06719c0.83936,10.96328 9.61297,19.73345 20.57281,20.57281l41.28,0.06719c11.38296,0 20.64,-9.25704 20.64,-20.64v-3.44h37.84c1.90232,0 3.44,-1.54112 3.44,-3.44v-17.43515c13.5536,-1.7028 24.08,-13.27717 24.08,-27.28485c0,-14.00768 -10.5264,-25.58205 -24.08,-27.28485v-38.07515c0,-1.89888 -1.53768,-3.44 -3.44,-3.44h-82.56c-1.90232,0 -3.44,1.54112 -3.44,3.44v110.08c0,1.89888 1.53768,3.44 3.44,3.44h37.84v3.44c0,7.58864 -6.17136,13.76 -13.76,13.76h-39.56c-8.53464,0 -15.48,-6.94536 -15.48,-15.48v-81.33719c7.94296,-2.32888 13.76,-12.60577 13.76,-25.30281c0,-14.46864 -7.55424,-25.8 -17.2,-25.8zM141.04,68.8c11.38296,0 20.64,9.25704 20.64,20.64c0,11.38296 -9.25704,20.64 -20.64,20.64c-11.38296,0 -20.64,-9.25704 -20.64,-20.64c0,-11.38296 9.25704,-20.64 20.64,-20.64zM141.10047,85.94625c-0.90042,0.01806 -1.82013,0.38904 -2.49265,1.06156c-1.34504,1.34504 -1.48952,3.68445 0,4.86437c2.93776,2.32544 12.75219,7.88781 12.75219,7.88781c0,0 -5.56237,-9.81443 -7.88781,-12.75219c-0.58996,-0.74476 -1.4713,-1.07962 -2.37172,-1.06156z"></path></g></g></svg>
                            </div>
                            <div class="col-md-5">
                                Presión arterial
                            </div>
                            <div class="col-md-3">
                                @if ($ef)
									{{$ef->presion}}
								@endif
                            </div>
                            <div class="col-md-2">
                                <span class="text-muted">%</span>
                            </div>
                        </div>
					</div>
				</div>
			</div>
		</div>

		<div id="validador_notasInternas" data-validacion="@if(Auth::user()->usuarioPrincipal() || isset(Session::get('Notas Internas')['update'])) true @else false @endif"></div>
		@if(Auth::user()->usuarioPrincipal() || isset(Session::get('Notas Internas')['update']))
		<div class="card">
			<div class="card-header bg-secondary" style="padding: .6rem !important;">
				<h6 class="card-title font-weight-light text-white">Notas Internas</h6>
				<button id="btnAddNewNota" style="display:none" data-toggle="popover" data-placement="top" data-content="Crea una nota nueva" data-trigger="hover" data-original-title="Nueva Nota" onclick="addNewNota({!! $expediente_id !!})" type="button" class="btn btn-icon btn-icon rounded-circle btn-primary btn-sm waves-effect waves-light mt-25">
					<i class="feather float-right icon-plus"></i>
				</button>
				<span class="spinner-border spinner primary float-rigth mt-25" role="status" aria-hidden="true"></span>
				<small class="block text-white">Las notas solo seran visibles para el médico</small>
			</div>
			<div class="card-content collapse show" style="">
				<div class="card-body">
					@if (count($notas) > 0)
					<div class="col-md-12" id="div_notas">
						@foreach ($notas->sort() as $nota)
								<div id="div_nota{!! $nota->id !!}">
									<div class="btn-group w-100">
										<input onkeyup="actualizarTitulo({!! $nota->id !!})" id="titulo_nota{!! $nota->id !!}" placeholder="Título de la nota" type="text" class="form-control" style="border-radius: 0px;" value="{!! $nota->titulo !!}">
										<button onclick="deleteNota({!! $nota->id !!})" type="button" class="btn-outline-dark btn-danger">
											<i class="feather float-right icon-trash-2"></i>
										</button>
									</div>
									<div class="mb-2" id="editor{!! $nota->id !!}" data-id="{!! $nota->id !!}"></div>
								</div>
						@endforeach
					</div>
					@else
						<div class="col-12 p-0" id="div_notas">
							<div class="row">
								Sin notas
							</div>
						</div>
					@endif
				</div>
			</div>
		</div>
		@endif
		<div id="validador_documentosGenerales" data-validacion="@if(Auth::user()->usuarioPrincipal() || isset(Session::get('Documentos Generales')['delete'])) true @else false @endif"></div>
		@if(Auth::user()->usuarioPrincipal() || isset(Session::get('Documentos Generales')['select']))
		<div class="card">
			<div class="card-header bg-secondary" style="padding: .6rem !important;">
				<h6 class="card-title font-weight-light text-white">
					Documentos Generales
				</h6>
				@if(Auth::user()->usuarioPrincipal() || isset(Session::get('Documentos Generales')['insert']))
				<form data-toggle="popover" data-placement="top" data-content="Agrega archivos del paciente" data-trigger="hover" data-original-title="Archivos" id="docuementPatient" enctype="multipart/form-data" method="post">
					<input type="hidden" name="expId" value="{{ $expediente_id }}">
					<label class="fileContainer btn btn-sm  btn-icon btn-icon rounded-circle btn-primary waves-effect waves-light">
							<i class="feather addDoc icon-plus editIcon"></i>
							<span class="spinner-border text-white spinnerDoc float-rigth spinner-border-sm" role="status" aria-hidden="true" style="display:none"></span>
								<input type="file" name="file" id="documentosPaciente">
					</label>
				</form>
				@endif

			</div>
			<div class="card-content collapse show" style="">
				<div class="card-body">
					<div class="col-md-12 p-0">


						<div class="row content_document">
							@forelse ($documentos as $documento)
								<div  id="document_{{ $documento->id }}" class="d-flex col-md-12 mt-1 justify-content-start align-items-center mb-1">
									<a class="mr-50" target="_blank" href="{!! asset('storage/app/documentos/'.$documento->storage) !!}">
									<img src="https://img.icons8.com/color/48/000000/download-from-cloud.png"/>
									</a>
										<div class="user-page-info">
											<h6 class="mb-0">
												{{ substr($documento->nombre, 0, 23).'...'   }}

											</h6>
											<small class="block text-muted">{{ Carbon::parse($documento->created_at)->diffForHumans() }} </small>
										</div>
										@if(Auth::user()->usuarioPrincipal() || isset(Session::get('Documentos Generales')['delete']))
										<button type="button" data-key="{{ $documento->id }}" class="btn deleteD btn-danger btn-icon ml-auto waves-effect waves-light text-white">
											<i class="feather icon-trash"></i>
										</button>
										@endif
								</div>
								@empty
									No hay archivos agregados
							@endforelse
						</div>
					</div>
				</div>
			</div>
		</div>
		@endif
</section>
