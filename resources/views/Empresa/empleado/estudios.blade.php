<section class="estudios col-md-4">

  <div class="card">
    <div class="card-header bg-secondary" style="padding: .6rem !important;">
      <h6 class="card-title font-weight-light text-white">Consultas</h6>
    </div>
    <div class="card-content collapse show" style="">
      <div class="card-body  row pb-0" id="consultas-list">
        @if(Auth::user()->usuarioPrincipal() || isset(Session::get('Agenda')['select']))
          @if ($agenda && sizeof($agenda) > 0)
            <div class="col-md-12">
              <div class="alert alert-secondary p-0" role="alert">
                  <p class="text-center">Consultas Agendadas</p>
              </div>
              <div class="list-group">
                @foreach ($agenda as $value)
                  <a onclick="agenda('{{ $empleado->id }}','{{ $value->reason }}','{{ $value->id }}')" class="list-group-item list-group-item-action">
                    <div class="d-flex w-100 justify-content-between">
                      <h5 class="mb-1 secondary">{{ $value->reason }}</h5>
                      <small class="text-info">{{ \Carbon\Carbon::parse($value->created_at)->diffForHumans() }}</small>
                    </div>
                    <div class="row">
                      <div class="col-md-6">
                        <h6 class="secondary">
                          Fecha agendada:
                        </h6>
                        <p>{{ $value->date }}</p>
                      </div>
                      <div class="col-md-6">
                        <h6 class="secondary">
                          Hora de Inicio:
                        </h6>
                      <p>{{ $value->start_time }}</p>
                      </div>
                      <div class="col-md-6">
                        <h6 class="secondary">
                          Hora de Termino:
                        </h6>
                        <p>{{ $value->end_time }}</p>
                      </div>
                    </div>
                  </a>
                @endforeach
            </div>
            <hr>
            </div>
          @endif
        @endif
        <div class="col-md-12">
          <div class="alert alert-secondary p-0" role="alert">
              <p class="text-center">Consultas Iniciadas</p>
          </div>
        </div>
        <div class="col-md-12 pr-0 pl-0 mb-1 ">
          <div class="col-md-12">
             <input type="text" class="form-control search input-sm" placeholder="Buscar Consulta">
          </div>
          @if(Auth::user()->usuarioPrincipal() || isset(Session::get('Consultas')['insert']))
          <div class="col-md-12 mt-1">
            <a id="motivo_consulta" class="btn text-white float-right btn-block btn-secondary btn-sm" name="button">Iniciar Nueva Consulta</a>
          </div>
          @endif
        </div>
        @if(Auth::user()->usuarioPrincipal() || isset(Session::get('Consultas')['select']))
          @if($consultas)
            <div class="card col-md-12 mb-1">
              <div class="card-content collapse show" style="">
                <div class="list">
                  @foreach ($consultas as $value)
                      @if ($value->finalizado != null)
                          <a href="{!! route('consulta_finalizada',['id'=> encrypt($value->id)]) !!}" class="media cards_item col-md-12 pr-0 pl-0 mb-1 border-bottom-1 border-bottom-light">
                      @else
                          <a href="{!! route('consulta_view',['id'=> encrypt($value->id)]) !!}" class="media cards_item col-md-12 pr-0 pl-0 mb-1 border-bottom-1 border-bottom-light">
                      @endif
                        <div class="media-body mb-1">
                          <h6 class="media-heading secondary name">
                            <small class="text-muted d-block text-capitalize mr-1">
                              @php
                                $medico = \DB::table('users')
                                ->where('id',$value->id_user)
                                ->first();
                              @endphp
                              Dr. {{ $medico->nombre }}
                            </small>
                              {{ $value->motivo }}
                            <small class="text-muted d-block float-right text-capitalize mr-1 estado_consulta"> {{ $value->estado }} </small>
                          </h6>

                          <small class="mb-0 mr-1 float-right fecha_consulta">
                            {{ \Carbon::parse($value->created_at)->format('d-m-Y') }}
                          </small>

                          <?php // NOTE: Medicamentos ?>
                          @php
                            $diagnosticos = \DB::table('consultaDiagnostico')
                            ->where('consulta_id',$value->id)
                            ->get();
                          @endphp
                          @if ($diagnosticos)
                            @foreach ($diagnosticos as  $diagnostico)
                              <small class="d-block secondary diagnostico_consulta">{{ $diagnostico->nombre }}</small>
                            @endforeach
                          @endif

                          <?php // NOTE: Dignositico ?>
                          @php
                            $medicamentos = \DB::table('consultaMedicamentos')
                            ->where('consulta_id',$value->id)
                            ->get();
                          @endphp
                          @if ($medicamentos)
                            @foreach ($medicamentos as  $medicamento)
                              <small class="d-block secondary medicamentos_consulta">{{  ucfirst(strtolower($medicamento->nombre))  }}</small>
                            @endforeach
                          @endif


                        </div>

                    </a>
                  @endforeach
                </div>
                <div class="d-flex align-items-center justify-content-center">
                    <div id="anterior"></div>
                    <ul class="pagination justify-content-center m-0"></ul>
                    <div id="siguiente"></div>
                </div>

              </div>
            </div>
          @endif
        @endif

      </div>
    </div>
  </div>

  @if(Auth::user()->usuarioPrincipal() || isset(Session::get('Resultados Laboratorio')['select']))
    @if ($estudios)
        <div class="card" id="tomas">
          <div class="card-header bg-secondary" style="padding: .6rem !important;">
            <h6 class="card-title font-weight-light text-white">Estudios de Laboratorio</h6>
          </div>
          <div class="card-content collapse show" style="">
            <div class="card-body">

                <div class="row">
                    <div class="col-md-12 mb-1">
                        <input type="text" class="form-control search input-sm" placeholder="Buscar Consulta">
                    </div>
                    <div class="col-md-12 list">
                        @forelse ($estudios as $estudio)

                        <div class="twitter-feed">
                            <div class="d-flex justify-content-start align-items-center ">
                            <div class="avatar mr-50">
                                <img src="https://asesores.ac-labs.com.mx/agenda/assets/img/covid.jpeg" alt="avtar img holder" height="35" width="35">
                            </div>
                            <div class="user-page-info">
                                <p class="text-bold-600 mb-0">
                                Asesores Diagnóstico Clínico
                                </p>
                                <small class="d-block fecha">{{ \Carbon::parse($estudio->created_at)->format('d - m - Y') }}</small>
                                <small class="d-block toma">Codigo de toma: {{ $estudio->folio }}</small>
                            </div>
                            </div>
                            <div class="col-md-12 text-center">
                                <a data-id="{{$estudio->id}}" class="btn btn-primary btn-sm showEstudio text-white mt-1">
                                    Ver Resultados
                                </a>
                            </div>
                            <hr>
                        </div>
                        @empty
                            <div class="alert alert-secondary">
                                <p>No Hay Estudios Realizados</p>
                            </div>
                        @endforelse
                    </div>

                    <div class="col-md-12 mt-1">
                        <div class="d-flex align-items-center justify-content-center">
                            <div id="anterior"></div>
                            <ul class="pagination justify-content-center m-0"></ul>
                            <div id="siguiente"></div>
                        </div>
                    </div>

                </div>

            </div>
          </div>
        </div>
    @endif
  @endif

</section>

{{-- Mostrar todos lo resultados de la toma  --}}
<div class="modal fade text-left" id="estudiosShow" tabindex="-1" role="dialog" aria-labelledby="myModalLabel160" aria-hidden="true" style="display: none;">
  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
        <div class="col-md-12 text-center loadEstudios" style="display:none">
            <div class="spinner-border" style="width: 3rem; height: 3rem;" role="status">
                <span class="sr-only">Loading...</span>
            </div>
        </div>

        <div class="row contEstudios">

        </div>
      </div>
    </div>
  </div>
</div>
