<div class="media mb-2">
    <a class="mr-2 my-25" href="#">
        <img id="img_logo_empresa" src=" {!! asset('storage/app/empresas/'.\Session::get('empresa')->logo)  !!} " alt="users avatar" class="users-avatar-shadow rounded" height="90" width="90">
        <span id="status_img_logo_empresa"></span>
    </a>
    <div class="media-body mt-50">
        <div class="col-12 d-flex mt-1 px-0">
            <a href="#" id="btn-imagen-destacada" class="btn btn-primary btn-sm d-none d-sm-block mr-75 waves-effect waves-light">Cambiar Logo</a>
            <input id="input_imagen_logo_empresa" class="inputImage d-none" accept="image/*" name="file" type="file"/ >
        </div>
    </div>
</div>
<form id="form_datos" method="post">
    <div class="row">
        <div class="col-12 col-sm-6">
            <div class="form-group validate">
                <div class="controls">
                    <label>Nombre de la empresa</label>
                    @if ($empresa)
                    <input type="text" class="form-control" value="{{ $empresa->nombre }}" name="nombre_empresa" required>
                    @else
                    <input type="text" class="form-control" value="" name="nombre_empresa" required>
                    @endif
                    <div class="help-block"></div>
                </div>
            </div>
            <div class="form-group validate">
                <div class="controls">
                    <label>Giro</label>
                    @if ($empresa)
                        <select class="form-control" required name="giro" id="giro">
                            @foreach ($giros as $giro)
                                <option {{ $giro->id == $empresa->giro_id ? "selected" : "" }} value="{{ $giro->id }}">{!! $giro->nombre !!}</option>                                        
                            @endforeach
                        </select>
                    @else
                        <select class="form-control" required name="giro">
                            @foreach ($giros as $giro)
                                <option value="{{ $giro->id }}">{!! $giro->nombre !!}</option>                                        
                            @endforeach
                        </select>
                    @endif
                    <div class="help-block"></div>
                </div>
            </div>
            <div class="form-group validate">
                <div class="controls">
                    <label>Página web</label>
                    @if ($empresa)
                    <input type="text" class="form-control" value="{{ $empresa->pagina }}" name="pagina" required>
                    @else
                    <input type="text" class="form-control" value="" name="pagina" required>
                    @endif
                    <div class="help-block"></div>
                </div>
            </div>
        </div>
        <div class="col-12 col-sm-6">
            <div class="form-group validate">
                <div class="controls">
                    <label>Dirección</label>
                    @if ($empresa)
                    <input type="text" class="form-control" value="{{ $empresa->direccion }}" name="direccion_empresa" required>
                    @else
                    <input type="text" class="form-control" value="" name="direccion_empresa" required>
                    @endif
                    <div class="help-block"></div>
                </div>
            </div>
            <div class="form-group validate">
                <div class="controls">
                    <label>Teléfono</label>
                    @if ($empresa)
                    <input type="text" class="form-control" value="{{ $empresa->telefono }}" name="telefono" required>
                    @else
                    <input type="text" class="form-control" value="" name="telefono" required>
                    @endif
                    <div class="help-block"></div>
                </div>
            </div>
            <div class="form-group validate">
                <br />
                <div class="custom-control custom-switch custom-control-inline">
                <input type="checkbox" class="custom-control-input" value="1" id="terminos_condiciones" name="aceptado" required
                @if ($empresa && $empresa->check_aceptado  == 1)
                    checked
                @endif
                >
                <label class="custom-control-label" for="terminos_condiciones">
                </label>
                <span class="switch-label">Certifico el uso de privacidad.</span>
                </div>
            </div>
        </div>
        <div class="col-12 d-flex flex-sm-row flex-column justify-content-end mt-1">
            <button type="submit" class="btn btn-primary submit btn-sm">
                <span class="spinner-border spinner spinner-border-sm" role="status" aria-hidden="true"></span>
                Guardar cambios
            </button>
        </div>
    </div>
</form>