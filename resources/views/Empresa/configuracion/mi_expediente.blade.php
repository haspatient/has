<form id="form_update_expediente" action="{{ url('update_expediente_medico') }}" method="post" enctype="multipart/form-data">
    <div class="row">
        <div class="col-12">
            <h4 class="secondary">Actualizar mi expediente</h4>
        </div>
        <div class="col-12">
            <hr>
        </div>
        <div class="col-12 col-sm-6">
            <div class="form-group validate">
                <fieldset class="form-group">
                    <label for="file_cedula_main">Sube tu cédula profesional</label>
                    <div class="custom-file">
                        <input type="file" class="custom-file-input"
                        @if($mi_expediente == null||$mi_expediente == "")
                            required
                        @endif
                        id="file_cedula_main" name="file_cedula_main">
                        <label class="custom-file-label" for="file_cedula_main">Elija el archivo</label>
                    </div>
                    @if($mi_expediente != null)
                        @if($mi_expediente->archivos_medico()->where('check_principal','si')->first() != null)
                        <div class="alert alert-primary mb-2" role="alert">
                            <strong>Archivo actual:</strong> <a download href="{!! asset('storage/app/medicos/'.$mi_expediente->archivos_medico()->where('check_principal','si')->first()->archivo) !!}">
                            <span>{!! $mi_expediente->archivos_medico()->where('check_principal','si')->first()->nombre !!} <i class="feather icon-download"></i></span>
                            </a>
                        </div>
                        @endif
                    @endif
                </fieldset>
            </div>
        </div>
        <div class="col-12 col-sm-6">
            <div class="form-group validate">
                <fieldset class="form-group">
                    <label for="file_cv">Sube tu CV</label>
                    <div class="custom-file">
                        <input type="file" class="custom-file-input"
                        @if($mi_expediente == null||$mi_expediente == "")
                            required
                        @endif
                        id="file_cv" name="file_cv">
                        <label class="custom-file-label" for="file_cv">Elija el archivo</label>
                    </div>
                    @if($mi_expediente != null)
                        @if($mi_expediente->curriculum != null&&$mi_expediente->curriculum != "")    
                        <div class="alert alert-primary mb-2" role="alert">
                            <strong>Archivo actual:</strong> <a download href="{!! asset('storage/app/medicos/'.$mi_expediente->curriculum) !!}">
                            <span>CV <i class="feather icon-download"></i></span>
                            </a>
                        </div>
                        @endif
                    @endif
                </fieldset>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12 col-sm-6">
            <div class="form-group validate">
                <p>¿Cuentas con otro documento que añadir? <b>Agregalo aquí</b></p>

                <label for="tipo_archivo">1.- Selecciona que tipo de documento vas a subir</label>
                <select class="form-control" id="tipo_archivo">
                    <option selected disabled value="-1">Selecciona el tipo de documento</option>
                    <option value="cedula">Cédula</option>
                    <option value="diplomado">Diplomado</option>
                    <option value="certificacion">Certificación</option>
                </select>
                <span class="invalid-feedback" id="error_tipo_archivo" role="alert"></span>
                <br />
                <label for="nombre_archivo">2.- Escribe el nombre que representa tu documento</label>
                <input id="nombre_archivo" type="text" class="form-control" placeholder="Ej: Especialidad en Neurología">
                <span class="invalid-feedback" id="error_nombre_archivo" role="alert"></span>
                <br />
                <button type="button" id="btnAddFile" class="btn btn-sm btn-outline-primary btn-md waves-effect waves-light" tabindex="0">
                    <span><i class="feather icon-upload"></i> Agregar documento</span>
                </button>
            </div>
        </div>
        <div class="col-12 col-sm-6">
            <div class="form-group validate" id="containerNewFiles">
                @if($mi_expediente != null)
                @foreach ($mi_expediente->archivos_medico()->where('check_principal','no')->get() as $archivo_medico)
                <input type="hidden" name="archivos_existentes[]" value="{{ $archivo_medico->id }}">
                <div id="div_archivo_custom_{{ $archivo_medico->id }}">
                    <fieldset class="form-group">
                        <div class="clearfix mb-1">
                            <label class="nombre_doc" for="file_archivo_custom_{!! $archivo_medico->id !!}">Actualiza tu documento: {{ $archivo_medico->tipo_archivo == "cedula" ? "Cédula" : ucfirst($archivo_medico->tipo_archivo)  }}/{!! $archivo_medico->nombre !!}</label>
                            <button data-id="{{ $archivo_medico->id }}" data-tipo="{!! $archivo_medico->tipo_archivo !!}" data-nombre="{!! $archivo_medico->nombre !!}" class="btn btn-outline-adn btn-sm  waves-effect waves-light edit_archivo_custom">
                                <i class="feather icon-edit-1"></i>
                                Editar
                            </button>
                        </div>
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" id="archivo_custom_{!! $archivo_medico->id !!}" name="archivo_custom_{!! $archivo_medico->id !!}">
                            <label class="custom-file-label" for="file_archivo_custom_{!! $archivo_medico->id !!}">Elija el archivo</label>
                        </div>
                        <div class="alert alert-primary mb-2" role="alert">
                            <div class="clearfix">
                                <div class="float-left">
                                    <strong>Archivo actual:</strong> 
                                    <a download href="{!! asset('storage/app/medicos/'.$archivo_medico->archivo) !!}">
                                        <span> {{ $archivo_medico->tipo_archivo == "cedula" ? "Cédula" : ucfirst($archivo_medico->tipo_archivo)  }}/{!! $archivo_medico->nombre !!} <i class="feather icon-download"></i></span>
                                    </a>
                                </div>
                                <button data-id="{{ $archivo_medico->id }}" data-nombre="{{ $archivo_medico->tipo_archivo == "cedula" ? "Cédula" : ucfirst($archivo_medico->tipo_archivo)  }}/{!! $archivo_medico->nombre !!}" class="delete_archivo_custom float-right btn btn-icon btn-icon rounded-circle btn-outline-adn btn-sm waves-effect waves-light" type="button">
                                    <i class="feather icon-trash"></i>
                                </button>
                            </div>
                        </div>
                    </fieldset>
                </div>
                @endforeach
                @endif
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12 d-flex flex-sm-row flex-column justify-content-end mt-1">
            <button type="submit" id="btnUpdateExpediente" class="btn btn-primary submit btn-sm">
                <span class="spinner-border spinner spinner-border-sm" role="status" aria-hidden="true"></span>
                Guardar cambios
            </button>
        </div>
    </div>
</form>

{{-- Modal de confirmación de eliminación de archivo --}}
<div id="confirmEliminacionArchivoModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h4 class="modal-title float-left">Eliminación de archivo</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <form method="post" id="form_delete_archivo_medico">
                    <div class="col-12">
                        <div class="alert alert-primary">
                            <h2 class="alert-heading">
                                Estas a punto de eliminar el archivo:
                                <strong id="form_delete_archivo_medico_texto"></strong>
                            </h2>
                            <p>Al eliminar este archivo, se eliminara de manera definitiva</p>
                        </div>
                    </div>
                    <input type="hidden" name="archivo_medico_id" id="input_delete_archivo_medico_id">
                    <div class="button-guardar float-right">
                        <button type="submit" class="btn btn-sm btn-primary" id="btn_delete_archivo_medico_modal">
                            <span class="spinner-border spinner spinner-border-sm" role="status" aria-hidden="true"></span>
                            Eliminar
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

{{-- Modal para la edición de un archivo ya creado, se modifica el nombre y el tipo --}}
<div id="editArchivoCustomModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h4 class="modal-title float-left">Edición de archivo</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <form method="post" id="form_edit_archivo">
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label for="edit_name_file">Nombre del documento</label>
                            <input type="text" name="edit_name_file"  class="form-control" id="edit_name_file" required>
                        </div>
                        <div class="form-group col-md-12">
                            <label for="edit_type_file">Tipo de documento</label>
                            <select class="form-control" id="edit_type_file" name="edit_type_file">
                                <option value="cedula">Cédula</option>
                                <option value="diplomado">Diplomado</option>
                                <option value="certificacion">Certificación</option>
                            </select>
                        </div>
                        </span>
                    </div>
                    <input type="hidden" name="edit_id_file" id="edit_id_file">
                    <div class="button-guardar float-right">
                        <button type="submit" class="btn btn-sm btn-primary" id="btn_edit_archivoCustom_modal">
                            <span class="spinner-border spinner spinner-border-sm" role="status" aria-hidden="true"></span>
                            Guardar
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>