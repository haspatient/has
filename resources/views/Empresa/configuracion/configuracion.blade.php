@extends('layouts.Vuexy')
@section('begin_vendor_css')
<link rel="stylesheet" href="{!! asset('public/vuexy/app-assets/vendors/css/animate/animate.css') !!}">
<link rel="stylesheet" href="{!! asset('public/vuexy/app-assets/vendors/css/extensions/sweetalert2.min.css') !!}">
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
@endsection
@section('page_css')
<link rel="stylesheet" type="text/css" href="{!! asset('public/vuexy/app-assets/css/pages/app-user.min.css') !!}">
<link href="https://cdn.quilljs.com/1.3.6/quill.snow.css" rel="stylesheet">
@endsection
@section('title')
Configuración
@endsection
@section('css_custom')
<link rel="stylesheet" href="{!! asset('public/css/empresa/info_paciente.css') !!}">
@endsection
@section('content')
<section class="users-edit">
	<div class="card">
		<div class="card-content">
			<div class="card-body">
				<div class="row">
					<div class="col-md-3 col-sm-12">
						<ul class="nav nav-pills flex-column" role="tablist">
							<li class="nav-item">
								<a class="nav-link d-flex align-items-center" id="account-tab" data-toggle="tab" href="#account" aria-controls="account" role="tab" aria-selected="true">
									<span>
										<i class="feather icon-clipboard"></i>
										Mi receta médica
									</span>
								</a>
							</li>
							@if(Auth::user()->role_id == 2)
							<li class="nav-item">
								<a class="nav-link d-flex align-items-center " id="datos-tab" data-toggle="tab" href="#datos" aria-controls="datos" role="tab" aria-selected="true">
									<span>
										<i class="fa fa-building-o"></i>
										Datos empresa
									</span>
								</a>
							</li>
                            @endif
                            <li class="nav-item">
								<a class="nav-link d-flex align-items-center " id="misdatos-tab" data-toggle="tab" href="#misdatos" aria-controls="misdatos" role="tab" aria-selected="true">
									<span>
										<i class="feather icon-user-check"></i>
										Mis datos
									</span>
								</a>
							</li>

							<li class="nav-item">
								<a class="nav-link d-flex align-items-center " id="miexpediente-tab" data-toggle="tab" href="#miexpediente" aria-controls="miexpediente" role="tab" aria-selected="true">
									<span>
										<i class="feather icon-file"></i>
										Mi expediente
									</span>
								</a>
							</li>

						 @if (Auth::user()->usuarioPrincipal()||Session::has('Historial Clínico'))
                            <li class="nav-item">
								<a class="nav-link d-flex align-items-center " id="historialClinico-tab" data-toggle="tab" href="#historialClinico" aria-controls="historialClinico" role="tab" aria-selected="true">
									<span>
										<i class="fa fa-h-square"></i>
										Personalizar HC
									</span>
								</a>
							</li>
					  	@endif
						</ul>
					</div>
					<div class="col-md-9 col-sm-12">
						<div class="tab-content">
							<div class="tab-pane overflow-hidden" id="account" aria-labelledby="account-tab" role="tabpanel">
								@include('Empresa.configuracion.datos_configuracion')
							</div>
							@if(Auth::user()->role_id == 2)
							<div class="tab-pane overflow-hidden" id="datos" aria-labelledby="datos-tab" role="tabpanel">
								@include('Empresa.configuracion.datos_empresa')
							</div>
              @endif
              <div class="tab-pane overflow-hidden" id="misdatos" aria-labelledby="misdatos-tab" role="tabpanel">
								@include('Empresa.configuracion.mis_datos')
							</div>


							<div class="tab-pane overflow-hidden" id="miexpediente" aria-labelledby="miexpediente-tab" role="tabpanel">
								@include('Empresa.configuracion.mi_expediente')
							</div>

							@if (Auth::user()->usuarioPrincipal()||Session::has('Historial Clínico'))
              <div class="tab-pane overflow-hidden" id="historialClinico" aria-labelledby="historialClinico-tab" role="tabpanel">
								@include('Empresa.configuracion.historial_clinico')
							</div>
							@endif

						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection

@section('page_vendor_js')
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
@endsection
@section('page_js')
<script src="https://cdn.quilljs.com/1.3.6/quill.js"></script>
<script src=" {!! asset('public/vuexy/app-assets/js/scripts/modal/components-modal.min.js') !!} "></script>
<script src="{!! asset('public/vuexy/app-assets/js/scripts/extensions/sweet-alerts.min.js') !!}"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
@endsection
@section('js_custom')
<script src="{!! asset('public/js/empresa/configuracion/config.js') !!}"></script>
@if(Auth::user()->role_id == 2)
<script src="{!! asset('public/js/empresa/configuracion/empresas.js') !!}"></script>
@endif
<script src="{!! asset('public/js/empresa/configuracion/misdatos.js') !!}"></script>
<script src="{!! asset('public/js/empresa/configuracion/historial_clinico.js') !!}"></script>
<script src="{!! asset('public/js/empresa/configuracion/miexpediente.js') !!}"></script>
@endsection
