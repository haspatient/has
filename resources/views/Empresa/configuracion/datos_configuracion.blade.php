<!-- users edit media object start -->
{{-- <div class="media mb-2">
    <a class="mr-2 my-25" href="#">
        <img id="logo_receta_medica" src=" {!! asset('storage/app/empresas/'.\Session::get('empresa')->logo)  !!} " alt="users avatar" class="users-avatar-shadow rounded" height="90" width="90">
    </a>
    <div class="media-body mt-50">
        <h4 id="nombre_receta_medica" class="media-heading">{{  \Session::get('empresa')->nombre }}</h4>
        <div class="col-12 d-flex mt-1 px-0">
        </div>
    </div>
</div> --}}
    <!-- users edit media object ends -->
    <!-- users edit account form start -->
<form id="config" method="post">
    <div class="row">
        <div class="col-12">
            <h4 class="float-left secondary">Mi receta médica</h4>
        </div>
        <div class="col-12">
            <hr>
        </div>
        <div class="col-12 col-sm-6">
            <div class="form-group validate">
                <div class="controls">
                    <label>Cédula Profesional</label>
                    @if ($config)
                    <input type="text" class="form-control" value="{{ $config->cedula }}" name="cedula" required>
                    @else
                    <input type="text" class="form-control" value="" name="cedula" required>
                    @endif
                    <div class="help-block"></div>
                </div>
            </div>
            <div class="form-group">
                <div class="controls">
                    <label>Especialidad</label>
                    {{-- @if ($config)
                        <input type="text" class="form-control" value="{{ $config->especialidad }}" required name="especialidad">
                        @else
                        <input type="text" class="form-control" required name="especialidad">
                    @endif --}}
                    @if ($config)
                    <select class="select2 input-sm especilidad_select" name="especialidad" id="especilidad_select" style="width:100%" placeholder="Especialidad">
                        @foreach ($especialidades as $item)
                            <option @if($item->id == $config->especialidad_id) selected @endif value="{{ $item->id }}">{{ $item->nombre }}</option>
                        @endforeach
                    </select>
                    @else
                    <select class="select2 input-sm especilidad_select" name="especialidad" id="especilidad_select" style="width:100%" placeholder="Especialidad">
                        @foreach ($especialidades as $item)
                            <option value="{{ $item->id }}">{{ $item->nombre }}</option>
                        @endforeach
                    </select>
                    @endif
                    <div class="help-block"></div>
                </div>
            </div>

        </div>
        <div class="col-12 col-sm-6">
            <div class="form-group validate">
                <label>Plantilla</label>
                <select class="form-control" required name="platilla">
                    <option value="default">Default</option>
                </select>
            </div>
            <div class="form-group">
                <div class="controls">
                    <label>Institución que otorgó la cédula</label>
                    @if ($config)
                        <input type="text" class="form-control" value="{{ $config->institucion }}" required name="institucion">
                        @else
                        <input type="text" class="form-control"  required name="institucion">

                    @endif
                    <div class="help-block"></div>
                </div>
            </div>
            <div class="form-group">
                <div class="custom-control custom-switch custom-control-inline">
                    <input type="checkbox" class="custom-control-input" id="customSwitch1" name="aceptado" required
                    @if ($config && $config->aceptado  == 1)
                        checked
                    @endif
                    >
                    <label class="custom-control-label" for="customSwitch1">
                    </label>
                    <span class="switch-label ml-1">Certifico que la información es correcta y puede ser usada para generar recetas médicas.</span>
              </div>
            </div>
        </div>
        <div class="col-12 d-flex flex-sm-row flex-column justify-content-end mt-1">
            <button type="submit" class="btn btn-primary submit btn-sm">
                <span class="spinner-border spinner spinner-border-sm" role="status" aria-hidden="true"></span>
                Guardar cambios
            </button>
        </div>
    </div>
</form>
<!-- users edit account form ends -->
