<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"> 
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="shortcut icon" href="https://www.laboratorioasesores.com/assets/frontend//img/favicon_asesores.png" type="image/png">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link href="../resources/sass/fontawesome/css/all.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="../resources/sass/css/stylesExpediente2.css">
    <title>Expediente personal</title>
</head>

<body>
    <div class="content">
        <div class="uno">
            <h2>Expediente personal</h2>
            <div class="imag">
                <h1><span class="iconocolor"><i class="far fa-user"></i></i></span></h1>
            </div>
            
            <div class="formularioo">
                <div class="input-group">
                    <input type="text" class="form-control inpExpe" id="" name="" placeholder="Nombre">
                </div>
            
                <div class="input-group">
                    <input type="text" class="form-control inpExpe" id="" name="" placeholder="Edad">
                </div>  
            
                <div class="input-group">
                    <input type="text" class="form-control inpExpe" id="" name="" placeholder="Género">
                </div>

                <div class="input-group">
                    <input type="text" class="form-control inpExpe" id="" name="" placeholder="Fecha Nacimiento">
                </div>

                <div class="input-group">
                    <select class="selare" name="Area" >
                        <option selected>Área</option>
                        <option>Programación</option>
                        <option>Contabilidad</option>
                    </select>
                </div>
            </div>

            <a class="regresar" href="http://asesoresconsultoreslabs.com/expedienteclinico/Clinica/public/Administrador">Regresar</a>
        </div>

        <div class="dos">
            <div class="navs">
                <ul class="nav nav-pills">
                    <li class="nav-item li1">
                        <a class="nav-link " href="#">Rayos X</a>
                    </li>

                    <li class="nav-item li2">
                        <a class="nav-link" href="#">Fisioterapia</a>
                    </li>

                    <li class="nav-item li3">
                        <a class="nav-link" href="#">Certificados</a>
                    </li>
                </ul>
            </div>

            <div class="resultados textt">
                <h3 > Resultados</h3>
                <h4 > Lorem ipsum</h4>
                <br>
                <p class="stylp">Lorem ipsum, dolor sit amet consectetur adipisicing elit. Hic, omnis dolores explicabo, accusamus perspiciatis sed molestiae labore excepturi vero doloribus dolore est blanditiis earum tempore voluptas eligendi culpa! Velit, officiis!</p>
            </div>

            <div class=icn>
                <div class="icn1 ico">
                    <div class="icnsup">
                        <a href=""><i class="far fa-copy"></i></a>
                    </div>
                    <div class= subicn>
                        <a href=""><i class="fas fa-eye"></i></a>
                        <a href=""><i class="fas fa-download"></i></a>
                    </div>
                </div>

                <div class="icn2 ico">
                    <div class="icnsup">
                        <a href=""><i class="far fa-copy"></i></a>
                    </div>
                    <div class= subicn>
                        <a href=""><i class="fas fa-eye"></i></a>
                        <a href=""><i class="fas fa-download"></i></a>
                    </div>
                </div>

                <div class="icn3 ico">
                    <div class="icnsup">
                        <a href=""><i class="far fa-copy"></i></a>
                    </div>
                    <div class= subicn>
                        <a href=""><i class="fas fa-eye"></i></a>
                        <a href=""><i class="fas fa-download"></i></a>
                    </div>
                </div>
            </div>
            
            <div>
                <h4 class=comto> Comentarios</h4>
                <p class="stylp textt">Lorem ipsum dolor sit amet consectetur adipisicing elit. Deserunt possimus, animi vero ad vel id voluptatum delectus labore, aperiam ducimus nam? Illum iusto rem laboriosam cum dignissimos temporibus nisi nostrum!</p>
            </div>
        </div>
    </div>
</body>

<script src="../resources/js/jquery.min.js"></script>
<script src="../resources/js/bootstrap.min.js"></script>
<script src="../resources/js/bootbox.min.js"></script>

</html>