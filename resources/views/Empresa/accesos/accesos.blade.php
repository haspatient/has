
@extends('layouts.Vuexy')
@section('title','Accesos')
@section('begin_vendor_css')
  <link rel="stylesheet" type="text/css" href=" {!! asset('public/vuexy/app-assets/vendors/css/tables/datatable/datatables.min.css') !!}">
<link rel="stylesheet" type="text/css" href=" {!! asset('public/vuexy/app-assets/vendors/css/file-uploaders/dropzone.min.css') !!}">

@endsection
@section('page_css')
  <link rel="stylesheet" type="text/css" href=" {!! asset('public/vuexy/app-assets/css/plugins/file-uploaders/dropzone.min.css') !!}">
<link rel="stylesheet" type="text/css" href=" {!! asset('public/vuexy/app-assets/css/pages/data-list-view.min.css') !!}">
@endsection
@section('css_custom')
<link rel="stylesheet" href="{!! asset('public/css/empresa/pacientes.css') !!}">
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
@endsection

@section('content')

<div id="validacion_accesos" 
    data-insert="@if (Auth::user()->usuarioPrincipal() || isset(Session::get('Accesos')['insert'])) true @else false @endif"
    data-delete="@if (Auth::user()->usuarioPrincipal() || isset(Session::get('Accesos')['delete'])) true @else false @endif"
    data-update="@if (Auth::user()->usuarioPrincipal() || isset(Session::get('Accesos')['update'])) true @else false @endif"
></div>
<div id="validacion_modulos" 
    data-insert="@if (Auth::user()->usuarioPrincipal() || isset(Session::get('Roles')['insert'])) true @else false @endif"
    data-delete="@if (Auth::user()->usuarioPrincipal() || isset(Session::get('Roles')['delete'])) true @else false @endif"
    data-update="@if (Auth::user()->usuarioPrincipal() || isset(Session::get('Roles')['update'])) true @else false @endif"
></div>
<div class="row">
  <div class="col-md-6">
    <div class="card">
      <div class="card-header">
        <h4 class="card-title">Roles de Usuario</h4>
        @if (Auth::user()->usuarioPrincipal() || isset(Session::get('Roles')['insert']))
        <button type="button" class="btn btn-sm btn-icon btn-icon rounded-circle btn-primary addRol waves-effect waves-light" data-toggle="popover" data-content="Agregar Roles" data-trigger="hover" data-original-title="Roles" data-placement="left">
            <i class="feather icon-plus"></i>
          </button>
        @endif
      </div>
      <div class="card-content collapse show">
        <div class="card-body">
          <section>
            <!-- DataTable starts -->

            <div class="table-responsive">
              <table class="table data-list-view" id="rolTable">
                <thead>
                  <tr>
                    <th>Nombre</th>
                    <th>Descripción</th>
                    <th>Fecha de Creación</th>
                    <th>Ver</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach ($roles as $rol)
                    <tr>
                      <td>{{$rol->tipo}}</td>
                      <td>{{$rol->descripcion}}</td>
                      <td>{{Carbon::parse($rol->created_at)->format('d-m-Y')}}</td>
                      <td>
                        @if (Auth::user()->usuarioPrincipal() || isset(Session::get('Roles')['select']))
                        <button type="button" data-rol="{{ $rol->id }}" class="btn btn-icon permisos btn-sm btn-primary" name="button">
                          <i class="feather icon-eye"></i>
                        </button>
                        @endif
                        @if (Auth::user()->usuarioPrincipal() || isset(Session::get('Roles')['delete']))
                        <button type="button" data-delete="{{ $rol->id }}" class="btn btn-icon permisosdelete btn-sm btn-secondary" name="button">
                          <i class="feather icon-trash"></i>
                        </button>
                        @endif
                        </td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </section>
        </div>
      </div>
    </div>
  </div>

  <div class="col-md-6">
    <div class="card">
      <div class="card-header">
        <h4 class="card-title">Permisos</h4>
        @if (Auth::user()->usuarioPrincipal() || isset(Session::get('Accesos')['insert']))
        <button type="button" class="btn btn-sm btn-icon btn-icon rounded-circle btn-primary addPermisos waves-effect waves-light" data-toggle="popover" data-content="Agregar Permisos" data-trigger="hover" data-original-title="Permisos" data-placement="left">
          <i class="feather icon-plus"></i>
        </button>
        @endif
      </div>
      <div class="card-content collapse show">
        <div class="card-body">
          <section>
            <!-- DataTable starts -->

            <div class="table-responsive">
              <table class="table data-list-view" id="permisosTable">
                <thead>
                  <tr>
                    <th>Rol</th>
                    <th>Modulo</th>
                    <th>Tipo de operación</th>
                    <th>Operación</th>
                  </tr>
                </thead>
                <tbody>

                </tbody>
              </table>
            </div>
          </section>
        </div>
      </div>
    </div>
  </div>
</div>


<?php // NOTE: Modal ?>
<div class="modal fade text-left" id="addpermisos"  style="display: none;">
  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header bg-primary white">
        <h5 class="modal-title" id="myModalLabel160">Agregar Permisos</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">

      <form  id="formPermisos" method="post">
        <div class="row">
          <div class="col-md-6 form-group">
            <label >Roles</label>
            <select class="select2 rolselect input-sm" style="width:100%" name="role">
              @foreach ($roles as $rol)
                <option value="{{ $rol->id }}">{{ $rol->tipo }}</option>
              @endforeach
            </select>
          </div>
          <div class="col-md-6 form-group">
            <label >Modulos</label>
            <select class="select2 select-modulo input-sm" style="width:100%" name="modulo">
              @foreach ($modulos as $modulo)
                <option value="{{$modulo->id}}">{{ $modulo->nombre }}</option>
              @endforeach
            </select>
          </div>
          <div class="col-md-12 form-group">
            <label >Operación</label>
            <select class="select2 select-multiple input-sm" style="width:100%" name="operacion[]" required multiple>
              @foreach ($cruds as $crud)
                <option value="{{ $crud->id }}">{{ $crud->nombre }}</option>
              @endforeach
            </select>
          </div>
          <div class="col-md-12">
            <button type="submit" class="btn btnAddP btn-secondary btn-sm" name="button">
              <i class="feather icon-save"></i>
              Guardar
            </button>
            <div class="spinner-border loadAddp text-primary" role="status" style="display:none">
              <span class="sr-only">Loading...</span>
            </div>
          </div>
        </div>

      </form>

      </div>
    </div>
  </div>
</div>
<div class="modal fade text-left" id="addroles"  style="display: none;">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header bg-primary white">
        <h5 class="modal-title" id="myModalLabel160">Agregar Rol</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">

      <form  id="formRol" method="post">
        <div class="row">
          <div class="col-md-6 form-group">
            <label >Roles</label>
            <input class="form-control" type="text" name="rol" required>
          </div>
          <div class="col-md-6 form-group">
            <label >Descripción</label>
            <input class="form-control" type="text" name="description" required>
          </div>
          <div class="col-md-12">
            <button type="submit" class="btn btnAddR btn-secondary btn-sm" name="button">
              <i class="feather icon-save"></i>
              Guardar
            </button>
            <div class="spinner-border loadAddR text-primary" role="status" style="display:none">
              <span class="sr-only">Loading...</span>
            </div>
          </div>
        </div>

      </form>

      </div>
    </div>
  </div>
</div>


@endsection

@section('page_vendor_js')
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src=" {!! asset('public/vuexy/app-assets/vendors/js/extensions/dropzone.min.js') !!}   "></script>
<script src=" {!! asset('public/vuexy/app-assets/vendors/js/tables/datatable/datatables.min.js') !!}   "></script>
<script src=" {!! asset('public/vuexy/app-assets/vendors/js/tables/datatable/datatables.buttons.min.js') !!}   "></script>
<script src=" {!! asset('public/vuexy/app-assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js') !!}   "></script>
<script src=" {!! asset('public/vuexy/app-assets/vendors/js/tables/datatable/buttons.bootstrap.min.js') !!}   "></script>
<script src=" {!! asset('public/vuexy/app-assets/vendors/js/tables/datatable/dataTables.select.min.js') !!}   "></script>
<script src=" {!! asset('public/vuexy/app-assets/vendors/js/tables/datatable/datatables.checkboxes.min.js') !!}   "></script>
@endsection
@section('page_js')
<script src="{!! asset('public/vuexy/app-assets/js/scripts/popover/popover.min.js') !!}"></script>
@endsection
@section('js_custom')
  <script src="{!! asset('public/js/empresa/accesos/accesos.js') !!}"></script>
  <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>

@endsection
