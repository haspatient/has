<section class="incapacidad">
	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header">
					<div class="display-5">Incapacidad</div>
				</div>
				<div class="card-body">
					<div class="row">
                        @if($datos["incapacidad"] != null)
                            <div class="col-12 col-sm-4">
                                Fecha inicial: {!! $datos["incapacidad"]->fechaInicial !!}
                            </div>
                            <div class="col-12 col-sm-4">
                                Fecha final: {!! $datos["incapacidad"]->fechaFinal !!}
                            </div>
                            <div class="col-12 col-sm-4">
                                Estatus: @if($datos["incapacidad"]->estado == 1) Activa @else Inactiva @endif
                            </div>
                            <div class="col-12">
                                <hr>
                                <h6>{!! $datos["incapacidad"]->motivo !!}</h6>
                                {!! $datos["incapacidad"]->observacion !!}
                            </div>
                        @endif
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
{{-- "id" => 7
"consulta_id" => 38
"fechaInicial" => "2020-11-07"
"fechaFinal" => "2020-11-09"
"motivo" => "Prueba de incapacidad"
"observacion" => "<p>Esta es una observación de las incapacidades.</p>"
"estado" => 1
"created_at" => "2020-11-07 11:28:03"
"updated_at" => "2020-11-07 11:28:36" --}}
