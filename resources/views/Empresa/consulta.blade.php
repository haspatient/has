@extends('layouts.Vuexy')
@section('begin_vendor_css')
<link rel="stylesheet" type="text/css" href="{!! asset('public/vuexy/app-assets/vendors/css/extensions/toastr.css') !!}   ">
@endsection
@section('page_css')
<link href="https://cdn.quilljs.com/1.3.6/quill.snow.css" rel="stylesheet">
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />

<link rel="stylesheet" type="text/css" href="{!! asset('public/vuexy/app-assets/css/plugins/extensions/toastr.min.css') !!}">

@endsection
@section('title')
  Consulta
@endsection
@section('css_custom')
  <style media="screen">
    .icon_vital{
      font-size: 25px;
      color:#002b46!important;
    }
  </style>
@endsection

@section('content')

<section >
  <nav aria-label="breadcrumb">
    <ol class="breadcrumb breadcrumb-chevron">
      <li class="breadcrumb-item"><a href="{!! route('empleados') !!}">Empleados</a></li>
      <li class="breadcrumb-item"><a href="{{ route('empleados.show', $empleado->CURP)}}">Expediente</a></li>
      <li class="breadcrumb-item active" aria-current="page">Consulta: {{$empleado->nombre}} {{$empleado->apellido_paterno}}</li>
    </ol>
  </nav>
</section>

<?php // NOTE: Es el id de la consulta ?>
<input type="hidden" class="historial_id" id="historial_id" name="historial_id" value="{{ $historial->id }}">

<div class="row">
  <div class="col-md-6">
    <div class="card">
      <div class="card-header p-1  bg-secondary">
        <div class="display-5 text-white" id="campoNombreCompleto">{{ $empleado->nombre }} {{ $empleado->apellido_paterno }} {{ $empleado->apellido_materno }}</div>
      </div>
      <div class="card-body">
        <div class="avatar mr-1 avatar-xl">
          @if ($empleado->imagen)
            <img src="../storage/app/pacientes/{{ $empleado->imagen }}" alt="avatar" width="120px" height="120px"  >
            @else
            <img src="{!! asset('public/img/2665817.jpg') !!}" alt="avtar img holder">
          @endif
        </div>
        <div class="row">
          <div class="col-md-4">
            <p class="display-6">Motivo de la consulta</p>
            <div class="border-primary text-center rounded">{{ $historial->motivo }}</div>
          </div>
          <div class="col-md-4">
            <p class="display-6">Fecha de Inicio</p>
            <div class="border-primary text-center rounded">{{ \Carbon::parse($historial->created_at)->format('d-m-Y')  }}</div>
          </div>
          <div class="col-md-4">
            <p class="display-6">Estado</p>
            <div class="border-primary text-center rounded">{{ $historial->estado }}</div>
          </div>
        </div>

      </div>
    </div>

  </div>
  <div class="col-md-6">
    <div class="card ">
      <div class="card-header p-1 bg-secondary">
        <div class="display-5 text-white" id="campoNombreCompleto">
          Detalles del Paciente
        </div>
      </div>
      <div class="card-body row">
        <div class="col-md-4">
          <h6 class="display-5">Curp</h6>
          <p>{{ $empleado->CURP }}</p>
        </div>
        <div class="col-md-4">
          <h6 class="display-5">Fecha de Nacimiento</h6>
          <p>{{ $empleado->fecha_nacimiento }}</p>
        </div>
        <div class="col-md-4">
          <h6 class="display-5">Genero</h6>
          <p>{{ ucfirst(strtolower($empleado->genero ))}}</p>
        </div>
        <div class="col-md-4">
          <h6 class="display-5">Dirección</h6>
          <p>{{ $empleado->direccion }}</p>
        </div>
        <div class="col-md-4">
          <h6 class="display-5">Email</h6>
          <p>{{$empleado->email}}</p>
        </div>
        <div class="col-md-4">
          <h6 class="display-5">Telefono</h6>
          <p>{{$empleado->telefono}}</p>
        </div>
        @if ($historial->estado == 'proceso')
          <div class="col-md-2">
            <button type="button" class="btn btn-secondary btn-sm waves-effect waves-light" data-toggle="tooltip" data-placement="top" data-original-title="Terminar Consulta"  id="save_consulta">
                <i class="feather icon-save"></i>
            </button>
          </div>
          <div class="col-md-2">
            <button type="button" class="btn btn-danger btn-sm waves-effect waves-light" data-toggle="tooltip" data-placement="top" data-original-title="Eliminar Consulta"  id="deleteConsulta">
                <i class="feather icon-trash-2"></i>
            </button>
          </div>
        @endif

      </div>
    </div>

  </div>
</div>

<div class="divider divider-secondary">
  <div class="divider-text">Padecimientos</div>
</div>


@include('Empresa.include_consulta.padecimientos')

<div class="divider divider-secondary">
  <div class="divider-text">Examen Físico</div>
</div>

@include('Empresa.include_consulta.exploracion_f')

<div class="divider divider-secondary">
  <div class="divider-text">Diagnostico</div>
</div>

@include('Empresa.include_consulta.diagnostico')
@include('Empresa.include_consulta.incapacidad')

<div class="divider divider-secondary">
  <div class="divider-text">Archivos Adjuntos</div>
</div>

@include('Empresa.include_consulta.archivos')

<div class="divider divider-secondary">
    <div class="divider-text">Archivos médicos</div>
</div>
@include('Empresa.include_consulta.ecg')

@endsection

@section('page_vendor_js')
    <script src="https://cdn.quilljs.com/1.3.6/quill.js"></script>
    <script src="{!! asset('public/vuexy/app-assets/vendors/js/extensions/toastr.min.js') !!} "></script>

@endsection
@section('page_js')
@endsection
@section('js_custom')
  <script src="{!! asset('public/js/empresa/consulta.js') !!}" charset="utf-8"></script>
  <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
  <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
  <script src="{!! asset('public/js/empresa/consulta/exploracion.js') !!}" charset="utf-8"></script>
  <script src="{!! asset('public/js/empresa/consulta/diagnostico.js') !!}" charset="utf-8"></script>
  <script src="{!! asset('public/js/empresa/consulta/guardar.js') !!}" charset="utf-8"></script>
  <script src="{!! asset('public/js/empresa/consulta/archivos.js') !!}" charset="utf-8"></script>
  <script src="{!! asset('public/js/empresa/consulta/incapacidad.js') !!}" charset="utf-8"></script>
  <script src="{!! asset('public/js/empresa/consulta/ecg.js') !!}" charset="utf-8"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/list.js/2.3.0/list.min.js"></script>

@endsection
