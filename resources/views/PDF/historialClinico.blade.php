<!DOCTYPE html>
<html>
  <head>

    <title>Historial Clinico</title>
    <link rel="stylesheet" href="{!! asset('css/bootstrap.min.css') !!}">

  </head>
  <body>
    <div class="content">
      <div class="text-center mb-1">
        <h4 class="font-weight-normal" style="font-size:20px">EXAMEN MÉDICO</h4>
      </div>
      <div id="identificacion">
        <div class="alert alert-secondary text-center" role="alert">
          Identificación
        </div>
        <table class="table table-sm table-bordered">
            <tbody>
              <tr>
                <td>Fecha: {{\Carbon::parse($identificacion->fecha)->format('d/m/Y')}}</td>
                <td>N° de Empleado: {{$identificacion->numEmpleado}}</td>
                <td>Departamento: {{$identificacion->departamento}}</td>
                <td>Edad: {{\Carbon::parse($empleado->fecha_nacimiento)->age}}</td>
                <td>Sexo {{$empleado->genero}}</td>
              </tr>
              <tr>
                <td colspan="2">Apellido Paterno: {{$empleado->apellido_paterno}}</td>
                <td colspan="2">Apellido Materno: {{$empleado->apellido_materno}}</td>
                <td>Nombre: {{$empleado->nombre}}</td>
              </tr>
              <tr>
                <td>Estado Civil: {{$identificacion->estadoCivil}}</td>
                <td>Escolaridad: {{$identificacion->escolaridad}}</td>
                <td colspan="2">Domicilio: {{$identificacion->domicilio}}</td>
                <td> <span class="">Lugar de Nacimiento: {{$identificacion->lugarNacimiento}}</span> </td>
              </tr>
              <tr>
                <td colspan="2">Fecha de Nacimiento: <br> {{$empleado->fecha_nacimiento}}</td>
                <td>Ciudad o ejido: {{$identificacion->ciudad}}</td>
                <td>Municipio: {{$identificacion->municipio}}</td>
                <td>Estado: {{$identificacion->estado}}</td>
              </tr>
              <tr>
                <td colspan="3">En caso de Emergencia Llamar a: {{$identificacion->nom_per_em}}</td>
                <td>Parentesco: {{$identificacion->parentesco}}</td>
                <td>Telefono: {{$identificacion->telefono_1}}</td>
              </tr>
              <tr>
                <td colspan="2">Domicilio: {{$identificacion->domicilio_em}}</td>
                <td colspan="2">Lugar de Trabajo:</td>
                <td>Telefono: {{$identificacion->telefono_2}}</td>
              </tr>
            </tbody>
          </table>
      </div>
      <div id="heredofamiliares">
        <div class="alert alert-secondary text-center" role="alert">
          Heredofamiliares
        </div>
        <table class="table table-sm table-bordered">
            <tbody>
              <tr>
                <th class="text-center">Familiar</th>
                <th class="text-center">Estado</th>
                <th class="text-center">Enfermedades</th>
              </tr>
              @php
                $heredofamilia= json_decode($heredofamiliar->data);
              @endphp
                @foreach ($heredofamilia as $familiar)
                  <tr>
                    <td>{{$familiar->familiar}}</td>
                    <td>{{$familiar->estado}}</td>
                    <td>
                      @if ($familiar->enfermedad != null)
                        @foreach ($familiar->enfermedad as $value)
                              {{$value.', ' }}
                        @endforeach
                        @else
                          Ninguno
                      @endif

                     </td>
                  </tr>
                @endforeach
            </tbody>
          </table>
      </div>
      <div id="nopatologico">
        @php
          $vacunas =$nopatologico->vacunas;
        @endphp
        <div class="alert alert-secondary text-center" role="alert">
          Antecedentes Personales no Patológicos
        </div>
        <p class="text-center">Vacunas Recibidas</p>
        <hr>
        <table class="table table-sm table-bordered">
            <tbody>
              <tr>
                <th class="text-center">Vacuna</th>
                <th class="text-center">Realizado</th>
                <th class="text-center">Comentario</th>
                <th class="text-center">Vacuna</th>
                <th class="text-center">Realizado</th>
                <th class="text-center">Comentario</th>
              </tr>
              <tr>
                    @foreach ($vacunas->tetano as $value)
                      @if ($value=='no')
                        <td>Tetano</td>
                        <td>No</td>
                        @else
                        @if ($value=='Tetano')
                           <td>{{$value}}</td>
                           <td>Si</td>
                       @endif
                       @if ($value!='no' && $value!='Tetano' && $value != null)
                         <td>{{$value}}</td>
                       @endif
                       @if ($value==null)
                         <td>Ninguno</td>
                       @endif
                     @endif
                    @endforeach

                    @foreach ($vacunas->rubeola as $value)
                      @if ($value=='no')
                        <td>Rubeola</td>
                        <td>No</td>
                        @else
                        @if ($value=='Rubeola')
                           <td>{{$value}}</td>
                           <td>Si</td>
                       @endif
                       @if ($value!='no' && $value!='Rubeola' && $value != null)
                         <td>{{$value}}</td>
                       @endif
                       @if ($value==null)
                         <td>Ninguno</td>
                       @endif
                     @endif
                    @endforeach
                  </tr>
                  <tr>
                    @foreach ($vacunas->hepatitis as $value)
                      @if ($value=='no')
                        <td>Hepatitis</td>
                        <td>No</td>
                        @else
                        @if ($value=='Hepatitis')
                           <td>{{$value}}</td>
                           <td>Si</td>
                       @endif
                       @if ($value!='no' && $value!='Hepatitis' && $value != null)
                         <td>{{$value}}</td>
                       @endif
                       @if ($value==null)
                         <td>Ninguno</td>
                       @endif
                     @endif
                    @endforeach
                    @foreach ($vacunas->bcg as $value)
                      @if ($value=='no')
                        <td>(BCG)</td>
                        <td>No</td>
                        @else
                        @if ($value=='(BCG)')
                           <td>{{$value}}</td>
                           <td>Si</td>
                       @endif
                       @if ($value!='no' && $value!='(BCG)' && $value != null)
                         <td>{{$value}}</td>
                       @endif
                       @if ($value==null)
                         <td>Ninguno</td>
                       @endif
                     @endif
                    @endforeach
                 </tr>
                 <tr>
                   @foreach ($vacunas->influenza as $value)
                     @if ($value=='no')
                       <td>Influenza</td>
                       <td>No</td>
                       @else
                       @if ($value=='Influenza')
                          <td>{{$value}}</td>
                          <td>Si</td>
                      @endif
                      @if ($value!='no' && $value!='Influenza' && $value != null)
                        <td>{{$value}}</td>
                      @endif
                      @if ($value==null)
                        <td>Ninguno</td>
                      @endif
                    @endif
                   @endforeach
                   @foreach ($vacunas->neumococica as $value)
                     @if ($value=='no')
                       <td>Neumococica</td>
                       <td>No</td>
                       @else
                       @if ($value=='Neumococica')
                          <td>{{$value}}</td>
                          <td>Si</td>
                      @endif
                      @if ($value!='no' && $value!='Neumococica' && $value != null)
                        <td>{{$value}}</td>
                      @endif
                      @if ($value==null)
                        <td>Ninguno</td>
                      @endif
                    @endif
                   @endforeach
                </tr>
                <tr>
                  <td colspan="2">Otras Vacunas:</td>
                  <td colspan="4">{{$vacunas->otros}}</td>
                </tr>
              <tr>
                <td>Grupo Sanguinio: <br> {{$nopatologico->g_sanguineo}}</td>
                <td colspan="2">¿Alguna vez ha recibido usted alguna transfusión sanguinea? {{$nopatologico->transfusion}}</td>
                <td colspan="3">Mencione los medicamentos que haya tomado en las últimas dos semanas: <br>{{$nopatologico->medica_ingeridos}}</td>
              </tr>
            </tbody>
        </table>
        <p class="text-center">Uso del Cigarro</p>
        <hr>
        @php
          $cigarro= $nopatologico->cigarro;
          $alcohol= $nopatologico->alcohol;
          $drogas= $nopatologico->drogas;
          $du= $nopatologico->drogas_usadas;
          $deporte= $nopatologico->deporte;
          $dieta=$nopatologico->dieta;

        @endphp
        <table class="table table-sm table-bordered">
            <tbody>
              <tr>
                <td>¿Fuma usted? {{$cigarro->Cigarro}}</td>
                <td>¿Desde que edad?<br> {{$cigarro->edad}}</td>
                <td>¿Numero promedio de cigarros que fuma?<br> {{$cigarro->frecuencia}}</td>
                <td>¿A que edad dejo de fumar? <br> {{$cigarro->noConsumir}}</td>
              </tr>
            </tbody>
        </table>
        <p class="text-center">Uso del Alcohol</p>
        <hr>
        <table class="table table-sm table-bordered">
            <tbody>
              <tr>
                <td>¿Toma bebidas alcoholicas? {{$alcohol->Alcohol}}</td>
                <td>¿Desde que edad? <br> {{$alcohol->edad}}</td>
                <td colspan="2">¿Con que frecuencia y cantidad? <br> {{$alcohol->frecuencia}}</td>
              </tr>
            </tbody>
        </table>
        <p class="text-center">Uso del Drogas</p>
        <hr>
        <table class="table table-sm table-bordered">
            <tbody>
              <tr>
                <td>¿Alguna vez usó drogas ? {{$drogas->Drogas}}</td>
                <td>¿Desde que edad? {{$drogas->edad}}</td>
                <td colspan="2">¿Con que frecuencia y cantidad? {{$drogas->frecuencia}}</td>
              </tr>
              <tr>
                <td colspan="2">Mencione que drogas ha usado:
                @if ($du != null)
                  @foreach ($du as $value)
                    {{$value.', '}}
                  @endforeach
                  @else
                    Ninguna
                @endif
                </td>

                <td>
                  ¿Se le ha indicado alguna dieta? {{$dieta->Dieta.' '.$dieta->descripcion}}
                </td>
                <td>
                  ¿Practica algún deporte? {{$deporte->Deporte}}
                </td>
              </tr>
              <tr>
                <td colspan="2">Especifique cual y frecuencia: {{$deporte->frecuencia}}</td>
                <td colspan="2">Indique cuál es su pasatiempo favorito: {{$nopatologico->pasatiempo}}</td>
              </tr>
            </tbody>
        </table>

      </div>
      <div id="patologico">
        @php
         $enfermedades = $patologico->enfermPadecidas;
         $cl= $patologico->cirugiasLesion;
        @endphp
        <div class="alert alert-secondary text-center" role="alert">
          Antecedentes Patológicos Personales
        </div>
        <table class="table table-sm table-bordered">
          <tbody>
            <tr class="border-0">
              <td class="border-0"></td>
              <td class="border-0"></td>
              <td class="border-0"></td>
              <td class="border-0"></td>
            </tr>
            <tr>
              <td>Enfermedades Padecidas:</td>
              <td colspan="3">
                @if ($enfermedades == null)
                  Ninguna
                @else
                  @foreach ($enfermedades as $value)
                    {{$value.', '}}
                  @endforeach
                @endif
            </td>
            </tr>
            <tr>
              <td>Transtornos emocionales o psiquiatricos</td>
              <td colspan="3">{{$patologico->transtornos}}</td>
            </tr>
            <tr>
              <td>Alergia de medicamentos: {{$patologico->alergiaMedica}}</td>
              <td>Alergia en la piel o sensibilidad: {{$patologico->alergiaPiel}}</td>
              <td>Otro tipo de alergias: {{$patologico->alergiaOtro}}</td>
              <td>Tumor o cancer: {{$patologico->tumorCancer}}</td>
            </tr>
            <tr>
              <td>Problema de la vista: {{$patologico->probVista}}</td>
              <td>Enfermedad del oido: {{$patologico->enfOido}}</td>
              <td>Problema en la columna vertebral: {{$patologico->probCulumVert}}</td>
              <td>Huesos y articulaciones: {{$patologico->huesoArticulacion}}</td>
            </tr>
            <tr>
              <td>Otros problemas médicos no enlistados:</td>
              <td colspan="3">{{$patologico->otroProbMedico}}</td>
            </tr>
          </tbody>
        </table>
        <p class="text-center">Lesiones o cirugias</p>
        <hr>
        <table class="table table-sm table-bordered">
          <tbody>
            @foreach ($cl as $json)
              <tr>
                @foreach ($json as $key => $value)
                  <td>{{$key}}</td>
                  <td>{{$value}}</td>
                @endforeach
              </tr>
            @endforeach
            <tr>
              <td colspan="1">Otras cirugías incluyendo accidentes automovilisticos o del trabajo:</td>
              <td colspan="3">{{$patologico->otra_cirugia}}</td>
            </tr>
          </tbody>
        </table>
      </div>
      @if ($empleado->genero=='FEMENINO' || $empleado->genero=='Femenino')
        <div id="genicoobstetrico">
          <div class="alert alert-secondary text-center" role="alert">
            Antecedentes Genicoobstetricos
          </div>
          <table class="table table-sm table-bordered">
            <tbody>
              <tr>
                <td>¿A qué edad inició su regla?</td>
                <td>{{$genicoobstetrico->inicioRegla}}</td>
                <td>¿Frecuencia de regla?</td>
                <td>{{$genicoobstetrico->fRegla}}</td>
              </tr>
              <tr>
                <td>¿Cuántos días dura su regla?</td>
                <td>{{$genicoobstetrico->duraRegla}}</td>
                <td>¿Edad en que se inició sus relaciones sexuales?</td>
                <td>{{$genicoobstetrico->relacionSexInicio}}</td>
              </tr>
              <tr>
                <td>¿Cuál método de planificacion ha usado?</td>
                <td>{{$genicoobstetrico->metodoUsado}}</td>
                <td>¿Cuál fue la fecha de su última mestruacion?</td>
                <td>{{$genicoobstetrico->ultimaMestruacion}}</td>
              </tr>
              <tr>
                <td>¿Cúantos embarazos ha tenido?</td>
                <td>{{$genicoobstetrico->numEmbarazos}}</td>
                <td>¿Cuántos partos ha tenido?</td>
                <td>{{$genicoobstetrico->numPartos}}</td>
              </tr>
              <tr>
                <td>¿Cuántas cesáreas ha tenido?</td>
                <td>{{$genicoobstetrico->numCesareas}}</td>
                <td>¿Cúantos abortos ha tenido?</td>
                <td>{{$genicoobstetrico->numAbortos}}</td>
              </tr>
              <tr>
                <td>¿Cuándo se realizó el último exámen del cáncer de matriz?</td>
                <td>{{$genicoobstetrico->fecha_examenCancer}}</td>
                <td>¿Cuándo se realizó el último exámen de mamas?</td>
                <td>{{$genicoobstetrico->fecha_examenMamas}}</td>
              </tr>
              <tr>
                <td>¿A qué edas tuvo su menopausia?</td>
                <td class="border-right">{{$genicoobstetrico->edadMenopausia}}</td>
              </tr>
            </tbody>
          </table>
        </div>
      @endif
      <div id="historialaboral">
        @php
         $hl = $historiaLaboral->trabajoRealizados;
         $tm = $historiaLaboral->trabajoMateriales;
         $empleos=  $historiaLaboral->empleos;
         $equipo = $historiaLaboral->equipoSegActual;
         $vista = $historiaLaboral->examenVista;
         $lentes = $historiaLaboral->lentesContacto;
         $lugares = $historiaLaboral->lugaresVividos;
        @endphp
        <div class="alert alert-secondary text-center" role="alert">
          Historial Laboral
        </div>
        <table class="table table-sm table-bordered">
          <tbody>
            <tr>
              <td>
                Ha realizado uno de los siguientes trabajos:
              </td>
              <td>
                @if ($hl==null)
                  Ninguno
                  @else
                    @foreach ($hl as $value)
                      {{$value.', '}}
                    @endforeach
                @endif
              </td>
              <td>Otros Trabajos Realizados:</td>
              <td>{{$historiaLaboral->otrosTrabajos}}</td>
            </tr>
            <tr>
              <td>¿Se ha expuesto o ha trabajado con alguno de stios materiales?</td>
              <td>
                @if ($tm==null)
                  Ninguno
                  @else
                    @foreach ($tm as $value)
                      {{$value.', '}}
                    @endforeach
                @endif
              </td>
              <td>Otras exposiciones:</td>
              <td>{{$historiaLaboral->otrasExposiciones}}</td
            </tr>
          </tbody>
        </table>
        @foreach ($empleos as $empleo)
          <p class="text-center">
            @if ($empleo->tipo==1)
              Empleado Actual
            @elseif ($empleo->tipo==2)
              Empleado Anterior
            @elseif ($empleo->tipo==3)
              Empleado Anterior
            @endif
          </p>
          <hr>
          <table class="table table-sm table-bordered">
            <tbody>
              <tr>
                <td>Nombre de la empresa: {{$empleo->nombre}}</td>
                <td>Puesto: {{$empleo->puesto}}</td>
                </tr>
                <tr>
                <td>Descripción: de su actividad: {{$empleo->actividad}}</td>
                <td>Duración del empleo: {{$empleo->duracion}}</td>
                </tr>
                <tr>
                <td colspan="2">Equipo de seguridad usado: {{$empleo->seguridad}}</td>
                </tr>
                <tr>
                <td colspan="2">Posibles daños a la salud relacionados con el trabajo: {{$empleo->danos}}</td>
              </tr>
            </tbody>
          </table>
        @endforeach
        <p class="text-center">Equipo de seguridad usado en el puesto actual</p>
        <hr>
        <table class="table table-sm table-bordered">
          <tbody>
            <tr>
              <td class="border-0"></td>
              <td class="border-0"></td>
              <td class="border-0"></td>
              <td class="border-0"></td>
            </tr>
            <tr>
              <td>Equipo:</td>
              <td colspan="3">
                @if ($equipo== null)
                  Ninguno
                  @else
                    @foreach ($equipo as $value)
                      {{$value.', '}}
                    @endforeach
                @endif
              </td>
            </tr>
            <tr>
              <td>¿Usa lentes graduados? {{$vista[0]}}</td>
              <td>Fecha del ultimo exámen: {{$vista[1]}}</td>
              @if (count($lentes)==1)
                <td>¿Usa lentes de contacto? {{$lentes[0]}}</td>
                <td>¿De que tipo?</td>
                @else
                  <td>¿Usa lentes de contacto? {{$lentes[0]}}</td>
                  <td>¿De que tipo? {{$lentes[1]}}</td>
              @endif
            </tr>
            <tr>
              <td colspan="2">Otras exposiciones: {{$historiaLaboral->otras_exposiciones}}</td>
              <td colspan="2">¿Alguien de su familia trabaja con materiales peligrosos?(Asbesto, Plomo, Etc.)  {{$historiaLaboral->famMaterialPeligroso}}</td>
            </tr>
            <tr>
              <td colspan="2">Alguna vez ha vivido cerca de (Fabricas, Basurero, Mina, Otro lugar que genere residuos peligroso):
              @if ($historiaLaboral->lugaresVividos==NULL)
                  Ninguno
                @else
                  @foreach ($historiaLaboral->lugaresVividos as $value)
                    {{$value.', '}}
                  @endforeach

              @endif
              </td>
             <td colspan="2">
               Alguna otra exposición a materiales peligrosos: {{$historiaLaboral->expMaterialPeligroso}}
             </td>
            </tr>
          </tbody>
        </table>
        <p class="text-center">
          Interrogatorio por Aparatos y Sistemas
        </p>
        <hr>
        <table class="table table-sm table-bordered">
            <thead>
              <tr class="text-center">
                <td colspan="4">¿Durante el año pasado tuvo usted alguno de los siguientes síntomas?</td>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td></td>
                <td>Síntomas</td>
                <td></td>
                <td>Síntomas</td>
              </tr>
              <tr>
                <td>Neurológico / Psicológico</td>
                <td>
                  @if ($historiaLaboral->neurologico==NULL)
                    Ninguno
                    @else
                      @foreach ($historiaLaboral->neurologico as $value)
                        {{$value.' '}}
                      @endforeach
                  @endif
                </td>
                <td>Cardiovascular</td>
                <td>
                  @if ($historiaLaboral->cardiovasucular==NULL)
                    Ninguno
                    @else
                      @foreach ($historiaLaboral->cardiovasucular as $value)
                        {{$value.' '}}
                      @endforeach
                  @endif
                </td>
              </tr>
              <tr>
                <td>Gastro Intestinal</td>
                <td>
                  @if ($historiaLaboral->gastroIntestinal==NULL)
                    Ninguno
                    @else
                      @foreach ($historiaLaboral->gastroIntestinal as $value)
                        {{$value.' '}}
                      @endforeach
                  @endif
                </td>
                <td>Pulmunar</td>
                <td>
                  @if ($historiaLaboral->pulmunar==NULL)
                    Ninguno
                    @else
                      @foreach ($historiaLaboral->pulmunar as $value)
                        {{$value.' '}}
                      @endforeach
                  @endif
                </td>
              </tr>
              <tr>
                <td>Genitourinario</td>
                <td>
                  @if ($historiaLaboral->genitourinario==NULL)
                    Ninguno
                    @else
                      @foreach ($historiaLaboral->genitourinario as $value)
                        {{$value.' '}}
                      @endforeach
                  @endif
                </td>
                <td>Endocrino</td>
                <td>
                  @if ($historiaLaboral->endocrino==NULL)
                    Ninguno
                    @else
                      @foreach ($historiaLaboral->endocrino as $value)
                        {{$value.' '}}
                      @endforeach
                  @endif
                </td>
              </tr>
              <tr>
                <td>Musculoesqueletico</td>
                <td>
                  @if ($historiaLaboral->musculoesqueletico==NULL)
                    Ninguno
                    @else
                      @foreach ($historiaLaboral->musculoesqueletico as $value)
                        {{$value.' '}}
                      @endforeach
                  @endif
                </td>
                <td>Inmunológico</td>
                <td>
                  @if ($historiaLaboral->inmunologico==NULL)
                    Ninguno
                    @else
                      @foreach ($historiaLaboral->inmunologico as $value)
                        {{$value.' '}}
                      @endforeach
                  @endif
                </td>
              </tr>
              <tr>
                <td>Dermatológico</td>
                <td>
                  @if ($historiaLaboral->dermatologico==NULL)
                    Ninguno
                    @else
                      @foreach ($historiaLaboral->dermatologico as $value)
                        {{$value.' '}}
                      @endforeach
                  @endif
                </td>
                <td>Hematológico</td>
                <td>
                  @if ($historiaLaboral->hematologico==NULL)
                    Ninguno
                    @else
                      @foreach ($historiaLaboral->hematologico as $value)
                        {{$value.' '}}
                      @endforeach
                  @endif
                </td>
              </tr>
              <tr class="text-center">
                <td colspan="4">Alergias</td>
              </tr>
              <tr>
                <td colspan="2">Reacción alergica a medicamentos:
                  @if ($historiaLaboral->alergiaMedicamentos=='no')
                    No
                    @else
                      Si
                  @endif
                </td>
                <td colspan="2">¿Algún problema de salud en este momento? {{$historiaLaboral->problemaSalud}}</td>
              </tr>
              <tr class="text-center">
                <td colspan="4">Reproductivo</td>
              </tr>
              @if ($empleado->genero=='FEMENINO' || $empleado->genero=='Femenino')
                <tr>
                  <td>Se ha presentado los siguiente:</td>
                  <td colspan="3">
                    @if ($historiaLaboral->problemaMujer==null)
                      Ningun problema presentado
                      @else
                        @foreach ($historiaLaboral->problemaMujer as  $value)
                          {{$value.' '}}
                        @endforeach
                    @endif
                  </td>
                </tr>
                <tr>
                  <td colspan="2">¿Ha tenido abortos?¿En que año? {{$historiaLaboral->abortos}}</td>
                  <td colspan="2">Si esta embarazada cual es la fecha probable de parto: {{$historiaLaboral->fechaParto}}</td>
                </tr>
                @else
                  <tr>
                    <td>Se ha presentado los siguiente:</td>
                    <td>
                      @if ($historiaLaboral->problemaHombre==null)
                        Ningun problema presentado
                        @else
                          @foreach ($historiaLaboral->problemaHombre as  $value)
                            {{$value.' '}}
                          @endforeach
                      @endif
                    </td>
                    <td colspan="2">Otros padecimientos: {{$historiaLaboral->padecimientoHombre}}</td>
                  </tr>
              @endif

            </tbody>
        </table>
      </div>
      <div id="examenFisico">
        <div class="alert alert-secondary text-center" role="alert">
          Examen Fisico
        </div>
        <table class="table table-sm table-bordered">
          <tr>
            <td colspan="2">Apariencia General: {{$examen->apariencia}}</td>
            <td>Temperatura: {{$examen->temperatura}}</td>
            <td>Fc: {{$examen->fc}}</td>
            <td>Fr: {{$examen->fr}}</td>
          </tr>
          <tr>
            <td>Altura: {{$examen->altura}}</td>
            <td>Peso: {{$examen->peso}}</td>
            <td>Imc: {{$examen->imc}}</td>
            <td>Presión Sanguinea (Brazo Derecho):{{$examen->psBrazoDerecho}}</td>
            <td>Brazo Izquierdo: {{$examen->psBrazoDerecho}}</td>
          </tr>
          <tr>
            <td colspan="2"></td>
            <th>Observaciones Detectadas</th>
            <td></td>
            <th>Observaciones Detectadas</th>
          </tr>
          <tr>
            <td colspan="2">Cabeza y cuero / ojos</td>
            <td>{{$examen->cabezaCueroOjos}}</td>
            <td> Oidos / Nariz / Boca</td>
            <td>{{$examen->oidoNarizBoca}}</td>
          </tr>
          <tr>
            <td colspan="2">Cuello</td>
            <td>{{$examen->cuello}}</td>
            <td> Dientes / Fringe</td>
            <td>{{$examen->dientesFaringe}}</td>
          </tr>
          <tr>
            <td colspan="2">Tiroides</td>
            <td>{{$examen->tiroides}}</td>
            <td>Nodo linfatico</td>
            <td>{{$examen->nodoLinfatico}}</td>
          </tr>
          <tr>
            <td colspan="2">Torak y Pulmones</td>
            <td>{{$examen->toraxPulmones}}</td>
            <td>Pecho</td>
            <td>{{$examen->pecho}}</td>
          </tr>
          <tr>
            <td colspan="2">Corazón</td>
            <td>{{$examen->corazon}}</td>
            <td>Abdomen</td>
            <td>{{$examen->abdomen}}</td>
          </tr>
          <tr>
            <td colspan="2">Rectal Digital</td>
            <td>{{$examen->rectalDigital}}</td>
            <td>Genitales</td>
            <td>{{$examen->genitales}}</td>
          </tr>
          <tr>
            <td colspan="2">Columna Vertebral</td>
            <td>{{$examen->columnaVertebral}}</td>
            <td>Piel</td>
            <td>{{$examen->piel}}</td>
          </tr>

          <tr>
            <td colspan="2">Extremidades</td>
            <td>{{$examen->extremidades}}</td>
            <td>Musculoesqueleto</td>
            <td>{{$examen->musculoesqueleto}}</td>
          </tr>
          <tr>
            <td colspan="2">Reflejos y Neurológicos</td>
            <td>{{$examen->reflejosNeurologicos}}</td>
            <td>Estado Mental</td>
            <td>{{$examen->estadoMental}}</td>
          </tr>
          <tr>
            <td colspan="2">Pulso Arterial</td>
            <td>{{$examen->pulsoArterial}}</td>
            <td>Otros Comentarios</td>
            <td>{{$examen->otrosComentarios}}</td>
          </tr>
           <tr class="text-center">
            <td colspan="5">Agudeza Visual</td>
          </tr>
          <tr>
            <td colspan="2">Ojo Izquierdo</td>
            <td>{{$examen->ojoIzquierdo}}</td>
            <td>Ojo Derecho</td>
            <td>{{$examen->ojoDerecho}}</td>
          </tr>
          <tr class="text-center">
            <td colspan="5">Valoración y plan de recomendaciones del personal médico</td>
          </tr>
          <tr>
            <td colspan="2">Uso de Epp:</td>
            <td>{{$examen->uso_epp}}</td>
            <td>Audiometria</td>
            <td>{{$examen->audiometria}}</td>
          </tr>
          <tr>
            <td colspan="2">Respiradores</td>
            <td>{{$examen->respiradores}}</td>
            <td>Reentrenar en:</td>
            <td>{{$examen->reentrenar}}</td>
          </tr>
          <tr>
            <td colspan="2">Otros:</td>
            <td colspan="3">{{$examen->otrasRecomend}}</td>
          </tr>
          <tr class="text-center">
            <td colspan="5">Recomendaciones de estilo de vida</td>
          </tr>
          <tr>

            <td colspan="5">
              @if ($examen->recomEstiloVida==null)
                Sin Recomendaciones
                @else
                @foreach ($examen->recomEstiloVida as $value)
                  {{$value.', '}}
                @endforeach
            @endif
            </td>
          </tr>
          <tr class="text-center">
            <td colspan="5">Diagnostico Audiológico</td>
          </tr>
          <tr>
            <td colspan="5">{{$examen->diagnosticoAudio}}</td>
          </tr>
        </table>
      </div>
      <div id="Resultados">
        <div class="alert alert-secondary text-center" role="alert">
            Resultados
        </div>
        <table class="table table-sm table-bordered">
          <tbody>
            <tr>
              <td>Estado de Salud:</td>
              <td>{{$resultados->estadoSalud}}</td>
            </tr>
            <tr>
              <td>Resultaos de Aptitud</td>
              <td>{{$resultados->aptitud}}</td>
            </tr>
            <tr>
              <td>Comentarios</td>
              <td>{{$resultados->comentarios}}</td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </body>
  <script src="{!! asset('js/bootstrap.min.js') !!}"></script>
</html>
