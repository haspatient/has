@extends('layouts.VuexyLaboratorio')

@section('title', 'Audiometría')

@section('begin_vendor_css')
<link rel="stylesheet" href="{!! asset('public/vuexy/app-assets/vendors/css/extensions/sweetalert2.min.css') !!}">
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="{!! asset('public/vuexy/app-assets/vendors/css/forms/spinner/jquery.bootstrap-touchspin.css') !!}">
    <link rel="stylesheet" type="text/css" href="{!! asset('public/vuexy/app-assets/vendors/css/charts/apexcharts.css') !!}">
@endsection

@section('content')
<nav aria-label="breadcrumb">
    <ol class="breadcrumb ml-1">
        <li class="breadcrumb-item"><a href="{{route('medicina-pacientes')}}">Pacientes</a></li>
        <li class="breadcrumb-item"><a href="{{route('medicina_paciente',['id'=> encrypt($empleado->id)])}}">Estudios del paciente</a></li>
        <li class="breadcrumb-item active" id="tituloGeneral" aria-current="page">Audiometría de {{ucfirst($empleado->nombre) .' '. ucfirst($empleado->apellido_paterno).' '. ucfirst($empleado->apellido_materno)}}</li>
    </ol>
</nav>
<div class="canvas_div_pdf">
<div class="row" id="pruebaRow">
    <div class="col-xl-12 col-md-12 col-sm-12">
        <div class="card">
            <div class="card-content">
                <div class="card-body">
                    <h5 class="text-center" id="pr1">EXAMEN AUDIOMETRICO</h5>
                    <div class="row">
                        <div class="col-12 col-md-6">
                            <p class="card-subtitle">Nombre del paciente: {!! $empleado->nombre." ".$empleado->apellido_paterno." ".$empleado->apellido_materno !!}</p>
                            <p class="card-subtitle">Empresa: {!! $empleado->empresa->nombre !!}</p>
                        </div>
                        <div class="col-12 col-md-6 text-right">
                            <p class="card-subtitle">Nombre del paciente: {!! $empleado->nombre." ".$empleado->apellido_paterno." ".$empleado->apellido_materno !!}</p>
                            <p class="card-subtitle">Empresa: {!! $empleado->empresa->nombre !!}</p>
                        </div>
                        <div class="col-12">
                            <div id="line-chart2"></div>
                            <input type="hidden" name="audiometria_id" id="audiometria_id" value="{!! $audiometria->id !!}">
                        </div>
                    </div>
                    <hr>
                    <div id="pruebaNueva">
                    <div class="row">
                        <div class="col-6 border">
                            <div class="row text-center">
                                <div class="col-12 bg-primary pt-1 pb-1 text-white mb-2">
                                    EVALUACION Y SIGNIFICADO DEL INDICE SAL
                                </div>
                                <div class="col-2">
                                    <span class="small">GRADO</span>
                                </div>
                                <div class="col-3">
                                    <span class="small">SAL (dB)</span>
                                </div>
                                <div class="col-3">
                                    <span class="small">NOMBRE DE LA CLASE</span>
                                </div>
                                <div class="col-4">
                                    <span class="small">CARACTERISTICAS</span>
                                </div>
                            </div>
                            <div class="row align-items-center text-center height-75 border">
                                <div class="col-2">
                                    A
                                </div>
                                <div class="col-3">
                                    -16
                                </div>
                                <div class="col-3">
                                    Normal
                                </div>
                                <div class="col-4">
                                    <span class="small">Los dos oídos están dentro de los limites normales.</span>
                                </div>
                            </div>
                            <div class="row align-items-center text-center height-75 border">
                                <div class="col-2">
                                    B
                                </div>
                                <div class="col-3">
                                    16-30
                                </div>
                                <div class="col-3">
                                    Casi normal
                                </div>
                                <div class="col-4">
                                    <span class="small">Tiene dificultades en conversaciones en voz baja nada más.</span>
                                </div>
                            </div>
                            <div class="row align-items-center text-center height-75 border">
                                <div class="col-2">
                                    C
                                </div>
                                <div class="col-3">
                                    31 - 45
                                </div>
                                <div class="col-3">
                                    Ligero empeoramiento
                                </div>
                                <div class="col-4">
                                    <span class="small">Tiene dificultades en una conversación normal, pero no si se levanta la voz.</span>
                                </div>
                            </div>
                            <div class="row align-items-center text-center height-75 border">
                                <div class="col-2">
                                    D
                                </div>
                                <div class="col-3">
                                    46 - 60
                                </div>
                                <div class="col-3">
                                    Serio empeoramiento
                                </div>
                                <div class="col-4">
                                    <span class="small">Tiene dificultades incluso cuando se levanta la voz.</span>
                                </div>
                            </div>
                            <div class="row align-items-center text-center height-75 border">
                                <div class="col-2">
                                    E
                                </div>
                                <div class="col-3">
                                    61 - 90
                                </div>
                                <div class="col-3">
                                    Grave empeoramiento
                                </div>
                                <div class="col-4">
                                    <span class="small">Sólo puede oír una conversación amplificada.</span>
                                </div>
                            </div>
                            <div class="row align-items-center text-center height-75 border">
                                <div class="col-2">
                                    F
                                </div>
                                <div class="col-3">
                                    91 o +
                                </div>
                                <div class="col-3">
                                    Profundo empeoramiento
                                </div>
                                <div class="col-4">
                                    <span class="small">No puede entender ni una conversación amplificada.</span>
                                </div>
                            </div>
                            <div class="row align-items-center text-center height-75 border">
                                <div class="col-2">
                                    G
                                </div>
                                <div class="col-6">
                                    Sordera total ambos oidos
                                </div>
                                <div class="col-4">
                                    <span class="small">No puede oír sonido alguno.</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-6 border">
                            <div class="row text-center">
                                <div class="col-12 bg-primary pt-1 pb-1 text-white">
                                    (INDICE DE FLETCHER MODIFICADO POR BERRUECO)
                                </div>
                            </div>
                            <div class="row text-center height-50 border align-items-center">
                                <div class="col-8">
                                    DSHL*
                                </div>
                                <div class="col-4">
                                    % Pérdida
                                </div>
                            </div>
                            <div class="row text-center height-75 border align-items-center">
                                <div class="col-3">
                                    Oído derecho
                                </div>
                                <div class="col-5">
                                    {!! $dshl_d !!} dB
                                </div>
                                <div class="col-4">
                                    {!! $dshl_d / 4 * 0.8 !!} %
                                </div>
                            </div>
                            <div class="row text-center height-75 border align-items-center">
                                <div class="col-3">
                                    Oído izquierdo
                                </div>
                                <div class="col-5">
                                    {!! $dshl_i !!} dB
                                </div>
                                <div class="col-4">
                                    {!! $dshl_i / 4 * 0.8 !!} %
                                </div>
                            </div>
                            <div class="row text-center">
                                <div class="col-12">
                                    <span class="small">*DSHL:Suma de pérdidas a 500,1000, 2000 y 4000Hz</span>
                                </div>
                            </div>
                            <div class="row text-center height-75 border align-items-center">
                                <div class="col-12">
                                    ÍNDICE SAL
                                </div>
                            </div>
                            <div class="row text-center height-75 border align-items-center">
                                <div class="col-3">
                                    Oído derecho
                                </div>
                                <div class="col-5">
                                    {!! $indice_sal_d !!} dB
                                </div>
                                <div class="col-4">
                                    @if($indice_sal_d >= 91)
                                    F
                                    @elseif($indice_sal_d > 60 && $indice_sal_d < 91)
                                    E
                                    @elseif($indice_sal_d > 45 && $indice_sal_d < 61)
                                    D
                                    @elseif($indice_sal_d > 30 && $indice_sal_d < 46)
                                    C
                                    @elseif($indice_sal_d > 15 && $indice_sal_d < 31)
                                    B
                                    @else
                                    A
                                    @endif
                                </div>
                            </div>
                            <div class="row text-center height-75 border align-items-center">
                                <div class="col-3">
                                    Oído izquierdo
                                </div>
                                <div class="col-5">
                                    {!! $indice_sal_i !!} dB
                                </div>
                                <div class="col-4">
                                    @if($indice_sal_i >= 91)
                                    F
                                    @elseif($indice_sal_i > 60 && $indice_sal_i < 91)
                                    E
                                    @elseif($indice_sal_i > 45 && $indice_sal_i < 61)
                                    D
                                    @elseif($indice_sal_i > 30 && $indice_sal_i < 46)
                                    C
                                    @elseif($indice_sal_i > 15 && $indice_sal_i < 31)
                                    B
                                    @else
                                    A
                                    @endif
                                </div>
                            </div>
                            <div class="row text-center height-75 border align-items-center">
                                <div class="col-12">
                                    <b>Interpretación</b>
                                    <br>
                                    {!! $audiometria->interpretacion !!}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-2">
                        <div class="col-12">
                            Antecedentes
                        </div>
                        <div class="col-6">
                            <ul>
                                @if($audiometria->check_proteccion_auditiva == "on")
                                <li>uso de protección auditiva actualmente</li>
                                @endif
                                @if($audiometria->check_tce == "on")
                                <li>refiere de traumatismo craneoencefálico</li>
                                @endif
                                @if($audiometria->tiempo_trabajando_exposicion != null)
                                <li>refiere exposició a ruido laboral durante {!! $audiometria->tiempo_trabajando_exposicion !!}</li>
                                @endif
                                @if($audiometria->tiempo_puesto != null)
                                <li>antigüedad de {!! $audiometria->tiempo_puesto !!} en el empleo actual</li>
                                @endif
                                @if($audiometria->check_explosion_cercana == "on")
                                <li>ha estado en una explosión cercana, oido afectado: {!! $audiometria->ec_oido !!}</li>
                                @endif
                                @if($audiometria->check_audifonos_musica == "on")
                                <li>escucha música {!! $audiometria->am_no_dias_semana !!} día(s) a la semana, en un volumen de {!! $audiometria->am_volumen !!} durante {!! $audiometria->am_cuantos_anios  !!} año(s)</li>
                                @endif
                                @if($audiometria->check_gripa_infancia == "on")
                                <li>refiere gripa frecuente en infancia</li>
                                @endif
                                @if($audiometria->check_gripa_actualidad == "on")
                                <li>refiere gripa en la actualidad</li>
                                @endif
                                @if($audiometria->check_infeccion_oidos == "on")
                                <li>refiere infección de oídos</li>
                                @endif
                                @if($audiometria->check_hipertension == "on")
                                <li>refiere tener hipertensión</li>
                                @endif
                                @if($audiometria->check_paralisis_facial == "on")
                                <li>refiere tener paralisis facial</li>
                                @endif
                                @if($audiometria->check_diabetes == "on")
                                <li>refiere tener diabetes</li>
                                @endif
                                @if($audiometria->check_eventos == "on")
                                <li>refiere acudir a fiestas, antros, bailes con la frecuencia de {!! $audiometria->e_frecuencia !!}</li>
                                @endif
                                @if($audiometria->check_varicela == "on")
                                <li>refiere tener o haber tenido varicela</li>
                                @endif
                                @if($audiometria->check_practicas == "on")
                                <li>refiere practicar natación, aviación o uso de motocicleta con la frecuencia de {!! $audiometria->p_frecuencia !!}</li>
                                @endif
                                @if($audiometria->check_medicamentos == "on")
                                <li>refiere tomar {!! $audiometria->m_cual !!} frecuentemente</li>
                                @endif
                                @if($audiometria->check_familiar_problemas == "on")
                                <li>refiere tener algún familiar con problemas de audición</li>
                                @endif
                            </ul>
                        </div>
                        <div class="col-6 text-center align-self-center">
                            <img src="{!! asset("storage/app/datosHtds/".$user_htds->firma) !!}" style="width: 200px;">
                            <br>
                            {!! $user_htds->titulo_profesional !!}. {!! $user_htds->nombre_completo !!}
                            <br>
                            Cedula Profesional {!! $user_htds->cedula !!}
                        </div>
                    </div>
                    </div>
                    <div class="row mt-2">
                        <div class="col-12">
                            <a id="crearPdf" href="{!! url("crearPDF/audiometria/".$audiometria->id) !!}" class="btn btn-primary btn-sm float-right btn_export_pdf text-white">Crear PDF</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@endsection

@section('page_vendor_js')
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
@endsection

@section('js_custom')
    <script src="{!! asset('public/vuexy/app-assets/js/scripts/extensions/sweet-alerts.min.js') !!}"></script>
    <script src="{!! asset('public/vuexy/app-assets\vendors\js\forms\spinner\jquery.bootstrap-touchspin.js') !!}"></script>
    <script src="{!! asset('public/vuexy/app-assets/js/scripts/forms/number-input.min.js') !!}"></script>
    {{-- <script src="{!! asset('public/vuexy/app-assets/vendors/js/charts/apexcharts.min.js') !!}"></script> --}}
    <script src="https://cdn.jsdelivr.net/npm/apexcharts"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.3/jspdf.min.js"></script>
    <script src="https://html2canvas.hertzen.com/dist/html2canvas.js"></script>
    <script>
        var chart = null;
        $(document).ready(function() {


            var array_oido_izquierdo = [0,0,0,0,0,0,0,0,0];
            var array_oido_derecho = [0,0,0,0,0,0,0,0,0];
            var audiometria_id = $("#audiometria_id").val();

            $.ajax({
                type: "GET",
                url: "../Audiometria/getResultadosAudiometria/"+audiometria_id+"/izquierdo",
                dataType: "json",
            }).done(res=>{
                array_oido_izquierdo = [];
                res.forEach(element => {
                    array_oido_izquierdo.push(parseInt(element.desibelios));
                    $("#oido_izquierdo_"+element.frecuencia).val(element.desibelios)
                });
                initGrafica();
            }).fail(err=>{
                console.log(err);
            });

            $.ajax({
                type: "GET",
                url: "../Audiometria/getResultadosAudiometria/"+audiometria_id+"/derecho",
                dataType: "json",
            }).done(res=>{
                array_oido_derecho = [];
                res.forEach(element => {
                    array_oido_derecho.push(parseInt(element.desibelios));
                    $("#oido_derecho_"+element.frecuencia).val(element.desibelios)
                });
                initGrafica();
            }).fail(err=>{
                console.log(err);
            });

            function initGrafica() {
                let series = [
                    {
                        name: "Oído izquierdo",
                        data:array_oido_izquierdo,
                    },
                    {
                        name: "Oído derecho",
                        data:array_oido_derecho
                    }
                ];
                chart.updateSeries(series,true);
            }

            var options = {
                series: [
                    {
                        name: "Oído izquierdo",
                        data:array_oido_izquierdo,
                    },
                    {
                        name: "Oído derecho",
                        data:array_oido_derecho
                    }
                ],
                chart: {
                    height: 300,
                    type: 'line',
                    zoom: {
                        enabled: false
                    },
                    toolbar: {
                        show: false
                    }
                },
                colors:["#001eff", "#fc0000"],
                dataLabels: {
                    enabled: false
                },
                stroke: {
                    curve: 'straight',
                    colors:["#001eff","#fc0000"],
                    width: 3,
                },
                legend:{
                    markers: {
                        width: 12,
                        height: 12,
                        strokeWidth: 1,
                        strokeColor:["#001eff","#fc0000"] ,
                        fillColor: ["#001eff","#fc0000"],
                        radius: 12,
                        offsetX: 0,
                        offsetY: 0
                    },
                },
                grid: {
                    row: {
                        colors: ['#f3f3f3', 'transparent'], // takes an array which will be repeated on columns
                        opacity: 1
                    },
                    column: {
                        colors: ['transparent','#f3f3f3'], // takes an array which will be repeated on columns
                        opacity: 0.5
                    },
                },
                xaxis: {
                    categories: ['125', '250', '500', '1K', '2K', '3K', '4K', '6K', '8K'],
                    position: "top",
                    offsetX: 0,
                    offsetY: -0,
                    labels:{
                        show:true,
                        hideOverlappingLabels:true,
                        // offsetY:-15,
                    }
                },
                yaxis:{
                    reversed: true,
                    tickAmount:10,
                    forceNiceScale:true,
                    min: -10,
                    max: 120,
                }
            };

            chart = new ApexCharts($("#line-chart2")[0], options);
            chart.render();

            $("#crearPdf").click(function(e) {
                e.preventDefault();
                var img_temp = "";
                var dataURL = chart.dataURI().then(({ imgURI, blob }) => {
                    $.ajax({
                        type: "POST",
                        url: "../Audiometria/guardarFinal",
                        dataType: "json",
                        data: {
                            audiometria_id: audiometria_id,
                            imagen: imgURI
                        }
                    }).done(res=>{
                        url = $(this).attr("href");
                        window.open(url, '_blank');
                        return false;
                    }).fail(err=>{
                        console.log(err);
                    });
                });
            })
        });



        // function getPDF(){
        //     var img_temp = "";
        //     // var dataURL = chart.dataURI().then(({ imgURI, blob }) => {
        //     //     console.log(imgURI);
        //     //     img_temp = imgURI;
        //     // });

        //     $.ajax({
        //         type: "POST",
        //         url: "../Audiometria/guardarFinal/",
        //         dataType: "json",
        //         data: {
        //             audiometria_id: audiometria_id,
        //             imagen: img_temp
        //         }
        //     }).done(res=>{
        //       console.log(res);
        //     }).fail(err=>{
        //         console.log(err);
        //     });
        //     return false;
        //     // console.log(img_temp);
        //     // var HTML_Width = $(".canvas_div_pdf").width();
        //     // var HTML_Height = $(".canvas_div_pdf").height();
        //     // var top_left_margin = 15;
        //     // var PDF_Width = HTML_Width+(top_left_margin*2);
        //     // var PDF_Height = (PDF_Width*1.5)+(top_left_margin*2);
        //     // var canvas_image_width = HTML_Width;
        //     // var canvas_image_height = HTML_Height;

        //     // var totalPDFPages = Math.ceil(HTML_Height/PDF_Height)-1;


        //     // html2canvas($("body")[0],{
        //     //         allowTaint:true,
        //     //         removeContainer:true,
        //     //         useCORS:true,
        //     //         backgroundColor:"#ffffff",
        //     //         ignoreElements:function (element){
        //     //             if($(element).hasClass("content-overlay") || $(element).hasClass("header-navbar") || $(element).hasClass("pace-inactive") || $(element).hasClass("main-menu") || $(element).hasClass("breadcrumb") || $(element).hasClass("header-navbar-shadow") || $(element).hasClass("footer") || $(element).hasClass("btn_export_pdf"))
        //     //             {
        //     //                 return true;
        //     //             }
        //     //         }
        //     //     }).then(function(canvas) {
        //     //     canvas.getContext('2d');
        //     //     var imgData = canvas.toDataURL("image/jpeg", 1.0);
        //     //     var pdf = new jsPDF('p', 'pt',  [PDF_Width , (PDF_Height - 300)]);
        //     //     pdf.addImage(imgData, 'JPG', 15, -100,canvas_image_width,canvas_image_height);

        //     //     pdf.save($("#tituloGeneral").text()+".pdf");
        //     // });
        // };
    </script>
@endsection
