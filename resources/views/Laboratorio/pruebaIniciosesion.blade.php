<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <title>Prueba de inicio de sesión</title>
</head>
<body>
    <button onclick="sesionRecepcion()">Recepción</button>
    <button onclick="sesionTecnicoRadiologo()">Técnico radiólogo</button>
    <button onclick="sesionMedicoRadiologo()">Médico radiólogo</button>
    <button onclick="sesionMedicina()">Medicina</button>
    <button onclick="sesionLaboratorio()">Laboratorio</button>

    <!--Scripts-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
    <script>
        var recepcion = "772a2af944f0cb295424bff440274ebf4d93f1d6c9e8a07eda8bee137612460c1511a05afe4d99c014e746be6619bb7188fc34e1dff81ad6b8c256ce77df30eb";
        var tecnicoRadiologo = "2628f3a9495fe3f5f753cb2d4462d555acd55968c906dcd2e7599e94ede4c7159bf422734d34b304eac66f8bc23345e45c5445c7d99dba616044792e6c788451";
        var medicoRadiologo = "b4a1977fb651e08c933f56e8b00908d6099d4177f037f6ec219fecd43ce7e29630d06105fae887675c8ce1879658d4b459911f5376e2b8e38ed40d0d8d6b14fe";
        var Medicina = "5c6cd82bec3b8f61b962e86ff3f47f26197570069857e63fb7009bd0629ff7ad9368fe20eb8bf2d2cc96982f582f0e02d7338cd3237a5f711fa1917128940a15";
        var LaboratorioS = "15";
        function sesionMedicina() {
          window.location.replace("auth/lab-login/" + Medicina);
        }
        function sesionRecepcion() {
            window.location.replace("auth/lab-login/" + recepcion);
        }

        function sesionTecnicoRadiologo() {
            window.location.replace("auth/lab-login/" + tecnicoRadiologo);
        }

        function sesionMedicoRadiologo() {
            window.location.replace("auth/lab-login/" + medicoRadiologo);
        }

        function sesionLaboratorio() {
            window.location.replace("auth/lab-login/" + LaboratorioS);
        }

    </script>
</body>
</html>
