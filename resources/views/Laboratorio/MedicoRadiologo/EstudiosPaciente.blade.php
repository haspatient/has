@extends('layouts.laboratorioApp')

@section('title')
  Paciente: {{$paciente->nombre.' '.$paciente->apellido_paterno.' '.$paciente->apellido_materno}}
@endsection

@section('styles')
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="{!! asset('/') !!}/resources/sass/css/normalize.css">
    <link rel="stylesheet" href="{!! asset('/') !!}/resources/sass/fontawesome/css/all.css">
    <link rel="stylesheet" type="text/css" href="{!! asset('/') !!}/resources/sass/css/Laboratorio/menu_styles.css">
    <link rel="stylesheet" type="text/css" href="{!! asset('/') !!}/resources/sass/css/Laboratorio/estudios.css">
@endsection

@section('content')
    <main class="contents ml-5">
    <nav aria-label="breadcrumb">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{route('pacientes')}}">Pacientes</a></li>
        <li class="breadcrumb-item active" aria-current="page"><span>Estudios</span></li>
      </ol>
    </nav>
    <div class="content">
          <div class="title-content">
              <h1 class="title">Historial de Estudios</h1>
          </div>
          <div class="panel panel-default">
           <div class="panel-heading">
           </div>
           <div class="panel-body">

           @forelse ($estudios as $estudio)
             <div class="card" data-toggle="tooltip" data-placement="right" title="Estudio Pendiente">
                 <div class="firstinfo">
                   <div class="profileinfo">
                     <h1>{{$paciente->nombre.' '.$paciente->apellido_paterno.' '.$paciente->apellido_materno}}</h1>
                     <h3>{{$estudio->nombre}}</h3>
                     <span class="bio">{{\Carbon::parse($paciente->fecha_nacimiento)->age}} años</span>
                     <span class="bio">{{$paciente->CURP}}</span>
                     <span class="bio">{{$paciente->genero}}</span>
                     <span class="bio">{{\Carbon\Carbon::parse($estudio->updated_at)->diffForHumans()}}</span>
                   </div>
                 </div>
                 <a href="{{route('estudioinfo',['id'=> encrypt($estudio->id)])}}" class="links"><i class="iconec far fa-arrow-alt-circle-right"></i></a>
            </div>
           @empty

           @endforelse
           {{-- @forelse ($estudios_old as $old)
             <div class="card old" data-toggle="tooltip" data-placement="right" title="Finalizado">
                 <div class="firstinfo">
                   <div class="profileinfo">
                     <h1>{{$paciente->nombre.' '.$paciente->apellido_paterno.' '.$paciente->apellido_materno}}</h1>
                     <h3 class="old_text">{{$old->nombre}}</h3>
                     <span class="bio">{{\Carbon::parse($paciente->fecha_nacimiento)->age}} años</span>
                     <span class="bio">{{$paciente->CURP}}</span>
                     <span class="bio">{{$paciente->genero}}</span>
                     <span class="bio">{{ \Carbon::parse($old->created_at)->toFormattedDateString() }}</span>
                     <span class="bio coment">{{$old->comentarios}}</span>
                   </div>
                 </div>
                 <a href="{{route('estudioinfo',['id'=>encrypt($paciente->id)])}}" class="links"><i class="iconec far fa-arrow-alt-circle-right"></i></a>
            </div>
           @empty

           @endforelse --}}
           </div>
          </div>
        </div>
      </main>
@endsection

@section('specificScripts')
<script src="{!! asset('/') !!}/resources/js/Laboratorio/lista.js"></script>
@endsection
