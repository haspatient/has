@extends('layouts.VuexyLaboratorio')

@section('title', 'Audiometría')

@section('begin_vendor_css')
<link rel="stylesheet" href="{!! asset('public/vuexy/app-assets/vendors/css/extensions/sweetalert2.min.css') !!}">
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="{!! asset('public/vuexy/app-assets/vendors/css/forms/spinner/jquery.bootstrap-touchspin.css') !!}">
    <link rel="stylesheet" type="text/css" href="{!! asset('public/vuexy/app-assets/vendors/css/charts/apexcharts.css') !!}">
    <link href="https://cdn.quilljs.com/1.3.6/quill.snow.css" rel="stylesheet">
@endsection

@section('content')
<nav aria-label="breadcrumb">
    <ol class="breadcrumb ml-1">
        <li class="breadcrumb-item"><a href="{{route('medicina-pacientes')}}">Pacientes</a></li>
        <li class="breadcrumb-item"><a id="linkPaciente" href="{{route('medicina_paciente',['id'=> encrypt($empleado->id)])}}">Estudios del paciente</a></li>
        <li class="breadcrumb-item active" id="tituloGeneral" aria-current="page">Audiometría de {{ucfirst($empleado->nombre) .' '. ucfirst($empleado->apellido_paterno).' '. ucfirst($empleado->apellido_materno)}}</li>
    </ol>
</nav>
<div class="row">
    <div class="col-xl-12 col-md-12 col-sm-12">
        <div class="card">
            <div class="card-content">
                <div class="card-header">
                    <h4 class="card-title">Carta de consentimiento</h4>
                </div>
                <div class="card-body">
                    <h5 class="card-subtitle">Fecha: {!! now()->format('d/M/Y') !!}</h5>
                    <h5 class="card-subtitle">Empresa: {!! $empresa->nombre !!}</h5>
                    <hr>
                    <form class="form" id="form_carta_consentimiento">
                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-6 col-12">
                                    <div class="form-label-group">
                                        <input type="text" id="puesto_trabajo" name="puesto_trabajo" class="form-control" placeholder="Puesto de trabajo solicitado" value="{!! $audiometria->puesto_trabajo_solicitado !!}">
                                    </div>
                                    <div class="form-label-group">
                                        <div class="custom-control custom-switch custom-switch-primary mb-1">
                                            <label for="">¿Usa protección auditiva? &nbsp;</label>
                                            <input {{ $audiometria->check_proteccion_auditiva == "on" ? "checked" : "" }} type="checkbox" class="custom-control-input" name="proteccion_auditiva" id="proteccion_auditiva">
                                            <label class="custom-control-label" for="proteccion_auditiva">
                                              <span class="switch-text-left">Si</span>
                                              <span class="switch-text-right">No</span>
                                            </label>
                                            <div class="float-md-right">
                                                <label for="">¿TCE? &nbsp;</label>
                                                <input {{ $audiometria->check_tce == "on" ? "checked" : "" }} type="checkbox" class="custom-control-input" name="tce" id="tce">
                                                <label class="custom-control-label" for="tce">
                                                  <span class="switch-text-left">Si</span>
                                                  <span class="switch-text-right">No</span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-label-group">
                                        @if($audiometria->molestias_oido == null)
                                        <select class="molestia_oido" id="molestia_oido" multiple="multiple" style="width:100%" name="molestia_oido[]">
                                            <option value="dolor">Dolor</option>
                                            <option value="prurito">Prurito</option>
                                            <option value="acufeno">Acufeno</option>
                                            <option value="plenitud">Plenitud</option>
                                            <option value="cuerpo">Cuerpo extraño</option>
                                            <option value="escucha">Escucha menos</option>
                                        </select>
                                        @else
                                        <?php
                                        $array_molestias = explode(",",$audiometria->molestias_oido);
                                        ?>
                                        <select class="molestia_oido" id="molestia_oido" multiple="multiple" style="width:100%" name="molestia_oido[]">
                                            <option {{ (array_search("dolor",$array_molestias) > -1) ? "selected" : "" }} value="dolor">Dolor</option>
                                            <option {{ (array_search("prurito",$array_molestias) > -1) ? "selected" : "" }} value="prurito">Prurito</option>
                                            <option {{ (array_search("acufeno",$array_molestias) > -1) ? "selected" : "" }} value="acufeno">Acufeno</option>
                                            <option {{ (array_search("plenitud",$array_molestias) > -1) ? "selected" : "" }} value="plenitud">Plenitud</option>
                                            <option {{ (array_search("cuerpo",$array_molestias) > -1) ? "selected" : "" }} value="cuerpo">Cuerpo extraño</option>
                                            <option {{ (array_search("escucha",$array_molestias) > -1) ? "selected" : "" }} value="escucha">Escucha menos</option>
                                        </select>
                                        @endif
                                    </div>
                                    <div class="form-label-group">
                                        <div class="custom-control custom-switch custom-switch-primary mb-1">
                                            <label for="">¿Has tenido alguna explosión cercana? &nbsp;</label>
                                            <input type="checkbox" {{ $audiometria->check_explosion_cercana == "on" ? "checked" : "" }} class="custom-control-input" name="explosion_cercana" id="explosion_cercana">
                                            <label class="custom-control-label" for="explosion_cercana">
                                              <span class="switch-text-left">Si</span>
                                              <span class="switch-text-right">No</span>
                                            </label>
                                            @if($audiometria->check_explosion_cercana == "on")
                                            <div class="float-md-right" id="div_explosion_oido">
                                                <input required type="text" id="explosion_oido" name="explosion_oido" class="form-control form-control-sm" placeholder="¿En que oído?" value="{{ $audiometria->ec_oido }}">
                                            </div>
                                            @else
                                            <div class="float-md-right" id="div_explosion_oido" style="display: none">
                                                <input type="text" id="explosion_oido" name="explosion_oido" class="form-control form-control-sm" placeholder="¿En que oído?">
                                            </div>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-label-group">
                                        <div class="custom-control custom-switch custom-switch-primary mb-1">
                                            <label for="">Usa audifonos para música &nbsp;</label>
                                            <input type="checkbox" {{ $audiometria->check_audifonos_musica == "on" ? "checked" : "" }} class="custom-control-input" name="audifonos_musica" id="audifonos_musica">
                                            <label class="custom-control-label" for="audifonos_musica">
                                              <span class="switch-text-left">Si</span>
                                              <span class="switch-text-right">No</span>
                                            </label>
                                            @if($audiometria->check_audifonos_musica == "on")
                                            <div class="row mt-1" id="div_audifonos_musica">
                                                <div class="col-sm-4">
                                                    <input value="{{ $audiometria->am_volumen }}" required type="text" id="audifonos_musica_volumen" name="audifonos_musica_volumen" class="form-control form-control-sm" placeholder="Volumen">
                                                </div>
                                                <div class="col-sm-4">
                                                    <input value="{{ $audiometria->am_no_dias_semana }}" required type="text" id="audifonos_musica_no_dias" name="audifonos_musica_no_dias" class="form-control form-control-sm" placeholder="No. de días por semana">
                                                </div>
                                                <div class="col-sm-4">
                                                    <input value="{{ $audiometria->am_cuantos_anios }}" required type="text" id="audifonos_musica_anios" name="audifonos_musica_anios" class="form-control form-control-sm" placeholder="Cuántos años?">
                                                </div>
                                            </div>
                                            @else
                                            <div class="row mt-1" id="div_audifonos_musica" style="display: none">
                                                <div class="col-sm-4">
                                                    <input type="text" id="audifonos_musica_volumen" name="audifonos_musica_volumen" class="form-control form-control-sm" placeholder="Volumen">
                                                </div>
                                                <div class="col-sm-4">
                                                    <input type="text" id="audifonos_musica_no_dias" name="audifonos_musica_no_dias" class="form-control form-control-sm" placeholder="No. de días por semana">
                                                </div>
                                                <div class="col-sm-4">
                                                    <input type="text" id="audifonos_musica_anios" name="audifonos_musica_anios" class="form-control form-control-sm" placeholder="Cuántos años?">
                                                </div>
                                            </div>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-label-group">
                                        <div class="custom-control custom-switch custom-switch-primary mb-1">
                                            <label for="">Gripa frecuente en infancia &nbsp;</label>
                                            <input type="checkbox" {{ $audiometria->check_gripa_infancia == "on" ? "checked" : "" }} class="custom-control-input" name="gripa_infancia" id="gripa_infancia">
                                            <label class="custom-control-label" for="gripa_infancia">
                                                <span class="switch-text-left">Si</span>
                                                <span class="switch-text-right">No</span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form-label-group">
                                        <div class="custom-control custom-switch custom-switch-primary mb-1">
                                            <label for="">Gripa frecuente en la actualidad &nbsp;</label>
                                            <input type="checkbox" {{ $audiometria->check_gripa_actualidad == "on" ? "checked" : "" }} class="custom-control-input" name="gripa_actualidad" id="gripa_actualidad">
                                            <label class="custom-control-label" for="gripa_actualidad">
                                                <span class="switch-text-left">Si</span>
                                                <span class="switch-text-right">No</span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form-label-group">
                                        <div class="custom-control custom-switch custom-switch-primary mb-1">
                                            <label for="">Infección de oídos &nbsp;</label>
                                            <input type="checkbox" {{ $audiometria->check_infeccion_oidos == "on" ? "checked" : "" }} class="custom-control-input" name="infeccion_oidos" id="infeccion_oidos">
                                            <label class="custom-control-label" for="infeccion_oidos">
                                                <span class="switch-text-left">Si</span>
                                                <span class="switch-text-right">No</span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form-label-group">
                                        <div class="custom-control custom-switch custom-switch-primary mb-1">
                                            <label for="">¿Tiene diabetes? &nbsp;</label>
                                            <input type="checkbox" {{ $audiometria->check_diabetes == "on" ? "checked" : "" }} class="custom-control-input" name="diabetes" id="diabetes">
                                            <label class="custom-control-label" for="diabetes">
                                                <span class="switch-text-left">Si</span>
                                                <span class="switch-text-right">No</span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="form-label-group">
                                        <input value="{{ $audiometria->nim }}" type="text" id="nim" name="nim" class="form-control" placeholder="NIM">
                                    </div>
                                    <div class="form-label-group">
                                        <input value="{{ $audiometria->tiempo_trabajando_exposicion }}" type="text" id="tiempo_trabajando_exposion_ruido" name="tiempo_trabajando_exposion_ruido" class="form-control" placeholder="¿Cuánto tiempo lleva trabajando con exposición al ruido?">
                                    </div>
                                    <div class="form-label-group">
                                        <input value="{{ $audiometria->tiempo_puesto }}" type="text" id="tiempo_puesto" name="tiempo_puesto" class="form-control" placeholder="Tiempo en el puesto">
                                    </div>
                                    <div class="form-label-group">
                                        <div class="custom-control custom-switch custom-switch-primary mb-1">
                                            <label for="">Acude a fiestas, antros, bailes &nbsp;</label>
                                            <input type="checkbox" {{ $audiometria->check_eventos == "on" ? "checked" : "" }} class="custom-control-input" name="acude_eventos" id="acude_eventos">
                                            <label class="custom-control-label" for="acude_eventos">
                                                <span class="switch-text-left">Si</span>
                                                <span class="switch-text-right">No</span>
                                            </label>
                                            @if($audiometria->check_eventos == "on")
                                            <div class="float-md-right" id="div_acude_eventos">
                                                <input value="{{ $audiometria->e_frecuencia }}" required type="text" id="acude_eventos_frecuencia" name="acude_eventos_frecuencia" class="form-control form-control-sm" placeholder="Frecuencia">
                                            </div>
                                            @else
                                            <div class="float-md-right" id="div_acude_eventos" style="display: none">
                                                <input type="text" id="acude_eventos_frecuencia" name="acude_eventos_frecuencia" class="form-control form-control-sm" placeholder="Frecuencia">
                                            </div>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-label-group">
                                        <div class="custom-control custom-switch custom-switch-primary mb-1">
                                            <label for="">¿Varicela? &nbsp;</label>
                                            <input type="checkbox" {{ $audiometria->check_varicela == "on" ? "checked" : "" }} class="custom-control-input" name="varicela" id="varicela">
                                            <label class="custom-control-label" for="varicela">
                                              <span class="switch-text-left">Si</span>
                                              <span class="switch-text-right">No</span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form-label-group">
                                        <div class="custom-control custom-switch custom-switch-primary mb-1">
                                            <label for="">Practica natación, aviación o uso de motocicleta &nbsp;</label>
                                            <input type="checkbox" {{ $audiometria->check_practicas == "on" ? "checked" : "" }} class="custom-control-input" name="practica_actividades" id="practica_actividades">
                                            <label class="custom-control-label" for="practica_actividades">
                                                <span class="switch-text-left">Si</span>
                                                <span class="switch-text-right">No</span>
                                            </label>
                                            @if($audiometria->check_practicas == "on")
                                            <div class="float-md-right" id="div_practica_actividades">
                                                <input value="{{ $audiometria->p_frecuencia }}" required type="text" id="practica_actividades_frecuencia" name="practica_actividades_frecuencia" class="form-control form-control-sm" placeholder="Frecuencia">
                                            </div>
                                            @else
                                            <div class="float-md-right" id="div_practica_actividades" style="display: none">
                                                <input type="text" id="practica_actividades_frecuencia" name="practica_actividades_frecuencia" class="form-control form-control-sm" placeholder="Frecuencia">
                                            </div>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-label-group">
                                        <div class="custom-control custom-switch custom-switch-primary mb-1">
                                            <label for="">Medicamentos que use con frecuencia &nbsp;</label>
                                            <input type="checkbox" {{ $audiometria->check_medicamentos == "on" ? "checked" : "" }} class="custom-control-input" name="medicamentos_frecuentes" id="medicamentos_frecuentes">
                                            <label class="custom-control-label" for="medicamentos_frecuentes">
                                                <span class="switch-text-left">Si</span>
                                                <span class="switch-text-right">No</span>
                                            </label>
                                            @if($audiometria->check_medicamentos == "on")
                                            <div class="float-md-right" id="div_medicamentos_frecuentes">
                                                <input value="{{ $audiometria->m_cual }}" required type="text" id="medicamentos_frecuentes_cuales" name="medicamentos_frecuentes_cuales" class="form-control form-control-sm" placeholder="¿Cuál?">
                                            </div>
                                            @else
                                            <div class="float-md-right" id="div_medicamentos_frecuentes" style="display: none">
                                                <input type="text" id="medicamentos_frecuentes_cuales" name="medicamentos_frecuentes_cuales" class="form-control form-control-sm" placeholder="¿Cuál?">
                                            </div>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-label-group">
                                        <div class="custom-control custom-switch custom-switch-primary mb-1">
                                            <label for="">Tiene algún familiar con problemas de audición &nbsp;</label>
                                            <input type="checkbox" {{ $audiometria->check_familiar_problemas == "on" ? "checked" : "" }} class="custom-control-input" name="familiar_problemas_audicion" id="familiar_problemas_audicion">
                                            <label class="custom-control-label" for="familiar_problemas_audicion">
                                              <span class="switch-text-left">Si</span>
                                              <span class="switch-text-right">No</span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form-label-group">
                                        <div class="custom-control custom-switch custom-switch-primary mb-1">
                                            <label for="">¿Tiene hipertensión? &nbsp;</label>
                                            <input type="checkbox" {{ $audiometria->check_hipertension == "on" ? "checked" : "" }} class="custom-control-input" name="hipertension" id="hipertension">
                                            <label class="custom-control-label" for="hipertension">
                                                <span class="switch-text-left">Si</span>
                                                <span class="switch-text-right">No</span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form-label-group">
                                        <div class="custom-control custom-switch custom-switch-primary mb-1">
                                            <label for="">¿Tiene paralisis facial? &nbsp;</label>
                                            <input type="checkbox" {{ $audiometria->check_paralisis_facial == "on" ? "checked" : "" }} class="custom-control-input" name="paralisis_facial" id="paralisis_facial">
                                            <label class="custom-control-label" for="paralisis_facial">
                                                <span class="switch-text-left">Si</span>
                                                <span class="switch-text-right">No</span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <input type="hidden" value="{!! $empleado->CURP !!}" name="curp">
                                <input type="hidden" value="{!! $registro_entrada->id !!}" name="registro_id">
                                <div class="col-12 text-right">
                                    <button type="submit" class="btn btn-sm btn-primary mr-1 mb-1 waves-effect waves-light">Guardar</button>
                                    <button type="reset" class="btn btn-sm btn-outline-warning mr-1 mb-1 waves-effect waves-light">Reiniciar</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xl-6 col-md-12 col-sm-12">
        <div class="card">
            <div class="card-content">
                <div class="card-header">
                    <h4 class="card-title">Otoscopia</h4>
                </div>
                <div class="card-body">
                    <form class="form">
                        <div class="form-body">
                            <div class="row mb-1">
                                <div class="col-md-5">
                                    <div class="text-center">
                                        <h5>Oído izquierdo</h5>
                                    </div>
                                </div>
                                <div class="col-md-2 text-center">
                                    <h5>Hz</h5>
                                </div>
                                <div class="col-md-5">
                                    <div class="text-center">
                                        <h5>Oído derecho</h5>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-5">
                                    <div class="input-group w-100">
                                        <input type="number" class="input_decibeles" value="0" data-oido="i" data-hz="125" name="oido_izquierdo_125" id="oido_izquierdo_125">
                                    </div>
                                </div>
                                <div class="col-md-2 text-center">
                                    <div class="form-group">
                                        <h4>125</h4>
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <div class="input-group w-100">
                                        <input type="number" class="input_decibeles" value="0" data-oido="d" data-hz="125" name="oido_derecho_125" id="oido_derecho_125">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-5">
                                    <div class="input-group w-100">
                                        <input type="number" class="input_decibeles" value="0" data-oido="i" data-hz="250" name="oido_izquierdo_250" id="oido_izquierdo_250">
                                    </div>
                                </div>
                                <div class="col-md-2 text-center">
                                    <div class="form-group">
                                        <h4>250</h4>
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <div class="input-group w-100">
                                        <input type="number" class="input_decibeles" value="0" data-oido="d" data-hz="250" name="oido_derecho_250" id="oido_derecho_250">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-5">
                                    <div class="input-group w-100">
                                        <input type="number" class="input_decibeles" value="0" data-oido="i" data-hz="500" name="oido_izquierdo_500" id="oido_izquierdo_500">
                                    </div>
                                </div>
                                <div class="col-md-2 text-center">
                                    <div class="form-group">
                                        <h4>500</h4>
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <div class="input-group w-100">
                                        <input type="number" class="input_decibeles" value="0" data-oido="d" data-hz="500" name="oido_derecho_500" id="oido_derecho_500">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-5">
                                    <div class="input-group w-100">
                                        <input type="number" class="input_decibeles" value="0" data-oido="i" data-hz="1000" name="oido_izquierdo_1000" id="oido_izquierdo_1000">
                                    </div>
                                </div>
                                <div class="col-md-2 text-center">
                                    <div class="form-group">
                                        <h4>1000</h4>
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <div class="input-group w-100">
                                        <input type="number" class="input_decibeles" value="0" data-oido="d" data-hz="1000" name="oido_derecho_1000" id="oido_derecho_1000">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-5">
                                    <div class="input-group w-100">
                                        <input type="number" class="input_decibeles" value="0" data-oido="i" data-hz="2000" name="oido_izquierdo_2000" id="oido_izquierdo_2000">
                                    </div>
                                </div>
                                <div class="col-md-2 text-center">
                                    <div class="form-group">
                                        <h4>2000</h4>
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <div class="input-group w-100">
                                        <input type="number" class="input_decibeles" value="0" data-oido="d" data-hz="2000" name="oido_derecho_2000" id="oido_derecho_2000">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-5">
                                    <div class="input-group w-100">
                                        <input type="number" class="input_decibeles" value="0" data-oido="i" data-hz="3000" name="oido_izquierdo_3000" id="oido_izquierdo_3000">
                                    </div>
                                </div>
                                <div class="col-md-2 text-center">
                                    <div class="form-group">
                                        <h4>3000</h4>
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <div class="input-group w-100">
                                        <input type="number" class="input_decibeles" value="0" data-oido="d" data-hz="3000" name="oido_derecho_3000" id="oido_derecho_3000">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-5">
                                    <div class="input-group w-100">
                                        <input type="number" class="input_decibeles" value="0" data-oido="i" data-hz="4000" name="oido_izquierdo_4000" id="oido_izquierdo_4000">
                                    </div>
                                </div>
                                <div class="col-md-2 text-center">
                                    <div class="form-group">
                                        <h4>4000</h4>
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <div class="input-group w-100">
                                        <input type="number" class="input_decibeles" value="0" data-oido="d" data-hz="4000" name="oido_derecho_4000" id="oido_derecho_4000">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-5">
                                    <div class="input-group w-100">
                                        <input type="number" class="input_decibeles" value="0" data-oido="i" data-hz="6000" name="oido_izquierdo_6000" id="oido_izquierdo_6000">
                                    </div>
                                </div>
                                <div class="col-md-2 text-center">
                                    <div class="form-group">
                                        <h4>6000</h4>
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <div class="input-group w-100">
                                        <input type="number" class="input_decibeles" value="0" data-oido="d" data-hz="6000" name="oido_derecho_6000" id="oido_derecho_6000">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-5">
                                    <div class="input-group w-100">
                                        <input type="number" class="input_decibeles" value="0" data-oido="i" data-hz="8000" name="oido_izquierdo_8000" id="oido_izquierdo_8000">
                                    </div>
                                </div>
                                <div class="col-md-2 text-center">
                                    <div class="form-group">
                                        <h4>8000</h4>
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <div class="input-group w-100">
                                        <input type="number" class="input_decibeles" value="0" data-oido="d" data-hz="8000" name="oido_derecho_8000" id="oido_derecho_8000">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <input type="hidden" value="{!! $audiometria->id !!}" id="audiometria_id" name="audiometria_id">
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xl-6 col-md-12 col-sm-12">
        <div class="card">
            <div class="card-content">
                <div class="card-header">
                    <h4 class="card-title">Datos</h4>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <div id="line-chart2"></div>
                        </div>
                    </div>
                    @if ($user_htds != null)
                        @if($user_htds->cedula == "" || $user_htds->firma == "" || $user_htds->cedula == null || $user_htds->firma == null)
                        <div class="row">
                            <div class="col-12" id="divBtnModal">
                                <p>Debes de actualizar tus datos para generar este PDF <button type="button" id="btnModal" class="btn btn-sm btn-outline-adn"><i class="feather icon-upload"></i> Subir</button></p>
                            </div>
                        </div>
                        @else
                        <div class="row">
                            <div class="col-12">
                                <p>Puedes actualizar tus datos <button type="button" id="btnModal" class="btn btn-sm btn-outline-adn"><i class="feather icon-upload"></i> Actualizar</button></p>
                            </div>
                        </div>
                        @endif
                    @endif
                    <div class="modal fade text-left" id="modal" tabindex="-1" role="dialog" aria-labelledby="labelArchivos" aria-hidden="true" style="display: none;">
                        <div class="modal-dialog" role="document">
                            <form id="formDatos" class="modal-content">
                                <div class="modal-header bg-primary">
                                    <h4 class="modal-title" id="labelArchivos">Actualizar información</h4>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <div class="col-md-12 loadSass text-center pt-1 pb-1">
                                        <div class="row align-items-baseline">
                                            <div class="col-4">
                                                <fieldset class="form-group">
                                                    <label>Titulo profesional</label>
                                                    <input value="{!! $user_htds->titulo_profesional !!}" placeholder="Ej: Dr" required class="form-control" type="text" name="titulo_profesional" id="titulo_profesional">
                                                </fieldset>
                                            </div>
                                            <div class="col-8">
                                                <fieldset class="form-group">
                                                    <label>Nombre completo</label>
                                                    <input value="{!! $user_htds->nombre_completo !!}" placeholder="Javier Vite Ramirez" required class="form-control" type="text" name="nombre_completo" id="nombre_completo">
                                                </fieldset>
                                            </div>
                                            <div class="col-4">
                                                <fieldset class="form-group">
                                                    <label>Cédula profesional</label>
                                                    <input value="{!! $user_htds->cedula !!}" placeholder="99999999" required class="form-control" type="text" name="cedula" id="cedula">
                                                </fieldset>
                                            </div>
                                            <div class="col-8">
                                                <fieldset class="form-group">
                                                    <label for="img_firma"s>Imagen de la firma</label>
                                                    <div class="custom-file">
                                                        <input
                                                        @if($user_htds->cedula == "" || $user_htds->firma == "" || $user_htds->cedula == null || $user_htds->firma == null)
                                                        required
                                                        @endif
                                                        type="file" accept="image/png,image/jpg" class="custom-file-input" id="img_firma">
                                                        <label class="custom-file-label" for="img_firma">Selecciona una imagen</label>
                                                    </div>
                                                </fieldset>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="submit" class="btn btn-primary btn-sm cal-add-event waves-effect waves-light">
                                        Guardar datos
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <form method="post" id="formFinal" class="row">
                        <div class="col-12">
                            <input type="hidden" name="audiometria_id" value="{!! $audiometria->id !!}">
                            @if($audiometria->interpretacion != null && $audiometria->interpretacion != "")
                            <div id="editor">{!! $audiometria->interpretacion !!}</div>
                            @else
                            <div id="editor"></div>
                            @endif
                            <textarea class="form-control d-none" name="interpretacion" id="interpretacion_input" rows="2" placeholder="Escriba su interpretación">{{ $audiometria->interpretacion }}</textarea>
                        </div>
                        <br>
                        <div class="col-12 text-right mt-5">
                            <button
                            @if($user_htds->cedula == "" || $user_htds->firma == "" || $user_htds->cedula == null || $user_htds->firma == null)
                            disabled
                            @endif
                            type="submit" id="btnFormFinal" class="btn btn-sm btn-primary mb-1 waves-effect waves-light">Guardar y cerrar</button>
                            @if($user_htds->cedula == "" || $user_htds->firma == "" || $user_htds->cedula == null || $user_htds->firma == null)
                            <br>
                            <span id="helpDatos" class="font-small-1 text-danger">Debes subir tus datos primero</span>
                            @endif
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('page_vendor_js')
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
@endsection

@section('js_custom')
    <script src="{!! asset('public/vuexy/app-assets/js/scripts/extensions/sweet-alerts.min.js') !!}"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
    <script src="{!! asset('public/vuexy/app-assets\vendors\js\forms\spinner\jquery.bootstrap-touchspin.js') !!}"></script>
    <script src="{!! asset('public/vuexy/app-assets/js/scripts/forms/number-input.min.js') !!}"></script>
    {{-- <script src="{!! asset('public/vuexy/app-assets/vendors/js/charts/apexcharts.min.js') !!}"></script> --}}
    <script src="{!! asset('js/Laboratorio/audiometria/audiometria.js') !!}"></script>
    <script src="https://cdn.quilljs.com/1.3.6/quill.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/apexcharts"></script>
@endsection
