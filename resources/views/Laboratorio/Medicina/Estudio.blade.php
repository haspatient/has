@extends('layouts.VuexyLaboratorio')

@section('title')
  Paciente: {{$paciente->nombre.' '.$paciente->apellido_paterno.' '.$paciente->apellido_materno}}
  {{-- Estudio: {{$estudio->nombre}} --}}
@endsection

@section('begin_vendor_css')
  <!-- BEGIN VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="../app-assets/vendors/css/forms/selects/selectize.css">
    <link rel="stylesheet" type="text/css" href="../app-assets/vendors/css/forms/selects/selectize.default.css">
    <link rel="stylesheet" type="text/css" href="../app-assets/vendors/css/forms/selects/select2.min.css">
    <link rel="stylesheet" type="text/css" href="../app-assets/vendors/css/extensions/sweetalert.css">
  <!-- END VENDOR CSS-->
@endsection
@section('page_css')
  <link rel="stylesheet" type="text/css" href="../app-assets/css/plugins/animate/animate.css">
  <link rel="stylesheet" type="text/css" href="../app-assets/css/plugins/forms/selectize/selectize.css">
  <link rel="stylesheet" type="text/css" href="../app-assets/css/plugins/forms/checkboxes-radios.css">
  <link rel="stylesheet" type="text/css" href="../app-assets/css/plugins/forms/wizard.css">
  <link rel="stylesheet" type="text/css" href="../app-assets/css/plugins/pickers/daterange/daterange.css">
@endsection
@section('css_custom')

@endsection
{{-- BEGIN body html --}}
@section('content')
  <nav aria-label="breadcrumb">
      <ol class="breadcrumb ml-1">
        <li class="breadcrumb-item"><a href="{{ url('Medicina-Pacientes') }}">Pacientes</a></li>
        <li class="breadcrumb-item active" aria-current="page">Historial Clinico de {{$paciente->nombre.' '.$paciente->apellido_paterno.' '.$paciente->apellido_materno}}</li>
      </ol>
    </nav>

  {{-- inicio de formulario --}}
  <div class="">
    <div class="content-body"><!-- Form wizard with number tabs section start -->
    <section id="number-tabs">
      <div class="row">
        <div class="col-md-12">
    			<div class="card">
    				<div class="card-header">
    					<h4 class="card-title">Resultados de estudios finalizados</h4>
    					<a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
    					<div class="heading-elements">
    						<ul class="list-inline mb-0">
    							<li><a data-action="collapse"><i class="ft-minus"></i></a></li>
    						</ul>
    					</div>
    				</div>
    				<div class="card-content collapse show" style="">
              <div class="row pr-3 pl-3">
                @foreach ($estudios as $estudio)
                  @if ($estudio->nombre=="17 ALFA HIDROXIPROGESTERONA")
                    <a href="#" class="col-md-4" onclick="resultEstudios('{{$paciente->id}}','{{$estudio->id}}')">
                      @else
                        <a href="#" class="col-md-3" onclick="resultEstudios('{{$paciente->id}}','{{$estudio->id}}')">
                  @endif
                   <div class="card bg-info">
                      <div class="card-content">
                          <div class="card-body">
                              <div class="media d-flex">
                                  <div class="media-body text-white text-left">
                                      <h4 class="text-white">{{$estudio->nombre}}</h4>
                                      <span class="d-block">{{$estudio->categoria}}</span>
                                      <span class="d-block">{{\Carbon::parse($estudio->fecha)->format('y-m-d') }}</span>
                                      <div class="badge badge-primary">Capturado</div>
                                  </div>
                                  <div class="align-self-center">
                                    <i class="fa fa-flask text-white font-large-2 float-right"></i>
                                  </div>
                              </div>
                          </div>
                        </div>
                      </div>
                   </a>
                @endforeach
               </div>
    				</div>
    			</div>
	    	</div>
       <div class="col-12">
        <div class="card">
            <div class="card-header bg-secondary">
                <h4 class="card-title text-white">Examen Médico</h4>
                @if(count($errors) > 0)
                 <div class="alert alert-danger">
                  <button type="button" class="close" data-dismiss="alert">×</button>
                  <strong>Error en la validación</strong> <br>
                  <ul>
                   @foreach($errors->all() as $error)
                   <li>{{ $error }}</li>
                   @endforeach
                  </ul>
                 </div>
                @endif

            </div>
            <div class="card-content collapse show">
                <div class="card-body">
                  {{-- Desea todos los campos vacios?
                  <input type="radio" name="FormVacio" value="Vacio" onclick="RecargarForm('{{encrypt($paciente->CURP)}}')">SI
                  <input type="radio" name="FormVacio" value="Lleno" onclick="RecargarForm('{{encrypt($paciente->CURP)}}')">NO --}}

                    <form id="form" method="post" class="number-tab-steps wizard-circle">
                      @csrf
                      <input type="hidden" name="pacienteId" value="{{encrypt($paciente->id)}}">
                      <input type="hidden" id="generoPaciente" name="generoPaciente" value="{{$paciente->genero}}">
                         <!-- Step 1 -->
                        <h6>Identificacón</h6>
                        @include('Laboratorio.Medicina.includes.identificacion')
                        <!-- Step 2 -->
                        <h6>Heredofamiliares</h6>
                        @include('Laboratorio.Medicina.includes.heredofamiliares')
                         <!-- Step 3 -->
                          <h6>Antecedentes personales no patológicos</h6>
                        @include('Laboratorio.Medicina.includes.no_patologicos')
                        <!-- Step 4 -->
                        <h6>Antecedentes personales patológicos</h6>
                        @include('Laboratorio.Medicina.includes.patologicos')
                        <!-- Step 5 (damas) -->
                      @if ($paciente->genero=='Femenino'||$paciente->genero=='FEMENINO')
                          <h6>Antecedentes Ginecoobstetricos</h6>
                        @include('Laboratorio.Medicina.includes.ginecoobstetricos')
                        @endif
                        <!-- Step 6 -->
                        <h6>Historial Laboral</h6>
                        @include('Laboratorio.Medicina.includes.historia_laboral')
                          <h6>Examen Físico</h6>
                        @include('Laboratorio.Medicina.includes.examen_fisico')
                       <h6>Resultados de Aptitud</h6>
                        @include('Laboratorio.Medicina.includes.resultados')
                    </form>
                </div>
            </div>
        </div>
        </div>
        </div>
    </section>
    </div>
  </div>
  {{-- fin de formulario --}}

<!-- Modal -->
<div class="modal animated bounceInDown text-left" id="bounceInDown" tabindex="-1" role="dialog" aria-labelledby="myModalLabel47" style="display: none;" aria-hidden="true">
  <div class="modal-dialog modal-xl" role="document">
  <div class="modal-content">
        <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel47">Resultados</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
        </div>
          <div class="modal-body">
            <div class="row">
              <div class="col-md-2">
                <div class="col-md-12" id="iframes">
                  <a href="#" id="toolIframe">

                  </a>
                </div>
              </div>
              <div class="col-md-10"id="contentIframe">
                <iframe width="" height="" class="col-12" style="display:none; height: 67vh;" id="iframeBg"></iframe>
              </div>
            </div>
          </div>
        <div class="modal-footer">
        <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Cerrar</button>
        </div>
  </div>
  </div>
</div>
@endsection


@section('page_vendor_js')
  <!-- BEGIN PAGE VENDOR JS-->
  <script src="../app-assets/vendors/js/extensions/sweetalert.min.js"></script>
  <script src="../app-assets/vendors/js/menu/jquery.mmenu.all.min.js"></script>
  <script src="../app-assets/vendors/js/extensions/jquery.steps.min.js"></script>
  <script src="../app-assets/vendors/js/forms/select/selectize.min.js"></script>
  <script src="../app-assets/vendors/js/pickers/dateTime/moment-with-locales.min.js"></script>
  <script src="../app-assets/vendors/js/pickers/daterange/daterangepicker.js"></script>
  <script src="../app-assets/vendors/js/forms/validation/jquery.validate.min.js"></script>
  <script src="../app-assets/js/core/libraries/jquery_ui/jquery-ui.min.js"></script>

  {{-- checkbox --}}
  <script src="../app-assets/vendors/js/menu/jquery.mmenu.all.min.js"></script>
  <script src="../app-assets/vendors/js/forms/icheck/icheck.min.js"></script>
  <!-- END PAGE VENDOR JS-->
@endsection


@section('js_custom')
   <!-- BEGIN PAGE LEVEL JS-->
  {{-- <script src="app-assets/js/scripts/forms/wizard-steps.js"></script> --}}
  {{-- checkbox --}}
  <script src="../app-assets/js/scripts/forms/checkbox-radio.js"></script>
  <script src="../resources/js/Laboratorio/MedicinaPaciente.js"></script>
  <script src="../app-assets/js/scripts/forms/select/form-selectize.js"></script>
  <!-- END PAGE LEVEL JS-->
@endsection
