@extends('layouts.VuexyLaboratorio')

@section('title', 'Pacientes')

@section('styles')
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="../resources/sass/css/normalize.css">
    <link rel="stylesheet" href="../resources/sass/fontawesome/css/all.css">
    <link rel="stylesheet" type="text/css" href="../resources/sass/css/Laboratorio/menu_styles.css">
    <link rel="stylesheet" type="text/css" href="../resources/sass/css/Laboratorio/medicinaPaci.css">
@endsection

@section('content')

<div class="title-content">
   <h1 class="title">Pacientes Ingresados</h1>
</div>

<div class="panel panel-default">
<div class="panel-heading">
  <br>
</div>
<div class="panel-body">
  <div class="row content_card">
      <form id="search" method="post" class="col-md-12 mb-3">
        @csrf
        <div class="row">
          <div class="col-12 input-group input-focus">
            <div class="input-group-prepend">
              <span class="input-group-text bg-white" ><i class="fa fa-search"></i></span>
            </div>
            <input type="text" class="form-control search border-left-0 col-12" name="search"  placeholder="Nombre del paciente">
          </div>
          </div>
        </div>
     </form>
     <div class="turnt content_card row">
       @forelse ($pacientes as $ingreso)
         @if ($ingreso->categoria_id == 3)



           <div class="col-md-4 cards_item" id="{{$ingreso->CURP}}">
            <a class="card" href="{{route('medicina_paciente',['id'=> encrypt($ingreso->id)])}}">
              <div class="card-header">
                <h6 class="card-title secondary font-weight-normal">{{ ucwords(strtolower($ingreso->nombre)).' '.ucwords(strtolower($ingreso->app)).' '.ucwords(strtolower($ingreso->apm))}}</h6>
              </div>
              <div class="card-content collapse show">
                <div class="card-body">
                  <p class="small ">Edad: {{\Carbon::parse($ingreso->fecha_nacimiento)->age. ' años'}}</p>
                  <p class="small ">Genero: {{$ingreso->genero}}</p>
                  <p class="small ">Curp: {{$ingreso->CURP}}</p>
                </div>
              </div>
            </a>
          </div>



           {{-- <div class="col-md-4 cards_item" id="{{$ingreso->CURP}}" >
             <a class="card " href="{{route('medicina_paciente',['id'=> encrypt($ingreso->id)])}}">
               <i class="far fa-user i-user"></i>
               <h3 class="ml-3">{{ ucwords(strtolower($ingreso->nombre)).' '.ucwords(strtolower($ingreso->app)).' '.ucwords(strtolower($ingreso->apm))}}</h3>
               <p class="small ml-3">Edad: {{\Carbon::parse($ingreso->fecha_nacimiento)->age. ' años'}}</p>
               <p class="small ml-3">Genero: {{$ingreso->genero}}</p>
               <p class="small ml-3">Curp: {{$ingreso->CURP}}</p>
               <div class="go-corner" href="#">
                 <div class="go-arrow">
                   →
                 </div>
               </div>
             </a>
           </div> --}}
         @else
           @if ($ingreso->categoria_id == 1 && $ingreso->status == 1)



             <div class="col-md-4 cards_item" id="{{$ingreso->CURP}}">
              <a class="card" href="{{route('medicina_paciente',['id'=> encrypt($ingreso->id)])}}">
                <div class="card-header">
                  <h6 class="card-title secondary font-weight-normal">{{ ucwords(strtolower($ingreso->nombre)).' '.ucwords(strtolower($ingreso->app)).' '.ucwords(strtolower($ingreso->apm))}}</h6>
                </div>
                <div class="card-content collapse show">
                  <div class="card-body">
                    <p class="small ">Edad: {{\Carbon::parse($ingreso->fecha_nacimiento)->age. ' años'}}</p>
                    <p class="small ">Genero: {{$ingreso->genero}}</p>
                    <p class="small ">Curp: {{$ingreso->CURP}}</p>
                  </div>
                </div>
              </a>
            </div>

           @endif



         @endif

       @empty
         <p>Aún no hay pacientes</p>
       @endforelse
       <div class="aviso-vacio ml-5 pl-4" style="display: none;">
           <p>Ningun paciente coincide con la búsqueda.</p>
       </div>
     </div>
  </div>

</div>


@endsection

@section('js_custom')
<script src=" {!! asset('resources/js/Laboratorio/Medicina.js') !!} "></script>
@endsection
