@extends('layouts.VuexyLaboratorio')

@section('title')
Historial Clinico de {{$paciente->nombre.' '.$paciente->apellido_paterno.' '.$paciente->apellido_materno}}
@endsection

@section('begin_vendor_css')
    <!-- BEGIN VENDOR CSS-->
        <link rel="stylesheet" type="text/css" href="{!! url("app-assets/vendors/css/forms/selects/selectize.css") !!}">
        <link rel="stylesheet" type="text/css" href="{!! url("app-assets/vendors/css/forms/selects/selectize.default.css") !!}">
        <link rel="stylesheet" type="text/css" href="{!! url("app-assets/vendors/css/forms/selects/select2.min.css") !!}">
        <link rel="stylesheet" type="text/css" href="{!! url("app-assets/vendors/css/extensions/sweetalert.css") !!}">
    <!-- END VENDOR CSS-->
@endsection
@section('page_css')
    <link rel="stylesheet" type="text/css" href="{!! url("app-assets/css/plugins/animate/animate.css") !!}">
    <link rel="stylesheet" type="text/css" href="{!! url("app-assets/css/plugins/forms/selectize/selectize.css") !!}">
    <link rel="stylesheet" type="text/css" href="{!! url("app-assets/css/plugins/forms/checkboxes-radios.css") !!}">
    <link rel="stylesheet" type="text/css" href="{!! url("app-assets/css/plugins/forms/wizard.css") !!}">
    <link rel="stylesheet" type="text/css" href="{!! url("app-assets/css/plugins/pickers/daterange/daterange.css") !!}">
@endsection
@section('css_custom')
    <style>
        .app-content .wizard.wizard-circle > .steps > ul > li:before, .app-content .wizard.wizard-circle > .steps > ul > li:after{
            background-color: #f26b3e;
        }
        .app-content .wizard > .steps > ul > li.done .step{
            border-color: #f26b3e;
            background-color: #f26b3e;
        }
        .app-content .wizard > .steps > ul > li.current .step{
            color: #f26b3e;
            border-color: #f26b3e;
        }
        .app-content .wizard > .actions > ul > li > a{
            background-color: #f26b3e;
            border-radius: .4285rem;
        }
    </style>
@endsection
{{-- BEGIN body html --}}
@section('content')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb ml-1">
            <li class="breadcrumb-item"><a href="{{ url('Medicina-Pacientes') }}">Pacientes</a></li>
            <li class="breadcrumb-item active" aria-current="page">Historial Clinico de {{$paciente->nombre.' '.$paciente->apellido_paterno.' '.$paciente->apellido_materno}}</li>
        </ol>
    </nav>
    {{-- inicio de formulario --}}
    <div class="">
        <div class="content-body"><!-- Form wizard with number tabs section start -->
            <section id="number-tabs">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header bg-secondary">
                                <h4 class="card-title text-white">Examen Médico</h4>
                            </div>
                            <div class="card-content collapse show">
                                <div class="card-body">
                                    <div class="number-tab-steps wizard-circle">
                                        @foreach ($formulario->secciones as $key => $seccion)
                                        <h6>{!! $seccion->nombre !!}</h6>
                                        <fieldset>
                                            <div id="seccion_{!! $key !!}" data-id="{!! $seccion->id !!}">
                                                <form method="POST" id="form_{!! $seccion->id !!}" class="row">
                                                    <input type="hidden" name="historial_clinico_id" value="{!! $historial_clinico->id !!}">
                                                    @foreach ($seccion->preguntas as $pregunta)
                                                    <div class="col-{!! $pregunta->columnas !!}">
                                                        <div class="form-group">
                                                            <label for="campo_{{ $pregunta->id }}">{!! $pregunta->nombre !!}:</label>
                                                            @if($pregunta->tipo == "abierta")
                                                                <textarea class="form-control" name="campo_{{ $pregunta->id }}" id="campo_{{ $pregunta->id }}" rows="2">@if(!is_null($value = $pregunta->respuesta($historial_clinico->id,$pregunta->id))){{ $value->respuesta }}@endif</textarea>
                                                            @elseif($pregunta->tipo == "numero")
                                                                <input type="number" class="form-control" name="campo_{{ $pregunta->id }}" id="campo_{{ $pregunta->id }}" value="@if(!is_null($value = $pregunta->respuesta($historial_clinico->id,$pregunta->id))){{ $value->respuesta }}@endif">
                                                            @elseif($pregunta->tipo == "fecha")
                                                                <input type="date" class="form-control" name="campo_{{ $pregunta->id }}" id="campo_{{ $pregunta->id }}" value="@if(!is_null($value = $pregunta->respuesta($historial_clinico->id,$pregunta->id))){{ $value->respuesta }}@endif">
                                                            @elseif($pregunta->tipo == "select")
                                                                <select class="form-control" name="campo_{{ $pregunta->id }}" id="campo_{{ $pregunta->id }}">
                                                                    @foreach ($pregunta->respuestas as $respuesta)
                                                                        <option
                                                                        @if(!is_null($value = $pregunta->respuesta($historial_clinico->id,$pregunta->id))) {!! $value->respuesta_id == $respuesta->id ? 'selected' : '' !!}@endif
                                                                        value="{!! $respuesta->id !!}"
                                                                        >{!! $respuesta->nombre !!}</option>
                                                                    @endforeach
                                                                </select>
                                                            @elseif($pregunta->tipo == "texto")
                                                                <input type="text" class="form-control" name="campo_{{ $pregunta->id }}" id="campo_{{ $pregunta->id }}" value="@if(!is_null($value = $pregunta->respuesta($historial_clinico->id,$pregunta->id))){{ $value->respuesta }}@endif">
                                                            @endif
                                                        </div>
                                                    </div>
                                                    @endforeach
                                                </form>
                                            </div>
                                        </fieldset>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
    {{-- fin de formulario --}}
@endsection


@section('page_vendor_js')
<!-- BEGIN PAGE VENDOR JS-->
<script src="{!! url("app-assets/vendors/js/extensions/sweetalert.min.js") !!}"></script>
<script src="{!! url("app-assets/vendors/js/menu/jquery.mmenu.all.min.js") !!}"></script>
<script src="{!! url("app-assets/vendors/js/extensions/jquery.steps.min.js") !!}"></script>
{{-- <script src="{!! url("app-assets/vendors/js/forms/select/selectize.min.js") !!}"></script> --}}
<script src="{!! url("app-assets/vendors/js/pickers/dateTime/moment-with-locales.min.js") !!}"></script>
<script src="{!! url("app-assets/vendors/js/pickers/daterange/daterangepicker.js") !!}"></script>
<script src="{!! url("app-assets/vendors/js/forms/validation/jquery.validate.min.js") !!}"></script>
<script src="{!! url("app-assets/js/core/libraries/jquery_ui/jquery-ui.min.js") !!}"></script>

{{-- checkbox --}}
<script src="{!! url("app-assets/vendors/js/menu/jquery.mmenu.all.min.js") !!}"></script>
<script src="{!! url("app-assets/vendors/js/forms/icheck/icheck.min.js") !!}"></script>
<!-- END PAGE VENDOR JS-->

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/numeral.js/2.0.6/numeral.min.js"></script>

@endsection


@section('js_custom')
<script src="{!! url("app-assets/js/scripts/forms/checkbox-radio.js") !!}"></script>
<script>
    $(".number-tab-steps").steps({
        headerTag: "h6",
        bodyTag: "fieldset",
        transitionEffect: "fade",
        enableAllSteps: true,
        titleTemplate: '<span class="step">#index#</span> #title#',
        cssClass: 'wizard',
        labels: {
            finish: 'Guardar',
            previous: 'Anterior',
            next: 'Siguiente'
        },
        onStepChanging: function (event, currentIndex, newIndex) {
            let seccion = $("#seccion_"+currentIndex);
            let seccion_id = seccion.data('id');
            let formulario = $("#form_"+seccion_id);
            $.ajax({
                type: 'POST',
                dataType: 'json',
                data: formulario.serialize(),
                url: '../../save_formulario/'+seccion_id
            }).done((res) => {
                console.log(res);
            }).fail((err) => {
                console.log(err);
            });
            return true;
        },
        onFinished: function(event, currentIndex) {
            swal({
                title: "¿Seguro que desea terminar el historial clinico?",
                text: "Esta acción es irreversible",
                icon: "warning",
                buttons: {
                    cancel: {
                        text: "Cancelar",
                        value: null,
                        visible: true,
                        className: "",
                        closeModal: false,
                    },
                    confirm: {
                        text: "Si, terminar",
                        value: true,
                        visible: true,
                        className: "",
                        closeModal: false
                    }
                }
                }).then(isConfirm => {
                if (isConfirm) {
                    let data = {
                        "historial_clinico_id":$("input[name='historial_clinico_id']").val()
                    }
                    $.ajax({
                        type: 'POST',
                        dataType: 'json',
                        data: data,
                        url: '../../save_formulario/finalizado'
                    }).done((res) => {
                        console.log(res);
                        location.reload();
                    }).fail((err) => {
                        console.log(err);
                    });
                    swal("Exito", "Guardado con exito", "success");
                } else {
                    swal("Operacion Cancelada", "Puede continuar...", "error");
                }
            });
        }
    });
</script>
@endsection
