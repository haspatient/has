@extends('layouts.VuexyLaboratorio')

@section('title')
Empresas
@endsection

@section('style_vendor')
  <!-- BEGIN VENDOR CSS-->

  <!-- END VENDOR CSS-->
@endsection
@section('page_level_css')
 <link rel="stylesheet" type="text/css" href="../../app-assets/css/pages/project.css">
@endsection
@section('styles')

<!-- BEGIN Page Level CSS Custom-->
<link rel="stylesheet" type="text/css" href="../resources/sass/css/Laboratorio/menu_styles.css">
<link rel="stylesheet" type="text/css" href="../resources/sass/css/styleScroll.css">
<link rel="stylesheet" type="text/css" href="../resources/sass/css/Laboratorio/medicinaDiagnostico.css">
<link rel="stylesheet" href="../resources/sass/fontawesome/css/all.css">
<!-- END Page Level CSS Custom-->

@endsection
{{-- BEGIN body html --}}
@section('content')
<div class="content_all">
  <div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Empresas</h4>
                <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                <div class="heading-elements">
                    <ul class="list-inline mb-0">
                        <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                    </ul>
                </div>
                <hr>
            </div>
            <div class="card-content">
                <div id="recent-buyers" class="media-list">
                  <div class="row p-1">
                    @forelse ($empresas as $empresa)
                      <div class="col-lg-3" style="">
                        <div class="card">
                            <div class="card-content">
                              @if ($empresa->logo != null)
                                <img width="100%" style="height:11rem;object-fit: scale-down;" class="img-fluid" src="{!! asset('storage/app/empresas/'.$empresa->logo) !!} ">
                                @else
                                <img class="card-img-top img-fluid" src="https://pbs.twimg.com/profile_images/717381648682094592/I6edjlVa_400x400.jpg">
                              @endif
                                <div class="card-body">
                                  <h4 class="card-title height-75">{{ $empresa->nombre }}</h4>
                                    <p class="card-text height-75">{{ $empresa->direccion }}</p>
                                </div>
                            </div>
                            <div class="card-footer text-muted">
                                <a href="{{ route('historial',$empresa->id) }}" class="float-right btn btn-block btn-sm btn-secondary">ver Pacientes</a>
                            </div>
                        </div>
                    </div>
                    @empty
                      No hay ningun paciente registrado por el momento
                    @endforelse
                  </div>
                </div>
            </div>
        </div>
    </div>
  </div>
@endsection


@section('page_vendor_js')
<!-- BEGIN PAGE VENDOR JS-->
    <script src="../../app-assets/vendors/js/charts/echarts/echarts.js"></script>
<!-- END PAGE VENDOR JS-->
@endsection


@section('script')
<!-- BEGIN PAGE LEVEL JS-->
<script src="https://asesoresconsultoreslabs.com/expedienteclinico/app-assets/vendors/js/charts/echarts/chart/pie.js"></script>
<script src="https://asesoresconsultoreslabs.com/expedienteclinico/app-assets/vendors/js/charts/echarts/chart/funnel.js"></script>
  {{-- <script src="../resources/js/Laboratorio/Medico/charts.js"></script> --}}

  <!-- END PAGE LEVEL JS-->
@endsection
