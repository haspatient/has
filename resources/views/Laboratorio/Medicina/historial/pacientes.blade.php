@extends('layouts.VuexyLaboratorio')

@section('title', 'Pacientes')

@section('styles')
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="../resources/sass/css/normalize.css">
    <link rel="stylesheet" href="../resources/sass/fontawesome/css/all.css">
    <link rel="stylesheet" type="text/css" href="../resources/sass/css/Laboratorio/menu_styles.css">
    <link rel="stylesheet" type="text/css" href="../resources/sass/css/Laboratorio/medicinaPaci.css">
@endsection

@section('content')
<div class="title-content">
   <h1 class="title">Pacientes Ingresados</h1>
</div>
<div class="panel panel-default">
    <div class="panel-heading">
        <br>
    </div>
    <div class="panel-body">
        <div class="row content_card" id="admin_list">
            <div id="search" method="post" class="col-md-12 mb-3">
                <div class="row">
                    <div class="col-12 input-group input-focus">
                        <div class="input-group-prepend">
                            <span class="input-group-text bg-white" ><i class="fa fa-search"></i></span>
                        </div>
                        <input type="text" class="form-control search border-left-0 col-12 input_buscador" name="search"  placeholder="Busqueda de pacientes">
                        <button type="button" data-sort="search_numero_estudios" class="ml-1 btn_sort sort align-items-sm-center btn btn-sm btn-group btn-relief-primary waves-effect waves-light">
                            <div class="text-dark"><i class="feather icon-chevron-up"></i><br><i class="feather icon-chevron-down"></i></div>
                            &nbsp; No. estudios
                        </button>
                        <button type="button" data-sort="search_nombre" class="ml-1 btn_sort sort align-items-sm-center btn btn-sm btn-group btn-relief-primary waves-effect waves-light">
                            <div class="text-dark"><i class="feather icon-chevron-up"></i><br><i class="feather icon-chevron-down"></i></div>
                            &nbsp; Nombre
                        </button>
                        <button type="button" data-sort="search_curp" class="ml-1 btn_sort sort align-items-sm-center btn btn-sm btn-group btn-relief-primary waves-effect waves-light">
                            <div class="text-dark"><i class="feather icon-chevron-up"></i><br><i class="feather icon-chevron-down"></i></div>
                            &nbsp; CURP
                        </button>
                    </div>
                </div>
            </div>
            <div class="col-12">
                <div class="turnt content_card row list">
                @forelse ($pacientes as $key => $paciente)
                        <div class="col-xl-4 col-md-6 col-sm-12 profile-card-1 list_custom">
                            @if($paciente->expediente != null)
                                @if ($paciente->expediente->estudioscompletadosMedicina->count() > 0)
                                <a href="{{route('historial_pacientes_estudios',['paciente_id'=> encrypt($paciente->id)])}}" class="card">
                                @else
                                <a href="#" class="card a_modal_no_estudios" data-name="{!! $paciente->nombre.' '.$paciente->apellido_paterno.' '.$paciente->apellido_materno !!}">
                                @endif
                            @else
                                <a href="#" class="card a_modal_no_estudios" data-name="{!! $paciente->nombre.' '.$paciente->apellido_paterno.' '.$paciente->apellido_materno !!}">
                            @endif
                                <div class="card-header mx-auto">
                                    <div class="avatar avatar-xl">
                                        @if ($paciente->imagen != null && $paciente->imagen != "")
                                            <img class="img-fluid" src="{!! asset('storage/app/pacientes/'.$paciente->imagen) !!}">
                                        @else
                                            <img class="img-fluid" src="https://expedienteclinico.humanly-sw.com/dev/public/img/2665817.jpg">
                                        @endif
                                    </div>
                                </div>
                                <div class="card-content">
                                    <div class="card-body text-center">
                                        <h4 class="height-50 search_nombre">{{ ucwords(strtolower($paciente->nombre)).' '.ucwords(strtolower($paciente->apellido_paterno)).' '.ucwords(strtolower($paciente->apellido_materno))}}</h4>
                                        <p class="search_curp">{!! $paciente->CURP !!}</p>
                                        <div class="card-btns d-flex justify-content-between">
                                            <p class="search_edad">Edad: {{\Carbon::parse($paciente->fecha_nacimiento)->age. ' años'}}</p>
                                            <p class="search_genero">Genero: {{$paciente->genero}}</p>
                                        </div>
                                        <hr class="my-2">
                                        <div class="row">
                                            <div class="col-6 align-self-center">
                                                Fecha del último estudio realizado
                                                <br>
                                                @if($paciente->expediente != null)
                                                    @if ($paciente->expediente->estudioscompletadosMedicina->count() > 0)
                                                    <b>{!! $paciente->expediente->estudioscompletadosMedicina->sortByDesc('created_at')->first()->created_at->format('d/M/Y') !!}</b>
                                                    @else
                                                    <b>N/A</b>
                                                    @endif
                                                @else
                                                <b>N/A</b>
                                                @endif
                                            </div>
                                            <div class="col-6 align-self-center">
                                                <i class="feather icon-clipboard text-primary mr-50"></i>
                                                <br>
                                                <b class="search_numero_estudios">{!! ($paciente->expediente != null) ? $paciente->expediente->estudioscompletadosMedicina->count() : 0 !!}</b> Estudio(s) <br> realizados
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                @empty
                    <p>Aún no hay pacientes</p>
                @endforelse
                </div>
            </div>
            <div class="col-12 mb-2 mt-1">
                <div class="row">
                    <div class="col-12">
                        <h1 id="msg-result" class="d-none">0 resultados</h1>
                        <div class="d-flex align-items-center justify-content-center">
                            <div id="anterior"></div>
                            <ul class="pagination justify-content-center m-0"></ul>
                            <div id="siguiente"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div id="modal_no_estudios" class="modal fade" role="dialog" id="staticBackdrop" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h6 class="modal-title float-left text-white">Sin estudios</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="loader-wrapper mx-auto" style="display:none" id="load">
                    <div class="loader-container">
                        <div class="ball-clip-rotate-multiple loader-success">
                            <div></div>
                            <div></div>
                        </div>
                    </div>
                </div>
                <div class="col-12">
                    <p>El paciente <span id="name_paciente"></span> no cuenta con estudios de medicina registrados.</p>
                </div>
                <div class="col-12 pt-2">
                    <button type="button" data-dismiss="modal" class="mx-auto btn  btn-secondary btn-sm float-right">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js_custom')
<script src="https://cdnjs.cloudflare.com/ajax/libs/list.js/2.3.0/list.min.js"></script>
<script src="{!! asset('/js/Laboratorio/historial_pacientes/pacientes.js') !!}"></script>
@endsection
