<fieldset>
  <div id="examen">

    <div class="row">
      <div class="col-md-3">
        <fieldset class="form-group">
          <label for="apariencia">Apariencia general:</label>
          @if (isset($historial) && !empty($historial['historialfisico']['apariencia']))
          <input type="text" class="form-control" id="apariencia" name="apariencia" placeholder="Observaciones Detectadas" value="{{$historial['historialfisico']['apariencia']}}">
          @else
          <input type="text" class="form-control" id="apariencia" name="apariencia" placeholder="Observaciones Detectadas">
          @endif
        </fieldset>
      </div>
      <div class="col-md-3">
        <fieldset class="form-group">
          <label for="temperatura">Temperatura</label>
          @if (isset($historial) && !empty($historial['historialfisico']['temperatura']))
          <input type="text" class="form-control" id="temperatura" name="temperatura" placeholder="Observaciones Detectadas" value="{{$historial['historialfisico']['temperatura']}}">
          @else
          <input type="text" class="form-control" id="temperatura" name="temperatura" placeholder="Observaciones Detectadas">
          @endif
        </fieldset>
      </div>
      <div class="col-md-3">
        <fieldset class="form-group">
          <label for="fc">Fc:</label>
          @if (isset($historial) && !empty($historial['historialfisico']['fc']))
          <input type="text" class="form-control" id="fc" name="fc" onkeypress='return validaNumericos(event)' placeholder="Observaciones Detectadas" value="{{$historial['historialfisico']['fc']}}">
          @else
          <input type="text" class="form-control" id="fc" name="fc" onkeypress='return validaNumericos(event)' placeholder="Observaciones Detectadas">
          @endif
        </fieldset>
      </div>
      <div class="col-md-3">
        <fieldset class="form-group">
          <label for="fr">Fr:</label>
          @if (isset($historial) && !empty($historial['historialfisico']['fr']))
          <input type="text" class="form-control" id="fr" name="fr" onkeypress='return validaNumericos(event)' placeholder="Observaciones Detectadas" value="{{$historial['historialfisico']['fr']}}">
          @else
          <input type="text" class="form-control" id="fr" name="fr" onkeypress='return validaNumericos(event)' placeholder="Observaciones Detectadas">
          @endif
        </fieldset>
      </div>
      <div class="col-md-2">
        <fieldset class="form-group">
          <label for="altura">Altura:</label>
          @if (isset($historial) && !empty($historial['historialfisico']['altura']))
          <input type="text" class="form-control imc" pattern="^[0-9]+(.[0-9]+)?$" id="altura" placeholder="Metros" name="altura" step="0.01" placeholder="Observaciones Detectadas" value="{{$historial['historialfisico']['altura']}}">
          @else
          <input type="text" class="form-control imc" pattern="^[0-9]+(.[0-9]+)?$" id="altura" placeholder="Metros" name="altura" step="0.01" placeholder="Observaciones Detectadas">
          @endif
        </fieldset>
      </div>
      <div class="col-md-2">
        <fieldset class="form-group">
          <label for="peso">Peso:</label>
          @if (isset($historial) && !empty($historial['historialfisico']['peso']))
          <input type="text" class="form-control imc" id="peso" placeholder="Kilogramos" name="peso" pattern="^[0-9]+(.[0-9]+)?$" placeholder="" value="{{$historial['historialfisico']['peso']}}">
          @else
          <input type="text" class="form-control imc" id="peso" placeholder="Kilogramos" name="peso" pattern="^[0-9]+(.[0-9]+)?$" placeholder="">
          @endif
        </fieldset>
      </div>
      <div class="col-md-2">
        <fieldset class="form-group">
          <label for="imc">IMC</label>
          @if (isset($historial) && !empty($historial['historialfisico']['imc']))
          <input type="text" class="form-control" id="imc" pattern="^[0-9]+(.[0-9]+)?$" name="imc" placeholder="" value="{{$historial['historialfisico']['imc']}}">
          @else
          <input type="text" class="form-control" id="imc" pattern="^[0-9]+(.[0-9]+)?$" name="imc" placeholder="">
          @endif
        </fieldset>
      </div>
      <div class="col-md-3">
        <fieldset class="form-group">
          <label for="presion_derecho">Presión sanguínea: Brazo derecho</label>
          @if (isset($historial) && !empty($historial['historialfisico']['psBrazoDerecho']))
          <input type="text" class="form-control" id="presion_derecho" name="presion_derecho" placeholder="" value="{{$historial['historialfisico']['psBrazoDerecho']}}">
          @else
          <input type="text" class="form-control" id="presion_derecho" name="presion_derecho" placeholder="">
          @endif
        </fieldset>
      </div>
      <div class="col-md-3">
        <fieldset class="form-group">
          <label for="presion_izquierdo">Brazo izquierdo</label>
          @if (isset($historial) && !empty($historial['historialfisico']['psBrazoIzquierdo']))
          <input type="text" class="form-control" id="presion_izquierdo" name="presion_izquierdo" placeholder="" value="{{$historial['historialfisico']['psBrazoIzquierdo']}}">
          @else
          <input type="text" class="form-control" id="presion_izquierdo" name="presion_izquierdo" placeholder="">
          @endif
        </fieldset>
      </div>
      <div class="col-md-4">
        <fieldset class="form-group">
          <label for="observaciones_cabeza">Cabeza y cuero/ojos</label>
          @if (isset($historial) && !empty($historial['historialfisico']['cabezaCueroOjos']))
          <input type="text" class="form-control" id="observaciones_cabeza" name="observaciones_cabeza" placeholder="Observaciones Detectadas" value="{{$historial['historialfisico']['cabezaCueroOjos']}}">
          @else
          <input type="text" class="form-control" id="observaciones_cabeza" name="observaciones_cabeza" placeholder="Observaciones Detectadas">
          @endif
        </fieldset>
      </div>
      <div class="col-md-4">
        <fieldset class="form-group">
          <label for="observaciones_oidos">Oidos/Nariz/Boca</label>
          @if (isset($historial) && !empty($historial['historialfisico']['oidoNarizBoca']))
          <input type="text" class="form-control" id="observaciones_oidos" name="observaciones_oidos" placeholder="Observaciones Detectadas" value="{{$historial['historialfisico']['oidoNarizBoca']}}">
          @else
          <input type="text" class="form-control" id="observaciones_oidos" name="observaciones_oidos" placeholder="Observaciones Detectadas">
          @endif
        </fieldset>
      </div>
      <div class="col-md-4">
        <fieldset class="form-group">
          <label for="observaciones_dientes">Dientes/Faringe</label>
          @if (isset($historial) && !empty($historial['historialfisico']['dientesFaringe']))
          <input type="text" class="form-control" id="observaciones_dientes" name="observaciones_dientes" placeholder="Observaciones Detectadas" value="{{$historial['historialfisico']['dientesFaringe']}}">
          @else
          <input type="text" class="form-control" id="observaciones_dientes" name="observaciones_dientes" placeholder="Observaciones Detectadas">
          @endif
        </fieldset>
      </div>
      <div class="col-md-4">
        <fieldset class="form-group">
          <label for="observaciones_cuello">Cuello</label>
          @if (isset($historial) && !empty($historial['historialfisico']['cuello']))
          <input type="text" class="form-control" id="observaciones_cuello" name="observaciones_cuello" placeholder="Observaciones Detectadas" value="{{$historial['historialfisico']['cuello']}}">
          @else
          <input type="text" class="form-control" id="observaciones_cuello" name="observaciones_cuello" placeholder="Observaciones Detectadas">
          @endif
        </fieldset>
      </div>
      <div class="col-md-4">
        <fieldset class="form-group">
          <label for="observaciones_tiroides">Tiroides</label>
          @if (isset($historial) && !empty($historial['historialfisico']['tiroides']))
          <input type="text" class="form-control" id="observaciones_tiroides" name="observaciones_tiroides" placeholder="Observaciones Detectadas" value="{{$historial['historialfisico']['tiroides']}}">
          @else
          <input type="text" class="form-control" id="observaciones_tiroides" name="observaciones_tiroides" placeholder="Observaciones Detectadas">
          @endif
        </fieldset>
      </div>
      <div class="col-md-4">
        <fieldset class="form-group">
          <label for="observaciones_nodo">Nodo Linfático</label>
          @if (isset($historial) && !empty($historial['historialfisico']['nodoLinfatico']))
          <input type="text" class="form-control" id="observaciones_nodo" name="observaciones_nodo" placeholder="Observaciones Detectadas" value="{{$historial['historialfisico']['nodoLinfatico']}}">
          @else
          <input type="text" class="form-control" id="observaciones_nodo" name="observaciones_nodo" placeholder="Observaciones Detectadas">
          @endif
        </fieldset>
      </div>
      <div class="col-md-4">
        <fieldset class="form-group">
          <label for="observaciones_torax">Torax y Pulmones</label>
          @if (isset($historial) && !empty($historial['historialfisico']['toraxPulmones']))
          <input type="text" class="form-control" id="observaciones_torax" name="observaciones_torax" placeholder="Observaciones Detectadas" value="{{$historial['historialfisico']['toraxPulmones']}}">
          @else
          <input type="text" class="form-control" id="observaciones_torax" name="observaciones_torax" placeholder="Observaciones Detectadas">
          @endif
        </fieldset>
      </div>
      <div class="col-md-4">
        <fieldset class="form-group">
          <label for="observaciones_pecho">Pecho</label>
          @if (isset($historial) && !empty($historial['historialfisico']['pecho']))
          <input type="text" class="form-control" id="observaciones_pecho" name="observaciones_pecho" placeholder="Observaciones Detectadas" value="{{$historial['historialfisico']['pecho']}}">
          @else
          <input type="text" class="form-control" id="observaciones_pecho" name="observaciones_pecho" placeholder="Observaciones Detectadas">
          @endif
        </fieldset>
      </div>
      <div class="col-md-4">
        <fieldset class="form-group">
          <label for="observaciones_corazon">Corazón</label>
          @if (isset($historial) && !empty($historial['historialfisico']['corazon']))
          <input type="text" class="form-control" id="observaciones_corazon" name="observaciones_corazon" placeholder="Observaciones Detectadas" value="{{$historial['historialfisico']['corazon']}}">
          @else
          <input type="text" class="form-control" id="observaciones_corazon" name="observaciones_corazon" placeholder="Observaciones Detectadas">
          @endif
        </fieldset>
      </div>
      <div class="col-md-4">
        <fieldset class="form-group">
          <label for="observaciones_abdomen">Abdomen</label>
          @if (isset($historial) && !empty($historial['historialfisico']['abdomen']))
          <input type="text" class="form-control" id="observaciones_abdomen" name="observaciones_abdomen" placeholder="Observaciones Detectadas" value="{{$historial['historialfisico']['abdomen']}}">
          @else
          <input type="text" class="form-control" id="observaciones_abdomen" name="observaciones_abdomen" placeholder="Observaciones Detectadas">
          @endif
        </fieldset>
      </div>
      <div class="col-md-4">
        <fieldset class="form-group">
          <label for="observaciones_rectal">Rectal Digital</label>
          @if (isset($historial) && !empty($historial['historialfisico']['rectalDigital']))
          <input type="text" class="form-control" id="observaciones_rectal" name="observaciones_rectal" placeholder="Observaciones Detectadas" value="{{$historial['historialfisico']['rectalDigital']}}">
          @else
          <input type="text" class="form-control" id="observaciones_rectal" name="observaciones_rectal" placeholder="Observaciones Detectadas">
          @endif
        </fieldset>
      </div>
      <div class="col-md-4">
        <fieldset class="form-group">
          <label for="observaciones_genitales">Genitales</label>
          @if (isset($historial) && !empty($historial['historialfisico']['genitales']))
          <input type="text" class="form-control" id="observaciones_genitales" name="observaciones_genitales" placeholder="Observaciones Detectadas" value="{{$historial['historialfisico']['genitales']}}">
          @else
          <input type="text" class="form-control" id="observaciones_genitales" name="observaciones_genitales" placeholder="Observaciones Detectadas">
          @endif
        </fieldset>
      </div>
      <div class="col-md-4">
        <fieldset class="form-group">
          <label for="observaciones_columna">Columna vertebral</label>
          @if (isset($historial) && !empty($historial['historialfisico']['columnaVertebral']))
          <input type="text" class="form-control" id="observaciones_columna" name="observaciones_columna" placeholder="Observaciones Detectadas" value="{{$historial['historialfisico']['columnaVertebral']}}">
          @else
          <input type="text" class="form-control" id="observaciones_columna" name="observaciones_columna" placeholder="Observaciones Detectadas">
          @endif
        </fieldset>
      </div>
      <div class="col-md-4">
        <fieldset class="form-group">
          <label for="observaciones_piel">Piel</label>
          @if (isset($historial) && !empty($historial['historialfisico']['piel']))
          <input type="text" class="form-control" id="observaciones_piel" name="observaciones_piel" placeholder="Observaciones Detectadas" value="{{$historial['historialfisico']['piel']}}">
          @else
          <input type="text" class="form-control" id="observaciones_piel" name="observaciones_piel" placeholder="Observaciones Detectadas">
          @endif
        </fieldset>
      </div>
      <div class="col-md-4">
        <fieldset class="form-group">
          <label for="observaciones_pulso">Pulso Arteral</label>
          @if (isset($historial) && !empty($historial['historialfisico']['pulsoArterial']))
          <input type="text" class="form-control" id="observaciones_pulso" name="observaciones_pulso" placeholder="Observaciones Detectadas" value="{{$historial['historialfisico']['pulsoArterial']}}">
          @else
          <input type="text" class="form-control" id="observaciones_pulso" name="observaciones_pulso" placeholder="Observaciones Detectadas">
          @endif
        </fieldset>
      </div>
      <div class="col-md-4">
        <fieldset class="form-group">
          <label for="observaciones_extremidades">Extremidades</label>
          @if (isset($historial) && !empty($historial['historialfisico']['extremidades']))
          <input type="text" class="form-control" id="observaciones_extremidades" name="observaciones_extremidades" placeholder="Observaciones Detectadas" value="{{$historial['historialfisico']['extremidades']}}">
          @else
          <input type="text" class="form-control" id="observaciones_extremidades" name="observaciones_extremidades" placeholder="Observaciones Detectadas">
          @endif
        </fieldset>
      </div>
      <div class="col-md-4">
        <fieldset class="form-group">
          <label for="observaciones_musculoesque">Musculoesqueleto</label>
          @if (isset($historial) && !empty($historial['historialfisico']['musculoesqueleto']))
          <input type="text" class="form-control" id="observaciones_musculoesque" name="observaciones_musculoesque" placeholder="Observaciones Detectadas" value="{{$historial['historialfisico']['musculoesqueleto']}}">
          @else
          <input type="text" class="form-control" id="observaciones_musculoesque" name="observaciones_musculoesque" placeholder="Observaciones Detectadas">
          @endif
        </fieldset>
      </div>
      <div class="col-md-4">
        <fieldset class="form-group">
          <label for="observaciones_relejos">Reflejos y Neurológicos</label>
          @if (isset($historial) && !empty($historial['historialfisico']['reflejosNeurologicos']))
          <input type="text" class="form-control" id="observaciones_relejos" name="observaciones_relejos" placeholder="Observaciones Detectadas" value="{{$historial['historialfisico']['reflejosNeurologicos']}}">
          @else
          <input type="text" class="form-control" id="observaciones_relejos" name="observaciones_relejos" placeholder="Observaciones Detectadas">
          @endif
        </fieldset>
      </div>
      <div class="col-md-4">
        <fieldset class="form-group">
          <label for="observaciones_mental">Estado Mental</label>
          @if (isset($historial) && !empty($historial['historialfisico']['estadoMental']))
          <input type="text" class="form-control" id="observaciones_mental" name="observaciones_mental" placeholder="Observaciones Detectadas" value="{{$historial['historialfisico']['estadoMental']}}">
          @else
          <input type="text" class="form-control" id="observaciones_mental" name="observaciones_mental" placeholder="Observaciones Detectadas">
          @endif
        </fieldset>
      </div>
      <div class="col-md-4">
        <fieldset class="form-group">
          <label for="observaciones_comentarios">Otros Comentarios</label>
          @if (isset($historial) && !empty($historial['historialfisico']['otrosComentarios']))
          <input type="text" class="form-control" id="observaciones_comentarios" name="observaciones_comentarios" placeholder="Observaciones Detectadas" value="{{$historial['historialfisico']['otrosComentarios']}}">
          @else
          <input type="text" class="form-control" id="observaciones_comentarios" name="observaciones_comentarios" placeholder="Observaciones Detectadas">
          @endif
        </fieldset>
      </div>
    </div>
    <hr>
    <p class="text-center">Agudeza Visual</p>
    <hr>
    <div class="row">
      <div class="col-md-6">
        <fieldset class="form-group">
          <label for="agudeza_izquierda">Ojo Izquierdo</label>
          @if (isset($historial) && !empty($historial['historialfisico']['ojoIzquierdo']))
          <input type="text" class="form-control" id="agudeza_izquierda" name="agudeza_izquierdo" placeholder="" value="{{$historial['historialfisico']['ojoIzquierdo']}}">
          @else
          <input type="text" class="form-control" id="agudeza_izquierda" name="agudeza_izquierdo" placeholder="">
          @endif
        </fieldset>
      </div>
      <div class="col-md-6">
        <fieldset class="form-group">
          <label for="agudeza_derecho">Ojo Derecho</label>
          @if (isset($historial) && !empty($historial['historialfisico']['ojoDerecho']))
          <input type="text" class="form-control" id="agudeza_derecho" name="agudeza_derecho" placeholder="" value="{{$historial['historialfisico']['ojoDerecho']}}">
          @else
          <input type="text" class="form-control" id="agudeza_derecho" name="agudeza_derecho" placeholder="">
          @endif
        </fieldset>
      </div>
    </div>
    <hr>
    <p class="text-center">Valoración y plan de recomendaciones del personal médico</p>
    <hr>
    <div class="row">
      <div class="col-md-6">
        <fieldset class="form-group">
          <label for="uso_epp">Uso de EPP:</label>
          @if (isset($historial) && !empty($historial['historialfisico']['uso_epp']))
          <input type="text" class="form-control" id="uso_epp" name="uso_epp" placeholder="" value="{{$historial['historialfisico']['uso_epp']}}">
          @else
          <input type="text" class="form-control" id="uso_epp" name="uso_epp" placeholder="">
          @endif
        </fieldset>
      </div>
      <div class="col-md-6">
        <fieldset class="form-group">
          <label for="audiometria">Audiometria</label>
          @if (isset($historial) && !empty($historial['historialfisico']['audiometria']))
          <input type="text" class="form-control" id="audiometria" name="audiometria" placeholder="" value="{{$historial['historialfisico']['audiometria']}}">
          @else
          <input type="text" class="form-control" id="audiometria" name="audiometria" placeholder="">
          @endif
        </fieldset>
      </div>
      <div class="col-md-6">
        <fieldset class="form-group">
          <label for="respiradores_r">Respiradoes</label>
          @if (isset($historial) && !empty($historial['historialfisico']['respiradores']))
          <input type="text" class="form-control" id="respiradores_r" name="respiradores_r" placeholder="" value="{{$historial['historialfisico']['respiradores']}}">
          @else
          <input type="text" class="form-control" id="respiradores_r" name="respiradores_r" placeholder="">
          @endif
        </fieldset>
      </div>
      <div class="col-md-6">
        <fieldset class="form-group">
          <label for="reentrenar">Reentrenar en:</label>
          @if (isset($historial) && !empty($historial['historialfisico']['reentrenar']))
          <input type="text" class="form-control" id="reentrenar" name="reentrenar" placeholder="" value="{{$historial['historialfisico']['reentrenar']}}">
          @else
          <input type="text" class="form-control" id="reentrenar" name="reentrenar" placeholder="">
          @endif
        </fieldset>
      </div>
      <div class="col-md-6">
        <fieldset class="form-group">
          <label for="otra_recomendacion">Otros</label>
          @if (isset($historial) && !empty($historial['historialfisico']['otrasRecomend']))
          <input type="text" class="form-control" id="otra_recomendacion" name="otra_recomendacion" placeholder="" value="{{$historial['historialfisico']['otrasRecomend']}}">
          @else
          <input type="text" class="form-control" id="otra_recomendacion" name="otra_recomendacion" placeholder="">
          @endif
        </fieldset>
      </div>
    </div>
    <hr>
    <p class="text-center">Recomendaciones de estilo de vida</p>
    <hr>
    @php
    $recomendaciones = array(
    'Df' => 'Dejar de fumar',
    'RA' => 'Reducir consume de alcohol',
    'BP' =>'Bajar de peso',
    'TRC' => 'Tratamineto para reducir de colesterol',
    'ME' => 'Manejo de estres',
    'EPP' => 'Examen del pecho personal',
    'ETP' => 'Examen Testicular Personal',
    'RE' => 'Rutina de ejercicio',
    'PRE' => 'Programa regular de ejercicios',
    'O' => 'Otros',
    );
    @endphp
    <div class="row">
      <div class="col-md-6">
        <fieldset class="form-group">
          <label for="reentrenar">Recomendaciones</label>
          <select class="selectize-multiple" class="form-control" name="recomendaciones_vida[]" placeholder="Recomendaciones" multiple>
            @if (isset($historial) && !empty($historial['historialfisico']['recomEstiloVida']))
              @foreach ($historial['historialfisico']['recomEstiloVida'] as $key => $recomendacionesBD)
                ///
                @foreach ($recomendaciones as $keyBD => $value)
                  @if ($keyBD == $recomendacionesBD)
                    <option selected value={{$keyBD}}>{{$value}}</option>
                    @unset($recomendaciones[$keyBD]);
                  @endif
                @endforeach
                ////
            @endforeach
            @foreach ($recomendaciones as $key => $value)
            <option value={{$key}}>{{$value}}</option>
            @endforeach
              @else
                @foreach ($recomendaciones as $key => $value)
                <option value={{$key}}>{{$value}}</option>
                @endforeach
            @endif

          </select>
        </fieldset>
      </div>
      <div class="col-md-6">
        <fieldset class="form-group">
          <label for="diagnostico_audiologico">Diagnostico Audiologico</label>
          @if (isset($historial) && !empty($historial['historialfisico']['diagnosticoAudio']))
            <textarea name="diagnostico_audiologico" id="diagnostico_audiologico" class="form-control" rows="8" cols="80">{{$historial['historialfisico']['diagnosticoAudio']}}</textarea>
          @else
            <textarea name="diagnostico_audiologico" id="diagnostico_audiologico" class="form-control" rows="8" cols="80"></textarea>
          @endif
        </fieldset>
      </div>
    </div>

  </div>
  </fieldset>
