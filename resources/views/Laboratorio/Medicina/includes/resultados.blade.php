<fieldset>
  <div id="resultado">
    <div id="AlertaResultado">

    </div>
    <div class="row">
      <div class="col-md-6">
        <fieldset class="form-group">
          <label for="estado_salud">Estado de salud</label>
          @if (isset($historial) && !empty($historial['resultado']['estadoSalud']))
          <input type="text" class="form-control" id="estado_salud" name="estado_salud" placeholder="" id="estado_salud" value="{{$historial['resultado']['estadoSalud']}}">
          @else
          <input type="text" class="form-control" id="estado_salud" name="estado_salud" placeholder="" id="estado_salud" required>
          @endif
        </fieldset>
      </div>
      @php
      $resultados = array(
      'Cumple Requerimientos Mèdicos (Satisfactorios)' => 'Cumple Requerimientos Médicos (Satisfactorio)',
      'Cumple Requerimientos Médicos Condicionados (Satisfactorio Condicionado)' => 'Cumple Requerimientos Médicos Condicionados (Satisfactorio Condicionado)',
      'No Cumple Requerimientos Médicos (No Satisfactorios)' => 'No Cumple Requerimientos Médicos (No Satisfactorios)'
      );
      @endphp
      <div class="col-md-6">
        <fieldset class="form-group">
          <label for="estado_salud">Resultados de Aptitud</label>
          <select class="form-control custom-select" name="resultado_aptitud">
            @if (isset($historial) && !empty($historial['resultado']['aptitud']))
            @foreach ($resultados as $key => $value)
            @if ($historial['resultado']['aptitud'] == $value)
            <option selected value={{$value}}>{{$value}}</option>
            @unset($resultados[$value]);
            @endif
            @endforeach
            @foreach ($resultados as $key => $value)
            <option value={{$value}}>{{$value}}</option>
            @endforeach
            @else
            @foreach ($resultados as $key => $value)
            <option value={{$value}}>{{$value}}</option>
            @endforeach
            @endif
          </select>
        </fieldset>
      </div>
      <div class="col-md-12">
        <fieldset class="form-group">
          <label for="comentarios_finales">Comentarios</label>
          @if (isset($historial) && !empty($historial['resultado']['comentarios']))
          <textarea class="form-control" name="comentarios_finales" id="comentarios_finales" rows="8" cols="80">{{$historial['resultado']['comentarios']}}</textarea>
          @else
          <textarea class="form-control" name="comentarios_finales" id="comentarios_finales" rows="8" cols="80"></textarea>
          @endif
        </fieldset>
      </div>
    </div>
  </div>
  </fieldset>
