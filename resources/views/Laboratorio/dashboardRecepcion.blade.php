@extends('layouts.laboratorioApp')

@section('title', 'Recepción')

@section('styles')
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous"-->
    <link rel="stylesheet" href="../resources/sass/css/normalize.css">
    <link rel="stylesheet" href="../resources/sass/fontawesome/css/all.css">
    <link rel="stylesheet" type="text/css" href="../resources/sass/css/styleScroll.css">
    <link rel="stylesheet" type="text/css" href="../resources/sass/css/Laboratorio/menu_styles.css">
    <link rel="stylesheet" type="text/css" href="../resources/sass/css/Laboratorio/laboratorio_styles.css">
@endsection

@section('content')
    <section class="principal">


            <div class="title-content">
                <h1 class="title">Empresas Registradas</h1>
            </div>

            <div class="panel panel-default">
             <div class="panel-heading">
              <h3 class="panel-title"></h3>
             </div>
             <div class="panel-body">
               <header>
                   <div class="barra-busqueda">
                       <input type="text" name="texto-busqueda" class="campo-busqueda form-control my-0 py-1" placeholder="Nombre de la empresa">
                       <div class="input-group-append">
                         <span class="input-group-text red lighten-3" id="basic-text1"><i class="fas fa-search text-grey"
                             aria-hidden="true"></i></span>
                       </div>
                   </div>
               </header>
               <div class="container-fluid">
                   <div class="container">
                       <div class="row iconos-empresas">
                           @forelse ($empresas as $empresa)
                               <div class="card-empresa card text-center col-sm-4">
                                   <div class="title" >
                                       <img id="{{$empresa->logo}}" src="{{route('getLogoEmpresa', ['filename'=>$empresa->logo])}}" alt="Logo de {{ $empresa->nombre }}" class="img-fluid rounded-circle img-thumbnail shadow-sm imagen-logo">
                                       <hr>
                                       <h2>{{ $empresa->nombre }}</h2>
                                   </div>
                                   <a href="{{route('lab-infoEmpresa', $empresa->nombre)}}" id="btn-ver-empresa" class="btn btn-custom">Ver</a>
                               </div>
                           @empty
                               <p>Aún no hay empresas registradas en el sistema</p>
                           @endforelse

                           <div class="aviso-vacio" style="display: none;">
                               <p>Ninguna empresa coincide con la búsqueda.</p>
                           </div>
                       </div>
                   </div>
               </div>

             </div>
            </div>


    </section>

@endsection

@section('specificScripts')

    <script src="../resources/js/Laboratorio/laboratorio_recepcion.js"></script>
@endsection
