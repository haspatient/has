<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title></title>
	<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css"><link rel="stylesheet" type="text/css" href="https://ac-labs.com.mx/htds/Agenda/resources/sass/css/style_mail.css">
</head>
<body>
	<div class="img">
		<img height="200" width="200" src="https://has.humanly-sw.com/public/new_logo_has.svg" alt="Diagnosticos" >
	</div>
	<div class="content">
    <h1>Health Administration Solution</h1>
	<p>
		Un Administrador de Health Administration Solution ha creado una cuenta para usted. Ahora puede iniciar una sesión haciendo clic en este enlace o copiándolo y pegándolo en su navegador
		<a href="https://has.humanly-sw.com">https://has.humanly-sw.com</a>
	</p>
	<p>
		Podrá iniciar sesión identificándose con los datos.
	</p>
	<strong>Correo: <br>{{$correoAdmin}}</strong><br>
	<strong>Contraseña: <br>{{$passwordAdmin}}</strong><br>
	<h3>El equipo de Health Administration Solution</h3>
	</div>

</body>
</html>
