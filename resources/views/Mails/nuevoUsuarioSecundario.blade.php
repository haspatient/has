<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title></title>
	<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css"><link rel="stylesheet" type="text/css" href="../resources/sass/css/style_mail.css">
</head>
<body>
	<div class="img">
		<img src="{!! asset('storage/app/empresas/'.\Session::get('empresa')->logo)  !!}" alt="{{ $empresa->nombre }}" >
	</div>
	<div class="content">
    <h1>Health Administration Solution </h1>
	<p>
		Un Administrador de {{ $empresa->nombre }} ha creado una cuenta para usted. Ahora puede iniciar una sesión haciendo clic en este enlace o copiándolo y pegándolo en su navegador
		<a href="https://has.humanly-sw.com">has.humanly-sw.com/</a>
	</p>
	<p>
		Podrá iniciar sesión identificándose con los datos siguientes.
	</p>
	<strong>Usuario: {{$correo}}</strong>
	<br>
	<strong>Password: {{$user_password}}</strong>
	<h3>El equipo de {{ $empresa->nombre }}</h3>
	</div>

</body>
</html>
