 <div class="vertical-nav" id="sidebar">
  <div class="py-2 px-1 mb-1">
    <div class="media d-flex align-items-center">

      <a href="{{route('admin')}}">
        <img src="{!! url('resources/sass/images/login.png') !!}" alt="..." width="80" class="mr-3 rounded-circle img-thumbnail shadow-sm">
      </a>
    </div>
    <div class="media d-flex align-items-center">
        <a href="{{route('admin')}}">
          <img src="{!! url('resources/sass/images/perfil.jpg') !!}" alt="..." width="80" class="mr-3 rounded-circle img-thumbnail shadow-sm">
        </a>
    </div>
  </div>
  <ul class="nav flex-column bg-white mb-0 mendes ">
  <li class="nav-item">
      <a href="{{route('empresa')}}" title="Registro de Empresas"  class="nav-link text-dark {{request()->routeIs('empresa') ? 'actived':''}}">
        <i class="fa fa-briefcase fa-fw"></i>
     </a>
    </li>
    <li class="nav-item">
        <a href="{{ route('admin-verEmpresas') }}" class="nav-link text-dark">
         <i class="fa fa-search fa-fw"></i>
      </a>
    </li>
    <li class="nav-item">
      <a href="{{route('estudios')}}" class="nav-link text-dark {{request()->routeIs('estudios') ? 'actived':''}}">
         <i class="fa fa-file-alt fa-fw"></i>
      </a>
    </li>
    <li class="nav-item logout">
        <div class="n1">
      <button type="submit" class="nav-link text-dark">
        <i class="fa fa-user-cog"></i>
      </button>
      </div>
      <div class="n2">
      <form action="{{ route('logout') }}" method="POST">
        @csrf
        <button type="submit" class="nav-link text-dark">
          <i class="fa fa-sign-out-alt fa-fw"></i>
        </button>
      </form>
      </div>
    </li>
  </ul >

  </div>
