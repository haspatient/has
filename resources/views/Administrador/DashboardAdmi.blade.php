@extends('Administrador.AppLayout')
@section('title')
HAS - Admin
@endsection

@section('content')
<div class="row">
    <div class="col-lg-4 col-sm-12">
        <div class="card">
            <div class="card-content">
                <img class="card-img img-fluid" src="{!! asset('public/vuexy') !!}/app-assets/images/slider/04.jpg" alt="Card image">
                <div class="card-img-overlay overflow-hidden overlay-danger overlay-lighten-2">
                    <h4 class="card-title text-white">Card Image Overlay</h4>
                    <p class="card-text text-white">Sugar plum tiramisu sweet. Cake jelly marshmallow cotton candy chupa chups.</p>
                    <p class="card-text"><small class="text-white">Last updated 3 mins ago</small></p>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-4 col-sm-12">
        <div class="card">
            <div class="card-content">
                <img class="card-img img-fluid" src="{!! asset('public/vuexy') !!}/app-assets/images/slider/04.jpg" alt="Card image">
                <div class="card-img-overlay overflow-hidden overlay-danger overlay-lighten-2">
                    <h4 class="card-title text-white">Card Image Overlay</h4>
                    <p class="card-text text-white">Sugar plum tiramisu sweet. Cake jelly marshmallow cotton candy chupa chups.</p>
                    <p class="card-text"><small class="text-white">Last updated 3 mins ago</small></p>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-4 col-sm-12">
        <div class="card">
            <div class="card-content">
                <img class="card-img img-fluid" src="{!! asset('public/vuexy') !!}/app-assets/images/slider/04.jpg" alt="Card image">
                <div class="card-img-overlay overflow-hidden overlay-danger overlay-lighten-2">
                    <h4 class="card-title text-white">Card Image Overlay</h4>
                    <p class="card-text text-white">Sugar plum tiramisu sweet. Cake jelly marshmallow cotton candy chupa chups.</p>
                    <p class="card-text"><small class="text-white">Last updated 3 mins ago</small></p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
