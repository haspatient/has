<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>HAS Admin</title>
    <link rel="shortcut icon" href="https://www.laboratorioasesores.com/assets/frontend//img/favicon_asesores.png" type="image/png">
    <link rel="stylesheet" type="text/css" href="{!! asset('resources/sass/css/style_admin_menu.css') !!}  ">
    <link rel="stylesheet" type="text/css" href="{!! asset('resources/sass/css/Administrador/style_menu.css') !!} ">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link href="{!! asset('/resources/sass/fontawesome/css/all.') !!} " rel="stylesheet">
</head>
<body>
    <!-- End vertical navbar -->
    @include('..layouts.menuE')
    <!-- Page content holder -->

    <div class="content_present content_margin"></div>

	<div class="content_menu content_margin">
        <div class="content_text">
            <h1>
                Inicio
            </h1>
            <h3>
                Administrador
            </h3>
        </div>
        <div class="reference">
            <div>
                <a href="{{ route('admin-verEmpresas') }}">
                    <i class="fas fa-building"></i>
                </a>
                <h3>Empresas</h3>
            </div>
            <div>
                <a href="{{route('empresa')}}">
                    <i class="far fa-building"></i>
                </a>
                <h3>Alta de empresas</h3>
            </div>
            <div>
                <a href="{{route('estudios')}}">
                    <i class="far fa-file-alt"></i>
                </a>
                <h3>Estudios</h3>
            </div>
        </div>
	</div>

<!-- End demo content -->
</body>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.bundle.min.js"></script>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
</html>
