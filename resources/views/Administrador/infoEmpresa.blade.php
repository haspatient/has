{{-- @extends('layouts.administradorApp') --}}
@extends('Administrador.AppLayout')

@section('title', $empresa->nombre)

@section('page_css')
<link rel="stylesheet" href="https://expedienteclinico.humanly-sw.com/dev/public/css/empresa/info_paciente.css">
    <style>
        .imagen-logo{
            object-fit: cover;height:150px;width:150px;border: none;
        }
    </style>
@endsection

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="alerts">
                @if(session('message'))
                    <div class="alert alert-success" >
                    {{session('message')}}
                    </div>
                @elseif ($errors->any())
                    <div class="alert alert-danger" >
                        <p>Ha ocurrido un error.</p>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{$error}}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
        </div>
        <div class="col-4">
            <div class="name-logo">
                @if($empresa->foto)
                <img id="{{$empresa->logo}}" src="{!! url('storage/app/empresas/'.$empresa->logo) !!}" class="img-fluid rounded-circle img-thumbnail shadow-sm imagen-logo">
                @endif
                <h1>{{ $empresa->nombre }}</h1>
                <hr>
                <button type="button" onclick="showModalLogotipo()" class="btn mr-1 mb-1 btn-primary btn-sm waves-effect waves-light">Cambiar logotipo</button>
            </div>
            <div style="margin-top:3%">
                <button type="button"  onclick="EmpresaAdmin('{{$empresa->nombre}}')" class="btn mr-1 mb-1 btn-primary btn-sm waves-effect waves-light">Ir dashboard admin empresa  -></button>
            </div>
        </div>
        <div class="col-8">
            <section class="derecha-arriba">
                <h2>Información de la empresa</h2>
                <div class="datos-empresa">
                    <p><strong>Dirección:</strong> {{ $empresa->direccion }}</p>
                    <p><strong>Giro:</strong> {{ $empresa->giro->nombre }}</p>
                    <p><strong>Teléfono:</strong> {{ $empresa->telefono }}</p>
                    <p><strong>Página web:</strong> {{ $empresa->pagina }}</p>
                </div>
                <div class="botones-empresa">
                    {{-- <button class="btn mr-1 mb-1 btn-primary btn-sm waves-effect waves-light" onclick="showEstudiosModal( '{{$encryptedID}}')">Estudios</button> --}}
                    <a href="{{  route('editarEmpresa', $empresa->nombre) }}" class="btn mr-1 mb-1 btn-primary btn-sm waves-effect waves-light">Modificar datos de la empresa</a>
                </div>
            </section>
            <section class="derecha-abajo">
                <h2>Información del contacto de la empresa</h2>
                <div class="datos-contacto">
                    <p><strong>Nombre:</strong> {{ $contacto->nombre }} {{ $contacto->apellido_paterno }} {{ $contacto->apellido_materno }}</p>
                    <p><strong>Correo electrónico:</strong> {{ $contacto->email }}</p>
                    <p><strong>Área:</strong> {{ $contacto->area }}</p>
                    <p><strong>Puesto:</strong> {{ $contacto->puesto }}</p>
                    <p><strong>Teléfono:</strong> {{ $contacto->telefono }}</p>
                </div>
                <div class="botones-contacto">
                    <a href="{{  route('editarContacto', $empresa->nombre) }}" class="btn mr-1 mb-1 btn-primary btn-sm waves-effect waves-light">Modificar datos del contacto</a>
                </div>
            </section>
        </div>
    </div>
    <form role="form" id="registro_empresa" method="post" action="{{route('cambiarLogotipo')}}" enctype="multipart/form-data">
        @csrf
        <div id="modalLogotipo" class="modal fade" role="dialog" id="staticBackdrop" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
            <div class="modal-dialog">
                {{-- Modal content --}}
                <div class="modal-content">
                    <div class="modal-header bg-primary">
                        <h4 class="modal-title float-left text-white" id="estudio_title">Cambiar logotipo de empresa </h4>
                    </div>
                    <div class="modal-body text-center">
                        <div class="form text-center">
                            <input type="hidden" name="encryptedID" value="{{ $encryptedID }}" id="inputEncryptedID">
                            @if($empresa->foto)
                            <img id="load_image" src="{!! url('storage/app/empresas/'.$empresa->logo) !!}" alt="Logo de {{ $empresa->nombre }}" class="img-fluid rounded-circle img-thumbnail shadow-sm imagen-logo">
                            @endif
                            <div class="text-center">
                                <label class="fileContainer btn btn-primary mr-1 waves-effect waves-light" style="float: initial !important">
                                    Cargar imagen <input type="file" accept="image/*" name="logo" id="logo" >
                                </label>
                            </div>
                            @error('nombre')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-primary btn-guardar" type="submit">Guardar cambios</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection

@section('page_js')
    <script src="{!! asset('/') !!}/resources/js/Administrador/infoEmpresa.js"></script>
@endsection
