<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Administrador</title>
  <link rel="shortcut icon" href="https://www.laboratorioasesores.com/assets/frontend//img/favicon_asesores.png" type="image/png">
  <link rel="stylesheet" type="text/css" href="../../resources/sass/css/style_admin_menu.css">
  <link rel="stylesheet" type="text/css" href="../../resources/sass/css/Administrador/style_vistaE.css">
   <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
   <link href="../../resources/sass/fontawesome/css/all.css" rel="stylesheet">
</head>
<body>

<!-- End vertical navbar -->
@include('..layouts.menuAdministrador')
<!-- Page content holder -->
<div class="content">
<div class="clsimg">
<!--img src="../resources/sass/images/fond.png" class="icls" alt=""-->
</div>
<div class="row">
    <div class="div1">

    <div class="imag">
        <h6 class="txt">Nombre de la empresa</h6>
                         <div class="imgen">
                           <img src="../resources/sass/images/paisa.png" id="" class="img-fluid" alt="">
                         </div>
                       <span class="btn btn-primary btn-file">
                             Subir archivo <input type="file">
                        </span>
                       </div>

    </div>
    <div class="div2">
        <h6 class="titl">
            Datos de la empresa
        </h6>
        <div class="clsizq">
          <ul style="none">
        <li><i class="fas fa-map-marker-alt"></i><p class="p">Lorem ipsum dolor sit, amet</p></li>
        <li><i class="fas fa-briefcase"></i><p class="p">Lorem ipsum dolor sit, amet</p></li>
        <li><i class="fas fa-copy"></i><p class="p">Lorem ipsum dolor sit, amet</p></li>
          </ul>
 
      </div>
      <div class="clsder">
      <ul style="none">
        <li><i class="fas fa-phone-alt"></i><p class="pa">Lorem ipsum dolor sit, amet</p></li>
        <li><i class="fas fa-globe"></i><p class="pa">Lorem ipsum dolor sit, amet</p></li>
        <li><i class="fas fa-pencil-alt"></i><p class="pa">Lorem ipsum dolor sit, amet</p></li>
          </ul>
        
        
        
      </div>
    </div>
    <div class="div3">
    <h6 class="titl">Datos de contacto </h6>
    <div class="cl1">
    <ul style="none">
        <li><i class="far fa-user"></i><p class="p">Lorem ipsum dolor sit, amet</p></li>
        <li><i class="fas fa-at"></i><p class="p">Lorem ipsum dolor sit, amet</p></li>
          </ul>
          </div>
    <div class="cl2">
    <ul style="none">
        <li><i class="fas fa-briefcase"></i><p class="pa">Lorem ipsum dolor sit, amet</p></li>
        <li><i class="fas fa-pencil-alt"></i><p class="pa">Lorem ipsum dolor sit, amet</p></li>
          </ul>
    </div>
    </div>
  </div>
</div>

<!-- End demo content -->
</body>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.bundle.min.js"></script>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
</html>