<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Administrador</title>
  <link rel="shortcut icon" href="https://www.laboratorioasesores.com/assets/frontend//img/favicon_asesores.png" type="image/png">
  <link rel="stylesheet" type="text/css" href="../resources/sass/css/style_admin_menu.css">
  <link rel="stylesheet" type="text/css" href="../resources/sass/css/Administrador/style_contact.css">
   <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
   <link href="../resources/sass/fontawesome/css/all.css" rel="stylesheet">
</head>
<body>
<!-- End vertical navbar -->
@include('..layouts.menuAdministrador')
<!-- Page content holder -->
<div class="conten ">

<h1 class="h1txt">Datos de Contacto
</h1>
<div class="contback">
<img class="fontim" src="../resources/sass/images/cru.png" alt="">
</div>
<div class="conticon">
<h1><span class="iconocolor"><i class="far fa-user clasic"></i></span>
</h1>
</div>
<div class="contizq">
<div class="container">
    <div class="">
    <div class="col-md-6">
    <div class="well well-sm">
    <form class="form-horizontal" method="post">
        <fieldset>
        <div class="grpicon">
            <span class="col-md-1 col-md-offset-2 text-center"><i class="fas fa-map-marker-alt"> </i><p class="texto">loreeeeeeeeeeeeeeee</p>
            </span>
        </div>
        <div class="grpicon">
            <span class="col-md-1 col-md-offset-2 text-center"><i class="fas fa-briefcase"></i><p class="texto">loxasnjxcbsdcjnvscbjcbjscbdssj</p>
            </span>
        </div>

        <div class="grpicon">
            <span class="col-md-1 col-md-offset-2 text-center "><i class="fa fa-file-alt fa-fw icnfile grpicon" id=icnfile></i><p class="texto">scsioncsdocbscosifubsdchsdjcskcb</p>
            </span>
        </div>

        <div class="grpicon">
            <span class="col-md-1 col-md-offset-2 text-center"><i class="fas fa-phone-alt"></i><p class="texto">4462315484345</p>
            </span>
        </div>

        <div class="grpicon">
            <span class="col-md-1 col-md-offset-2 text-center"><i class="fas fa-globe"></i><p class="texto">pag.web.com.mx</p></span>
        </div>
        </fieldset>
    </form>
    </div>
    </div>
    </div>
</div>
</div>
<div class="contdere">

    <div class="row">
        <div class="col-md-12">
            <div class="well well-sm">
                <form class="form-horizontal" method="post">
                    <fieldset>
                <div class="form-group ">
                <div class="col-md-8">
                     <input class="inpsty" id="" name="" type="text" placeholder="Nombre y Apellido" class="form-control">
                </div>
                </div>
                <div class="form-group">
                <div class="col-md-8">
                    <input class="inpsty" id="" name="" type="text" placeholder="Edad" class="form-control">
                </div>
                </div>

                <div class="form-group">
                <div class="col-md-8">
                    <input class="inpsty" id="" name="" type="text" placeholder="Género" class="form-control">
                </div>
                </div>

                <div class="form-group">
                <div class="col-md-8">
                    <input class="inpsty" id="" name="" type="text" placeholder="Fecha de Nacimiento" class="form-control">
                </div>
                </div>

                <div class="form-group">
                <div class="col-md-8">
                    <input class="inpsty" id="" name="" type="text" placeholder="Área" class="form-control">
                </div>
                </div>

                <div class="form-group">
                    <div class="col-md-12 text-center">
                        <button type="submit" class="btn btn-primary btn-lg btnclas">Guardar</button>
                    </div>
                </div>
                    </fieldset>
                </form>
            </div>
        </div>
    </div>
</div>
</div>
</div>


<!-- End demo content -->
</body>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.bundle.min.js"></script>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
</html>
