{{-- @extends('layouts.administradorApp') --}}
@extends('Administrador.AppLayout')
@section('title')
Empresas
@endsection

@section('title', 'Empresas')

@section('begin_vendor_css')
    <!-- BEGIN VENDOR CSS-->
        <link rel="stylesheet" type="text/css" href="{!! url("app-assets/vendors/css/forms/selects/selectize.css") !!}">
        <link rel="stylesheet" type="text/css" href="{!! url("app-assets/vendors/css/forms/selects/selectize.default.css") !!}">
        <link rel="stylesheet" type="text/css" href="{!! url("app-assets/vendors/css/forms/selects/select2.min.css") !!}">
        <link rel="stylesheet" type="text/css" href="{!! url("app-assets/vendors/css/extensions/sweetalert.css") !!}">
    <!-- END VENDOR CSS-->
@endsection
@section('page_css')
    <link rel="stylesheet" type="text/css" href="{!! url("app-assets/css/plugins/animate/animate.css") !!}">
    <style>
        .imagen-logo{
            object-fit: cover;height:150px;width:150px;margin-top:2%; margin-left:2%;
        }
    </style>
@endsection

@section('content')
    <section class="principal">
        <div class="title-content">
            <h1 class="title">Empresas registradas</h1>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"></h3>
            </div>
            <div class="panel-body">
                <div class="row content_card" id="admin_list">
                    <div id="search" method="post" class="col-md-12 mb-3">
                        <div class="row">
                            <div class="col-12 input-group input-focus">
                                <div class="input-group-prepend">
                                    <span class="input-group-text bg-white" ><i class="fa fa-search"></i></span>
                                </div>
                                <input type="text" class="form-control search border-left-0 col-12 input_buscador" name="search"  placeholder="Busqueda de empresas">
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="turnt content_card row list">
                            @forelse ($empresas as $empresa)
                            <div class="col-lg-6 col-md-6 col-sm-12 list_custom">
                                <div class="card border-primary text-center bg-transparent">
                                    <div class="card-content">
                                        @if($empresa->foto)
                                        <img id="{{$empresa->logo}}" src="{!! url('storage/app/empresas/'.$empresa->logo) !!}" class="img-fluid rounded-circle img-thumbnail shadow-sm imagen-logo float-left">
                                        @endif
                                        <div class="card-body">
                                            <h4 class="card-title mt-3 pl-1 pr-1 search_nombre">{{ $empresa->nombre }}</h4>
                                            <a href="{{route('admin-showEmpresa', $empresa->nombre)}}" class="btn btn-primary mt-1">Ver</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @empty
                            <p>Aún no hay empresas registradas en el sistema</p>
                            @endforelse
                        </div>
                    </div>
                    <div class="col-12 mb-2 mt-1">
                        <div class="row">
                            <div class="col-12">
                                <h1 id="msg-result" class="d-none">0 resultados</h1>
                                <div class="d-flex align-items-center justify-content-center">
                                    <div id="anterior"></div>
                                    <ul class="pagination justify-content-center m-0"></ul>
                                    <div id="siguiente"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('page_js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/list.js/2.3.0/list.min.js"></script>
<script>
var userList = new List('admin_list', {
    valueNames: [ 'search_nombre'],
    page: 4,
    pagination: {
        innerWindow: 1,
        left: 1,
        right: 1,
        paginationClass: "pagination",
        item:"<li class='page-item'><a class='page-link page' href='#'></a></li>",
    }
});
var length_lis = $('.list .list_custom').length;

if(length_lis == 0){
    $('.jPaginateNext').css('display','none');
    $('.jPaginateBack').css('display','none');
}
else{
    $("#anterior").addClass('pagination').html('<li class="page-item prev-item jPaginateBack"><button type="button" style="border-radius: 50%;" class="page-link navegadores"></button></li>');
    $("#siguiente").addClass('pagination').html('<li class="page-item next-item jPaginateNext"><button type="button" style="border-radius: 50%;" class="page-link navegadores"></button></li>');
    $('.jPaginateNext').css('display','block');
    $('.jPaginateBack').css('display','block');

    $('.jPaginateNext').on('click', function(e){
        var list = $('.pagination').find('li');
        $.each(list, function(position, element){
            if($(element).is('.active')){
                $(list[position+1]).find('a')[0].click();
            }
        })
    });

    $('.jPaginateBack').on('click', function(e){
        var list = $('.pagination').find('li');
        $.each(list, function(position, element){
            if($(element).is('.active')){
                $(list[position-1]).find('a')[0].click();
            }
        })
    });
}

$(".input_buscador").keyup(function(){
    if($('.list .list_custom').length == 0)
    {
        $("#msg-result").removeClass('d-none');
    }else{
        $("#msg-result").addClass('d-none');
    }
});

setInterval(() => {
    $(".page-link.page").click(function(e){
        e.preventDefault();
    });
    $(".a_modal_no_estudios").click(function(){
        $("#name_paciente").text($(this).data('name'));
        $("#modal_no_estudios").modal();
    });
}, 500);

$(".a_modal_no_estudios").click(function(){
    $("#name_paciente").text($(this).data('name'));
    $("#modal_no_estudios").modal();
});

$(".btn_sort").click(function(){
    $(".btn_sort .text-dark .feather").removeClass('text-white');
    if($(this).hasClass("asc"))
    {
        $(this).find('.text-dark .icon-chevron-up').addClass('text-white');
    }else{
        $(this).find('.text-dark .icon-chevron-down').addClass('text-white');
    }
});

</script>
@endsection
