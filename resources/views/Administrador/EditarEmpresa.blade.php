{{-- @extends('layouts.administradorApp') --}}
@extends('Administrador.AppLayout')


@section('title', 'Editar empresa')

@section('styles')
    {{-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous"-->
    <link rel="stylesheet" href="../../../resources/sass/css/normalize.css">
    <link rel="stylesheet" href="../../../resources/sass/fontawesome/css/all.css">
    <link rel="stylesheet" type="text/css" href="../../../resources/sass/css/styleScroll.css">
    <link rel="stylesheet" type="text/css" href="../../../resources/sass/css/style_admin_menu.css">
    <link rel="stylesheet" type="text/css" href="../../../resources/sass/css/Administrador/EditarEmpresa_styles.css"> --}}
@endsection

@section('page_css')
<link rel="stylesheet" href="https://expedienteclinico.humanly-sw.com/dev/public/css/empresa/info_paciente.css">
    <style>
        .imagen-logo{
            object-fit: cover;height:150px;width:150px;border: none;
        }
    </style>
@endsection

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="alerts">
                    @if(session('message'))
                        <div class="alert alert-success" >
                        {{session('message')}}
                        </div>
                    @elseif ($errors->any())
                        <div class="alert alert-danger" >
                            <p>Ha ocurrido un error al actualizar los datos del contacto.</p>y
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{$error}}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                </div>

                <form role="form" id="actualizarEmpresa" method="post" action="{{ route('actualizarEmpresa')}}" enctype="multipart/form-data">
                    @csrf
                    <h1>Editar datos de {{ $empresa->nombre }}</h1>
                    <div class="formulario">
                        <section class="izquierda">
                            <div class="cambio-logo">
                                <input type="hidden" name="encryptedID" value="{{ $encryptedID }}" id="inputEncryptedID">
                                @if($empresa->foto)
                                    <img id="load_image" src="{!! url('storage/app/empresas/'.$empresa->logo) !!}" alt="Logo de {{ $empresa->nombre }}" class="img-fluid rounded-circle img-thumbnail shadow-sm imagen-logo">
                                @endif
                                <label class="fileContainer btn btn-primary mr-1 waves-effect waves-light" style="float: initial !important">
                                    Cargar imagen <input type="file" accept="image/*" name="logo" id="logo" >
                                </label>
                                @error('nombre')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </section>

                        <section class="derecha">
                            <div class="datos-empresa">
                                <label for="inputNombre">Nombre de la empresa</label>
                                <input type="text" name="nombre"  class="form-control " value="{{$empresa->nombre}}" id="inputNombre">

                                <label for="inputDireccion">Dirección</label>
                                <input type="text" name="direccion"  class="form-control " value="{{$empresa->direccion}}" id="inputDireccion">

                                <label for="inputGiro">Giro</label>
                                <select class="form-control @error('giro') is-invalid @enderror" name='giro' id='inputGiro'>
                                    @foreach ($giros as $giro)
                                        @if ($giro->nombre == $empresa->giro->nombre)
                                            <option value="{{ $giro->nombre }}" selected>{{ $giro->nombre }}</option>
                                        @else
                                            <option value="{{ $giro->nombre }}">{{ $giro->nombre }}</option>
                                        @endif
                                    @endforeach
                                </select>

                                <label for="inputTelefono">Teléfono</label>
                                <input type="text" name="telefono"  class="form-control " value="{{$empresa->telefono}}" id="inputTelefono">

                                <label for="inputPagina ">Página web</label>
                                <input type="text" name="pagina"  class="form-control " value="{{$empresa->pagina}}" id="inputPagina">
                            </div>

                            {{-- <div class="area-estudios">
                                <button type="button" class="btn btn-custom" onclick="showEstudiosModal('{{ $encryptedID }}')">
                                    Estudios
                                </button>
                            </div> --}}
                        </section>
                    </div>
                    <div class="area-guardar mt-2">
                        <button class="btn btn-primary" type="submit">Guardar cambios</button>
                        <a href="{!! url('Empresas/'.$empresa->nombre) !!}" class="btn btn-secondary" data-dismiss="modal">Cancelar</a>
                    </div>

                    {{-- Modal --}}
                    <div id="estudiosModal" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title float-left" id="myModalLabel">Seleccione los estudios autorizados por la empresa</h4>
                                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                </div>
                                <div class="modal-body">
                                    <div class="categoria">
                                        <h3 class="selected">Categorias</h3>
                                        @foreach ($categorias as $item)
                                            <div class="attrib">
                                                <i class="fa fa-file-alt fa-fw icon"></i>
                                                <a href="#" data-value="{{ $item->id }}" data-nombre="{{ $item->nombre }}" class="links">
                                                    {{$item->nombre}}
                                                </a>
                                            </div>
                                        @endforeach
                                        @if(count($estudios) > 0)
                                            <input type="button" id="BtnSeleccionar" class="btn btn-primary im22 col-12" value="Seleccionar todo " >
                                        @endif
                                    </div>
                                    <div class="estudios">
                                        <h3 class="selected" id="selected">Selecciona la categoría</h3>
                                        <div class="custom-control custom-checkbox">
                                            @foreach ($categorias as $categoriarow)
                                                @foreach ($estudios as $estudio)
                                                    @if ($estudio->categoria_id == $categoriarow->id)
                                                        <div class="col-6 estudios_input {{ $categoriarow->nombre }}">
                                                            <input type="checkbox" name="estudios[{{ $estudio->id }}]" class="custom-control-input" value="{{ $estudio->id }}" id="estudio{{ $estudio->id }}">
                                                            <label class="custom-control-label" for="estudio{{ $estudio->id }}">{{ $estudio->nombre }}</label>
                                                        </div>
                                                    @endif
                                                @endforeach
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('specificScripts')
    <script src="../../../resources/js/Administrador/EditarEmpresa.js"></script>
    <script>
        cargarEstudios('{{ $encryptedID }}')
    </script>
@endsection
